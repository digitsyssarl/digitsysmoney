﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OMBanking.Web.Security
{
    public class FootPrint
    {
      public Guid Id { get; set; }

      public string Role { get; set; }

      public string UserAction { get; set; }

      public string AccessedBy { get; set; }

      public DateTime AccessedOn { get; set; }

      public string BranchID { get; set; }

      public string GSMOperationID { get; set; }
    }
}