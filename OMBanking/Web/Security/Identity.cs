﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OMBanking.Web.Security
{
    public class Identity
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string NationalityID { get; set; }

        public string PassportNo { get; set; }

        public string ResidentID { get; set; }

        public string WorkPermitNo { get; set; }
    }
}