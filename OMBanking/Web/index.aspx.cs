﻿using OMBanking.Web.Proxy.Bank;
using System;
using System.Web.UI;

namespace OMBanking.Web
{
    public partial class index : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        { 
           Response.Redirect("~/Pages/Views/Login.aspx?lid=" + Utils.Base64Encode("employee"), false);
        }
    }
}