﻿using OMBanking.Web.Log;
using System;
using System.Collections;
using System.Collections.Generic;

namespace OMBanking.Web.Bank
{
    public class TransactionLogComparer : IComparer<TransactionLog>
    {
        int IComparer<TransactionLog>.Compare(TransactionLog trx1, TransactionLog trx2)
        {
            return trx1.RequestId.CompareTo(trx1.RequestId);
        }
    }
}