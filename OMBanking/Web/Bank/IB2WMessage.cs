namespace OMBanking.Web.Bank
{
    public interface IB2WMessage
    {
        string encode();

        IB2WMessage decode(string soapResponse);
    }
}
