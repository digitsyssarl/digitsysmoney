using System;
using System.Linq;
using System.Xml.Linq;
//using System.ComponentModel.DataAnnotations;

namespace OMBanking.Web.Bank
{

    public class GetAccountBalance : B2WMessage
    {
        public HeaderRequest mmHeaderInfo { get; set; }
        
        //[StringLength(20, ErrorMessage = "The {0} must be maximum {1} characters long.")]
        public string accountNo { get; set; }
        
        //[StringLength(22, ErrorMessage = "The {0} must be maximum {1} characters long.")]
        public string accountAlias { get; set; }

        //[StringLength(50, ErrorMessage = "The {0} must be maximum {1} characters long.")]
        public string reason { get; set; }

        public override string encode()
        {
            return null;
        }

        public override IB2WMessage decode(string soapResponse) {
            XDocument xml = XDocument.Parse(soapResponse);
            var response1 = xml.Descendants()
                               .Where(x => (x.Name.LocalName == "AccountBalanceInquiryRequest"));
            if (response1 != null)
            {
                var response2 = response1.Select(x => new GetAccountBalance()
                {
                    mmHeaderInfo = x.Descendants()
                                    .Where(y => y.Name.LocalName == "mmHeaderInfo")
                                    .Select(y => new HeaderRequest()
                    {
                        operatorCode = (string)y.Element("operatorCode"),
                        requestId = (string)y.Element("requestId"),
                        requestToken = (string)y.Element("requestToken"),
                        requestType = (string)y.Element("requestType"),
                        affiliateCode = (string)y.Element("affiliateCode")
                    }).FirstOrDefault(),
                    accountNo = (string)x.Element(x.Name.Namespace + "accountNo"),
                    accountAlias = (string)x.Element(x.Name.Namespace + "accountAlias"),
                    reason = (string)x.Element(x.Name.Namespace + "reason")
                }).FirstOrDefault();

                return response2;
            }

            return null;
        }
    }
}
