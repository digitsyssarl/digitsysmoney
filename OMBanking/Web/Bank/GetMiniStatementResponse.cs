using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;

namespace OMBanking.Web.Bank
{
    public class GetMiniStatementResponse : B2WMessage
    {
        public HeaderResponse mmHeaderInfo { get; set; }
        
        public List<TransactionDetail> TransactionList { get; set; }

        public override string encode() {
            NumberFormatInfo nfi = new NumberFormatInfo();
            nfi.NumberDecimalSeparator = ".";

            string beginStatement = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                            + "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\""
                            + " xmlns:ns1=\"http://b2w.banktowallet.com/b2w\""
                            + " xmlns:ns2=\"http://b2w.banktowallet.com/b2w/1.0\">"
                            + "<SOAP-ENV:Body>"
                            + "<ns1:GetMiniStatementResponse>"
                            + "<return>"
                            + "<ns2:mmHeaderInfo>"
                            + "<operatorCode>"
                            + mmHeaderInfo.operatorCode
                            + "</operatorCode>"
                            + "<requestId>"
                            + mmHeaderInfo.requestId
                            + "</requestId>"
                            + "<affiliateCode>"
                            + mmHeaderInfo.affiliateCode
                            + "</affiliateCode>"
                            + "<responseCode>"
                            + mmHeaderInfo.responseCode
                            + "</responseCode>"
                            + "<responseMessage>"
                            + mmHeaderInfo.responseMessage
                            + "</responseMessage>"
                            + "</ns2:mmHeaderInfo>",
                   transactionList = "",
                   endStatement = "</return>"
                           + "</ns1:GetMiniStatementResponse>"
                           + "</SOAP-ENV:Body>"
                           + "</SOAP-ENV:Envelope>";
            if (TransactionList != null)
            {
                foreach (TransactionDetail transactionDetail in TransactionList)
                {
                    transactionList += "<ns2:TransactionList>";
                    transactionList += "<tranRefNo>"
                                       + transactionDetail.tranRefNo
                                       + "</tranRefNo>";
                    transactionList += "<tranDate>"
                                       + transactionDetail.tranDate.ToString("yyyy-MM-ddThh:mm:ss.fffZ")
                                       + "</tranDate>";
                    transactionList += "<tranType>"
                                       + ((transactionDetail.tranType != null) ? transactionDetail.tranType.ToString() : "")
                                       + "</tranType>";
                    transactionList += "<ccy>"
                                       + transactionDetail.ccy
                                       + "</ccy>";
                    transactionList += "<crDr>"
                                       + transactionDetail.crDr
                                       + "</crDr>";
                    transactionList += "<amount>"
                                       + transactionDetail.amount.ToString(nfi)
                                       + "</amount>";
                    transactionList += "<narration>"
                                       + transactionDetail.narration
                                       + "</narration>";
                    transactionList += "</ns2:TransactionList>";
                };
            }
            return beginStatement 
                   + transactionList 
                   + endStatement;
        }

        public override IB2WMessage decode(string soapResponse) { return null; }   
    }
}
