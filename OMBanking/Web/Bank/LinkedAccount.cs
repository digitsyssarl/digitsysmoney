﻿using System;

namespace OMBanking.Web.Bank
{
    public class LinkedAccount
    {
        public Guid Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string AccountNo { get; set; }

        public string PhoneNo { get; set; }

        public DateTime DateLinked { get; set; }

        public bool Active { get; set; }

        public string Alias { get; set; }

        public string SupervisedBy { get; set; }

        public DateTime SupervisedOn { get; set; }

        public string AuditedBy { get; set; }
        
        public DateTime AuditedOn { get; set; }

        public string Role { get; set; }
        
        public string BranchID { get; set; }

        public string GSMOperationID { get; set; }
    }

}