﻿using System;

namespace OMBanking.Web.Bank
{
    public class SmsAlert
    {
        public int alertid { get; set; }

        public string MOBILEnumber { get; set; }

        public string messagedescription { get; set; }

        public DateTime PostedOn { get; set; }

        public string PostedBy { get; set; }

        public string Provider { get; set; }
    }
}