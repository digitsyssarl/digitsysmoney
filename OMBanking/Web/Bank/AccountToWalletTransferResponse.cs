using System;
//using System.ComponentModel.DataAnnotations;


namespace OMBanking.Web.Bank
{
    public class AccountToWalletTransferResponse : B2WMessage
    {
        public HeaderResponse mmHeaderInfo { get; set; }
        
        //[StringLength(16, ErrorMessage = "The {0} must be maximum {1} characters long.")]
        public string externalRefNo { get; set; }
        
        //[StringLength(16, ErrorMessage = "The {0} must be maximum {1} characters long.")]
        public string CBAReferenceNo { get; set; }

        public override string encode() {
            return "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                   + "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\""
                   + " xmlns:ns1=\"http://b2w.banktowallet.com/b2w\""
                   + " xmlns:ns2=\"http://b2w.banktowallet.com/b2w/1.0\">"
                   + "<SOAP-ENV:Body>"
                   + "<ns1:AccountToWalletTransferResponse>"
                   + "<return>"
                   + "<ns2:mmHeaderInfo>"
                   + "<operatorCode>"
                   + mmHeaderInfo.operatorCode
                   + "</operatorCode>"
                   + "<requestId>"
                   + mmHeaderInfo.requestId
                   + "</requestId>"
                   + "<affiliateCode>"
                   + mmHeaderInfo.affiliateCode
                   + "</affiliateCode>"
                   + "<responseCode>"
                   + mmHeaderInfo.responseCode
                   + "</responseCode>"
                   + "<responseMessage>"
                   + mmHeaderInfo.responseMessage
                   + "</responseMessage>"
                   + "</ns2:mmHeaderInfo>"
                   + "<externalRefNo>"
                   + externalRefNo
                   + "</externalRefNo>"
                   + "<CBAReferenceNo>"
                   + CBAReferenceNo
                   + "</CBAReferenceNo>"
                   + "</return>"
                   + "</ns1:AccountToWalletTransferResponse>"
                   + "</SOAP-ENV:Body>"
                   + "</SOAP-ENV:Envelope>";
        }

        public override IB2WMessage decode(string soapResponse) { return null; }   
    }
}
