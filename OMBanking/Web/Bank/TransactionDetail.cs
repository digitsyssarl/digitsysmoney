using System;
//using System.ComponentModel.DataAnnotations;

namespace OMBanking.Web.Bank
{
    public class TransactionDetail
    {
        //[StringLength(16, ErrorMessage = "The {0} must be maximum {1} characters long.")]
        public string tranRefNo { get; set; }
        
        public DateTime tranDate { get; set; }
        
        //[StringLength(3, ErrorMessage = "The {0} must be maximum {1} characters long.")]
        public string tranType { get; set; }
        
        //[StringLength(3, ErrorMessage = "The {0} must be maximum {1} characters long.")]
        public string ccy { get; set; }

        //[StringLength(1, ErrorMessage = "The {0} must be maximum {1} characters long.")]
        public string crDr { get; set; }
        
        public double amount { get; set; }
        
        //[StringLength(105, ErrorMessage = "The {0} must be maximum {1} characters long.")]
        public string narration { get; set; }
    }
}
