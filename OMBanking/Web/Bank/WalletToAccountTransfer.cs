using System;
using System.Linq;
using System.Xml.Linq;
//using System.ComponentModel.DataAnnotations;

namespace OMBanking.Web.Bank
{
    public class WalletToAccountTransfer : B2WMessage
    {
        public HeaderRequest mmHeaderInfo { get; set; }

        //[StringLength(16, ErrorMessage = "The {0} must be maximum {1} characters long.")]
        public string externalRefNo { get; set; }
        
        //[StringLength(20, ErrorMessage = "The {0} must be maximum {1} characters long.")]
        public string mobileNo { get; set; }
        
        //[StringLength(50, ErrorMessage = "The {0} must be maximum {1} characters long.")]
        public string mobileName { get; set; }

        //[StringLength(22, ErrorMessage = "The {0} must be maximum {1} characters long.")]
        public string mobileAlias { get; set; }
        
        //[StringLength(20, ErrorMessage = "The {0} must be maximum {1} characters long.")]
        public string accountNo { get; set; }
        
        //[StringLength(22, ErrorMessage = "The {0} must be maximum {1} characters long.")]
        public string accountAlias { get; set; }
        
        //[StringLength(50, ErrorMessage = "The {0} must be maximum {1} characters long.")]
        public string accountName { get; set; }
        
        //[StringLength(50, ErrorMessage = "The {0} must be maximum {1} characters long.")]
        public string transferDescription { get; set; }
        
        //[StringLength(3, ErrorMessage = "The {0} must be maximum {1} characters long.")]
        public string ccy { get; set; }
        
        public double amount { get; set; }
        
        public double charge { get; set; }
        
        public DateTime tranDate { get; set; }
        
        //[StringLength(50, ErrorMessage = "The {0} must be maximum {1} characters long.")]
        public string udf1 { get; set; }
        
        //[StringLength(50, ErrorMessage = "The {0} must be maximum {1} characters long.")]
        public string udf2 { get; set; }
        
        //[StringLength(50, ErrorMessage = "The {0} must be maximum {1} characters long.")]
        public string udf3 { get; set; }

        public override string encode()
        {
            return null;
        }

        public override IB2WMessage decode(string soapResponse) {

            XDocument xml = XDocument.Parse(soapResponse);
            var response1 = xml.Descendants()
                               .Where(x => (x.Name.LocalName == "MobileTransferRequest"));
            if (response1 != null)
            {
                var response2 = response1.Select(x => new WalletToAccountTransfer()
                {
                    mmHeaderInfo = x.Descendants()
                                    .Where(y => y.Name.LocalName == "mmHeaderInfo")
                                    .Select(y => new HeaderRequest()
                    {
                        operatorCode = (string)y.Element("operatorCode"),
                        requestId = (string)y.Element("requestId"),
                        requestToken = (string)y.Element("requestToken"),
                        requestType = (string)y.Element("requestType"),
                        affiliateCode = (string)y.Element("affiliateCode")
                    }).FirstOrDefault(),
                    externalRefNo = (string)x.Element(x.Name.Namespace + "externalRefNo"),
                    mobileNo = (string)x.Element(x.Name.Namespace + "mobileNo"),
                    mobileName = (string)x.Element(x.Name.Namespace + "mobileName"),
                    mobileAlias = (string)x.Element(x.Name.Namespace + "mobileAlias"),
                    accountNo = (string)x.Element(x.Name.Namespace + "accountNo"),
                    accountAlias = (string)x.Element(x.Name.Namespace + "accountAlias"),
                    accountName = (string)x.Element(x.Name.Namespace + "accountName"),
                    transferDescription = (string)x.Element(x.Name.Namespace + "transferDescription"),
                    ccy = (string)x.Element(x.Name.Namespace + "ccy"),
                    amount = (((string)x.Element(x.Name.Namespace + "amount")) != null
                               && ((string)x.Element(x.Name.Namespace + "amount")) != "")
                             ? Double.Parse((string)x.Element(x.Name.Namespace + "amount"), System.Globalization.NumberStyles.AllowDecimalPoint, System.Globalization.NumberFormatInfo.InvariantInfo)
                             : -1,
                    charge = (((string)x.Element(x.Name.Namespace + "charge")) != null
                               && ((string)x.Element(x.Name.Namespace + "charge")) != "")
                             ? Double.Parse((string)x.Element(x.Name.Namespace + "charge"), System.Globalization.NumberStyles.AllowDecimalPoint, System.Globalization.NumberFormatInfo.InvariantInfo)
                             : -1,
                    tranDate = (((string)x.Element(x.Name.Namespace + "tranDate")) != null
                                 && ((string)x.Element(x.Name.Namespace + "tranDate")) != "")
                               ? DateTime.Parse((string)x.Element(x.Name.Namespace + "tranDate"))
                               : DateTime.Now,
                    udf1 = (string)x.Element(x.Name.Namespace + "udf1"),
                    udf2 = (string)x.Element(x.Name.Namespace + "udf2"),
                    udf3 = (string)x.Element(x.Name.Namespace + "udf3")
                }).FirstOrDefault();

                return response2;
            }

            return null;
        }
    }
}
