﻿using System;

namespace OMBanking.Web.Bank
{
    public class UserProfile
    {
        public Guid Id { get; set; }

        public string Email { get; set; }

        public string UserID { get; set; }

        public string Password { get; set; }

        public Boolean HasChanged { get; set; }

        public string Role { get; set; }

        public string BranchID { get; set; }

        public string GSMOperationID { get; set; }

        public DateTime LogInDate { get; set; }

        public DateTime LogOutDate { get; set; }

        public string SessionID { get; set; }

        public Boolean Active { get; set; }
    }
}