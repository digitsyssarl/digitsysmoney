using System;
namespace OMBanking.Web.Bank
{
    
    public class ErrorCodeHandler
    {
        private const string UNKNOWN_ERROR = "Unknown Error";
        private const string SUCCESS = "Success";
        public  const string BANK_SUCCESS_CODE = "00";
        private const string BANK_ERROR = "Bank Technical Error";
        private const string OP_TIMEOUT = "Operation in timeout. Use TransferStatusInquiry to check status";
        private const string UNAUTHORIZED_REQUEST = "Unauthorized Request on Account";
        public  const string INSUFFICIENT_BALANCE = "Insufficient Balance";
        public  const string OVERLIMIT_AMOUNT = "Amount is over the daily limit";
        public  const string INSUFFICIENT_BALANCE_CODE = "116";
        public  const string OVERLIMIT_AMOUNT_CODE = "112";
        private const string TRANSACTION_VIOLATION = "Transaction Violates Account's Minimum Balance";

        public  const string ACCOUNT_NOT_EXISTS = "Account Not Exists";
        public  const string BLOCKED_ACCOUNT    = "Blocked Account";

        public static string Description(string code) 
        { 
            if(String.Equals(code, "000"))
            {
                return SUCCESS;
            }
            else if(String.Equals(code, "E11"))
            {
                return OP_TIMEOUT;
            }
            else if(String.Equals(code, "E16"))
            {
                return INSUFFICIENT_BALANCE;
            }
            else if(String.Equals(code, "E22"))
            {
                return TRANSACTION_VIOLATION;
            }
            else if(String.Equals(code, "E13") 
                 ||  String.Equals(code, "E14")
                 ||  String.Equals(code, "E15"))
            {
                return UNAUTHORIZED_REQUEST;            
            }
            else if (String.Equals(code, "010") ||  String.Equals(code, "E01")
                 ||  String.Equals(code, "E02") ||  String.Equals(code, "E03")
                 ||  String.Equals(code, "E04") ||  String.Equals(code, "E05")
                 ||  String.Equals(code, "E06") ||  String.Equals(code, "E07")
                 ||  String.Equals(code, "E08") ||  String.Equals(code, "E09")
                 ||  String.Equals(code, "E10") ||  String.Equals(code, "E12")
                 ||  String.Equals(code, "E16") ||  String.Equals(code, "E17")
                 ||  String.Equals(code, "E18") ||  String.Equals(code, "E19")
                 ||  String.Equals(code, "E20") ||  String.Equals(code, "E21")
                 ||  String.Equals(code, "E35") ||  String.Equals(code, "E99"))
            {
                return BANK_ERROR;      
            }
            else 
            {
                return UNKNOWN_ERROR;
            }
        }
    }
}
