﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace OMBanking.Web.Bank
{
    public class Fees
    {
        public static double GetB2WFee(double amount)
        {
            double ora_b2w_fee;
            if (amount >= 100 && amount <= 100000)
            {
                ora_b2w_fee = double.Parse(ConfigurationManager.AppSettings.Get("ora_b2w_100to100k_fee")) * amount;
            }
            else if (amount >= 100001 && amount <= 500000)
            {
                ora_b2w_fee = double.Parse(ConfigurationManager.AppSettings.Get("ora_b2w_great101k_fee"));
            }
            else if (amount >= 500001)
            {
                ora_b2w_fee = 0;
            }
            else 
            {
                ora_b2w_fee = -1;
            }


            return ora_b2w_fee;
        }

        public static double GetW2BFee(double amount)
        {
            double ora_w2b_fee;
            if (amount >= 100 && amount <= 100000)
            {
                ora_w2b_fee = double.Parse(ConfigurationManager.AppSettings.Get("ora_w2b_100to100k_fee")) * amount;
            }
            else if (amount >= 100001 && amount <= 200000)
            {
                ora_w2b_fee = double.Parse(ConfigurationManager.AppSettings.Get("ora_w2b_100kto200k_fee"));
            }
            else if (amount >= 200001 && amount <= 300000)
            {
                ora_w2b_fee = double.Parse(ConfigurationManager.AppSettings.Get("ora_w2b_200kto300k_fee"));
            }
            else if (amount >= 300001 && amount <= 400000)
            {
                ora_w2b_fee = double.Parse(ConfigurationManager.AppSettings.Get("ora_w2b_300kto400k_fee"));
            }
            else if (amount >= 400001 && amount <= 500000)
            {
                ora_w2b_fee = double.Parse(ConfigurationManager.AppSettings.Get("ora_w2b_400kto500k_fee"));
            }
            else if (amount >= 500001)
            {
                ora_w2b_fee = 0;
            }
            else
            {
                ora_w2b_fee = -1;
            }

            return ora_w2b_fee;
        }

        public static double GetBalanceFee()
        {
            return double.Parse(ConfigurationManager.AppSettings.Get("ora_balance_fee"));
        }

        public static double GetStatementFee()
        {
            return double.Parse(ConfigurationManager.AppSettings.Get("ora_statement_fee"));
        }
    }
}