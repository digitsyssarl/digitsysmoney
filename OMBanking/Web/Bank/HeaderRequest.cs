//using System.ComponentModel.DataAnnotations;

namespace OMBanking.Web.Bank
{
    public class HeaderRequest
    {
        //[StringLength(20, ErrorMessage = "The {0} must be maximum {1} characters long.")]
        public string operatorCode { get; set; }
        
        //[StringLength(16, ErrorMessage = "The {0} must be maximum {1} characters long.")]
        public string requestId { get; set; }
        
        //[StringLength(128, ErrorMessage = "The {0} must be maximum {1} characters long.")]
        public string requestToken { get; set; }
        
        //[StringLength(20, ErrorMessage = "The {0} must be maximum {1} characters long.")]
        public string requestType { get; set; }
        
        //[StringLength(3, ErrorMessage = "The {0} must be maximum {1} characters long.")]
        public string affiliateCode { get; set; }
    }
}
