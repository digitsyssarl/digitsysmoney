﻿using System;

namespace OMBanking.Web.Bank
{
    public class Supervisor
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string BranchID { get; set; }
    }
}