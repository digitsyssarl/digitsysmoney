﻿namespace OMBanking.Web.Bank
{
    public abstract class B2WMessage : IB2WMessage
    {
        public abstract IB2WMessage decode(string soapResponse);
        public abstract string encode();
    }
}