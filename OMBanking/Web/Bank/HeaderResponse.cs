//using System.ComponentModel.DataAnnotations;

namespace OMBanking.Web.Bank
{
    public class HeaderResponse
    {
        //[StringLength(20, ErrorMessage = "The {0} must be maximum {1} characters long.")]
        public string operatorCode { get; set; }
        
        //[StringLength(16, ErrorMessage = "The {0} must be maximum {1} characters long.")]
        public string requestId { get; set; }
        
        //[StringLength(3, ErrorMessage = "The {0} must be maximum {1} characters long.")]
        public string affiliateCode { get; set; }
        
        //[StringLength(3, ErrorMessage = "The {0} must be maximum {1} characters long.")]
        public string responseCode { get; set; }
        
        //[StringLength(50, ErrorMessage = "The {0} must be maximum {1} characters long.")]
        public string responseMessage { get; set; }
    }
}
