using System;
using System.ComponentModel.DataAnnotations;
using System.Globalization;

namespace OMBanking.Web.Bank
{
    public class GetAccountBalanceResponse : B2WMessage
    {
        public HeaderResponse mmHeaderInfo { get; set; }
        
        //[StringLength(20, ErrorMessage = "The {0} must be maximum {1} characters long.")]
        public string accountNo { get; set; }
        
        //[StringLength(22, ErrorMessage = "The {0} must be maximum {1} characters long.")]
        public string accountAlias { get; set; }
        
        //[StringLength(50, ErrorMessage = "The {0} must be maximum {1} characters long.")]
        public string accountName { get; set; }
        
        //[StringLength(3, ErrorMessage = "The {0} must be maximum {1} characters long.")]
        public string ccy { get; set; }
        
        //[StringLength(3, ErrorMessage = "The {0} must be maximum {1} characters long.")]
        public string branchCode { get; set; }
        
        public double availableBalance { get; set; }
        
        public double currentBalance { get; set; }
        
        //[StringLength(1, ErrorMessage = "The {0} must be maximum {1} characters long.")]
        public string accountType { get; set; }
        
        //[StringLength(1, ErrorMessage = "The {0} must be maximum {1} characters long.")]
        public string accountClass { get; set; }

        //[StringLength(10, ErrorMessage = "The {0} must be maximum {1} characters long.")]
        public string accountStatus { get; set; }
        
        //[StringLength(50, ErrorMessage = "The {0} must be maximum {1} characters long.")]
        public string udf1 { get; set; }
        
        //[StringLength(50, ErrorMessage = "The {0} must be maximum {1} characters long.")]
        public string udf2 { get; set; }
        
        //[StringLength(50, ErrorMessage = "The {0} must be maximum {1} characters long.")]
        public string udf3 { get; set; }

        public override string encode() {
            NumberFormatInfo nfi = new NumberFormatInfo();
            nfi.NumberDecimalSeparator = ".";

            return "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                   + "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\""
                   + " xmlns:ns1=\"http://b2w.banktowallet.com/b2w\""
                   + " xmlns:ns2=\"http://b2w.banktowallet.com/b2w/1.0\">"
                   + "<SOAP-ENV:Body>"
                   + "<ns1:GetAccountBalanceResponse>"
                   + "<return>"
                   + "<ns2:mmHeaderInfo>"
                   + "<operatorCode>"
                   + mmHeaderInfo.operatorCode
                   + "</operatorCode>"
                   + "<requestId>"
                   + mmHeaderInfo.requestId
                   + "</requestId>"
                   + "<affiliateCode>"
                   + mmHeaderInfo.affiliateCode
                   + "</affiliateCode>"
                   + "<responseCode>"
                   + mmHeaderInfo.responseCode
                   + "</responseCode>"
                   + "<responseMessage>"
                   + mmHeaderInfo.responseMessage
                   + "</responseMessage>"
                   + "</ns2:mmHeaderInfo>"
                   + "<accountNo>"
                   + accountNo
                   + "</accountNo>"
                   + "<accountAlias>"
                   + accountAlias
                   + "</accountAlias>"
                   + "<accountName>"
                   + accountName
                   + "</accountName>"
                   + "<ccy>"
                   + ccy
                   + "</ccy>"
                   + "<branchCode>"
                   + branchCode
                   + "</branchCode>"
                   + "<availableBalance>"
                   + ((availableBalance == 0 && mmHeaderInfo.responseCode != "000") 
                      ? "" 
                      : availableBalance.ToString(nfi)
                     )
                   + "</availableBalance>"
                   + "<currentBalance>"
                   + ((currentBalance == 0 && mmHeaderInfo.responseCode != "000")
                      ? ""
                      : currentBalance.ToString(nfi)
                     )
                   + "</currentBalance>"
                   + "<accountType>"
                   + accountType
                   + "</accountType>"
                   + "<accountClass>"
                   + accountClass
                   + "</accountClass>"
                   + "<accountStatus>"
                   + accountStatus
                   + "</accountStatus>"
                   + "<udf1>"
                   + udf1
                   + "</udf1>"
                   + "<udf2>"
                   + udf2
                   + "</udf2>"
                   + "<udf3>"
                   + udf3
                   + "</udf3>"
                   + "</return>"
                   + "</ns1:GetAccountBalanceResponse>"
                   + "</SOAP-ENV:Body>"
                   + "</SOAP-ENV:Envelope>";
        }

        public override IB2WMessage decode(string soapResponse) { return null; }   
    }
}
