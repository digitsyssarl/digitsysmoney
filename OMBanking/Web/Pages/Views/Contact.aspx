﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="OMBanking.Web.Pages.Views.Contact" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>

    <title>Contact </title>
    <style>
        #foot {
            color: white;
            margin-top:2.5px;
            padding: 10px;
            text-align: center;
        }
    </style>
    <script type="text/javascript">
        window.onload = function()
        {
            document.getElementById("imgId").setAttribute("width", ((3.95*window.innerWidth)/4) + "");
            document.getElementById("imgId").setAttribute("height", ((3.3 * window.innerHeight) / 4) + "");
            let color = '<%=ConfigurationManager.AppSettings.Get("color")%>';
            document.getElementById('foot').style.backgroundColor = color;
        }
    </script>
</head>

<body>
    <div id="content">
        <img id="imgId" class="marginauto" src="Images/images.jpeg" alt="Visit our Website to get contact information."/>
        <br />
        <footer id="foot">
            &copy; Copyright <%=DateTime.Now.Year.ToString()%>, <%=ConfigurationManager.AppSettings.Get("bank_name")%>
        </footer>
    </div>
</body>

</html>
