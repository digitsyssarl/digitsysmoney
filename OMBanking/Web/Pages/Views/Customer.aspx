﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Customer.aspx.cs" Inherits="OMBanking.Web.Pages.Views.Customer" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>

    <title><%=ConfigurationManager.AppSettings.Get("bank_name")%></title>

    <!-- Bootstrap CSS CDN -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous"/>
    <!-- Our Custom CSS -->
    <link rel="stylesheet" href="css/style4.css"/>
    <style>
        #marginauto {
            margin: 10px auto 20px;
            display: block;
        }
    </style>
    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js" integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous"></script>
    <script type="text/javascript">
        window.onload = function() {
            applyTheme();
            let gsmOpID = '<%=((Request.QueryString["gsmOpId"] != null) ? OMBanking.Web.Proxy.Bank.Utils.Base64Decode(Request.QueryString["gsmOpId"].ToString()) : "")%>';
            if (gsmOpID.localeCompare('ORANGE') == 0) {
                document.getElementById('transfer').style.display = 'none';
                document.getElementById('transfer').style.visibility = 'false';
            }
        };

       window.prev_id = 'hf';
       function show(id, toggle) {
			location.href = location.href.split("#")[0] + "#" + toggle;
			if (toggle.localeCompare('home') != 0) 
			{
			    document.getElementsByTagName("IFRAME")[0].setAttribute('id', toggle);
			    document.getElementById(toggle).setAttribute('name', document.getElementById(id).getAttribute('target'));

			    let src = document.getElementById(id).getAttribute('name');
			    let sid = '<%=((Request.QueryString["sid"] != null) ? Request.QueryString["sid"].ToString() : "")%>';
			    src += '?sid=' + sid;
			    src += '&userId=<%=((Request.QueryString["userId"] != null) ? Request.QueryString["userId"].ToString() : "")%>';
			    src += '&branchId=<%=((Request.QueryString["branchId"] != null) ? Request.QueryString["branchId"].ToString() : "")%>';
			    src += '&gsmOpId=<%=((Request.QueryString["gsmOpId"] != null) ? Request.QueryString["gsmOpId"].ToString() : "")%>';

			    let role = '<%=((Request.QueryString["role"] != null) ? Request.QueryString["role"].ToString() : "")%>';
			    src += '&role=' + role;
			    document.getElementById(toggle).setAttribute('src', src);
			    document.getElementById(toggle).style.display = 'block';
			    document.getElementById('marginauto').style.display = 'none';
			    document.getElementById('marginauto').style.visibility = 'false';
			    document.getElementById(id).className = 'active';
			}
			else
			{
			    document.getElementById('marginauto').style.display = 'block';
			    document.getElementById('marginauto').style.visibility = 'true';
			}
			if (id.localeCompare(window.prev_id) != 0)
			    document.getElementById(window.prev_id).className = '';

			window.prev_id = id;
       }
	   
	   function isEmpty(s)
	   {
			return (s==="" || s==null);
	   }

	   function applyTheme()
	   {
			let color = '<%=ConfigurationManager.AppSettings.Get("color")%>';
	       document.querySelector(":root").style.setProperty('--forestgreen', color);
	   }
   </script>
</head>

<body>
    <div class="wrapper" runat="server">
        <nav id="sidebar">
            <div class="sidebar-header">
                <h3><img src="Images/Bank.jpg" width="200" height="200"/></h3>
                <strong><%=ConfigurationManager.AppSettings.Get("bank_name").Substring(0,2)%></strong>
            </div>

            <ul class="list-unstyled components">
                <li class="active">
                    <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" 
					   class="dropdown-toggle" id="hf" target="homeframe" onclick="show('hf','home')">
                        <i class="fas fa-home"></i>
                        Home
                    </a>
                    <ul class="collapse list-unstyled" id="homeSubmenu">
                        <li>
                            <a href="#">Welcome <%=((Request.QueryString["userId"] != null) ? OMBanking.Web.Proxy.Bank.Utils.Base64Decode(Request.QueryString["userId"].ToString()) : "")%> !</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a id="ab" target="accbalanceframe" name="AccountBalance.aspx" onclick="show('ab','balance')">
                        <i class="fas fa-"></i>
                        Balance
                    </a>
                </li>
                <li>
                    <a id="tf" target="transframe" name="MiniStatement.aspx" onclick="show('tf','ministatement')">
                        <i class="fas fa-list-alt"></i>
                        Statement
                    </a>
                </li>
                <li id="transfer">
                    <a href="#transferSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                        <i class="fas fa-sync-alt"></i>
                        Transfer
                    </a>
                    <ul class="collapse list-unstyled" id="transferSubmenu">
                        <li>
                            <a id="b2w" target="b2wframe" name="BankToWallet.aspx" onclick="show('b2w','banktowallet')">To Bank Account</a>
                        </li>
                        <li>
                            <a id="w2b" target="w2bframe" name="WalletToBank.aspx" onclick="show('w2b','wallettobank')">To MTN Account</a>
                        </li>
                    </ul>
                </li>
				<li>
                    <a href="#historySubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                        <i class="fas fa-history"></i>
                        History
                    </a>
                    <ul class="collapse list-unstyled" id="historySubmenu">
                        <li>
                            <a id="tlf" target="tranlogsframe" name="Transactions.aspx" onclick="show('tlf','transactions')">Transactions</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a id="cntct" target="cntctframe" name="Contact.aspx" onclick="show('cntct','contact')">
                        <i class="fas fa-paper-plane"></i>
                        Contact
                    </a>
                </li>
            </ul>
        </nav>

        <!-- Page Content  -->
        <div id="content">

            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="container-fluid">

                    <button type="button" id="sidebarCollapse" class="btn btn-info" style="background: var(--forestgreen);">
                        <i class="fas fa-align-left"></i>
                        <span></span>
                    </button>
                    <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" style="background: var(--forestgreen);" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <i class="fas fa-align-justify"></i>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="nav navbar-nav ml-auto">
                            <li class="nav-item active">
							    <form id="logout" runat="server">
									<asp:LinkButton OnClick="btnLogOut_Click" runat="server">
									  <b class="nav-link" style="color: forestgreen;">
									    Log Out
									    <img src="Images/logout.png" width="30" height="30"/>
									  </b>
									</asp:LinkButton>
								</form>
                                
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
			<iframe id="subcomponent" src="Loader.aspx" width="100%" height="86%" frameborder="0" name="subframe"></iframe>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
            });
        });
    </script>
</body>

</html>