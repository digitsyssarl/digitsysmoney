﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="OMBanking.Web.Pages.Views.Login" Async="true" %>
<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title>B2W | Login</title>
	<meta charset="UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="Images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css"/>
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css"/>
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css"/>
	<link rel="stylesheet" type="text/css" href="css/main.css"/>
<!--===============================================================================================-->
<script type="text/javascript">
    window.onload = function () {
        let color = '<%=ConfigurationManager.AppSettings.Get("color")%>';
        document.querySelector(":root").style.setProperty('--forestgreen', color);
        let id = '<%=((Request.QueryString["lid"] != null) ? Request.QueryString["lid"].ToString() : "")%>';
        if (id.localeCompare('Y3VzdG9tZXI=') == 0) {
            document.getElementById('br').style['display'] = 'none';
            document.getElementById('br').style['visibility'] = 'false';
            document.getElementById('rl').style['display'] = 'none';
            document.getElementById('rl').style['visibility'] = 'false';
            document.getElementById('su').setAttribute('href', 'SignUp.aspx?lid=<%=Request.QueryString["lid"]%>');
        }
    }
</script>
</head>
<body>
    
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<form id="form1" class="login100-form validate-form p-l-55 p-r-55 p-t-160" runat="server">
					<span class="login100-form-title">
                        <img src="Images/Bank.jpg" width="40"/><label style="font-size: 20px; margin-left: 35px; margin-right: 15px;">B2W | SignIn</label>
                        <img src="Images/momo.png" width="45"/>
                        <img src="Images/om.jpg" width="55"/>
					</span>
                    <div class="wrap-input100 validate-input m-b-16">
                        <asp:DropDownList id="gsmOperatorIDList" runat="server" CssClass="input100" name="dropdown">
                            <asp:ListItem Selected="True"  Value="ORANGE">Select Operator</asp:ListItem>
                            <asp:ListItem Selected="False"  Value="ORANGE">ORANGE</asp:ListItem>
                            <asp:ListItem Selected="False"  Value="MTN">MTN</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div id="br" class="wrap-input100 validate-input m-b-16">
                        <asp:DropDownList id="branchIDList" runat="server" CssClass="input100" name="dropdown">
                            <asp:ListItem Selected="True"  Value="00">Select Agency</asp:ListItem>
                            <asp:ListItem Selected="False"  Value="00">00</asp:ListItem>
                            <asp:ListItem Selected="False"  Value="01">01</asp:ListItem>
                            <asp:ListItem Selected="False"  Value="02">02</asp:ListItem>
                            <asp:ListItem Selected="False"  Value="03">03</asp:ListItem>
                            <asp:ListItem Selected="False"  Value="04">04</asp:ListItem>
                            <asp:ListItem Selected="False"  Value="05">05</asp:ListItem>
                            <asp:ListItem Selected="False"  Value="06">06</asp:ListItem>
                            <asp:ListItem Selected="False"  Value="07">07</asp:ListItem>
                            <asp:ListItem Selected="False"  Value="08">08</asp:ListItem>
                            <asp:ListItem Selected="False"  Value="10">10</asp:ListItem>
                            <asp:ListItem Selected="False"  Value="11">11</asp:ListItem>
                            <asp:ListItem Selected="False"  Value="12">12</asp:ListItem>
                            <asp:ListItem Selected="False"  Value="13">13</asp:ListItem>
                            <asp:ListItem Selected="False"  Value="14">14</asp:ListItem>
                            <asp:ListItem Selected="False"  Value="15">15</asp:ListItem>
                            <asp:ListItem Selected="False"  Value="16">16</asp:ListItem>
                            <asp:ListItem Selected="False"  Value="17">17</asp:ListItem>
                            <asp:ListItem Selected="False"  Value="18">18</asp:ListItem>
                            <asp:ListItem Selected="False"  Value="19">19</asp:ListItem>
                            <asp:ListItem Selected="False"  Value="20">20</asp:ListItem>
                            <asp:ListItem Selected="False"  Value="21">21</asp:ListItem>
                            <asp:ListItem Selected="False"  Value="22">22</asp:ListItem>
                            <asp:ListItem Selected="False"  Value="23">23</asp:ListItem>
                            <asp:ListItem Selected="False"  Value="24">24</asp:ListItem>
                            <asp:ListItem Selected="False"  Value="25">25</asp:ListItem>
                            <asp:ListItem Selected="False"  Value="26">26</asp:ListItem>
                            <asp:ListItem Selected="False"  Value="27">27</asp:ListItem>
                            <asp:ListItem Selected="False"  Value="28">28</asp:ListItem>
                            <asp:ListItem Selected="False"  Value="29">29</asp:ListItem>
                            <asp:ListItem Selected="False"  Value="30">30</asp:ListItem>
                        </asp:DropDownList>
                    </div>
					<div class="wrap-input100 validate-input m-b-16" data-validate="Please enter username">
						<asp:TextBox id="userID" runat="server" CssClass="input100" type="text" name="username" placeholder="Username"/>
						<span class="focus-input100"></span>
					</div>

					<div class="wrap-input100 validate-input" data-validate = "Please enter password">
						<asp:TextBox id="pwd" runat="server" CssClass="input100" type="password" name="pass" placeholder="Password"/>
						<span class="focus-input100"></span>
					</div>
                    <br />
                    <div id="rl" class="wrap-input100 validate-input">
                        <asp:DropDownList id="roleList" runat="server" CssClass="input100" name="dropdown">
                            <asp:ListItem Selected="True"  Value="Agent">Select Role</asp:ListItem>
                            <asp:ListItem Selected="False" Value="Agent">Agent</asp:ListItem>
                            <asp:ListItem Selected="False" Value="Auditor">Auditor</asp:ListItem>
                            <asp:ListItem Selected="False" Value="Supervisor">Supervisor</asp:ListItem>
                            <asp:ListItem Selected="False" Value="Admin">Admin</asp:ListItem>
                        </asp:DropDownList>
                    </div>

					<div class="text-right p-t-13 p-b-15">
						<span class="txt1">
							Forgot
						</span>

						<a href="Forgot.aspx" class="txt2">
							Username / Password?
						</a>
					</div>

					<div class="container-login100-form-btn">
						<asp:Button id="btnSignIn" runat="server" onclick="btnSignIn_Click" Text="Sign in" CssClass="login100-form-btn"></asp:Button>
					</div>
                    <br />
                    <asp:Label id="txtConfirm" runat="server" Text="" ForeColor="Green"/>
                    <br />
					<div class="flex-col-c p-b-40">
						<span class="txt1 p-b-9">
							Don't have an account?
						</span>
						<a id="su" href="SignUp.aspx" class="txt3 p-b-5">
							Sign up now
						</a>
                        <img src="https://seal.godaddy.com/images/3/en/siteseal_gd_3_h_d_m.gif" style="float: right;"/>
					</div>
				</form>
			</div>
		</div>
	</div>
<!--===============================================================================================-->
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

</body>
</html>
