﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Transactions.aspx.cs" Inherits="OMBanking.Web.Pages.Views.TransactionLog" Async="true" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>M-BANKING TRANSACTION LOGS</title>
    <link type="text/css" rel="stylesheet" href="https://cdn.datatables.net/1.10.9/css/dataTables.bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="https://cdn.datatables.net/responsive/1.0.7/css/responsive.bootstrap.min.css" />
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/responsive/1.0.7/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.9/js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script type="text/javascript">
        $(function () {
            $('[id*=transactionLogTable]').prepend($("<thead></thead>").append($(this).find("tr:first"))).DataTable({
                "responsive": true,
                "sPaginationType": "full_numbers"
            });
        });
        $(function () {
            $('[id*=responseTable]').prepend($("<thead></thead>").append($(this).find("tr:first"))).DataTable({
                "responsive": true,
                "sPaginationType": "full_numbers"
            });
        });
    </script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round"/>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <style>
    body {
        color: #566787;
        background: #f5f5f5;
        font-family: 'Varela Round', sans-serif;
        font-size: 13px;
    }
    .table-responsive {
        margin: 30px 0;
    }
    .table-wrapper {
        min-width: 1000px;
        background: #fff;
        padding: 20px 25px;
        border-radius: 3px;
        box-shadow: 0 1px 1px rgba(0,0,0,.05);
    }
    .table-title {
        padding-bottom: 15px;
        background: #228B22;
        color: #fff;
        padding: 16px 30px;
        margin: -20px -25px 10px;
        border-radius: 3px 3px 0 0;
    }
    .table-title h2 {
        margin: 5px 0 0;
        font-size: 24px;
    }
    .table-title .btn {
        color: #566787;
        float: right;
        font-size: 13px;
        background: #fff;
        border: none;
        min-width: 50px;
        border-radius: 2px;
        border: none;
        outline: none !important;
        margin-left: 10px;
    }
    .table-title .btn:hover, .table-title .btn:focus {
        color: #566787;
        background: #f2f2f2;
    }
    .table-title .btn i {
        float: left;
        font-size: 21px;
        margin-right: 5px;
    }
    .table-title .btn span {
        float: left;
        margin-top: 2px;
    }
    table.table tr th, table.table tr td {
        border-color: #e9e9e9;
        padding: 12px 15px;
        vertical-align: middle;
    }
    table.table tr th:first-child {
        width: 60px;
    }
    table.table tr th:last-child {
        width: 100px;
    }
    table.table-striped tbody tr:nth-of-type(odd) {
        background-color: #fcfcfc;
    }
    table.table-striped.table-hover tbody tr:hover {
        background: #f5f5f5;
    }
    table.table th i {
        font-size: 13px;
        margin: 0 5px;
        cursor: pointer;
    }	
    table.table td:last-child i {
        opacity: 0.9;
        font-size: 22px;
        margin: 0 5px;
    }
    table.table td a {
        font-weight: bold;
        color: #566787;
        display: inline-block;
        text-decoration: none;
    }
    table.table td a:hover {
        color: #2196F3;
    }
    table.table td a.settings {
        color: #2196F3;
    }
    table.table td a.delete {
        color: #F44336;
    }
    table.table td i {
        font-size: 19px;
    }
    table.table .avatar {
        border-radius: 50%;
        vertical-align: middle;
        margin-right: 10px;
    }
    .status {
        font-size: 30px;
        margin: 2px 2px 0 0;
        display: inline-block;
        vertical-align: middle;
        line-height: 10px;
    }
    .text-success {
        color: #10c469;
    }
    .text-info {
        color: #62c9e8;
    }
    .text-warning {
        color: #FFC107;
    }
    .text-danger {
        color: #ff5b5b;
    }
    .pagination {
        float: right;
        margin: 0 0 5px;
    }
    .pagination li a {
        border: none;
        font-size: 13px;
        min-width: 30px;
        min-height: 30px;
        color: #999;
        margin: 0 2px;
        line-height: 30px;
        border-radius: 2px !important;
        text-align: center;
        padding: 0 6px;
    }
    .pagination li a:hover {
        color: #666;
    }	
    .pagination li.active a, .pagination li.active a.page-link {
        background: #03A9F4;
    }
    .pagination li.active a:hover {        
        background: #0397d6;
    }
    .pagination li.disabled i {
        color: #ccc;
    }
    .pagination li i {
        font-size: 16px;
        padding-top: 6px
    }
    .hint-text {
        float: left;
        margin-top: 10px;
        font-size: 13px;
    }
    #btnCancel {
      padding: 5px 10px;
      border: none;
      border-radius: 4px;
      cursor: pointer;
      float: left;
    }
    #btnSelectAll {
      padding: 5px 10px;
      border: none;
      border-radius: 4px;
      cursor: pointer;
      float: left;
    }
</style>
<script type="text/javascript">
    window.onload = function () {
        var hColl = document.getElementsByClassName('scope');
        let color = '<%=ConfigurationManager.AppSettings.Get("color").ToLower()%>';
        changeColor(hColl, color, 'scope');
        hColl = document.getElementsByClassName('table-title');
        changeColor(hColl, color, 'table-title');
        hideIf('userActions');
        hideIf('userActions1');
        let role = '<%=((Request.QueryString["role"] != null) ? Request.QueryString["role"].ToString() : "")%>';
        hideBtn('btnValidate', role, color);
        hideBtn('btnSelectAll', role, color);
    }
    function changeColor(coll, color, cls) {

        for (var i = 0, len = coll.length; i < len; i++) {
            if (cls == 'scope') {
                coll[i].style['color'] = color;
            }
            else if (cls == 'table-title') {
                coll[i].style['background-color'] = color;
                coll[i].style['color'] = '#FFFFFF';
            }
            else {
                coll[i].style['background-color'] = color;
                coll[i].style['border-color'] = color;
                coll[i].style['color'] = '#FFFFFF';
            }
        }
    }
    function hideIf(id) {
        let role = '<%=((Request.QueryString["role"] != null) ? Request.QueryString["role"].ToString() : "")%>';
        var obj = document.getElementById(id);
        if (role === "") 
            obj.style['display'] = 'none';
    }
    function hideBtn(btn, role, color) {
        var btnId = document.getElementById(btn);
        if (role === "") {
            btnId.style['background-color'] = '#A9A9A9';
            btnId.style['color'] = 'white';
        }
        else {
            btnId.style['background-color'] = color;
            btnId.style['color'] = 'white';
        }
    }
</script>
</head>
<body>
    <br /><br />
    <h1 class="scope" style="font-size:xx-large;">Transactions</h1>
    <form id="form1" runat="server">
    <div><br /><br /><br /></div>
    <div class="table-title">
            <div class="row">
                <div class="col-sm-5">
                    <h2>History</h2>
                </div>
                <div class="col-sm-7" style="text-align: right;">
                    Search
                    <asp:TextBox ID="txtSearch" runat="server" OnTextChanged="txtSearch_TextChanged" AutoPostBack="true" style="width:130px; color:black;"/>&nbsp;
                    &nbsp;&nbsp;
                    Date From
                    <asp:TextBox ID="txtFrom" runat="server" TextMode="DateTimeLocal" style="width:140px; color:black;"/>&nbsp;
                    Date To
                    <asp:TextBox ID="txtTo" runat="server" TextMode="DateTimeLocal" style="width:140px; color:black;"/>&nbsp;
                    <asp:Button ID="btnSearch"  onclick="btnSearch_Click" Text="Filter" runat="server" style="width:60px; color:black;"/>
                    &nbsp;&nbsp;
                    <asp:LinkButton ID="btnToExcel" CssClass="toExcel" Width="40" Height="40" runat="server" OnClick="btnToExcel_Click" BorderColor="Transparent" ToolTip="Export to Excel">
                    <img src="Images/excel.png" alt="Export to Excel" width="40" height="40"/> 
                    </asp:LinkButton>
                    &nbsp;&nbsp;	
                    <asp:LinkButton ID="btnToPDF" Width="35" Height="35" runat="server" OnClick="btnToPDF_Click" BorderColor="Transparent" ToolTip="Export to PDF" >
                    <img src="Images/pdf.png" alt="Export to PDF" width="35" height="35"/>
                    </asp:LinkButton>		
                </div>
            </div>
        </div>
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <div id="transactionsId" style="overflow: scroll">
          <asp:UpdatePanel ID="UpdatePanel1" runat="server">
           <ContentTemplate>
            <asp:GridView ID="transactionLogTable" runat="server" AutoGenerateColumns="true" CssClass="table table-striped table-hover" 
                 ShowFooter ="true" ShowHeader="true" ShowHeaderWhenEmpty="true" 
                 AllowPaging="true" AllowSorting="true" PageSize="5" ForeColor="Black" 
                 OnPageIndexChanging="transactionLogTable_PageIndexChanging"
                 OnSorting="transactionLogTable_Sorting"
                 OnRowCreated="transactionLogTable_RowCreated">
                <PagerSettings  Mode="NextPreviousFirstLast" FirstPageText="First" PreviousPageText="Previous" NextPageText="Next" LastPageText="Last" />
                <Columns>
                    <asp:TemplateField HeaderText="Select">
                        <ItemTemplate>
                            <asp:CheckBox ID="validationCheckBox" runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
           </ContentTemplate>
          </asp:UpdatePanel>
        </div>
        <br />
        <fieldset>
            <div class="item">
                <label>
                    <asp:Button ID="btnCancel" Visible="false" runat="server" onclick="btnCancel_Click"  Text="Cancel" style="width:99px" />
                </label>
                <label>
                    <asp:Button ID="btnSelectAll" Visible="false" runat="server" onclick="btnSelectAll_Click"  Text="Select All" style="width:105px" />
                </label>
            </div>
        </fieldset> 
        <br /><br /><br /><br />
        <div id="userActions" class="table-title">
            <div class="row">
                <div class="col-sm-5">
                    <h2>User Action History</h2>
                </div>
                <div class="col-sm-7" style="text-align: right;">
                    Search
                    <asp:TextBox ID="txtSearch1" runat="server" OnTextChanged="txtSearch1_TextChanged" AutoPostBack="true" style="color:black;"/>&nbsp;
                    &nbsp;&nbsp;
                    <asp:LinkButton ID="btnToExcel1" CssClass="toExcel" Width="40" Height="40" runat="server" OnClick="btnToExcel1_Click" BorderColor="Transparent" ToolTip="Export to Excel">
                    <img src="Images/excel.png" alt="Export to Excel" width="40" height="40"/> 
                    </asp:LinkButton>
                    &nbsp;&nbsp;
                    <asp:LinkButton ID="btnToPDF1" Width="35" Height="35" runat="server" OnClick="btnToPDF1_Click" BorderColor="Transparent" ToolTip="Export to PDF" >
                    <img src="Images/pdf.png" alt="Export to PDF" width="35" height="35"/>
                    </asp:LinkButton>			
                </div>
            </div>
        </div>
        <div id="userActions1" style="overflow: scroll">
            <asp:GridView ID="responseTable" runat="server" AutoGenerateColumns="true" CssClass="table table-striped table-hover"
                 ShowFooter ="true" ShowHeader="true" ShowHeaderWhenEmpty="true" 
                 AllowPaging="true" AllowSorting="true" PageSize="3" ForeColor="Black" 
                 OnPageIndexChanging="responseTable_PageIndexChanging"
                 OnSorting="responseTable_Sorting">
                <PagerSettings  Mode="NextPreviousFirstLast" FirstPageText="First" PreviousPageText="Previous" NextPageText="Next" LastPageText="Last" />
            </asp:GridView>
        </div>
    </form>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script type="text/javascript" src="Scripts/jquery.blockUI.js"></script>
    <script type="text/javascript">
        $(function () {
            BlockUI("transactionsId");
            $.blockUI.defaults.css = {};
        });
        function BlockUI(elementID) {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_beginRequest(function () {
                $("#" + elementID).block({ message: '<div align = "center">' + '<img src="Images/loadingAnim.gif"/></div>',
                    css: {},
                    overlayCSS: { backgroundColor: '<%=ConfigurationManager.AppSettings.Get("color")%>', opacity: 0.6, border: '3px solid #63B2EB' }
                });
            });
            prm.add_endRequest(function () {
                $("#" + elementID).unblock();
            });
        };
    </script>
</body>
</html>
