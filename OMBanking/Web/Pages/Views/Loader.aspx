﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Loader.aspx.cs" Inherits="OMBanking.Web.Pages.Views.Loader" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style>
        #foot {
            color: white;
            margin-top:2.5px;
            padding: 10px;
            text-align: center;
        }
    </style>
    <script type="text/javascript">
        window.onload = function()
        {
            document.getElementById("imgId").setAttribute("width", ((3.95*window.innerWidth)/4) + "");
            document.getElementById("imgId").setAttribute("height", ((3.6 * window.innerHeight) / 4) + "");
            let color = '<%=ConfigurationManager.AppSettings.Get("color")%>';
            document.getElementById('foot').style.backgroundColor = color;
        }
    </script>
</head>
<body>
    <div id="content">
        <img id="imgId" class="marginauto" src="Images/images1.jpeg" alt="Thanks for using B2W app"/>
        <br />
        <footer id="foot">
            &copy; Copyright <%=DateTime.Now.Year.ToString()%>, <%=ConfigurationManager.AppSettings.Get("bank_name")%>
        </footer>
    </div>
</body>
</html>
