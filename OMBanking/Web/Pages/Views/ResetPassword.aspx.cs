﻿using OMBanking.Web.DB;
using OMBanking.Web.Log;
using OMBanking.Web.Proxy.Bank;
using OMBanking.Web.Security;
using System;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Drawing;
using System.Web.UI;

namespace OMBanking.Web.Pages.Views
{
    public partial class ResetPassword : Page
    {
        private const string SESSION_ID = "sid";
        private const string EMAIL_ID   = "email";
        private const string USER_ID    = "userId";

        protected async void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                NameValueCollection SessionParams = Request.QueryString;
                Session[EMAIL_ID] = (SessionParams[EMAIL_ID] != null)
                                    ? SessionParams[EMAIL_ID].ToString() : "";
                Session[USER_ID] = (SessionParams[USER_ID] != null)
                                      ? SessionParams[USER_ID].ToString() : "";

                string email = Utils.Base64Decode(Session[EMAIL_ID].ToString());
                if (email != "" && (string)Session[USER_ID] != "")
                {

                    string UserID = await new USSDHelper().GetUserByEmail(email),
                           _UserID = Utils.Base64Decode(Session[USER_ID].ToString());
                    if (UserID.CompareTo(_UserID) != 0)
                    {
                        Response.Redirect("~/Pages/Views/Login.aspx", false);
                    }
                }
                else
                {
                    Response.Redirect("~/Pages/Views/Login.aspx", false);
                }
            }
        }

        protected async void btnResetPassword_Click(object sender, EventArgs e)
        {
            string newPassword = pwd.Text;
            if(newPassword != null 
               && newPassword != ""
               && newPassword.Length >= 8)
            {
                newPassword = DataEncryptor.EncryptString(newPassword);
                USSDHelper helper = new USSDHelper();
                string email = Utils.Base64Decode(Session[EMAIL_ID].ToString());
                bool result = await helper.UpdatePassword(email, newPassword);
                if(!result)
                {
                    txtConfirm.ForeColor = Color.Red;
                    txtConfirm.Text = "<center>Resetting Password Failed. Try again !</center>";
                    ServiceLogger _Logger = (ServiceLogger)LogFactory.create(Logger.LoggingMode.SERVICE);
                    _Logger.Log(TraceLevel.Error, "Updating Password Failed !");
                }
                else
                {
                    txtConfirm.ForeColor = Color.Green;
                    txtConfirm.Text = "<center>Your password has been successfully updated !</center>";
                }
            }
            else
            {
                txtConfirm.ForeColor = Color.Red;
                txtConfirm.Text = "<center>Invalid Password (min. 8 characters).</center>";
            }
        }
    }
}