﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="~/Pages/Views/Subscription.aspx.cs" Inherits="OMBanking.Web.Pages.Views.Subscription" Async="true" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>
        M-BANKING SUBSCRIPTION
    </title>
    <link rel="stylesheet" href="form.css" type="text/css" />
    <link type="text/css" rel="stylesheet" href="https://cdn.datatables.net/responsive/1.0.7/css/responsive.bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="https://cdn.datatables.net/1.10.9/css/dataTables.bootstrap.min.css" />
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/responsive/1.0.7/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.9/js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript">
        $(function () {
            $('[id*=responseTable]').prepend($("<thead></thead>").append($(this).find("tr:first"))).DataTable({
                "responsive": true,
                "sPaginationType": "full_numbers"
            });
        });
        $(function () {
            $('[id*=checkTable]').prepend($("<thead></thead>").append($(this).find("tr:first"))).DataTable({
                "responsive": true,
                "sPaginationType": "full_numbers"
            });
        });
    </script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round"/>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
    <style>
    body {
        color: #566787;
        background: #f5f5f5;
        font-family: 'Varela Round', sans-serif;
        font-size: 13px;
    }
    .table-responsive {
        margin: 30px 0;
    }
    .table-wrapper {
        min-width: 1000px;
        background: #fff;
        padding: 20px 25px;
        border-radius: 3px;
        box-shadow: 0 1px 1px rgba(0,0,0,.05);
    }
    .table-title {
        padding-bottom: 15px;
        background: #228B22;
        color: #fff;
        padding: 16px 30px;
        margin: -20px -25px 10px;
        border-radius: 3px 3px 0 0;
    }
    .table-title h2 {
        margin: 5px 0 0;
        font-size: 24px;
    }
    .table-title .btn {
        color: #566787;
        float: right;
        font-size: 13px;
        background: #fff;
        border: none;
        min-width: 50px;
        border-radius: 2px;
        border: none;
        outline: none !important;
        margin-left: 10px;
    }
    .table-title .btn:hover, .table-title .btn:focus {
        color: #566787;
        background: #f2f2f2;
    }
    .table-title .btn i {
        float: left;
        font-size: 21px;
        margin-right: 5px;
    }
    .table-title .btn span {
        float: left;
        margin-top: 2px;
    }
    table.table tr th, table.table tr td {
        border-color: #e9e9e9;
        padding: 12px 15px;
        vertical-align: middle;
    }
    table.table tr th:first-child {
        width: 60px;
    }
    table.table tr th:last-child {
        width: 100px;
    }
    table.table-striped tbody tr:nth-of-type(odd) {
        background-color: #fcfcfc;
    }
    table.table-striped.table-hover tbody tr:hover {
        background: #f5f5f5;
    }
    table.table th i {
        font-size: 13px;
        margin: 0 5px;
        cursor: pointer;
    }	
    table.table td:last-child i {
        opacity: 0.9;
        font-size: 22px;
        margin: 0 5px;
    }
    table.table td a {
        font-weight: bold;
        color: #566787;
        display: inline-block;
        text-decoration: none;
    }
    table.table td a:hover {
        color: #2196F3;
    }
    table.table td a.settings {
        color: #2196F3;
    }
    table.table td a.delete {
        color: #F44336;
    }
    table.table td i {
        font-size: 19px;
    }
    table.table .avatar {
        border-radius: 50%;
        vertical-align: middle;
        margin-right: 10px;
    }
    .status {
        font-size: 30px;
        margin: 2px 2px 0 0;
        display: inline-block;
        vertical-align: middle;
        line-height: 10px;
    }
    .text-success {
        color: #10c469;
    }
    .text-info {
        color: #62c9e8;
    }
    .text-warning {
        color: #FFC107;
    }
    .text-danger {
        color: #ff5b5b;
    }
    .pagination {
        float: right;
        margin: 0 0 5px;
    }
    .pagination li a {
        border: none;
        font-size: 13px;
        min-width: 30px;
        min-height: 30px;
        color: #999;
        margin: 0 2px;
        line-height: 30px;
        border-radius: 2px !important;
        text-align: center;
        padding: 0 6px;
    }
    .pagination li a:hover {
        color: #666;
    }	
    .pagination li.active a, .pagination li.active a.page-link {
        background: #03A9F4;
    }
    .pagination li.active a:hover {        
        background: #0397d6;
    }
    .pagination li.disabled i {
        color: #ccc;
    }
    .pagination li i {
        font-size: 16px;
        padding-top: 6px
    }
    .hint-text {
        float: left;
        margin-top: 10px;
        font-size: 13px;
    }
    span
    {
        color:black;
    }
    #btnRegister {
      padding: 5px 10px;
      border: none;
      border-radius: 4px;
      cursor: pointer;
      float: left;
    }
    #btnClear {
      padding: 5px 10px;
      border: none;
      border-radius: 4px;
      cursor: pointer;
      float: left;
    }
    
    .grid-container {
      display: grid;
      grid-template-columns: auto auto;
      padding: 1px;
    }

</style>
<script type="text/javascript">
    window.onload = function () {
        var hColl = document.getElementsByClassName('scope');
        let color = '<%=ConfigurationManager.AppSettings.Get("color").ToLower()%>';
        changeColor(hColl, color, 'scope');
        hColl = document.getElementsByClassName('table-title');
        changeColor(hColl, color, 'table-title');
        let role = '<%=((Request.QueryString["role"] != null) ? Request.QueryString["role"].ToString() : "")%>';
        hideIf('userActions', role);
        hideIf('userActions1', role);
        hideIf('backCheck', role);
        hideIf('backCheck1', role);
        hideBtn('btnRegister', role, color);
        hideBtn('btnClear', role, color);
    }
    function changeColor(coll, color, cls) {

        for (var i = 0, len = coll.length; i < len; i++) {
            if (cls == 'scope') {
                coll[i].style['color'] = color;
            }
            else if (cls == 'table-title') {
                coll[i].style['background-color'] = color;
                coll[i].style['color'] = '#FFFFFF';
            }
            else {
                coll[i].style['background-color'] = color;
                coll[i].style['border-color'] = color;
                coll[i].style['color'] = '#FFFFFF';
            }
        }
    }
    function hideIf(id, role) {
        var obj = document.getElementById(id);
        if (role === "")
            obj.style['display'] = 'none';
    }

    function hideBtn(btn,role,color)
    {
        var btnId = document.getElementById(btn);
        if (role === "") {
            btnId.style['background-color'] = '#A9A9A9';
            btnId.style['color'] = 'white';
        }
        else {
            btnId.style['background-color'] = color;
            btnId.style['color'] = 'white';
        }
    }
</script>
</head>
<body>
    
    <div id='wrap'>
        <h1 class="scope" style="font-size:xx-large;">Subscription</h1>
        <div><br /></div>
		<section class='form'>
			<form id="form1" runat="server">
                <div class="item">
                    <label>
                        <span>Select Supervisor</span>
                        <asp:DropDownList id="SupervisorList" runat="server" name="dropdown">
                            <asp:ListItem Selected ="True" Value="" Text="--Choose--">--Choose--</asp:ListItem>
                        </asp:DropDownList>
                    </label>
                </div>
                <br /><br />
                <div class="grid-container">
                  <div class="grid-item">
                      <fieldset>
					    <div class="item">
                            <label>
							    <span>First Name</span>
							    <asp:TextBox id="txtfirstname" ToolTip="First Name" runat="server" name="txtfirstname" CssClass="required"/>
						    </label>
                        </div>
                    </fieldset>
                  </div>
                  <div class="grid-item">
                      <fieldset>
					        <div class="item">
                                <label>
                                    <span>Last Name</span>
							        <asp:TextBox id="txtlastname" ToolTip="Last Name" runat="server" name="txtlastname" CssClass="required"/>				
                                </label>
					        </div>
                        </fieldset>
                  </div>
                  <div class="grid-item">
                      <fieldset>
                        <div class="item">
						    <label>
                                <span>ID Type</span>
                                <asp:DropDownList id="idList" Width="210px" ToolTip="Select a document type (ID Card, Passport, Work Permit, Resident ID)" runat="server" name="dropdown">
                                    <asp:ListItem Selected ="True" Value="National">National</asp:ListItem>
                                    <asp:ListItem Selected ="False" Value="Passport">Passport</asp:ListItem>
                                    <asp:ListItem Selected ="False" Value="Resident">Resident</asp:ListItem>
                                    <asp:ListItem Selected ="False" Value="Work Permit">Work Permit</asp:ListItem>
                                </asp:DropDownList>
                            </label>
                        </div>
                    </fieldset>
                  </div>  
                  <div class="grid-item">
                      <fieldset>
                        <div class="item">
                            <label>
                                <span>ID Number</span>
                                <asp:TextBox id="txtidentity" ToolTip="Identity" runat="server" name="txtidentity" CssClass="required"/>
						    </label>
					    </div>
                    </fieldset>
                  </div>
                  <div class="grid-item">
                      <fieldset>
					    <div class="item">
                            <label>
							    <span>Phone No</span>
							    <asp:TextBox id="txtmsisdnreg" placeholder="e.g. 7702200047" ToolTip="Phone number must be minimum 7 and maximum 10 characters long" runat="server" name="txtmsisdnreg" CssClass="required"/>		
						    </label>
					    </div>
                    </fieldset>
                  </div>
                  <div class="grid-item">
                      <fieldset>
                        <div class="item">
                            <label>
							    <span>Account No</span>
							    <asp:TextBox id="txtaccountno" OnTextChanged="txtaccountno_TextChanged" AutoPostBack="true" placeholder="e.g. 21733623543109" ToolTip="Account Number " runat="server" name="txtaccountno"/>		
						    </label>
					    </div>
                    </fieldset>
                  </div>  
                  <div class="grid-item">
                      <fieldset>
                        <div class="item">
                            <label>
						        <span>Alias</span>
                                <asp:TextBox id="txtaliasreg" Enabled="false" placeholder="e.g. BNKSXJAU21733623543109" ToolTip="Alias must be 22 characters long" runat="server" CssClass="required"></asp:TextBox>	
					        </label>
				        </div>
                    </fieldset>
                  </div>
                  <div class="grid-item">
                      <fieldset>
                        <div class="item">
						    <label>
							    <span>Code Service</span>
                                <asp:DropDownList id="ddListCD" Width="210px" ToolTip="Code Service must have value 1 = Charge, 2 = Decharge, or 3 = All" runat="server" name="dropdown">
                                    <asp:ListItem Selected ="False" Value="1">Charge</asp:ListItem>
                                    <asp:ListItem Selected ="False" Value="2">Decharge</asp:ListItem>
                                    <asp:ListItem Selected ="True" Value="3">All</asp:ListItem>
                                </asp:DropDownList>
						    </label>
					    </div>
                    </fieldset>
                  </div>
                  <div class="grid-item">
                      <fieldset>
                        <div class="item">
                            <label>
							    <span>Libelle</span>
							    <asp:TextBox id="txtlibelle" ToolTip="Libelle must be minimum 0 and maximum 25 characters long" runat="server" name="txtlibelle" CssClass="required"/>		
						    </label>
					    </div>
                    </fieldset>
                  </div>
                  <div class="grid-item">
                      <fieldset>
                        <div class="item">
                            <label>
							    <span>Devise</span>
							    <asp:TextBox Enabled="false" ToolTip="Devise must be 3 characters long. Ex. 950 (XAF), 952 (XOF)" id="txtdevise" runat="server" name="txtdevise" CssClass="required"/>		
						    </label>
					    </div>
                     </fieldset>
                  </div>
                  <div class="grid-item"> 
                      <fieldset>
                        <div class="item">
                            <label>
							    <span>Key</span>
							    <asp:TextBox placeholder="e.g. D5MK91EM" ToolTip="Key must be 8 characters long" id="txtkeyreg" runat="server" name="txtkeyreg" CssClass="required"/>		
						    </label>
					    </div>
                    </fieldset>
                  </div> 
                  <div class="grid-item">
                      <fieldset>
                        <div class="item">
                            <label>
							    <span>Active Date</span>
							    <asp:TextBox TextMode="DateTimeLocal" ToolTip="Active Date must have format YYYY-MM-DD HH:MM:SS" id="txtactivedate" runat="server"/>		
						    </label>
					    </div>
				    </fieldset>
                  </div>
                </div>              
                <fieldset>
                    <div class="item">
                        <label>
                            <asp:Button ID="btnRegister" runat="server" onclick="btnRegister_Click"  Text="Register" style="width:99px" />
                        </label>
                        <label style="width: 10px;">
                        </label>
                        <label>
							<asp:Button ID="btnClear" runat="server" onclick="btnClear_Click" Text="Clear" style="width:99px" />
						</label>
					</div>
				</fieldset>  
                <br /><br /> 
                <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"/>
                <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
				<asp:ScriptManager ID="ScriptManager1" runat="server">
                </asp:ScriptManager>
                <fieldset>
                    <asp:Label id="lbl" runat="server" ForeColor="Red"></asp:Label><br />
                    <asp:Label id="response" runat="server" ForeColor="Green"> </asp:Label>
                    <br />
                    <div id="backCheck" class="table-title">
                    <div class="row">
                        <div class="col-sm-5">
                            <h2>Identity Checking</h2>
                        </div>
                        <div class="col-sm-7" style="text-align: right;">
                            Search
                            <asp:TextBox ID="txtSearch2" runat="server" OnTextChanged="txtSearch2_TextChanged" AutoPostBack="true" style="color:black;"/>			
                        </div>
                    </div>
                    </div>
                    <div id="backCheck1" style="overflow: scroll">
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                            <ContentTemplate>
                                <asp:GridView ID="checkTable" runat="server" AutoGenerateColumns="false" CssClass="table table-striped table-hover" 
                                    ShowFooter="true" ShowHeader="true" ShowHeaderWhenEmpty="true" 
                                    AllowPaging="true" AllowSorting="true" PageSize="3" ForeColor="Black" 
                                    OnPageIndexChanging="checkTable_PageIndexChanging">
                                    <PagerSettings  Mode="NextPreviousFirstLast" FirstPageText="First" PreviousPageText="Previous" NextPageText="Next" LastPageText="Last" />
                                    <Columns>
                                        <asp:BoundField DataField="FirstName" HeaderText="FirstName" />
                                        <asp:BoundField DataField="LastName" HeaderText="LastName" />
                                        <asp:BoundField DataField="MiddleName" HeaderText="MiddleName"/>
                                        <asp:BoundField DataField="GenderID" HeaderText="GenderID" />
                                        <asp:BoundField DataField="NationalityID" HeaderText="NationalityID" />
                                        <asp:BoundField DataField="DateOfBirth" HeaderText="DateOfBirth"/>
                                        <asp:BoundField DataField="Age" HeaderText="Age"/>
                                        <asp:BoundField DataField="MaritalStatusID" HeaderText="MaritalStatusID" />
                                        <asp:BoundField DataField="PassportNo" HeaderText="PassportNo" />
                                        <asp:BoundField DataField="PassportIssuedCityID" HeaderText="PassportIssuedCityID"/>
                                        <asp:BoundField DataField="PassportExpiryDate" HeaderText="PassportExpiryDate" />
                                        <asp:BoundField DataField="ResidentID" HeaderText="ResidentID" />
                                        <asp:BoundField DataField="WorkPermitNo" HeaderText="WorkPermitNo"/>
                                    </Columns>
                                </asp:GridView>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <br />
                        <label style="color: orangered;"> (*) Please check information provided by customers 
                            with existing information in the database. If records do not match, send them
                            to an agency to update their personal information.
                        </label>
                    </div>
                    <br />
                    <div id="backCheck2" class="table-title">
                    <div class="row">
                        <div class="col-sm-5">
                            <h2>Account Status Checking</h2>
                        </div>
                        <div class="col-sm-7" style="text-align: right;">
                            Search
                            <asp:TextBox ID="txtSearch3" runat="server" OnTextChanged="txtSearch3_TextChanged" AutoPostBack="true" style="color:black;"/>           
                        </div>
                    </div>
                    </div>
                    <div id="backCheck3" style="overflow: scroll">
                        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                            <ContentTemplate>
                                <asp:GridView ID="checkTable1" runat="server" AutoGenerateColumns="false" CssClass="table table-striped table-hover" 
                                    ShowFooter="true" ShowHeader="true" ShowHeaderWhenEmpty="true" 
                                    AllowPaging="true" AllowSorting="true" PageSize="3" ForeColor="Black" 
                                    OnPageIndexChanging="checkTable1_PageIndexChanging">
                                    <PagerSettings  Mode="NextPreviousFirstLast" FirstPageText="First" PreviousPageText="Previous" NextPageText="Next" LastPageText="Last" />
                                    <Columns>
                                        <asp:BoundField DataField="OurBranchID" HeaderText="OurBranchID" />
                                        <asp:BoundField DataField="AccountID" HeaderText="AccountID" />
                                        <asp:BoundField DataField="AccountStatusID" HeaderText="AccountStatusID"/>
                                        <asp:BoundField DataField="Name" HeaderText="Name" />
                                        <asp:BoundField DataField="Phone1" HeaderText="Phone1" />
                                        <asp:BoundField DataField="Mobile" HeaderText="Mobile"/>
                                        <asp:BoundField DataField="Address1" HeaderText="Address1"/>
                                        <asp:BoundField DataField="Address2" HeaderText="Address2" />
                                        <asp:BoundField DataField="CityID" HeaderText="CityID" />
                                        <asp:BoundField DataField="CountryID" HeaderText="CountryID"/>
                                        <asp:BoundField DataField="CreatedBy" HeaderText="CreatedBy" />
                                        <asp:BoundField DataField="CreatedOn" HeaderText="CreatedOn" />
                                        <asp:BoundField DataField="ModifiedBy" HeaderText="ModifiedBy"/>
                                        <asp:BoundField DataField="ModifiedOn" HeaderText="ModifiedOn"/>
                                    </Columns>
                                </asp:GridView>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
				</fieldset>
                <fieldset>
                    <br /><br />
                    <div id="userActions" class="table-title">
                    <div class="row">
                        <div class="col-sm-5">
                            <h2>User Action History</h2>
                        </div>
                        <div class="col-sm-7" style="text-align: right;">
                            Search
                            <asp:TextBox ID="txtSearch1" runat="server" OnTextChanged="txtSearch1_TextChanged" AutoPostBack="true" style="color:black;"/>&nbsp;
                            &nbsp;&nbsp;
                            <asp:LinkButton ID="btnToExcel" CssClass="toExcel" Width="40" Height="40" runat="server" OnClick="btnToExcel_Click" BorderColor="Transparent" ToolTip="Export to Excel">
                            <img src="Images/excel.png" alt="Export to Excel" width="40" height="40"/> 
                            </asp:LinkButton>
                            &nbsp;&nbsp;
                            <asp:LinkButton ID="btnToPDF" Width="35" Height="35" runat="server" OnClick="btnToPDF_Click" BorderColor="Transparent" ToolTip="Export to PDF" >
                            <img src="Images/pdf.png" alt="Export to PDF" width="35" height="35"/>
                            </asp:LinkButton>			
                        </div>
                    </div>
                    </div>
                    <div id="userActions1" style="overflow: scroll">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <asp:GridView ID="responseTable" runat="server" AutoGenerateColumns="true" CssClass="table table-striped table-hover" 
                                    ShowFooter="true" ShowHeader="true" ShowHeaderWhenEmpty="true" 
                                    AllowPaging="true" AllowSorting="true" PageSize="2" ForeColor="Black" 
                                    OnPageIndexChanging="responseTable_PageIndexChanging"
                                    OnSorting="responseTable_Sorting">
                                    <PagerSettings  Mode="NextPreviousFirstLast" FirstPageText="First" PreviousPageText="Previous" NextPageText="Next" LastPageText="Last" />
                                </asp:GridView>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
				</fieldset>
			</form>	
		</section>
	</div>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="http://dropthebit.com/demos/validator/multifield.js"></script>
    <script src="http://dropthebit.com/demos/validator/validator.js"></script>
	<script>
		// initialize the validator function
		validator.message['date'] = 'not a real date';

	    $('form')
			.on('blur', 'input[required], input.optional, select.required', validator.checkField)
			.on('change', 'select.required', validator.checkField);
			
		$('.multi.required')
			.on('keyup', 'input', function(){ 
				validator.checkField.apply( $(this).siblings().last()[0] );
			}).on('blur', 'input', function(){ 
				validator.checkField.apply( $(this).siblings().last()[0] );
			});
		$('form').submit(function (e) {
		    e.preventDefault();
		    var submit = true;
		    if (!validator.checkAll($(this))) {
		        submit = false;
		    }

		    if (submit)
		        this.submit();
		    return false;
		});
	</script>
    <script src="Scripts/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script src="Scripts/jquery.dynDateTime.min.js" type="text/javascript"></script>
    <script src="Scripts/calendar-en.min.js" type="text/javascript"></script>
    <link   href="Styles/calendar-blue.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%=txtactivedate.ClientID %>").dynDateTime({
                showsTime: true,
                ifFormat: "%Y/%m/%d %H:%M",
                daFormat: "%l;%M %p, %e %m,  %Y",
                align: "BR",
                electric: false,
                singleClick: false,
                displayArea: ".siblings('.dtcDisplayArea')",
                button: ".next()"
            });
        });
    </script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script type="text/javascript" src="Scripts/jquery.blockUI.js"></script>
    <script type="text/javascript">
        $(function () {
            BlockUI("userActions1");
            $.blockUI.defaults.css = {};
        });
        $(function () {
            BlockUI("backCheck1");
            $.blockUI.defaults.css = {};
        });
        $(function () {
            BlockUI("backCheck2");
            $.blockUI.defaults.css = {};
        });
        function BlockUI(elementID) {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_beginRequest(function () {
                $("#" + elementID).block({ message: '<div align = "center">' + '<img src="Images/loadingAnim.gif"/></div>',
                    css: {},
                    overlayCSS: { backgroundColor: '<%=ConfigurationManager.AppSettings.Get("color")%>', opacity: 0.6, border: '3px solid #63B2EB' }
                });
            });
            prm.add_endRequest(function () {
                $("#" + elementID).unblock();
            });
        };
    </script>
</body>
</html>


