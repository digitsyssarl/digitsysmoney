﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WalletToBank.aspx.cs" Inherits="OMBanking.Web.Pages.Views.WalletToBank" Async="true" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>
        M-BANKING W2B
    </title>
    <link rel="stylesheet" href="form.css" type="text/css" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round"/>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
    <style>
        body {
            color: #566787;
            background: #f5f5f5;
            font-family: 'Varela Round', sans-serif;
            font-size: 13px;
        }
        .table-responsive {
            margin: 30px 0;
        }
        .table-wrapper {
            min-width: 1000px;
            background: #fff;
            padding: 20px 25px;
            border-radius: 3px;
            box-shadow: 0 1px 1px rgba(0,0,0,.05);
        }
        .table-title {
            padding-bottom: 15px;
            background: #228B22;
            color: #fff;
            padding: 16px 30px;
            margin: -20px -25px 10px;
            border-radius: 3px 3px 0 0;
        }
        .table-title h2 {
            margin: 5px 0 0;
            font-size: 24px;
        }
        .table-title .btn {
            color: #566787;
            float: right;
            font-size: 13px;
            background: #fff;
            border: none;
            min-width: 50px;
            border-radius: 2px;
            border: none;
            outline: none !important;
            margin-left: 10px;
        }
        .table-title .btn:hover, .table-title .btn:focus {
            color: #566787;
            background: #f2f2f2;
        }
        .table-title .btn i {
            float: left;
            font-size: 21px;
            margin-right: 5px;
        }
        .table-title .btn span {
            float: left;
            margin-top: 2px;
        }
        table.table tr th, table.table tr td {
            border-color: #e9e9e9;
            padding: 12px 15px;
            vertical-align: middle;
        }
        table.table tr th:first-child {
            width: 60px;
        }
        table.table tr th:last-child {
            width: 100px;
        }
        table.table-striped tbody tr:nth-of-type(odd) {
            background-color: #fcfcfc;
        }
        table.table-striped.table-hover tbody tr:hover {
            background: #f5f5f5;
        }
        table.table th i {
            font-size: 13px;
            margin: 0 5px;
            cursor: pointer;
        }	
        table.table td:last-child i {
            opacity: 0.9;
            font-size: 22px;
            margin: 0 5px;
        }
        table.table td a {
            font-weight: bold;
            color: #566787;
            display: inline-block;
            text-decoration: none;
        }
        table.table td a:hover {
            color: #2196F3;
        }
        table.table td a.settings {
            color: #2196F3;
        }
        table.table td a.delete {
            color: #F44336;
        }
        table.table td i {
            font-size: 19px;
        }
        table.table .avatar {
            border-radius: 50%;
            vertical-align: middle;
            margin-right: 10px;
        }
        .status {
            font-size: 30px;
            margin: 2px 2px 0 0;
            display: inline-block;
            vertical-align: middle;
            line-height: 10px;
        }
        .text-success {
            color: #10c469;
        }
        .text-info {
            color: #62c9e8;
        }
        .text-warning {
            color: #FFC107;
        }
        .text-danger {
            color: #ff5b5b;
        }
        .pagination {
            float: right;
            margin: 0 0 5px;
        }
        .pagination li a {
            border: none;
            font-size: 13px;
            min-width: 30px;
            min-height: 30px;
            color: #999;
            margin: 0 2px;
            line-height: 30px;
            border-radius: 2px !important;
            text-align: center;
            padding: 0 6px;
        }
        .pagination li a:hover {
            color: #666;
        }	
        .pagination li.active a, .pagination li.active a.page-link {
            background: #03A9F4;
        }
        .pagination li.active a:hover {        
            background: #0397d6;
        }
        .pagination li.disabled i {
            color: #ccc;
        }
        .pagination li i {
            font-size: 16px;
            padding-top: 6px
        }
        .hint-text {
            float: left;
            margin-top: 10px;
            font-size: 13px;
        }
        span
        {
            color:black;
        }
        #btnTransfer {
          padding: 5px 10px;
          border: none;
          border-radius: 4px;
          cursor: pointer;
          float: left;
        }
</style>
<script type="text/javascript">
    window.onload = function () {
        var hColl = document.getElementsByClassName('scope');
        let color = '<%=ConfigurationManager.AppSettings.Get("color").ToLower()%>';
        changeColor(hColl, color, 'scope');
        hColl = document.getElementsByClassName('table-title');
        changeColor(hColl, color, 'table-title');
        let role = '<%=((Request.QueryString["role"] != null) ? Request.QueryString["role"].ToString() : "")%>';
        hideBtn(role, color);
    }
    function changeColor(coll, color, cls) {

        for (var i = 0, len = coll.length; i < len; i++) {
            if (cls == 'scope') {
                coll[i].style['color'] = color;
            }
            else if (cls == 'table-title') {
                coll[i].style['background-color'] = color;
                coll[i].style['color'] = '#FFFFFF';
            }
            else {
                coll[i].style['background-color'] = color;
                coll[i].style['border-color'] = color;
                coll[i].style['color'] = '#FFFFFF';
            }
        }
    }

    function hideIf(id, role) {
        var obj = document.getElementById(id);
        if (role === "")
            obj.style['display'] = 'none';
    }

    function hideBtn(role, color) {
        var btnId = document.getElementById('btnTransfer');
        if (role === "") {
            btnId.style['background-color'] = '#A9A9A9';
            btnId.style['color'] = 'white';
        }
        else
        {
            btnId.style['background-color'] = color;
            btnId.style['color'] = 'white';
        }
    }
</script>
</head>
<body>
    <div id='wrap'>
        <br />
        <h1 class="scope" style="font-size:xx-large;">Wallet To Bank</h1>
        <div><br /><br /></div>
	    <section class='form'>
		    <form id="form1" runat="server">
			    <fieldset id="wallet">
				    <div class="item">
                        <label>
						    <span>Account No</span>
                            <asp:TextBox id="txtaccount" placeholder="e.g. 233457432134" runat="server" CssClass="required"></asp:TextBox>	
					    </label>
				    </div>
			    </fieldset>
                <fieldset id="amount">
				    <div class="item">
                        <label>
						    <span>Amount</span>
                            <asp:TextBox id="txtamount" placeholder="e.g. 2334" runat="server" CssClass="required"></asp:TextBox>	
                            <span><%=(" " + ConfigurationManager.AppSettings.Get("currencyCode"))%></span>
					    </label>
				    </div>
			    </fieldset>
                <fieldset>
                    <div class="item">
                        <label>
                            <asp:Button id="btnTransfer" runat="server" onclick="btnTransfer_Click"  Text="Transfer" style="width:105px;"/>
                        </label>
                        <label style="width: 210px;">
                        </label>
                        <label>
                            <asp:Button ID="btnRefresh" runat="server" onclick="btnRefresh_Click" Text="Refresh" style="width:99px" />
                        </label>
                        <label style="width: 225px;">
                        </label>
                        <label>
							<asp:Button ID="btnCancel" runat="server" onclick="btnCancel_Click" Text="Cancel" style="width:99px" />
						</label>
					</div>
				</fieldset>
                <br /><br /> 
                <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"/>
                <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>  
				<fieldset>
                    <asp:Label id="response" runat="server" ForeColor="Green"></asp:Label>
                </fieldset>
		    </form>	
	    </section>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="http://dropthebit.com/demos/validator/multifield.js"></script>
    <script src="http://dropthebit.com/demos/validator/validator.js"></script>
	<script>
		// initialize the validator function
		validator.message['date'] = 'not a real date';

	    $('form')
			.on('blur', 'input[required], input.optional, select.required', validator.checkField)
			.on('change', 'select.required', validator.checkField);
			
		$('.multi.required')
			.on('keyup', 'input', function(){ 
				validator.checkField.apply( $(this).siblings().last()[0] );
			}).on('blur', 'input', function(){ 
				validator.checkField.apply( $(this).siblings().last()[0] );
			});
		$('form').submit(function(e){
			e.preventDefault();
			var submit = true;
			if( !validator.checkAll( $(this) ) ){
				submit = false;
			}

			if( submit )
				this.submit();
			return false;
		});
	</script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script type="text/javascript" src="Scripts/jquery.blockUI.js"></script>
</body>
</html>

