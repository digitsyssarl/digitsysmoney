﻿using OMBanking.Web.B2W.Mtn;
using OMBanking.Web.Bank;
using OMBanking.Web.DB;
using OMBanking.Web.Proxy.Bank;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OMBanking.Web.Pages.Views
{
    public partial class WalletToBank : Page
    {
        private const String SESSION_ID = "sid";
        private const String USER_ID = "userId";
        private const String BRANCH_ID = "branchId";
        private const String GSM_OP_ID = "gsmOpId";
        private const String ROLE_ID = "role";

        protected async void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                CacheSessionParams();
                txtaccount.Text = await new USSDHelper()
                                        .GetAccountIDByPhoneNo((string)Session[USER_ID]);
                Momo m = new Momo();
                bool IsActiveOrRegistered = await m.IsRegisteredAndActive((string)Session[USER_ID], Utils.AccessToken);
                if (!IsActiveOrRegistered)
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Info",
                    "alert('Your phone number is not active or registered on the MTN patner."
                    + " Please, go to an MTN agency to activate your phone number!');", true);
                    btnTransfer.Enabled = false;
                    txtaccount.Enabled = false;
                    txtamount.Enabled = false;
                }
            }
        }

        private void CacheSessionParams()
        {
            NameValueCollection SessionParams = Request.QueryString;
            Session[USER_ID] = (SessionParams[USER_ID] != null)
                                ? Utils.Base64Decode(SessionParams[USER_ID].ToString())
                                : "";
            Session[BRANCH_ID] = (SessionParams[BRANCH_ID] != null)
                                 ? Utils.Base64Decode(SessionParams[BRANCH_ID].ToString())
                                 : "";
            Session[GSM_OP_ID] = (SessionParams[GSM_OP_ID] != null)
                                 ? Utils.Base64Decode(SessionParams[GSM_OP_ID].ToString())
                                 : "";
            Session[ROLE_ID] = (SessionParams[ROLE_ID] != null)
                                ? Utils.Base64Decode(SessionParams[ROLE_ID].ToString())
                                : "";
            if ((string)Session[USER_ID] == ""
                || (string)Session[BRANCH_ID] == ""
                || (string)Session[GSM_OP_ID] == ""
                || (string)Session[ROLE_ID] == "")
            {
                Response.Redirect("~/customer.aspx", false);
            }
        }

        protected async void btnTransfer_Click(object sender, EventArgs e)
        {
            if (txtamount.Text != null && txtamount.Text != "")
            {
                double amount = double.Parse(txtamount.Text);
                if (txtaccount.Text != null && txtaccount.Text != "")
                {
                    btnTransfer.BackColor = Color.DarkGray;
                    btnTransfer.Text = "Wait...";

                    string accountNo = txtaccount.Text;
                    WalletToAccountTransfer req = new WalletToAccountTransfer();
                    req.mmHeaderInfo = new HeaderRequest();
                    req.mmHeaderInfo.requestId = accountNo;
                    req.mmHeaderInfo.operatorCode = ConfigurationManager.AppSettings.Get("operatorCode");
                    req.mmHeaderInfo.affiliateCode = ConfigurationManager.AppSettings.Get("affiliate");
                    req.accountNo = accountNo;
                    req.accountAlias = ConfigurationManager.AppSettings.Get("bic")
                                       + req.accountNo;
                    req.amount = amount;
                    Utils.ReferenceID = Guid.NewGuid().ToString();
                    WalletToAccountTransferResponse resp = await new B2WHelper()
                                                                 .WalletToAccountTransfer(req, "MTN");
                    if (resp.mmHeaderInfo.responseCode.CompareTo("000") == 0)
                    {
                        Momo m = new Momo();
                        TransferRequest rq = new TransferRequest();
                        rq.amount = -amount;
                        rq.currency = ConfigurationManager.AppSettings.Get("currencyCode");
                        rq.payeeMSISDN = await new USSDHelper().GetPhoneNoByAlias(req.accountAlias);
                        rq.externalId = resp.externalRefNo;
                        rq.payeeNote = "Wallet To Bank";
                        rq.referenceId = Utils.ReferenceID;
                        rq.payerMsg = "You have been debited of "
                                      + rq.amount + " " + rq.currency
                                      + " on your Momo account " + (string)Session[USER_ID] + "."
                                      + " From " + ConfigurationManager.AppSettings.Get("bank_name");

                        bool statusCode = await m.TransfertMoney(rq, Utils.AccessToken);
                        if (statusCode)
                        {
                            await ShowW2BResponse(rq);
                            Session["REQUEST"] = rq;
                        }
                        else
                        {
                            ClientScript.RegisterStartupScript(this.GetType(), "Error",
                                        "alert('Transaction failed ! Please, check your Internet connection.');", true);
                        }
                    }

                    btnTransfer.BackColor = Color.FromName(ConfigurationManager.AppSettings.Get("color"));
                    btnTransfer.Text = "Transfer";
                }
            }
        }

        private async Task ShowW2BResponse(TransferRequest rq)
        {
            Momo m = new Momo();
            TransferResponse res = await m.StatusInquiry(rq, Utils.AccessToken);
            if (res.status != null && res.status.CompareTo("SUCCESSFUL") == 0)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "Info",
                        "alert('Sucessfully transferred your money !');", true);
            }

            response.Text = "<table class=\"table table-striped table-bordered table-hover\"><tr>";
            response.Text += "<th>Payee Number</th>";
            response.Text += "<th>Amount</th>";
            response.Text += "<th>Currency</th>";
            response.Text += "<th>Status</th>";
            response.Text += "<th>Transaction ID</th>";
            response.Text += "<th>External ID</th></tr>";
            response.Text += "<tr>";
            response.Text += "<td>" + res.payeeMSISDN + "</td>";
            response.Text += "<td>" + res.amount + "</td>";
            response.Text += "<td>" + res.currency + "</td>";
            response.Text += "<td>" + res.status + "</td>";
            response.Text += "<td>" + res.financialTransactionId + "</td>";
            response.Text += "<td>" + res.externalId + "</td></tr>";
            response.Text += "</table>";
        }

        protected async void btnRefresh_Click(object sender, EventArgs e)
        {
            if (Session["REQUEST"] != null)
                await ShowW2BResponse((TransferRequest)Session["REQUEST"]);
            else
                ClientScript.RegisterStartupScript(this.GetType(), "Info",
                  "alert('Please, execute a transaction before refreshing !');", true);
        }

        protected async void btnCancel_Click(object sender, EventArgs e)
        {
            if(Session["REQUEST"] != null)
            {
                btnCancel.BackColor = Color.DarkGray;
                btnCancel.Text = "Wait...";

                // Cancel Transfer
                CancelTransfer ctReq = new CancelTransfer();
                ctReq.externalRefNo = Utils.ReferenceID;
                ctReq.mmHeaderInfo = new HeaderRequest();
                ctReq.mmHeaderInfo.requestId = txtaccount.Text;
                ctReq.mmHeaderInfo.operatorCode = ConfigurationManager.AppSettings.Get("operatorCode");
                ctReq.mmHeaderInfo.affiliateCode = ConfigurationManager.AppSettings.Get("affiliate");

                CancelTransferResponse ctResp = await new B2WHelper().CancelTransfer(ctReq, (string)Session[GSM_OP_ID]);
                if (ctResp.mmHeaderInfo.responseCode.CompareTo("000") == 0)
                {
                    Momo m = new Momo();
                    TransferRequest rq = new TransferRequest();
                    string accountNo = txtaccount.Text;
                    double amount = double.Parse(txtamount.Text);
                    rq.amount = +amount;
                    rq.currency = ConfigurationManager.AppSettings.Get("currencyCode");
                    string alias = ConfigurationManager.AppSettings.Get("bic")
                                   + accountNo;
                    rq.payeeMSISDN = await new USSDHelper().GetPhoneNoByAlias(alias);
                    rq.externalId = ctResp.externalRefNo;
                    rq.payeeNote = "Cancel Wallet To Bank";
                    rq.referenceId = Utils.ReferenceID;
                    rq.payerMsg = "Your transaction Wallet To Bank"
                                  + " has been cancelled"
                                  + " on your Momo account " + (string)Session[USER_ID] + "."
                                  + " From " + ConfigurationManager.AppSettings.Get("bank_name");

                    bool statusCode = await m.TransfertMoney(rq, Utils.AccessToken);
                    if (statusCode)
                    {
                        TransferResponse res = await m.StatusInquiry(rq, Utils.AccessToken);
                        if (res.status != null && res.status.CompareTo("SUCCESSFUL") == 0)
                        {
                            ClientScript.RegisterStartupScript(this.GetType(), "Info 1",
                              "alert('Your transaction was successfully cancelled !');", true);

                            response.Text = "<table class=\"table table-striped table-bordered table-hover\"><tr>";
                            response.Text += "<th>Payee Number</th>";
                            response.Text += "<th>Amount</th>";
                            response.Text += "<th>Currency</th>";
                            response.Text += "<th>Status</th>";
                            response.Text += "<th>Transaction ID</th>";
                            response.Text += "<th>External ID</th></tr>";
                            response.Text += "<tr>";
                            response.Text += "<td>" + res.payeeMSISDN + "</td>";
                            response.Text += "<td>+" + res.amount + "</td>";
                            response.Text += "<td>" + res.currency + "</td>";
                            response.Text += "<td>Cancelled</td>";
                            response.Text += "<td>" + res.financialTransactionId + "</td>";
                            response.Text += "<td>" + res.externalId + "</td></tr>";
                            response.Text += "</table>";
                        }
                    }
                    // Clear
                    response.Text = "";
                    txtamount.Text = "";
                    txtaccount.Text = "";
                }

                btnCancel.BackColor = Color.FromName(ConfigurationManager.AppSettings.Get("color"));
                btnCancel.Text = "Cancel";
            }
            else
                ClientScript.RegisterStartupScript(this.GetType(), "Info 2",
               "alert('Please, execute a transaction before cancelling !');", true);
        }
    }
}