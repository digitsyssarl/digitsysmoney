﻿using OMBanking.Web.Bank;
using OMBanking.Web.DB;
using OMBanking.Web.Log;
using OMBanking.Web.Proxy.Bank;
using OMBanking.Web.Reporting;
using OMBanking.Web.Security;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OMBanking.Web.Pages.Views
{
    public partial class Supervisors : Page
    {
        private const String SESSION_ID = "sid";
        private const String USER_ID = "userId";
        private const String BRANCH_ID = "branchId";
        private const String GSM_OP_ID = "gsmOpId";
        private const String ROLE_ID = "role";
        private DataTable _Table2;
        private const string PDF_COUNT2 = "PDF_COUNT2";
        private const string EXCEL_COUNT2 = "EXCEL_COUNT2";
        private const string ISENABLED2 = "ISENABLED2";
        private const string DATA2 = "TABLE2";

        protected async void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CacheSessionParams();
                await LoadSupervisors("Consult Supervisor List");
            }
        }

        private void CacheSessionParams()
        {
            if (Session[PDF_COUNT2] == null)
                Session[PDF_COUNT2] = 0;
            if (Session[EXCEL_COUNT2] == null)
                Session[EXCEL_COUNT2] = 0;
            if (Session[ISENABLED2] == null)
                Session[ISENABLED2] = false;

            NameValueCollection SessionParams = Request.QueryString;
            Session[USER_ID] = (SessionParams[USER_ID] != null)
                                ? Utils.Base64Decode(SessionParams[USER_ID].ToString()) : "";
            Session[BRANCH_ID] = (SessionParams[BRANCH_ID] != null)
                                 ? Utils.Base64Decode(SessionParams[BRANCH_ID].ToString()) : "";
            Session[GSM_OP_ID] = (SessionParams[GSM_OP_ID] != null)
                                 ? Utils.Base64Decode(SessionParams[GSM_OP_ID].ToString()) : "";
            Session[ROLE_ID] = (SessionParams[ROLE_ID] != null)
                                ? Utils.Base64Decode(SessionParams[ROLE_ID].ToString()) : "";

            if ((string)Session[USER_ID] == ""
                || (string)Session[BRANCH_ID] == ""
                || (string)Session[GSM_OP_ID] == ""
                || (string)Session[ROLE_ID] == "")
            {
                Response.Redirect("~/Pages/Views/Login.aspx", false);
            }
            else
            {
                string Role = (string)Session[ROLE_ID];
                if (Role.CompareTo("Admin") != 0)
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Info",
                        "alert('You are not allowed to access this menu (Role01).');", true);
                    Response.Redirect("~/Pages/Views/Loader.aspx", false);
                }
            }
        }

        public override void VerifyRenderingInServerForm(Control control)
        {

        }

        protected async void btnToPDF2_Click(object sender, EventArgs e)
        {
            if ((bool)Session[ISENABLED2])
            {
                String FileName = "SupervisorList" + Session[PDF_COUNT2] + "-"
                                  + DateTime.Today.ToString().Split(new char[] { ' ' })[0]
                                            .Replace("/", "-") + ".pdf";
                PDFReport report = (PDFReport)ReportFactory.create(ReportFactory.ReportType.PDF);
                string path = Server.MapPath(@"~/Pages/Views/Store/" + FileName);
                string dir = Server.MapPath(@"~/Pages/Views/Store/");
                string imgPath = Server.MapPath(@"~/Pages/Views/Images/Bank.jpg");
                try
                {
                    report.generatePDF((DataTable)Session[DATA2], "Supervisor List", path, imgPath, dir);
                    Session[PDF_COUNT2] = (int)Session[PDF_COUNT2] + 1;
                    await StoreFootPrint("Export Supervisor List to PDF");
                    ClientScript.RegisterStartupScript(this.GetType(), "Info",
                        "alert('Sucessfully generated PDF file !');", true);
                    // load PDF
                    string viewPage = "Store/" + FileName;
                    Response.Redirect("~/Pages/Views/Viewer.aspx" + "?v=" + viewPage, false);
                }
                catch (IOException)
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Error",
                        "alert('Another PDF file is open. Please close it and try again !');", true);
                }
            }
        }

        protected async void btnToExcel2_Click(object sender, EventArgs e)
        {
            if ((bool)Session[ISENABLED2])
            {
                String FileName = "SupervisorList" + Session[EXCEL_COUNT2] + "-"
                                  + DateTime.Today.ToString().Split(new char[] { ' ' })[0]
                                            .Replace("/", "-") + ".xls";
                ExcelReport report = (ExcelReport)ReportFactory.create(ReportFactory.ReportType.EXCEL);
                string path = Server.MapPath(@"~/Pages/Views/Store/" + FileName);
                string dir = Server.MapPath(@"~/Pages/Views/Store/");
                string imgPath = Server.MapPath(@"~/Pages/Views/Images/Bank.jpg");
                try
                {
                    report.generateExcel((DataTable)Session[DATA2], "Supervisor List", path, imgPath, dir);
                    Session[EXCEL_COUNT2] = (int)Session[EXCEL_COUNT2] + 1;
                    await StoreFootPrint("Export Supervisor List to Excel");
                    ClientScript.RegisterStartupScript(this.GetType(), "Info",
                        "alert('Sucessfully generated Excel file !');", true);
                    startExcelFile(path);
                }
                catch (IOException)
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Error",
                        "alert('Another Excel file is open. Please close it and try again !');", true);
                }
            }
        }

        private void startExcelFile(string path)
        {
            FileInfo file = new FileInfo(path);
            Response.Clear();
            Response.Charset = "UTF-8";
            Response.ContentEncoding = System.Text.Encoding.UTF8;
            Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
            Response.AddHeader("Content-Length", file.Length.ToString());
            Response.ContentType = "application/ms-excel";
            Response.WriteFile(file.FullName);
            Response.End();
        }

        protected async void btnAdd_Click(object sender, EventArgs e)
        {
            if ((branchID.Text != "" && branchID.Text.Length == 2)
                && isValidName(name.Text))
            {
                USSDHelper helper = new USSDHelper();
                Supervisor sup = new Supervisor();
                sup.BranchID = branchID.Text;
                sup.Name = name.Text;
                sup.Id = Guid.NewGuid();
                bool result = await helper.AddSupervisor(sup);
                if (!result)
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Error",
                        "alert('Adding supervisor " + sup.Name + " failed !');", true);
                    ServiceLogger _Logger = (ServiceLogger)LogFactory.create(Logger.LoggingMode.SERVICE);
                    _Logger.Log(TraceLevel.Error, "Adding supervisor Failed !");
                }
                else
                {
                    await LoadSupervisors("Add Supervisor #" + sup.Name);
                    ClientScript.RegisterStartupScript(this.GetType(), "Info",
                        "alert('Sucessfully created supervisor " + sup.Name + "');", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "Error",
                    "alert('Invalid supervisor name !');", true);
            }
        }

        bool isValidName(string userId)
        {
            return userId != null && userId != ""
                    && !userId.Contains('"') && !userId.Contains('\'')
                    && !userId.Contains('\\') && !userId.Contains('/')
                    && !userId.Contains('~') && !userId.Contains('%')
                    && !userId.Contains('#') && !userId.Contains('@')
                    && !userId.Contains('&') && !userId.Contains('*')
                    && !userId.Contains('!') && !userId.Contains('^')
                    && !userId.Contains('$') && !userId.Contains(',')
                    && !userId.Contains(';') && !userId.Contains('(')
                    && !userId.Contains(')') && !userId.Contains('{')
                    && !userId.Contains('}') && !userId.Contains('[')
                    && !userId.Contains(']') && !userId.Contains('`')
                    && !userId.Contains('?') && !userId.Contains('<')
                    && !userId.Contains('>') && !userId.Contains(':')
                    && !userId.Contains('-') && !userId.Contains('_')
                    && !userId.Contains('=') && !userId.Contains('+')
                    && !userId.Contains('|');
        }

        private async Task LoadSupervisors(string action)
        {
            supervisorsTable.Visible = true;
            _Table2 = await new USSDHelper().Supervisors();
            if (_Table2.Rows.Count > 0)
            {
                supervisorsTable.DataSource = _Table2;
                supervisorsTable.DataBind();
                Session[ISENABLED2] = true;
                Session[DATA2] = _Table2;
                await StoreFootPrint(action);
            }
        }

        private async Task StoreFootPrint(string action)
        {
            USSDHelper helper = new USSDHelper();
            FootPrint fp = new FootPrint();
            fp.Id = Guid.NewGuid();
            fp.UserAction = action;
            fp.AccessedBy = Session[USER_ID].ToString();
            fp.AccessedOn = DateTime.Now;
            fp.BranchID = Session[BRANCH_ID].ToString();
            fp.GSMOperationID = Session[GSM_OP_ID].ToString();
            fp.Role = Session[ROLE_ID].ToString();
            bool result = await helper.AddFootPrint(fp);
            if (!result)
            {
                ServiceLogger _Logger = (ServiceLogger)LogFactory.create(Logger.LoggingMode.SERVICE);
                _Logger.Log(TraceLevel.Error, "Adding FootPrint Failed !");
            }
        }

        protected async void supervisorsTable_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            string Name = supervisorsTable.DataKeys[e.RowIndex].Value.ToString();
            USSDHelper helper = new USSDHelper();
            bool result = await helper.DeleteSupervisor(Name);
            if (!result)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "Error",
                            "alert('Deleting supervisor " + Name + " failed !');", true);
                ServiceLogger _Logger = (ServiceLogger)LogFactory.create(Logger.LoggingMode.SERVICE);
                _Logger.Log(TraceLevel.Error, "Deleting Supervisor Failed !");
            }
            else
            {
                await LoadSupervisors("Delete Supervisor #" + Name);
                ClientScript.RegisterStartupScript(this.GetType(), "Info",
                            "alert('Sucessfully deleted supervisor " + Name + "');", true);
            }
        }

        protected async void supervisorsTable_RowEditing(object sender, GridViewEditEventArgs e)
        {
            supervisorsTable.EditIndex = e.NewEditIndex;
            string Name = supervisorsTable.DataKeys[e.NewEditIndex].Value.ToString();
            await LoadSupervisors("Edit Supervisor #" + Name);
        }

        protected async void supervisorsTable_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            supervisorsTable.EditIndex = -1;
            string Name = supervisorsTable.DataKeys[e.RowIndex].Value.ToString();
            await LoadSupervisors("Cancel Change #" + Name);
        }

        protected async void supervisorsTable_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            supervisorsTable.EditIndex = -1;
            Supervisor sp = new Supervisor();
            sp.Id = Guid.Parse(((TextBox)supervisorsTable.Rows[e.RowIndex].FindControl("supId")).Text);
            sp.BranchID = ((TextBox)supervisorsTable.Rows[e.RowIndex].FindControl("txtbranchid1")).Text;
            sp.Name = ((TextBox)supervisorsTable.Rows[e.RowIndex].FindControl("txtname")).Text;

            USSDHelper helper = new USSDHelper();
            bool result = await helper.UpdateSupervisor(sp);
            if (!result)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "Error",
                            "alert('Updating supervisor " + sp.Name + " failed !');", true);
                ServiceLogger _Logger = (ServiceLogger)LogFactory.create(Logger.LoggingMode.SERVICE);
                _Logger.Log(TraceLevel.Error, "Updating Supervisor Failed !");
            }
            else
            {
                await LoadSupervisors("Update Supervisor #" + sp.Name);
                ClientScript.RegisterStartupScript(this.GetType(), "Info",
                    "alert('Sucessfully updated supervisor " + sp.Name + "');", true);
            }
        }

        protected void supervisorsTable_PageIndexChanging(Object sender, GridViewPageEventArgs e)
        {
            supervisorsTable.DataSource = (DataTable)Session[DATA2];
            supervisorsTable.PageIndex = e.NewPageIndex;
            supervisorsTable.DataBind();
        }

        protected void supervisorsTable_Sorting(object sender, GridViewSortEventArgs e)
        {
            bool state = (Session["State"] != null)
                          ? ((string)Session["State"]).CompareTo("Select") == 0
                          : false;
            if (!state)
            {
                DataTable dataTable = (DataTable)Session[DATA2];
                if (dataTable != null)
                {
                    string sortPattern = BuildSortPattern("Sort", e.SortDirection, e.SortExpression);
                    dataTable.DefaultView.Sort = sortPattern;
                    Session[DATA2] = dataTable;
                    supervisorsTable.DataSource = dataTable;
                    supervisorsTable.DataBind();
                }
            }
            else
            {
                DataTable dt = (DataTable)Session[DATA2];
                int size = dt.Columns.Count;
                string columnName = e.SortExpression;
                for (int i = 0; i < size; i++)
                {
                    string column = dt.Columns[i].ColumnName;
                    if (column.Contains(columnName))
                    {
                        dt.Columns[i].ColumnName = column.Replace("\u2714 ", "");
                        break;
                    }
                }
                Session[DATA2] = dt;
                supervisorsTable.DataSource = (DataTable)Session[DATA2];
                supervisorsTable.DataBind();
            }
        }

        private string BuildSortPattern(string cacheKey, SortDirection sortDirection,
                                string sortExpression)
        {
            string dir = ConvertSortDirection(sortDirection),
                   sortPattern = (Session[cacheKey] != null)
                                  ? (string)Session[cacheKey] : "";
            if (sortPattern == "")
            {
                sortPattern = sortExpression + " " + dir;
                Session[cacheKey] = sortPattern;
            }
            else
            {
                if (sortPattern.Contains(sortExpression))
                {
                    sortPattern = sortPattern.Replace(" desc", " ")
                                             .Replace(" asc", " ");
                    sortPattern = sortPattern + dir;
                }
                else
                {
                    sortPattern = sortPattern.Replace(" desc", ",")
                                             .Replace(" asc", ",");
                    sortPattern = sortPattern + sortExpression + " " + dir;
                }
            }

            return sortPattern;
        }

        private string ConvertSortDirection(SortDirection sortDirection)
        {
            string newSortDirection = String.Empty;

            switch (sortDirection)
            {
                case SortDirection.Ascending:
                    newSortDirection = "asc";
                    break;

                case SortDirection.Descending:
                    newSortDirection = "desc";
                    break;
            }

            return newSortDirection;
        }
    }
}