﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Viewer.aspx.cs" Inherits="OMBanking.Web.Pages.Views.Viewer" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript">
        window.onload = function()
        {
            let path = '<%=((Request.QueryString["v"] != null) ? Request.QueryString["v"].ToString() :"http://get.adobe.com/reader/")%>';
            document.getElementById("pdfviewer").setAttribute("data", path);
            document.getElementById("link").setAttribute("href", path);
            document.getElementById("pdfviewer").setAttribute("width", window.innerWidth + "");
            document.getElementById("pdfviewer").setAttribute("height", window.innerHeight + "");
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <embed id="pdfviewer" src="http://get.adobe.com/reader/" width="100%" height="100%" alt="pdf" pluginspage="http://www.adobe.com/products/acrobat/readstep2.html"/>
        This browser does not support PDFs. You can download from <a id="link" href = "http://get.adobe.com/reader/">here</a>
        or install <a target = "_blank" href = "http://get.adobe.com/reader/">Adobe PDF Reader</a> to view the file.
    </div>
    </form>
</body>
</html>
