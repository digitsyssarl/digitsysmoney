﻿using OMBanking.Web.Bank;
using OMBanking.Web.DB;
using OMBanking.Web.Security;
using System;
using System.Drawing;
using System.Linq;
using System.Web.UI;
using System.Collections.Specialized;
using OMBanking.Web.Proxy.Bank;
using OMBanking.Web.B2W.Mtn;

namespace OMBanking.Web.Pages.Views
{
    public partial class Login : Page
    {
        private const int LOGIN_TIMEOUT = 5;
        private const String SESSION_ID = "sid";
        private const String EMAIL_ID = "email";
        private const String LOGIN_ID = "lid";
        private const String TIMEOUT = "timeout";
        private const String DECODE_EMAIL = "decode_email";

        protected async void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                NameValueCollection SessionParams = Request.QueryString;
                string SessionID = (SessionParams[SESSION_ID] != null)
                                    ? SessionParams[SESSION_ID].ToString() : "",
                       ValidatedEmail = (SessionParams[EMAIL_ID] != null)
                                         ? SessionParams[EMAIL_ID].ToString() : "",
                       LoginID = (SessionParams[LOGIN_ID] != null) ? SessionParams[LOGIN_ID] : "";

                Session[SESSION_ID] = "";
                Session[TIMEOUT] = 0;
                Session[LOGIN_ID] = Utils.Base64Decode(LoginID);
                if (ValidatedEmail != "")
                {
                    Session[DECODE_EMAIL] = Utils.Base64Decode(ValidatedEmail);
                    USSDHelper helper = new USSDHelper();
                    string UserSessionID = DataEncryptor.EncryptString(
                                            await helper.GetSessionID(Session[DECODE_EMAIL].ToString()));
                    Session[SESSION_ID] = UserSessionID;
                    txtConfirm.ForeColor = Color.Green;
                    txtConfirm.Text = "<center>Account " + Session[DECODE_EMAIL].ToString() + " is successfully actived !</center>";
                }
            }
        }

        protected async void btnSignIn_Click(object sender, EventArgs e)
        {
            string userId = userID.Text,
                   pass   = pwd.Text,
                   role   = roleList.SelectedValue;

            if (isValidUserPassword(userId, pass))
            {
                UserProfile user = new UserProfile();
                user.UserID = userId; 
                user.Password = DataEncryptor.EncryptString(pass);
                string Id = (string)Session[LOGIN_ID];
                if (Id.CompareTo("customer") != 0)
                    user.Role = role;
                else
                    user.Role = "Client";

                user.BranchID = branchIDList.SelectedValue;
                user.GSMOperationID = gsmOperatorIDList.SelectedValue;
                bool UserExists = await new USSDHelper().UserExists(user);
                if (UserExists)
                {
                    bool EmptySID = Session[SESSION_ID].ToString() == "";
                    if (EmptySID)
                    {
                        Session[SESSION_ID] = DataEncryptor.EncryptString(await new USSDHelper().GetSessionID(user.UserID, user.BranchID, user.Role));
                        user.SessionID = Session[SESSION_ID].ToString();
                    }

                    user.LogInDate = DateTime.Now;
                    await new USSDHelper().UpdateUser(user, true);
                    if (Id.CompareTo("customer") != 0)
                    {
                        Utils.AccessToken = await Utils.GetAccessToken();
                        Response.Redirect("~/Pages/Views/Employee.aspx?sid=" + Session[SESSION_ID].ToString()
                                        + "&userId=" + Utils.Base64Encode(user.UserID)
                                        + "&branchId=" + Utils.Base64Encode(user.BranchID)
                                        + "&gsmOpId=" + Utils.Base64Encode(user.GSMOperationID)
                                        + "&role=" + Utils.Base64Encode(user.Role), false);
                    }
                    else
                    {
                        bool yes = await new USSDHelper().HasSubscribed(user.UserID);
                        if (yes)
                        {
                            Utils.AccessToken = await Utils.GetAccessToken();
                            Response.Redirect("~/Pages/Views/Customer.aspx?sid=" + Session[SESSION_ID].ToString()
                                            + "&userId=" + Utils.Base64Encode(user.UserID)
                                            + "&branchId=" + Utils.Base64Encode(user.BranchID)
                                            + "&gsmOpId=" + Utils.Base64Encode(user.GSMOperationID)
                                            + "&role=" + Utils.Base64Encode(user.Role), false);
                        }
                        else
                        {
                            txtConfirm.ForeColor = Color.Red;
                            txtConfirm.Text = "<center>You have not subscribed for the Bank to Wallet service!"
                                              + "Please contact UNICS PLC.</center>";
                        }
                    }
                }
                else
                {
                    txtConfirm.ForeColor = Color.Red;
                    txtConfirm.Text = "<center>Account not exists! Retry again.</center>";
                    Session[TIMEOUT] = (int)Session[TIMEOUT] + 1;
                }
            }
            else
            {
                txtConfirm.ForeColor = Color.Red;
                txtConfirm.Text = "<center>Invalid User ID or Password (min. 8 characters)</center>";
                Session[TIMEOUT] = (int)Session[TIMEOUT] + 1;
            }

            if ((int)Session[TIMEOUT] == LOGIN_TIMEOUT)
            {
                Response.Redirect("~/Pages/Views/SignUp.aspx", false);
            }
        }

        bool isValidUserPassword(string userId, string pass)
        {
            return  userId != null && userId != ""
                    && !userId.Contains('"') && !userId.Contains('\'')
                    && !userId.Contains('\\') && !userId.Contains('/')
                    && !userId.Contains('~') && !userId.Contains('%')
                    && !userId.Contains('#') && !userId.Contains('@')
                    && !userId.Contains('&') && !userId.Contains('*')
                    && !userId.Contains('!') && !userId.Contains('^')
                    && !userId.Contains('$') && !userId.Contains(',')
                    && !userId.Contains(';') && !userId.Contains('(')
                    && !userId.Contains(')') && !userId.Contains('{')
                    && !userId.Contains('}') && !userId.Contains('[')
                    && !userId.Contains(']') && !userId.Contains('`')
                    && !userId.Contains('?') && !userId.Contains('<')
                    && !userId.Contains('>') && !userId.Contains(':')
                    && !userId.Contains('-') && !userId.Contains('_')
                    && !userId.Contains('=') && !userId.Contains('+')
                    && !userId.Contains('|')
                    && pass != null && pass != ""
                    && pass.Length >= 8;
        }
    }
}