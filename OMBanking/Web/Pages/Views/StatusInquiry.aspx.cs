﻿using OMBanking.Web.B2W.Orange.Register;
using OMBanking.Web.B2W.Orange.StatusInquiry;
using OMBanking.Web.Pages.Controller;
using OMBanking.Web.Proxy.B2W.StatusInquiry;
using System;
using System.Data;
using System.IO;
using System.Collections.Specialized;
using OMBanking.Web.DB;
using OMBanking.Web.Security;
using OMBanking.Web.Reporting;
using System.Collections.Generic;
using OMBanking.Web.Proxy.Bank;
using OMBanking.Web.Log;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Configuration;
using OMBanking.Web.B2W.Mtn;

namespace OMBanking.Web.Pages.Views
{
    public partial class StatusInquiry : System.Web.UI.Page
    {
        private TransactionStatusInquiryImplBinding _StatusInquiryBinding;
        private DataTable _Table;
        private DataTable _Table1;

        private string TRX_ID = "Trx_ID";
        private const string PDF_COUNT = "PDF_COUNT";
        private const string PDF_COUNT1 = "PDF_COUNT1";
        private const string EXCEL_COUNT = "EXCEL_COUNT";
        private const string EXCEL_COUNT1 = "EXCEL_COUNT1";
        private const string ISENABLED = "ISENABLED";
        private const string ISENABLED1 = "ISENABLED1";
        private const string DATA = "TABLE";
        private const string DATA1 = "TABLE1";

        private const String SESSION_ID = "sid";
        private const String USER_ID = "userId";
        private const String BRANCH_ID = "branchId";
        private const String GSM_OP_ID = "gsmOpId";
        private const String ROLE_ID = "role";

        protected async void Page_Load(object sender, EventArgs e)
        {
            _StatusInquiryBinding = new TransactionStatusInquiryImplBinding();
            CacheSessionParams();
            await LoadFootPrintTable("Status Inquiry", true);
        }

        private void CacheSessionParams()
        {
            Session[TRX_ID] = "";
            if (Session[PDF_COUNT] == null)
                Session[PDF_COUNT] = 0;
            if (Session[EXCEL_COUNT] == null)
                Session[EXCEL_COUNT] = 0;
            if (Session[ISENABLED] == null)
                Session[ISENABLED] = false;
            if (Session[PDF_COUNT1] == null)
                Session[PDF_COUNT1] = 0;
            if (Session[EXCEL_COUNT1] == null)
                Session[EXCEL_COUNT1] = 0;
            if (Session[ISENABLED1] == null)
                Session[ISENABLED1] = false;
            NameValueCollection SessionParams = Request.QueryString;
            Session[USER_ID] = (SessionParams[USER_ID] != null)
                                ? Utils.Base64Decode(SessionParams[USER_ID].ToString()) : "";
            Session[BRANCH_ID] = (SessionParams[BRANCH_ID] != null)
                                 ? Utils.Base64Decode(SessionParams[BRANCH_ID].ToString()) : "";
            Session[GSM_OP_ID] = (SessionParams[GSM_OP_ID] != null)
                                 ? Utils.Base64Decode(SessionParams[GSM_OP_ID].ToString()) : "";
            Session[ROLE_ID] = (SessionParams[ROLE_ID] != null)
                                ? Utils.Base64Decode(SessionParams[ROLE_ID].ToString()) : "";
            if ((string)Session[USER_ID] == ""
                || (string)Session[BRANCH_ID] == ""
                || (string)Session[GSM_OP_ID] == ""
                || (string)Session[ROLE_ID] == "")
            {
                Response.Redirect("~/Pages/Views/Login.aspx", false);
            }
            else
            {
                string Role = (string)Session[ROLE_ID];
                if (Role.CompareTo("Agent") == 0
                    || Role.CompareTo("Auditor") == 0)
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Info",
                        "alert('You are not allowed to access this menu (Role01).');", true);
                    Response.Redirect("~/Pages/Views/Loader.aspx", false);
                }
            }
        }

        protected async void btnStatusInquiry_Click(object sender, EventArgs e)
        {
            try
            {
                if (((string)Session[GSM_OP_ID]).CompareTo("ORANGE") == 0)
                {
                    btnStatusInquiry.BackColor = Color.DarkGray;
                    btnStatusInquiry.Text = "Wait...";

                    TransactionStatusInquiry req = CreateTransactionStatusInquiryRequest();
                    if (ddListID.SelectedValue != null && ddListID.SelectedValue.Length != 0)
                    {
                        TransactionStatusInquiryResponse res = await _StatusInquiryBinding.TransactionStatusInquiry(req);
                        Session[TRX_ID] = req.ID;
                        ShowTransactionStatusInquiryResponse(res);
                        await StoreFootPrint("Get Status Inquiry (" + ErrorCodeHandler.Description(int.Parse(res.statusInquiry.returnCode)) + ")");
                        await LoadFootPrintTable("Status Inquiry", false);
                    }

                    btnStatusInquiry.BackColor = Color.FromName(ConfigurationManager.AppSettings.Get("color"));
                    btnStatusInquiry.Text = "Shown";
                }
                else
                {
                    btnStatusInquiry.BackColor = Color.DarkGray;
                    btnStatusInquiry.Text = "Wait...";

                    if (txtid.Text != null && txtid.Text != "")
                    {
                        TransferRequest rq = new TransferRequest();
                        rq.referenceId = txtid.Text;
                        await ShowB2WResponse(rq);
                        await StoreFootPrint("Get Status Inquiry (OK: Validation)");
                        await LoadFootPrintTable("Status Inquiry", false);
                    }

                    btnStatusInquiry.BackColor = Color.FromName(ConfigurationManager.AppSettings.Get("color"));
                    btnStatusInquiry.Text = "Shown";
                }
            }
            catch (Exception)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "Info",
                        "alert('Unable to connect to Orange !');", true);
            }
        }

        private async Task ShowB2WResponse(TransferRequest rq)
        {
            Momo m = new Momo();
            TransferResponse res = await m.StatusInquiry(rq, Utils.AccessToken);
            if (res.status != null && res.status.CompareTo("SUCCESSFUL") == 0)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "Info",
                        "alert('Sucessfully transferred your money !');", true);
            }
            else
            {
                response.Text = "<table class=\"table table-striped table-bordered table-hover\"><tr>";
                response.Text += "<th>Payee Number</th>";
                response.Text += "<th>Amount</th>";
                response.Text += "<th>Currency</th>";
                response.Text += "<th>Status</th>";
                response.Text += "<th>Transaction ID</th>";
                response.Text += "<th>External ID</th></tr>";
                response.Text += "<tr>";
                response.Text += "<td>" + res.payeeMSISDN + "</td>";
                response.Text += "<td>+" + res.amount + "</td>";
                response.Text += "<td>" + res.currency + "</td>";
                response.Text += "<td>" + res.status + "</td>";
                response.Text += "<td>" + res.financialTransactionId + "</td>";
                response.Text += "<td>" + res.externalId + "</td></tr>";
                response.Text += "</table>";
            }
        }

        private TransactionStatusInquiry CreateTransactionStatusInquiryRequest()
        {
            TransactionStatusInquiry req = new TransactionStatusInquiry();

            object typeid = ddListID.SelectedValue;
            req.typeID = ((string)typeid).CompareTo("BANK_ID") == 0 
                             ? TypeID.BANK_ID 
                             : (((string)typeid).CompareTo("ORANGE_ID") == 0 
                                ? TypeID.ORANGE_ID
                                : TypeID.MTN_ID);
            req.ID = txtid.Text;

            return req;
        }

        private void ShowTransactionStatusInquiryResponse(TransactionStatusInquiryResponse res)
        {
            var statusInquiry = res.statusInquiry;
            int return_code = int.Parse(statusInquiry.returnCode);
            if (return_code == 200)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "Info",
                    "alert('Sucessfully checked status inquiry for transaction #" + Session[TRX_ID].ToString() + "');", true);
                response.Text = "<table class=\"table table-striped table-bordered table-hover\"><tr>";
                response.Text += "<th>Global Status</th>";
                response.Text += "<th>ID</th>";
                response.Text += "<th>Bank ID</th>";
                response.Text += "<th>Orange ID</th>";
                response.Text += "<th>Transaction Type</th>";
                response.Text += "<th>Date Time</th>";
                response.Text += "<th>Amount</th></tr>";

                var transaction = statusInquiry.transaction;
                if (transaction != null)
                {
                    response.Text += "<tr>";
                    response.Text += "<td>" + statusInquiry.status.ToString() + "</td>";
                    response.Text += "<td>" + Session[TRX_ID].ToString() + "</td>";
                    response.Text += "<td>" + transaction.bankID + "</td>";
                    response.Text += "<td>" + transaction.orangeID + "</td>";
                    response.Text += "<td>" + transaction.type.ToString() + "</td>";
                    response.Text += "<td>" + transaction.dateTime.ToString() + "</td>";
                    response.Text += "<td>" + transaction.amount.ToString() + "</td>";
                    response.Text += "</tr>";
                }
                response.Text += "</table>";
                var detailedStatuses = statusInquiry.detailedStatuses;
                if (detailedStatuses != null)
                {
                    _Table = new DataTable();
                    _Table.Columns.Add("Detailed Status #", typeof(string));
                    _Table.Columns.Add("Step", typeof(string));
                    _Table.Columns.Add("Step Status", typeof(string));
                    _Table.Columns.Add("Request Date", typeof(DateTime));

                    var counter = 1;
                    foreach (DetailedStatus d in detailedStatuses)
                    {
                        DataRow _Row = _Table.NewRow();
                        _Row["Detailed Status #"] = counter.ToString();
                        _Row["Step"] = d.step.ToString();
                        _Row["Step Status"] = d.status.ToString();
                        _Row["Request Date"] = d.requestDate;
                        _Table.Rows.Add(_Row);
                        counter++;
                    }
                    responseTable1.DataSource = _Table;
                    responseTable1.DataBind();
                    Session[ISENABLED] = true;
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "Error",
                    "alert('Status inquiry failed because transaction #" + Session[TRX_ID].ToString() + " is already processed !');", true);
            }
        }

        protected async void btnToPDF_Click(object sender, EventArgs e)
        {
            if (Session["State"] == null)
            {
                Session[DATA] = EnablePrintingMode(
                                  new string[] { "State", "PDF" },
                                  (DataTable)Session[DATA],
                                  true);
                responseTable1.DataSource = (DataTable)Session[DATA];
                responseTable1.DataBind();
            }
            else
            {
                if ((bool)Session[ISENABLED])
                {
                    String FileName = "StatusInquiryReport" + Session[PDF_COUNT] + "-"
                                      + DateTime.Today.ToString().Split(new char[] { ' ' })[0]
                                                .Replace("/", "-") + ".pdf";
                    PDFReport report = (PDFReport)ReportFactory.create(ReportFactory.ReportType.PDF);
                    string path = Server.MapPath(@"~/Pages/Views/Store/" + FileName);
                    string dir = Server.MapPath(@"~/Pages/Views/Store/");
                    string imgPath = Server.MapPath(@"~/Pages/Views/Images/Bank.jpg");
                    try
                    {
                        DataTable dtNew = PreparePrinting((DataTable)Session[DATA]);
                        // clear selected row
                        Session[DATA] = EnablePrintingMode(
                                         new string[] { "State", "PDF" },
                                         (DataTable)Session[DATA],
                                         false);
                        responseTable1.DataSource = (DataTable)Session[DATA];
                        responseTable1.DataBind();

                        report.generatePDF(dtNew, "Transaction #" + Session[TRX_ID].ToString() + "- Statut Inquiry", path, imgPath, dir);
                        Session[PDF_COUNT] = (int)Session[PDF_COUNT] + 1;
                        await StoreFootPrint("Export Status Inquiry to PDF");
                        await LoadFootPrintTable("Status Inquiry", false);
                        // load PDF
                        OpenPDF(FileName);
                    }
                    catch (IOException)
                    {
                        //ClientScript.RegisterStartupScript(this.GetType(), "Info",
                        //    "alert('Another PDF file is open. Please close it and try again !');", true);
                    }
                }
            }
        }

        protected async void btnToExcel_Click(object sender, EventArgs e)
        {
            if ((bool)Session[ISENABLED])
            {
                String FileName = "StatusInquiryReport" + Session[EXCEL_COUNT] + "-"
                                  + DateTime.Today.ToString().Split(new char[] { ' ' })[0]
                                            .Replace("/", "-") + ".xls";
                ExcelReport report = (ExcelReport)ReportFactory.create(ReportFactory.ReportType.EXCEL);
                string path = Server.MapPath(@"~/Pages/Views/Store/" + FileName);
                string dir = Server.MapPath(@"~/Pages/Views/Store/");
                string imgPath = Server.MapPath(@"~/Pages/Views/Images/Bank.jpg");
                try
                {
                    report.generateExcel((DataTable)Session[DATA], "Transaction #" + Session[TRX_ID].ToString() + "- Statut Inquiry", path, imgPath, dir);
                    Session[EXCEL_COUNT] = (int)Session[EXCEL_COUNT] + 1;
                    await StoreFootPrint("Export Status Inquiry to Excel");
                    await LoadFootPrintTable("Status Inquiry", false);
                    startExcelFile(path);
                }
                catch (IOException)
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Info",
                        "alert('Another PDF file is open. Please close it and try again !');", true);
                }
            }
        }

        protected async void btnToPDF1_Click(object sender, EventArgs e)
        {
            if (Session["State1"] == null)
            {
                Session[DATA1] = EnablePrintingMode(
                                  new string[] { "State1", "PDF1" },
                                  (DataTable)Session[DATA1],
                                  true);
                responseTable.DataSource = (DataTable)Session[DATA1];
                responseTable.DataBind();
            }
            else
            {
                if ((bool)Session[ISENABLED1])
                {
                    String FileName = "UserActionHistory_StatusInquiry" + Session[PDF_COUNT1] + "-"
                                      + DateTime.Today.ToString().Split(new char[] { ' ' })[0]
                                                .Replace("/", "-") + ".pdf";
                    PDFReport report = (PDFReport)ReportFactory.create(ReportFactory.ReportType.PDF);
                    string path = Server.MapPath(@"~/Pages/Views/Store/" + FileName);
                    string dir = Server.MapPath(@"~/Pages/Views/Store/");
                    string imgPath = Server.MapPath(@"~/Pages/Views/Images/Bank.jpg");
                    try
                    {
                        DataTable dtNew = PreparePrinting((DataTable)Session[DATA1]);
                        // clear selected row
                        Session[DATA1] = EnablePrintingMode(
                                         new string[] { "State1", "PDF1" },
                                         (DataTable)Session[DATA1],
                                         false);
                        responseTable.DataSource = (DataTable)Session[DATA1];
                        responseTable.DataBind();

                        report.generatePDF(dtNew, "User Action History - Status Inquiry", path, imgPath, dir);
                        Session[PDF_COUNT1] = (int)Session[PDF_COUNT1] + 1;
                        await StoreFootPrint("Export Status Inquiry Actions to PDF");
                        await LoadFootPrintTable("Status Inquiry", false);
                        // load PDF
                        OpenPDF(FileName);
                    }
                    catch (IOException)
                    {
                        //ClientScript.RegisterStartupScript(this.GetType(), "Info",
                        //    "alert('Another PDF file is open. Please close it and try again !');", true);
                    }
                }
            }
        }

        protected async void btnToExcel1_Click(object sender, EventArgs e)
        {
            if ((bool)Session[ISENABLED1])
            {
                String FileName = "UserActionHistory_StatusInquiry" + Session[EXCEL_COUNT1] + "-"
                                  + DateTime.Today.ToString().Split(new char[] { ' ' })[0]
                                            .Replace("/", "-") + ".xls";
                ExcelReport report = (ExcelReport)ReportFactory.create(ReportFactory.ReportType.EXCEL);
                string path = Server.MapPath(@"~/Pages/Views/Store/" + FileName);
                string dir = Server.MapPath(@"~/Pages/Views/Store/");
                string imgPath = Server.MapPath(@"~/Pages/Views/Images/Bank.jpg");
                try
                {
                    report.generateExcel((DataTable)Session[DATA1], "User Action History - Status Inquiry", path, imgPath, dir);
                    Session[EXCEL_COUNT1] = (int)Session[EXCEL_COUNT1] + 1;
                    await StoreFootPrint("Export Status Inquiry Actions to Excel");
                    await LoadFootPrintTable("Status Inquiry", false);
                    startExcelFile(path);
                }
                catch (IOException)
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Info",
                        "alert('Another Excel file is open. Please close it and try again !');", true);
                }
            }
        }

        private void startExcelFile(string path)
        {
            FileInfo file = new FileInfo(path);
            Response.Clear();
            Response.Charset = "UTF-8";
            Response.ContentEncoding = System.Text.Encoding.UTF8;
            Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
            Response.AddHeader("Content-Length", file.Length.ToString());
            Response.ContentType = "application/ms-excel";
            Response.WriteFile(file.FullName);
            Response.End();
        }

        private void AddColumnHeader()
        {
            _Table1 = new DataTable();
            _Table1.Columns.Add("Role", typeof(string));
            _Table1.Columns.Add("UserAction", typeof(string));
            _Table1.Columns.Add("AccessedBy", typeof(string));
            _Table1.Columns.Add("AccessedOn", typeof(DateTime));
            _Table1.Columns.Add("BranchID", typeof(string));
            _Table1.Columns.Add("GSMOperationID", typeof(string));

        }

        private async Task LoadFootPrintTable(string id, bool start)
        {
            string Role = Session[ROLE_ID].ToString();
            string BranchID = Session[BRANCH_ID].ToString();
            string GSMOpID = Session[GSM_OP_ID].ToString();

            responseTable.Visible = true;
            List<FootPrint> footPrints = await new USSDHelper().FootPrintById(id, Role, BranchID, GSMOpID);
            AddColumnHeader();
            if (footPrints != null)
            {
                FillFootPrintTable(footPrints);
                responseTable.DataSource = _Table1;
                responseTable.DataBind();
                Session[ISENABLED1] = true;
                Session[DATA1] = _Table1;
                //if (start)
                //    await StoreFootPrint("Consult Status Inquiry Actions");
            }
            else
            {
                _Table1.Rows.Add(_Table1.NewRow());
                responseTable.DataSource = _Table1;
                responseTable.DataBind();
                responseTable.Rows[0].Visible = false;
            }
        }

        private void FillFootPrintTable(List<FootPrint> footPrints)
        {
            foreach (FootPrint fp in footPrints)
            {
                DataRow _Row = _Table1.NewRow();
                _Row["Role"] = fp.Role;
                _Row["UserAction"] = fp.UserAction;
                _Row["AccessedBy"] = fp.AccessedBy.ToString();
                _Row["AccessedOn"] = fp.AccessedOn;
                _Row["BranchID"] = fp.BranchID;
                _Row["GSMOperationID"] = fp.GSMOperationID;
                _Table1.Rows.Add(_Row);
            }
        }

        private async Task StoreFootPrint(string action)
        {
            USSDHelper helper = new USSDHelper();
            FootPrint fp = new FootPrint();
            fp.Id = Guid.NewGuid();
            fp.UserAction = action;
            fp.AccessedBy = Session[USER_ID].ToString();
            fp.AccessedOn = DateTime.Now;
            fp.BranchID = Session[BRANCH_ID].ToString();
            fp.GSMOperationID = Session[GSM_OP_ID].ToString();
            fp.Role = Session[ROLE_ID].ToString();
            bool result = await helper.AddFootPrint(fp);
            if (!result)
            {
                ServiceLogger _Logger = (ServiceLogger)LogFactory.create(Logger.LoggingMode.SERVICE);
                _Logger.Log(TraceLevel.Error, "Adding FootPrint Failed !");
            }
        }

        protected void responseTable_PageIndexChanging(Object sender, GridViewPageEventArgs e)
        {
            responseTable.DataSource = (DataTable)Session[DATA1];
            responseTable.PageIndex = e.NewPageIndex;
            responseTable.DataBind();
        }

        private async Task LoadFootPrintTable(string id, bool start, string searchTxt)
        {
            string Role = Session[ROLE_ID].ToString();
            string BranchID = Session[BRANCH_ID].ToString();
            string GSMOpID = Session[GSM_OP_ID].ToString();

            USSDHelper helper = new USSDHelper();
            List<FootPrint> footPrints = await helper.FootPrintById(id, Role, BranchID, GSMOpID);
            AddColumnHeader();
            if (footPrints != null)
            {
                FillFootPrintTable(footPrints, searchTxt);
                responseTable.DataSource = _Table1;
                responseTable.DataBind();
                Session[ISENABLED1] = true;
                Session[DATA1] = _Table1;
            }
            else
            {
                _Table1.Rows.Add(_Table1.NewRow());
                responseTable.DataSource = _Table1;
                responseTable.DataBind();
                responseTable.Rows[0].Visible = false;
            }
        }

        private void FillFootPrintTable(List<FootPrint> footPrints, string searchTxt)
        {
            foreach (FootPrint fp in footPrints)
            {
                if (fp.Role.Contains(searchTxt)
                    || fp.UserAction.Contains(searchTxt)
                    || fp.AccessedBy.ToString().Contains(searchTxt)
                    || fp.AccessedOn.ToString().Contains(searchTxt)
                    || fp.BranchID.Contains(searchTxt)
                    || fp.GSMOperationID.Contains(searchTxt))
                {
                    DataRow _Row = _Table1.NewRow();
                    _Row["Role"] = fp.Role;
                    _Row["UserAction"] = fp.UserAction;
                    _Row["AccessedBy"] = fp.AccessedBy.ToString();
                    _Row["AccessedOn"] = fp.AccessedOn;
                    _Row["BranchID"] = fp.BranchID;
                    _Row["GSMOperationID"] = fp.GSMOperationID;
                    _Table1.Rows.Add(_Row);
                }
            }
        }

        protected async void txtSearch1_TextChanged(object sender, EventArgs e)
        {
            if (txtSearch1.Text != null && txtSearch1.Text != "")
                await LoadFootPrintTable("Status Inquiry", true, txtSearch1.Text);
        }

        protected void responseTable1_Sorting(object sender, GridViewSortEventArgs e)
        {
            bool state = (Session["State"] != null)
                          ? ((string)Session["State"]).CompareTo("Select") == 0
                          : false;
            if (!state)
            {
                DataTable dataTable = (DataTable)Session[DATA];
                if (dataTable != null)
                {
                    string sortPattern = BuildSortPattern("Sort", e.SortDirection, e.SortExpression);
                    dataTable.DefaultView.Sort = sortPattern;
                    Session[DATA] = dataTable;
                    responseTable1.DataSource = dataTable;
                    responseTable1.DataBind();
                }
            }
            else
            {
                DataTable dt = (DataTable)Session[DATA];
                int size = dt.Columns.Count;
                string columnName = e.SortExpression;
                for (int i = 0; i < size; i++)
                {
                    string column = dt.Columns[i].ColumnName;
                    if (column.Contains(columnName))
                    {
                        dt.Columns[i].ColumnName = column.Replace("\u2714 ", "");
                        break;
                    }
                }
                Session[DATA] = dt;
                responseTable1.DataSource = (DataTable)Session[DATA];
                responseTable1.DataBind();
            }
        }

        protected void responseTable_Sorting(object sender, GridViewSortEventArgs e)
        {
            bool state = (Session["State1"] != null)
                          ? ((string)Session["State1"]).CompareTo("Select") == 0
                          : false;
            if (!state)
            {
                DataTable dataTable = (DataTable)Session[DATA1];
                if (dataTable != null)
                {
                    string sortPattern = BuildSortPattern("Sort1", e.SortDirection, e.SortExpression);
                    dataTable.DefaultView.Sort = sortPattern;
                    Session[DATA1] = dataTable;
                    responseTable.DataSource = dataTable;
                    responseTable.DataBind();
                }
            }
            else
            {
                DataTable dt = (DataTable)Session[DATA1];
                int size = dt.Columns.Count;
                string columnName = e.SortExpression;
                for (int i = 0; i < size; i++)
                {
                    string column = dt.Columns[i].ColumnName;
                    if (column.Contains(columnName))
                    {
                        dt.Columns[i].ColumnName = column.Replace("\u2714 ", "");
                        break;
                    }
                }
                Session[DATA1] = dt;
                responseTable.DataSource = (DataTable)Session[DATA1];
                responseTable.DataBind();
            }
        }

        private string BuildSortPattern(string cacheKey, SortDirection sortDirection,
                                        string sortExpression)
        {
            string dir = ConvertSortDirection(sortDirection),
                   sortPattern = (Session[cacheKey] != null)
                                  ? (string)Session[cacheKey] : "";
            if (sortPattern == "")
            {
                sortPattern = sortExpression + " " + dir;
                Session[cacheKey] = sortPattern;
            }
            else
            {
                if (sortPattern.Contains(sortExpression))
                {
                    sortPattern = sortPattern.Replace(" desc", " ")
                                             .Replace(" asc", " ");
                    sortPattern = sortPattern + dir;
                }
                else
                {
                    sortPattern = sortPattern.Replace(" desc", ",")
                                             .Replace(" asc", ",");
                    sortPattern = sortPattern + sortExpression + " " + dir;
                }
            }

            return sortPattern;
        }

        private string ConvertSortDirection(SortDirection sortDirection)
        {
            string newSortDirection = String.Empty;

            switch (sortDirection)
            {
                case SortDirection.Ascending:
                    newSortDirection = "asc";
                    break;

                case SortDirection.Descending:
                    newSortDirection = "desc";
                    break;
            }

            return newSortDirection;
        }

        private void OpenPDF(string FileName)
        {
            string viewPage = Server.MapPath(@"~/Pages/Views/Store/" + FileName);
            FileStream fs = new FileStream(viewPage, System.IO.FileMode.Open, System.IO.FileAccess.Read);
            int fs_len = (int)fs.Length;
            byte[] ar = new byte[fs_len];
            fs.Read(ar, 0, fs_len);
            fs.Close();

            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("Accept-Header", fs_len.ToString());
            Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName + ".pdf");
            Response.AddHeader("Expires", "0");
            Response.AddHeader("Pragma", "cache");
            Response.AddHeader("Cache-Control", "private");
            Response.ContentType = "application/pdf";
            Response.AddHeader("Accept-Ranges", "bytes");
            Response.BinaryWrite(ar);
            Response.Flush();
            try { Response.End(); }
            catch { }
        }

        private DataTable EnablePrintingMode(string[] StateName, DataTable dt, bool state)
        {
            int colCount = dt.Columns.Count;
            for (int i = 0; i < colCount; i++)
            {
                string column = dt.Columns[i].ColumnName;
                if (state)
                    dt.Columns[i].ColumnName = "\u2714 " + column;
                else
                    dt.Columns[i].ColumnName = column.Replace("\u2714 ", "");
            }

            if (state)
            {
                Session[StateName[0]] = "Select";
                Session[StateName[1]] = new System.Web.UI.WebControls.Image
                {
                    ImageUrl = "Images/select.png",
                    Width = 35,
                    Height = 35,
                    BorderColor = Color.Transparent
                };
                if (StateName[0].CompareTo("State") == 0)
                    btnToPDF.Controls.Add((System.Web.UI.WebControls.Image)Session[StateName[1]]);
                else
                    btnToPDF1.Controls.Add((System.Web.UI.WebControls.Image)Session[StateName[1]]);
            }
            else
            {
                Session[StateName[0]] = null;
                if (StateName[0].CompareTo("State") == 0)
                {
                    btnToPDF.Controls.Remove((System.Web.UI.WebControls.Image)Session[StateName[1]]);
                    btnToPDF.Controls.Add(new System.Web.UI.WebControls.Image
                    {
                        ImageUrl = "Images/pdf.png",
                        Width = 35,
                        Height = 35,
                        BorderColor = Color.Transparent
                    });
                }
                else
                {
                    btnToPDF1.Controls.Remove((System.Web.UI.WebControls.Image)Session[StateName[1]]);
                    btnToPDF1.Controls.Add(new System.Web.UI.WebControls.Image
                    {
                        ImageUrl = "Images/pdf.png",
                        Width = 35,
                        Height = 35,
                        BorderColor = Color.Transparent
                    });
                }
            }

            return dt;
        }

        private DataTable PreparePrinting(DataTable dt)
        {
            DataTable dtNew = dt.Copy();
            int colCount = dt.Columns.Count;
            if (colCount > 0)
            {
                for (int i = 0; i < colCount; i++)
                {
                    DataColumn c = dt.Columns[i];
                    if (!c.ColumnName.Contains("\u2714 "))
                        dtNew.Columns.Remove(c.ColumnName);
                }
                int colNewCount = dtNew.Columns.Count;
                for (int i = 0; i < colNewCount; i++)
                {
                    string column = dtNew.Columns[i].ColumnName;
                    if (column.Contains("\u2714 "))
                        dtNew.Columns[i].ColumnName = column.Replace("\u2714 ", "");
                }
            }

            return dtNew;
        }
    }
}