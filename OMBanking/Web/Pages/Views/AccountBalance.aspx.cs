﻿using OMBanking.Web.Bank;
using OMBanking.Web.DB;
using OMBanking.Web.Proxy.Bank;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OMBanking.Web.Pages.Views
{
    public partial class AccountBalance : Page
    {
        private const String SESSION_ID = "sid";
        private const String USER_ID = "userId";
        private const String BRANCH_ID = "branchId";
        private const String GSM_OP_ID = "gsmOpId";
        private const String ROLE_ID = "role";

        protected async void Page_Load(object sender, EventArgs e)
        {
            if(!this.IsPostBack)
            {
                CacheSessionParams();
                await LoadAccountBalance();
            }
        }

        private void CacheSessionParams()
        {
            NameValueCollection SessionParams = Request.QueryString;
            Session[USER_ID] = (SessionParams[USER_ID] != null)
                                ? Utils.Base64Decode(SessionParams[USER_ID].ToString())
                                : "";
            Session[BRANCH_ID] = (SessionParams[BRANCH_ID] != null)
                                 ? Utils.Base64Decode(SessionParams[BRANCH_ID].ToString())
                                 : "";
            Session[GSM_OP_ID] = (SessionParams[GSM_OP_ID] != null)
                                 ? Utils.Base64Decode(SessionParams[GSM_OP_ID].ToString())
                                 : "";
            Session[ROLE_ID] = (SessionParams[ROLE_ID] != null)
                                ? Utils.Base64Decode(SessionParams[ROLE_ID].ToString())
                                : "";
            if ((string)Session[USER_ID] == ""
                || (string)Session[BRANCH_ID] == ""
                || (string)Session[GSM_OP_ID] == ""
                || (string)Session[ROLE_ID] == "")
            {
                Response.Redirect("~/customer.aspx", false);
            }
        }

        private async Task LoadAccountBalance()
        {
            GetAccountBalance bal = new GetAccountBalance();
            bal.mmHeaderInfo = new HeaderRequest();
            bal.mmHeaderInfo.requestId = (string)Session[USER_ID];
            bal.mmHeaderInfo.operatorCode = ConfigurationManager
                                            .AppSettings.Get("operatorCode");
            bal.mmHeaderInfo.affiliateCode = ConfigurationManager
                                             .AppSettings.Get("affiliate");
            bal.accountNo = await new USSDHelper()
                                  .GetAccountIDByPhoneNo((string)Session[USER_ID]);
            bal.accountAlias = ConfigurationManager.AppSettings.Get("bic")
                               + bal.accountNo;
            GetAccountBalanceResponse resp = await new B2WHelper().GetAccountBalance(bal, (string)Session[GSM_OP_ID]);
            txtbalance.Text = resp.currentBalance.ToString()
                              + " "
                              + ConfigurationManager.AppSettings.Get("currencyCode");
        }
    }
}