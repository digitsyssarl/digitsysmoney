﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ResetPassword.aspx.cs" Inherits="OMBanking.Web.Pages.Views.ResetPassword" Async="true"%>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title>B2W | Reset Password</title>
	<meta charset="UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="Images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css"/>
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css"/>
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css"/>
	<link rel="stylesheet" type="text/css" href="css/main.css"/>
<!--===============================================================================================-->
<script type="text/javascript">
    window.onload = function () {
        let color = '<%=ConfigurationManager.AppSettings.Get("color")%>';
        document.querySelector(":root").style.setProperty('--forestgreen', color);
    }
</script>
</head>
<body>
	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<form id="form1" class="login100-form validate-form p-l-55 p-r-55 p-t-178" runat="server">
					<span class="login100-form-title">
                        <img src="Images/Bank.jpg" width="40"/><label style="font-size: 20px; margin-left: 35px; margin-right: 15px;">B2W | Reset</label>
                        <img src="Images/momo.png" width="45"/>
                        <img src="Images/om.jpg" width="55"/>
					</span>

					<div class="wrap-input100 validate-input" data-validate = "Please enter new password">
						<asp:TextBox id="pwd" runat="server" CssClass="input100" type="password" name="pass" placeholder="Password"/>
						<span class="focus-input100"></span>
					</div>
                    <br />
					<div class="container-login100-form-btn">
						<asp:Button id="btnResetPassword" runat="server" onclick="btnResetPassword_Click" Text="Reset" CssClass="login100-form-btn"></asp:Button>
					</div>
                    <br />
                    <asp:Label id="txtConfirm" runat="server" Text="" ForeColor="Green"/>
                    <br />
                    <div>
                        <center><img src="https://seal.godaddy.com/images/3/en/siteseal_gd_3_h_d_m.gif" style="float: right;"/></center>
                        <br />
                    </div>
				</form>
			</div>
		</div>
	</div>
<!--===============================================================================================-->
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

</body>
</html>