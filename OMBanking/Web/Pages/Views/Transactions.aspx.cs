﻿using OMBanking.Web.DB;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using System.IO;
using System.Collections.Specialized;
using OMBanking.Web.Reporting;
using OMBanking.Web.Security;
using OMBanking.Web.Proxy.Bank;
using System.Threading;
using System.Threading.Tasks;
using OMBanking.Web.Log;
using System.Diagnostics;
using System.Web.UI.WebControls;
using System.Drawing;

namespace OMBanking.Web.Pages.Views
{
    public partial class TransactionLog : Page
    {
        private DataTable _Table;
        private DataTable _Table1;
        private const string PDF_COUNT = "PDF_COUNT";
        private const string PDF_COUNT1 = "PDF_COUNT1";
        private const string EXCEL_COUNT = "EXCEL_COUNT";
        private const string EXCEL_COUNT1 = "EXCEL_COUNT1";
        private const string ISENABLED = "ISENABLED";
        private const string ISENABLED1 = "ISENABLED1";
        private const string DATA = "TABLE";
        private const string DATA1 = "TABLE1";
        private const String SESSION_ID = "sid";
        private const String USER_ID = "userId";
        private const String BRANCH_ID = "branchId";
        private const String GSM_OP_ID = "gsmOpId";
        private const String ROLE_ID = "role";

        protected async void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CacheSessionParams();
                await LoadTransactions();
                if (((string)Session[ROLE_ID]).CompareTo("Client") != 0)
                {
                    await LoadFootPrintTable("Transaction", true);
                }
            }
        }

        private void CacheSessionParams()
        {
            if (Session[PDF_COUNT] == null)
                Session[PDF_COUNT] = 0;
            if (Session[EXCEL_COUNT] == null)
                Session[EXCEL_COUNT] = 0;
            if (Session[ISENABLED] == null)
                Session[ISENABLED] = false;
            if (Session[PDF_COUNT1] == null)
                Session[PDF_COUNT1] = 0;
            if (Session[EXCEL_COUNT1] == null)
                Session[EXCEL_COUNT1] = 0;
            if (Session[ISENABLED1] == null)
                Session[ISENABLED1] = false;
            NameValueCollection SessionParams = Request.QueryString;
            Session[USER_ID] = (SessionParams[USER_ID] != null)
                                ? Utils.Base64Decode(SessionParams[USER_ID].ToString()) : "";
            Session[BRANCH_ID] = (SessionParams[BRANCH_ID] != null)
                                 ? Utils.Base64Decode(SessionParams[BRANCH_ID].ToString()) : "";
            Session[GSM_OP_ID] = (SessionParams[GSM_OP_ID] != null)
                                 ? Utils.Base64Decode(SessionParams[GSM_OP_ID].ToString()) : "";
            Session[ROLE_ID] = (SessionParams[ROLE_ID] != null)
                                ? Utils.Base64Decode(SessionParams[ROLE_ID].ToString()) : "";

            if ((string)Session[USER_ID] == ""
                || (string)Session[BRANCH_ID] == ""
                || (string)Session[GSM_OP_ID] == ""
                || (string)Session[ROLE_ID] == "")
            {
                if (((string)Session[ROLE_ID]).CompareTo("Client") == 0)
                {
                    Response.Redirect("~/customer.aspx", false);
                }
                else
                {
                    Response.Redirect("~/index.aspx", false);
                }
            }
            else
            {
                string Role = (string)Session[ROLE_ID];
                if (Role.CompareTo("Agent") == 0)
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Info",
                        "alert('You are not allowed to access this menu (Role01).');", true);
                    Response.Redirect("~/Pages/Views/Loader.aspx", false);
                }
                else if (Role.CompareTo("Supervisor") == 0)
                {
                    btnCancel.Visible = true;
                    btnSelectAll.Visible = true;
                }
                btnCancel.BackColor = Color.FromName(ConfigurationManager.AppSettings.Get("color"));
                btnCancel.ForeColor = Color.White;
                btnSelectAll.BackColor = Color.FromName(ConfigurationManager.AppSettings.Get("color"));
                btnSelectAll.ForeColor = Color.White;
            }
        }

        private void AddColumnHeader()
        {
            _Table = new DataTable();
            _Table.Columns.Add("Date", typeof(DateTime));
            _Table.Columns.Add("FirstName", typeof(string));
            _Table.Columns.Add("LastName", typeof(string));
            _Table.Columns.Add("PhoneNo", typeof(string));
            _Table.Columns.Add("AccountNo", typeof(string));
            _Table.Columns.Add("TranType", typeof(string));
            _Table.Columns.Add("TranRef", typeof(string));
            _Table.Columns.Add("Complete", typeof(string));
            _Table.Columns.Add("ResponseCode", typeof(string));
            _Table.Columns.Add("ResponseMessage", typeof(string));
            _Table.Columns.Add("Amount", typeof(double));
            _Table.Columns.Add("Charge", typeof(string));
            _Table.Columns.Add("RequestId", typeof(string));
            _Table.Columns.Add("OperatorCode", typeof(string));
            _Table.Columns.Add("AffiliateCode", typeof(string));
            _Table.Columns.Add("ExternalRefNo", typeof(string));
            _Table.Columns.Add("Details", typeof(string));
            _Table.Columns.Add("SerialID", typeof(string));
            _Table.Columns.Add("ConsultedBy", typeof(string));
            _Table.Columns.Add("ConsultedOn", typeof(DateTime));
            _Table.Columns.Add("AuditedBy", typeof(string));
            _Table.Columns.Add("AuditedOn", typeof(DateTime));
            _Table.Columns.Add("Role", typeof(string));
            _Table.Columns.Add("BranchID", typeof(string));
            _Table.Columns.Add("GSMOperationID", typeof(string));
        }

        private void AddColumnHeader1()
        {
            _Table1 = new DataTable();
            _Table1.Columns.Add("Role", typeof(string));
            _Table1.Columns.Add("UserAction", typeof(string));
            _Table1.Columns.Add("AccessedBy", typeof(string));
            _Table1.Columns.Add("AccessedOn", typeof(DateTime));
            _Table1.Columns.Add("BranchID", typeof(string));
            _Table1.Columns.Add("GSMOperationID", typeof(string));

        }

        protected async void btnSearch_Click(object sender, EventArgs e)
        {
            String from = txtFrom.Text == null ? "" : txtFrom.Text,
                   to = txtTo.Text == null ? "" : txtTo.Text;

            if (from != "" && to != "")
            {
                from = Convert.ToDateTime(from).ToString("yyyy-MM-dd HH:mm:ss",
                                 System.Globalization.CultureInfo.InvariantCulture);
                to = Convert.ToDateTime(to).ToString("yyyy-MM-dd HH:mm:ss",
                                 System.Globalization.CultureInfo.InvariantCulture);

                string Role = Session[ROLE_ID].ToString();
                string BranchID = Session[BRANCH_ID].ToString();
                List<Log.TransactionLog> transactionLogs;

                if (Role.CompareTo("Client") != 0)
                {
                    transactionLogs = (Role.CompareTo("Admin") == 0)
                        ? await new USSDHelper().SearchTransactionLogs(true, from, to, Session[USER_ID].ToString())
                        : ((Role.CompareTo("Auditor") == 0)
                           ? await new USSDHelper().SearchTransactionLogs(false, from, to, Session[USER_ID].ToString())
                           : ((Role.CompareTo("Agent") == 0)
                              ? await new USSDHelper().SearchTransactionLogs(from, to,
                                        Session[USER_ID].ToString(), Role, Session[BRANCH_ID].ToString(),
                                        Session[GSM_OP_ID].ToString())
                              : await new USSDHelper().SearchTransactionLogs(from, to,
                                        Session[USER_ID].ToString(), null, ((BranchID.CompareTo("00") == 0) ? null : BranchID),
                                        Session[GSM_OP_ID].ToString()))); //Supervisor
                }
                else
                {
                    string AccountNo = await new USSDHelper()
                                             .GetAccountIDByPhoneNo((string)Session[USER_ID]);
                    transactionLogs = await new USSDHelper()
                                                .SearchTransactionLogsByAccountID(from, to, AccountNo, Role,
                                                    (string)Session[BRANCH_ID], (string)Session[GSM_OP_ID]);
                }

                AddColumnHeader();
                if (transactionLogs != null)
                {
                    FillTransactionsTable(transactionLogs);
                    transactionLogTable.DataSource = _Table;
                    transactionLogTable.DataBind();
                    Session[DATA] = _Table;
                    if (Role.CompareTo("Client") != 0)
                    {
                        await StoreFootPrint("Search Transactions");
                        await LoadFootPrintTable("Transaction", false);
                    }
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Info",
                        "alert('No data found! Please enter valid date period.');", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "Error",
                        "alert('Empty date inputs !');", true);
            }
        }

        protected async void btnToPDF_Click(object sender, EventArgs e)
        {
            if (Session["State"] == null)
            {
                Session[DATA] = EnablePrintingMode(
                                  new string[] { "State", "PDF" },
                                  (DataTable)Session[DATA],
                                  true);
                transactionLogTable.DataSource = (DataTable)Session[DATA];
                transactionLogTable.DataBind();
            }
            else
            {
                if ((bool)Session[ISENABLED])
                {
                    String FileName = "TransactionsHistory" + Session[PDF_COUNT] + "-"
                                      + DateTime.Today.ToString().Split(new char[] { ' ' })[0]
                                                .Replace("/", "-") + ".pdf";
                    PDFReport report = (PDFReport)ReportFactory.create(ReportFactory.ReportType.PDF);
                    string path = Server.MapPath(@"~/Pages/Views/Store/" + FileName);
                    string dir = Server.MapPath(@"~/Pages/Views/Store/");
                    string imgPath = Server.MapPath(@"~/Pages/Views/Images/Bank.jpg");
                    try
                    {
                        DataTable dtNew = PreparePrinting((DataTable)Session[DATA]);
                        // clear selected row
                        Session[DATA] = EnablePrintingMode(
                                         new string[] { "State", "PDF" },
                                         (DataTable)Session[DATA],
                                         false);
                        transactionLogTable.DataSource = (DataTable)Session[DATA];
                        transactionLogTable.DataBind();

                        report.generatePDF(dtNew, "Report of Transactions", path, imgPath, dir);
                        Session[PDF_COUNT] = (int)Session[PDF_COUNT] + 1;
                        if (((string)Session[ROLE_ID]).CompareTo("Client") != 0)
                        {
                            await StoreFootPrint("Export Transactions to PDF");
                            await LoadFootPrintTable("Transaction", false);
                        }
                        ClientScript.RegisterStartupScript(this.GetType(), "Info",
                            "alert('Sucessfully generated PDF file !');", true);
                        // load PDF
                        OpenPDF(FileName);
                    }
                    catch (IOException)
                    {
                        //ClientScript.RegisterStartupScript(this.GetType(), "Error",
                        //    "alert('Another PDF file is open. Please close it and try again !');", true);
                    }
                }
            }
        }

        protected async void btnToExcel_Click(object sender, EventArgs e)
        {
            if ((bool)Session[ISENABLED])
            {
                String FileName = "TransactionsReport" 
                                  + Session[EXCEL_COUNT] + "-"
                                  + DateTime.Today.ToString().Split(new char[] { ' ' })[0]
                                            .Replace("/", "-") + ".xls";
                ExcelReport report = (ExcelReport)ReportFactory.create(ReportFactory.ReportType.EXCEL);
                string path = Server.MapPath(@"~/Pages/Views/Store/" + FileName);
                string dir = Server.MapPath(@"~/Pages/Views/Store/");
                string imgPath = Server.MapPath(@"~/Pages/Views/Images/Bank.jpg");
                try
                {
                    report.generateExcel((DataTable)Session[DATA], "Report of Transactions", path, imgPath, dir);
                    Session[EXCEL_COUNT] = (int)Session[EXCEL_COUNT] + 1;
                    if (((string)Session[ROLE_ID]).CompareTo("Client") != 0)
                    {
                        await StoreFootPrint("Export Transactions to Excel");
                        await LoadFootPrintTable("Transaction", false);
                    }
                    ClientScript.RegisterStartupScript(this.GetType(), "Info",
                        "alert('Sucessfully generated Excel file !');", true);
                    startExcelFile(path);
                }
                catch (IOException)
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Info",
                        "alert('Another Excel file is open. Please close it and try again !');", true);
                }
            }
        }

        protected async void btnToPDF1_Click(object sender, EventArgs e)
        {
            if (Session["State1"] == null)
            {
                Session[DATA1] = EnablePrintingMode(
                                  new string[] { "State1", "PDF1" },
                                  (DataTable)Session[DATA1],
                                  true);
                responseTable.DataSource = (DataTable)Session[DATA1];
                responseTable.DataBind();
            }
            else
            {
                if ((bool)Session[ISENABLED1])
                {
                    String FileName = "UserActionHistory_Transactions" + Session[PDF_COUNT1] + "-"
                                      + DateTime.Today.ToString().Split(new char[] { ' ' })[0]
                                                .Replace("/", "-") + ".pdf";
                    PDFReport report = (PDFReport)ReportFactory.create(ReportFactory.ReportType.PDF);
                    string path = Server.MapPath(@"~/Pages/Views/Store/" + FileName);
                    string dir = Server.MapPath(@"~/Pages/Views/Store/");
                    string imgPath = Server.MapPath(@"~/Pages/Views/Images/Bank.jpg");
                    try
                    {
                        DataTable dtNew = PreparePrinting((DataTable)Session[DATA1]);
                        // clear selected row
                        Session[DATA1] = EnablePrintingMode(
                                         new string[] { "State1", "PDF1" },
                                         (DataTable)Session[DATA1],
                                         false);
                        responseTable.DataSource = (DataTable)Session[DATA1];
                        responseTable.DataBind();

                        report.generatePDF(dtNew, "User Action History - Transactions", path, imgPath, dir);
                        Session[PDF_COUNT1] = (int)Session[PDF_COUNT1] + 1;
                        await StoreFootPrint("Export Transaction Actions to PDF");
                        await LoadFootPrintTable("Transaction", false);
                        // load PDF
                        OpenPDF(FileName);
                    }
                    catch (IOException)
                    {
                        //ClientScript.RegisterStartupScript(this.GetType(), "Info",
                        //    "alert('Another PDF file is open. Please close it and try again !');", true);
                    }
                }
            }
        }

        protected async void btnToExcel1_Click(object sender, EventArgs e)
        {
            if ((bool)Session[ISENABLED1])
            {
                String FileName = "UserActionHistory_Transactions" 
                                  + Session[EXCEL_COUNT1] + "-"
                                  + DateTime.Today.ToString().Split(new char[] { ' ' })[0]
                                            .Replace("/", "-") + ".xls";
                ExcelReport report = (ExcelReport)ReportFactory.create(ReportFactory.ReportType.EXCEL);
                string path = Server.MapPath(@"~/Pages/Views/Store/" + FileName);
                string dir = Server.MapPath(@"~/Pages/Views/Store/");
                string imgPath = Server.MapPath(@"~/Pages/Views/Images/Bank.jpg");
                try
                {
                    report.generateExcel((DataTable)Session[DATA1], "User Action History - Transactions", path, imgPath, dir);
                    Session[EXCEL_COUNT1] = (int)Session[EXCEL_COUNT1] + 1;
                    await StoreFootPrint("Export Transaction Actions to Excel");
                    await LoadFootPrintTable("Transaction", false);
                    startExcelFile(path);
                }
                catch (IOException)
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Info",
                        "alert('Another Excel file is open. Please close it and try again !');", true);
                }
            }
        }

        private void startExcelFile(string path)
        {
            FileInfo file = new FileInfo(path);
            Response.Clear();
            Response.Charset = "UTF-8";
            Response.ContentEncoding = System.Text.Encoding.UTF8;
            Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
            Response.AddHeader("Content-Length", file.Length.ToString());
            Response.ContentType = "application/ms-excel";
            Response.WriteFile(file.FullName);
            Response.End();
        }

        private async Task LoadFootPrintTable(string id, bool start)
        {
            string Role = Session[ROLE_ID].ToString();
            string BranchID = Session[BRANCH_ID].ToString();
            string GSMOpID = Session[GSM_OP_ID].ToString();

            responseTable.Visible = true;
            List<FootPrint> footPrints = await new USSDHelper().FootPrintById(id, Role, BranchID, GSMOpID);
            AddColumnHeader1();
            if (footPrints != null)
            {
                FillFootPrintTable(footPrints);
                responseTable.DataSource = _Table1;
                responseTable.DataBind();
                Session[ISENABLED1] = true;
                Session[DATA1] = _Table1;
                //if (start)
                //    await StoreFootPrint("Consult Transaction Actions");
            }
            else
            {
                _Table1.Rows.Add(_Table1.NewRow());
                responseTable.DataSource = _Table1;
                responseTable.DataBind();
                responseTable.Rows[0].Visible = false;
            }
        }

        private void FillFootPrintTable(List<FootPrint> footPrints)
        {
            foreach (FootPrint fp in footPrints)
            {
                DataRow _Row = _Table1.NewRow();
                _Row["Role"] = fp.Role;
                _Row["UserAction"] = fp.UserAction;
                _Row["AccessedBy"] = fp.AccessedBy.ToString();
                _Row["AccessedOn"] = fp.AccessedOn;
                _Row["BranchID"] = fp.BranchID;
                _Row["GSMOperationID"] = fp.GSMOperationID;
                _Table1.Rows.Add(_Row);
            }
        }

        private async Task LoadTransactions()
        {
            string Role = Session[ROLE_ID].ToString();
            string BranchID = Session[BRANCH_ID].ToString();
            List<Log.TransactionLog> transactionLogs;

            if (Role.CompareTo("Client") != 0)
            {
                transactionLogs = (Role.CompareTo("Admin") == 0)
                ? await new USSDHelper().TransactionLogs(true,
                            Session[USER_ID].ToString())
                : ((Role.CompareTo("Auditor") == 0)
                   ? await new USSDHelper().TransactionLogs(false,
                               Session[USER_ID].ToString())
                   : ((Role.CompareTo("Agent") == 0)
                      ? await new USSDHelper().TransactionLogs(Session[USER_ID].ToString(),
                                  Role, Session[BRANCH_ID].ToString(),
                                  Session[GSM_OP_ID].ToString())
                      : await new USSDHelper().TransactionLogs(Session[USER_ID].ToString(),
                                  null, ((BranchID.CompareTo("00") == 0) ? null : BranchID), 
                                  Session[GSM_OP_ID].ToString()))); //Supervisor
            }
            else
            {
                string AccountNo = await new USSDHelper()
                                             .GetAccountIDByPhoneNo((string)Session[USER_ID]);
                transactionLogs = await new USSDHelper()
                                            .TransactionLogsByAccountID(AccountNo, Role,
                                                (string)Session[BRANCH_ID], (string)Session[GSM_OP_ID]);
            }

            AddColumnHeader();
            if (transactionLogs != null)
            {
                FillTransactionsTable(transactionLogs);
                transactionLogTable.DataSource = _Table;
                transactionLogTable.DataBind();
                Session[ISENABLED] = true;
                Session[DATA] = _Table;
                if (((string)Session[ROLE_ID]).CompareTo("Client") != 0)
                {
                    await StoreFootPrint("Consult Transactions");
                }
            }
            else
            {
                _Table.Rows.Add(_Table.NewRow());
                transactionLogTable.DataSource = _Table;
                transactionLogTable.DataBind();
                transactionLogTable.Rows[0].Visible = false;
            }
        }

        private void FillTransactionsTable(List<Log.TransactionLog> transactionLogs)
        {
            foreach (Log.TransactionLog tl in transactionLogs)
            {
                DataRow _Row = _Table.NewRow();
                _Row["Date"] = tl.Date;
                _Row["TranType"] = (tl.TranType == 0)
                                    ? "GetAccountBalance"
                                    : ((tl.TranType == 1)
                                    ? "GetMiniStatement"
                                    : ((tl.TranType == 2)
                                    ? "AccountToWalletTransfer"
                                    : ((tl.TranType == 3)
                                    ? "WalletToAccountTransfer"
                                    : ((tl.TranType == 4)
                                    ? "CancelTransfer"
                                    : "N/A"
                                    ))));
                _Row["FirstName"] = tl.FirstName;
                _Row["LastName"] = tl.LastName;
                _Row["PhoneNo"] = tl.PhoneNo;
                _Row["OperatorCode"] = tl.OperatorCode;
                _Row["AffiliateCode"] = tl.AffiliateCode;
                _Row["ExternalRefNo"] = tl.ExternalRefNo;
                _Row["RequestId"] = tl.RequestId;
                _Row["AccountNo"] = tl.AccountNo;
                _Row["Amount"] = tl.Amount;
                _Row["Charge"] = tl.Charge.ToString();
                _Row["ResponseCode"] = tl.ResponseCode.ToString();
                _Row["ResponseMessage"] = tl.ResponseMessage;
                _Row["TranRef"] = tl.TranRef;
                _Row["Complete"] = tl.Complete.ToString();
                _Row["Details"] = tl.Details;
                _Row["SerialID"] = tl.SerialID;
                _Row["ConsultedBy"] = tl.ConsultedBy;
                _Row["ConsultedOn"] = tl.ConsultedOn;
                _Row["AuditedBy"] = tl.AuditedBy;
                _Row["AuditedOn"] = tl.AuditedOn;
                _Row["Role"] = tl.Role;
                _Row["BranchID"] = tl.BranchID;
                _Row["GSMOperationID"] = tl.GSMOperationID;
                _Table.Rows.Add(_Row);
            }
        }

        private async Task StoreFootPrint(string action)
        {
            USSDHelper helper = new USSDHelper();
            FootPrint fp = new FootPrint();
            fp.Id = Guid.NewGuid();
            fp.UserAction = action;
            fp.AccessedBy = Session[USER_ID].ToString();
            fp.AccessedOn = DateTime.Now;
            fp.BranchID = Session[BRANCH_ID].ToString();
            fp.GSMOperationID = Session[GSM_OP_ID].ToString();
            fp.Role = Session[ROLE_ID].ToString();
            bool result = await helper.AddFootPrint(fp);
            if (!result)
            {
                ServiceLogger _Logger = (ServiceLogger)LogFactory.create(Logger.LoggingMode.SERVICE);
                _Logger.Log(TraceLevel.Error, "Adding FootPrint Failed !");
            }
        }

        protected void transactionLogTable_PageIndexChanging(Object sender, GridViewPageEventArgs e)
        {
            transactionLogTable.DataSource = (DataTable)Session[DATA];
            transactionLogTable.PageIndex = e.NewPageIndex;
            transactionLogTable.DataBind();
        }

        protected void responseTable_PageIndexChanging(Object sender, GridViewPageEventArgs e)
        {
            responseTable.DataSource = (DataTable)Session[DATA1];
            responseTable.PageIndex = e.NewPageIndex;
            responseTable.DataBind();
        }

        protected async void txtSearch_TextChanged(object sender, EventArgs e)
        {
            if (txtSearch.Text != null && txtSearch.Text != "")
                await LoadTransactions(txtSearch.Text);
        }

        private async Task LoadTransactions(string searchTxt)
        {
            string Role = Session[ROLE_ID].ToString();
            List<Log.TransactionLog> transactionLogs;

            if (Role.CompareTo("Client") != 0)
            {
                transactionLogs = (Role.CompareTo("Admin") == 0)
                         ? await new USSDHelper().TransactionLogs(true,
                                     Session[USER_ID].ToString())
                         : ((Role.CompareTo("Auditor") == 0)
                            ? await new USSDHelper().TransactionLogs(false,
                                        Session[USER_ID].ToString())
                            : ((Role.CompareTo("Agent") == 0)
                               ? await new USSDHelper().TransactionLogs(Session[USER_ID].ToString(),
                                           Role, Session[BRANCH_ID].ToString(),
                                           Session[GSM_OP_ID].ToString())
                               : await new USSDHelper().TransactionLogs(Session[USER_ID].ToString(),
                                           null, ((BranchID.CompareTo("00") == 0) ? null : BranchID),
                                           Session[GSM_OP_ID].ToString())));
            }
            else
            {
                string AccountNo = await new USSDHelper()
                                             .GetAccountIDByPhoneNo((string)Session[USER_ID]);
                transactionLogs = await new USSDHelper()
                                            .TransactionLogsByAccountID(AccountNo, Role,
                                                (string)Session[BRANCH_ID], (string)Session[GSM_OP_ID]);
            }

            AddColumnHeader();
            if (transactionLogs != null)
            {
                FillTransactionsTable(transactionLogs, searchTxt);
                transactionLogTable.DataSource = _Table;
                transactionLogTable.DataBind();
                Session[ISENABLED] = true;
                Session[DATA] = _Table;
            }
            else
            {
                _Table.Rows.Add(_Table.NewRow());
                transactionLogTable.DataSource = _Table;
                transactionLogTable.DataBind();
                transactionLogTable.Rows[0].Visible = false;
            }
        }

        private void FillTransactionsTable(List<Log.TransactionLog> transactionLogs, string searchTxt)
        {
            foreach (Log.TransactionLog tl in transactionLogs)
            {
                string transType = (tl.TranType == 0)
                                        ? "GetAccountBalance"
                                        : ((tl.TranType == 1)
                                        ? "GetMiniStatement"
                                        : ((tl.TranType == 2)
                                        ? "AccountToWalletTransfer"
                                        : ((tl.TranType == 3)
                                        ? "WalletToAccountTransfer"
                                        : ((tl.TranType == 4)
                                        ? "CancelTransfer"
                                        : "N/A"
                                        ))));
                if (tl.Date.ToString().Contains(searchTxt)
                   || transType.Contains(searchTxt)
                   || (tl.OperatorCode != null && tl.OperatorCode.Contains(searchTxt))
                   || (tl.AffiliateCode != null && tl.AffiliateCode.Contains(searchTxt))
                   || (tl.ExternalRefNo != null && tl.ExternalRefNo.Contains(searchTxt))
                   || (tl.RequestId != null && tl.RequestId.Contains(searchTxt))
                   || (tl.AccountNo != null && tl.AccountNo.Contains(searchTxt))
                   || tl.Amount.ToString().Contains(searchTxt)
                   || tl.Charge.ToString().Contains(searchTxt)
                   || tl.ResponseCode.ToString().Contains(searchTxt)
                   || tl.ResponseMessage.Contains(searchTxt)
                   || (tl.TranRef != null && tl.TranRef.Contains(searchTxt))
                   || tl.Complete.ToString().Contains(searchTxt)
                   || (tl.Details != null && tl.Details.ToString().Contains(searchTxt))
                   || (tl.SerialID != null && tl.SerialID.Contains(searchTxt))
                   || (tl.ConsultedBy != null && tl.ConsultedBy.Contains(searchTxt))
                   || (tl.AuditedBy != null && tl.AuditedBy.Contains(searchTxt))
                   || tl.AuditedOn.ToString().Contains(searchTxt)
                   || tl.ConsultedOn.ToString().Contains(searchTxt)
                   || (tl.Role != null && tl.Role.Contains(searchTxt))
                   || (tl.BranchID != null && tl.BranchID.Contains(searchTxt))
                   || (tl.GSMOperationID != null && tl.GSMOperationID.ToString().Contains(searchTxt))
                   || (tl.FirstName != null && tl.FirstName.ToString().ToLower().Contains(searchTxt.ToLower()))
                   || (tl.LastName != null && tl.LastName.ToString().ToLower().Contains(searchTxt.ToLower()))
                   || (tl.PhoneNo != null && tl.PhoneNo.ToString().Contains(searchTxt)))
                {
                    DataRow _Row = _Table.NewRow();
                    _Row["Date"] = tl.Date;
                    _Row["FirstName"] = tl.FirstName;
                    _Row["LastName"] = tl.LastName;
                    _Row["PhoneNo"] = tl.PhoneNo;
                    _Row["TranType"] = transType;
                    _Row["OperatorCode"] = tl.OperatorCode;
                    _Row["AffiliateCode"] = tl.AffiliateCode;
                    _Row["ExternalRefNo"] = tl.ExternalRefNo;
                    _Row["RequestId"] = tl.RequestId;
                    _Row["AccountNo"] = (tl.AccountNo != null) ? tl.AccountNo.Replace(ConfigurationManager.AppSettings.Get("bic"), "") : "";
                    _Row["Amount"] = tl.Amount;
                    _Row["Charge"] = tl.Charge.ToString();
                    _Row["ResponseCode"] = tl.ResponseCode.ToString();
                    _Row["ResponseMessage"] = tl.ResponseMessage;
                    _Row["TranRef"] = tl.TranRef;
                    _Row["Complete"] = tl.Complete.ToString();
                    _Row["Details"] = tl.Details;
                    _Row["SerialID"] = tl.SerialID;
                    _Row["ConsultedBy"] = tl.ConsultedBy;
                    _Row["ConsultedOn"] = tl.ConsultedOn;
                    _Row["AuditedBy"] = tl.AuditedBy;
                    _Row["AuditedOn"] = tl.AuditedOn;
                    _Row["Role"] = tl.Role;
                    _Row["BranchID"] = tl.BranchID;
                    _Row["GSMOperationID"] = tl.GSMOperationID;
                    _Table.Rows.Add(_Row);
                }
            }

        }

        protected async void txtSearch1_TextChanged(object sender, EventArgs e)
        {
            if (txtSearch1.Text != null && txtSearch1.Text != "")
                await LoadFootPrintTable("Linked Account", true, txtSearch1.Text);
        }

        private async Task LoadFootPrintTable(string id, bool start, string searchTxt)
        {
            string Role = Session[ROLE_ID].ToString();
            string BranchID = Session[BRANCH_ID].ToString();
            string GSMOpID = Session[GSM_OP_ID].ToString();

            responseTable.Visible = true;
            List<FootPrint> footPrints = await new USSDHelper().FootPrintById(id, Role, BranchID, GSMOpID);
            AddColumnHeader1();
            if (footPrints != null)
            {
                FillFootPrintTable(footPrints, searchTxt);
                responseTable.DataSource = _Table1;
                responseTable.DataBind();
                Session[ISENABLED1] = true;
                Session[DATA1] = _Table1;
            }
            else
            {
                _Table1.Rows.Add(_Table1.NewRow());
                responseTable.DataSource = _Table1;
                responseTable.DataBind();
                responseTable.Rows[0].Visible = false;
            }
        }

        private void FillFootPrintTable(List<FootPrint> footPrints, string searchTxt)
        {
            foreach (FootPrint fp in footPrints)
            {
                if (fp.Role.Contains(searchTxt)
                    || fp.UserAction.Contains(searchTxt)
                    || fp.AccessedBy.ToString().Contains(searchTxt)
                    || fp.AccessedOn.ToString().Contains(searchTxt)
                    || fp.BranchID.Contains(searchTxt)
                    || fp.GSMOperationID.Contains(searchTxt))
                {
                    DataRow _Row = _Table1.NewRow();
                    _Row["Role"] = fp.Role;
                    _Row["UserAction"] = fp.UserAction;
                    _Row["AccessedBy"] = fp.AccessedBy.ToString();
                    _Row["AccessedOn"] = fp.AccessedOn;
                    _Row["BranchID"] = fp.BranchID;
                    _Row["GSMOperationID"] = fp.GSMOperationID;
                    _Table1.Rows.Add(_Row);
                }
            }
        }

        protected void transactionLogTable_Sorting(object sender, GridViewSortEventArgs e)
        {
            bool state = (Session["State"] != null)
                          ? ((string)Session["State"]).CompareTo("Select") == 0
                          : false;
            if (!state)
            {
                DataTable dataTable = (DataTable)Session[DATA];
                if (dataTable != null)
                {
                    string sortPattern = BuildSortPattern("Sort", e.SortDirection, e.SortExpression);
                    dataTable.DefaultView.Sort = sortPattern;
                    Session[DATA] = dataTable;
                    transactionLogTable.DataSource = dataTable;
                    transactionLogTable.DataBind();
                }
            }
            else
            {
                DataTable dt = (DataTable)Session[DATA];
                int size = dt.Columns.Count;
                string columnName = e.SortExpression;
                for (int i = 0; i < size; i++)
                {
                    string column = dt.Columns[i].ColumnName;
                    if (column.Contains(columnName))
                    {
                        dt.Columns[i].ColumnName = column.Replace("\u2714 ", "");
                        break;
                    }
                }
                Session[DATA] = dt;
                transactionLogTable.DataSource = (DataTable)Session[DATA];
                transactionLogTable.DataBind();
            }
        }

        protected void responseTable_Sorting(object sender, GridViewSortEventArgs e)
        {
            bool state = (Session["State1"] != null)
                          ? ((string)Session["State1"]).CompareTo("Select") == 0
                          : false;
            if (!state)
            {
                DataTable dataTable = (DataTable)Session[DATA1];
                if (dataTable != null)
                {
                    string sortPattern = BuildSortPattern("Sort1", e.SortDirection, e.SortExpression);
                    dataTable.DefaultView.Sort = sortPattern;
                    Session[DATA1] = dataTable;
                    responseTable.DataSource = dataTable;
                    responseTable.DataBind();
                }
            }
            else
            {
                DataTable dt = (DataTable)Session[DATA1];
                int size = dt.Columns.Count;
                string columnName = e.SortExpression;
                for (int i = 0; i < size; i++)
                {
                    string column = dt.Columns[i].ColumnName;
                    if (column.Contains(columnName))
                    {
                        dt.Columns[i].ColumnName = column.Replace("\u2714 ", "");
                        break;
                    }
                }
                Session[DATA1] = dt;
                responseTable.DataSource = (DataTable)Session[DATA1];
                responseTable.DataBind();
            }
        }

        private string BuildSortPattern(string cacheKey, SortDirection sortDirection,
                                string sortExpression)
        {
            string dir = ConvertSortDirection(sortDirection),
                   sortPattern = (Session[cacheKey] != null)
                                  ? (string)Session[cacheKey] : "";
            if (sortPattern == "")
            {
                sortPattern = sortExpression + " " + dir;
                Session[cacheKey] = sortPattern;
            }
            else
            {
                if (sortPattern.Contains(sortExpression))
                {
                    sortPattern = sortPattern.Replace(" desc", " ")
                                             .Replace(" asc", " ");
                    sortPattern = sortPattern + dir;
                }
                else
                {
                    sortPattern = sortPattern.Replace(" desc", ",")
                                             .Replace(" asc", ",");
                    sortPattern = sortPattern + sortExpression + " " + dir;
                }
            }

            return sortPattern;
        }

        private string ConvertSortDirection(SortDirection sortDirection)
        {
            string newSortDirection = String.Empty;

            switch (sortDirection)
            {
                case SortDirection.Ascending:
                    newSortDirection = "asc";
                    break;

                case SortDirection.Descending:
                    newSortDirection = "desc";
                    break;
            }

            return newSortDirection;
        }

        private void OpenPDF(string FileName)
        {
            string viewPage = Server.MapPath(@"~/Pages/Views/Store/" + FileName);
            FileStream fs = new FileStream(viewPage, System.IO.FileMode.Open, System.IO.FileAccess.Read);
            int fs_len = (int)fs.Length;
            byte[] ar = new byte[fs_len];
            fs.Read(ar, 0, fs_len);
            fs.Close();

            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("Accept-Header", fs_len.ToString());
            Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName + ".pdf");
            Response.AddHeader("Expires", "0");
            Response.AddHeader("Pragma", "cache");
            Response.AddHeader("Cache-Control", "private");
            Response.ContentType = "application/pdf";
            Response.AddHeader("Accept-Ranges", "bytes");
            Response.BinaryWrite(ar);
            Response.Flush();
            try { Response.End(); }
            catch { }
        }

        private DataTable EnablePrintingMode(string[] StateName, DataTable dt, bool state)
        {
            int colCount = dt.Columns.Count;
            for (int i = 0; i < colCount; i++)
            {
                string column = dt.Columns[i].ColumnName;
                if (state)
                    dt.Columns[i].ColumnName = "\u2714 " + column;
                else
                    dt.Columns[i].ColumnName = column.Replace("\u2714 ", "");
            }

            if (state)
            {
                Session[StateName[0]] = "Select";
                Session[StateName[1]] = new System.Web.UI.WebControls.Image
                {
                    ImageUrl = "Images/select.png",
                    Width = 35,
                    Height = 35,
                    BorderColor = Color.Transparent
                };
                if(StateName[0].CompareTo("State") == 0)
                   btnToPDF.Controls.Add((System.Web.UI.WebControls.Image)Session[StateName[1]]);
                else
                   btnToPDF1.Controls.Add((System.Web.UI.WebControls.Image)Session[StateName[1]]);
            }
            else
            {
                Session[StateName[0]] = null;
                if (StateName[0].CompareTo("State") == 0)
                {
                    btnToPDF.Controls.Remove((System.Web.UI.WebControls.Image)Session[StateName[1]]);
                    btnToPDF.Controls.Add(new System.Web.UI.WebControls.Image
                    {
                        ImageUrl = "Images/pdf.png",
                        Width = 35,
                        Height = 35,
                        BorderColor = Color.Transparent
                    });
                }
                else
                {
                    btnToPDF1.Controls.Remove((System.Web.UI.WebControls.Image)Session[StateName[1]]);
                    btnToPDF1.Controls.Add(new System.Web.UI.WebControls.Image
                    {
                        ImageUrl = "Images/pdf.png",
                        Width = 35,
                        Height = 35,
                        BorderColor = Color.Transparent
                    });
                }
            }

            return dt;
        }

        private DataTable PreparePrinting(DataTable dt)
        {
            DataTable dtNew = dt.Copy();
            int colCount = dt.Columns.Count;
            if (colCount > 0)
            {
                for (int i = 0; i < colCount; i++)
                {
                    DataColumn c = dt.Columns[i];
                    if (!c.ColumnName.Contains("\u2714 "))
                        dtNew.Columns.Remove(c.ColumnName);
                }
                int colNewCount = dtNew.Columns.Count;
                for (int i = 0; i < colNewCount; i++)
                {
                    string column = dtNew.Columns[i].ColumnName;
                    if (column.Contains("\u2714 "))
                        dtNew.Columns[i].ColumnName = column.Replace("\u2714 ", "");
                }
            }

            return dtNew;
        }


        protected void transactionLogTable_RowCreated(object sender, GridViewRowEventArgs e)
        {
            string Role = Session[ROLE_ID].ToString();
            if (Role.CompareTo("Supervisor") != 0)
            {
                try
                {
                    ((DataControlField)transactionLogTable.Columns
                      .Cast<DataControlField>()
                      .Where(fld => fld.HeaderText == "Select")
                      .SingleOrDefault()).Visible = false;
                }
                catch (Exception) { }
            }
        }

        protected async void btnCancel_Click(object sender, EventArgs e)
        {
            int pageIndex = transactionLogTable.PageIndex,
                rowCount  = transactionLogTable.Rows.Count;

            DataTable dt = (DataTable)Session[DATA];
            for (int i = 0; i < rowCount; i++)
            {
                int idx = i,
                    cIdx = (5 * pageIndex) + i;

                GridViewRow row = transactionLogTable.Rows[idx];
                CheckBox CheckRow = (row.FindControl("validationCheckBox") as CheckBox);
                if (CheckRow != null && CheckRow.Checked)
                {
                    CancelTransfer req = new CancelTransfer();
                    req.externalRefNo = dt.Rows[cIdx].Field<string>("TranRef");
                    Log.TransactionLog tl = await new USSDHelper().SearchTransactionLogRef(req.externalRefNo);
                    req.mmHeaderInfo = new HeaderRequest();
                    req.mmHeaderInfo.affiliateCode = dt.Rows[cIdx].Field<string>("AffiliateCode");
                    req.mmHeaderInfo.operatorCode = dt.Rows[cIdx].Field<string>("OperatorCode");
                    req.mmHeaderInfo.requestId = dt.Rows[cIdx].Field<string>("RequestId");
                    string transType = dt.Rows[cIdx].Field<string>("TranType");
                    req.mmHeaderInfo.requestType = (transType.Contains("WalletToAccountTransfer")) 
                                                    ? "W2A" 
                                                    : ((transType.Contains("AccountToWalletTransfer")) 
                                                        ? "A2W" 
                                                        : "");
                    CancelTransferResponse res = await new B2WHelper().CancelTransfer(req);
                    if(res.mmHeaderInfo.responseCode.CompareTo("000") == 0
                       || res.mmHeaderInfo.responseMessage.CompareTo("Success") == 0)
                    {
                        string alias = dt.Rows[cIdx].Field<string>("AccountNo");
                        LinkedAccount lk = await new USSDHelper().LinkedAccountByAlias(alias);
                        await TriggerSmsAlerts(lk);

                        ClientScript.RegisterStartupScript(this.GetType(), "Info",
                            "alert('Sucessfully cancelled transactions !');", true);
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Info",
                            "alert('Cancelling transactions failed !');", true);
                    }
                }
            }
        }

        private async Task TriggerSmsAlerts(LinkedAccount lk)
        {
            // Triggering Alerts
            var smsAlert = new SmsAlert();
            string newNumber;
            if (lk.PhoneNo != null && !lk.PhoneNo.Contains("237"))
                newNumber = "237" + lk.PhoneNo;
            else
                newNumber = lk.PhoneNo;

            smsAlert.MOBILEnumber = newNumber;
            smsAlert.PostedOn = DateTime.Now;
            smsAlert.Provider = (string)Session[GSM_OP_ID]; //MTN
            smsAlert.messagedescription = "Dear Customer, your transaction " + lk.Alias
                                          + " has been successfully cancelled. " + ConfigurationManager.AppSettings.Get("slogan")
                                          + " From " + ConfigurationManager.AppSettings.Get("bank_name");
            string[] data = lk.SupervisedBy.Split(new char[] { '-' });
            if (data.Length > 0)
                smsAlert.PostedBy = data[0];

            bool result = await new B2WHelper().TriggerSmsAlert(smsAlert);
            if (!result)
            {
                ServiceLogger _Logger = (ServiceLogger)LogFactory.create(Logger.LoggingMode.SERVICE);
                _Logger.Log(TraceLevel.Error, "Triggering Alert # " + lk.PhoneNo + " Failed !");
            }
        }

        protected async void btnSelectAll_Click(object sender, EventArgs e)
        {
            if (transactionLogTable.Rows.Count != 0)
            {
                if (btnSelectAll.Text.Contains("UnSelect All"))
                {
                    btnSelectAll.Text = "Select All";
                    foreach (GridViewRow row in transactionLogTable.Rows)
                    {
                        CheckBox CheckRow = (row.FindControl("validationCheckBox") as CheckBox);
                        if (CheckRow != null && CheckRow.Checked)
                        {
                            CheckRow.Checked = false;
                        }
                    }
                }
                else
                {
                    btnSelectAll.Text = "UnSelect All";
                    foreach (GridViewRow row in transactionLogTable.Rows)
                    {
                        CheckBox CheckRow = (row.FindControl("validationCheckBox") as CheckBox);
                        if (CheckRow != null && !CheckRow.Checked)
                        {
                            CheckRow.Checked = true;
                        }
                    }
                }
                // Reload Transactions
                await LoadTransactions();
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "Info",
                        "alert('Transaction table is empty !');", true);
            }
        }
    }
}