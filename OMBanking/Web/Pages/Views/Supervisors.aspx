﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Supervisors.aspx.cs" Inherits="OMBanking.Web.Pages.Views.Supervisors" Async="true" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>M-BANKING USER MANAGEMENT</title>
    <link type="text/css" rel="stylesheet" href="https://cdn.datatables.net/1.10.9/css/dataTables.bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="https://cdn.datatables.net/responsive/1.0.7/css/responsive.bootstrap.min.css" />
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/responsive/1.0.7/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.9/js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script type="text/javascript">
        $(function () {
            $('[id*=supervisorsTable]').prepend($("<thead></thead>").append($(this).find("tr:first"))).DataTable({
                "responsive": true,
                "sPaginationType": "full_numbers"
            });
        });
    </script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round"/>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <style>
    body {
        color: #566787;
        background: #f5f5f5;
        font-family: 'Varela Round', sans-serif;
        font-size: 13px;
    }
    .table-responsive {
        margin: 30px 0;
    }
    .table-wrapper {
        min-width: 1000px;
        background: #fff;
        padding: 20px 25px;
        border-radius: 3px;
        box-shadow: 0 1px 1px rgba(0,0,0,.05);
    }
    .table-title {
        padding-bottom: 15px;
        background: #228B22;
        color: #fff;
        padding: 16px 30px;
        margin: -20px -25px 10px;
        border-radius: 3px 3px 0 0;
    }
    .table-title h2 {
        margin: 5px 0 0;
        font-size: 24px;
    }
    .table-title .btn {
        color: #566787;
        float: right;
        font-size: 13px;
        background: #fff;
        border: none;
        min-width: 50px;
        border-radius: 2px;
        border: none;
        outline: none !important;
        margin-left: 10px;
    }
    .table-title .btn:hover, .table-title .btn:focus {
        color: #566787;
        background: #f2f2f2;
    }
    .table-title .btn i {
        float: left;
        font-size: 21px;
        margin-right: 5px;
    }
    .table-title .btn span {
        float: left;
        margin-top: 2px;
    }
    table.table tr th, table.table tr td {
        border-color: #e9e9e9;
        padding: 12px 15px;
        vertical-align: middle;
    }
    table.table tr th:first-child {
        width: 60px;
    }
    table.table tr th:last-child {
        width: 100px;
    }
    table.table-striped tbody tr:nth-of-type(odd) {
        background-color: #fcfcfc;
    }
    table.table-striped.table-hover tbody tr:hover {
        background: #f5f5f5;
    }
    table.table th i {
        font-size: 13px;
        margin: 0 5px;
        cursor: pointer;
    }	
    table.table td:last-child i {
        opacity: 0.9;
        font-size: 22px;
        margin: 0 5px;
    }
    table.table td a {
        font-weight: bold;
        color: #566787;
        display: inline-block;
        text-decoration: none;
    }
    table.table td a:hover {
        color: #2196F3;
    }
    table.table td a.settings {
        color: #2196F3;
    }
    table.table td a.delete {
        color: #F44336;
    }
    table.table td i {
        font-size: 19px;
    }
    table.table .avatar {
        border-radius: 50%;
        vertical-align: middle;
        margin-right: 10px;
    }
    .status {
        font-size: 30px;
        margin: 2px 2px 0 0;
        display: inline-block;
        vertical-align: middle;
        line-height: 10px;
    }
    .text-success {
        color: #10c469;
    }
    .text-info {
        color: #62c9e8;
    }
    .text-warning {
        color: #FFC107;
    }
    .text-danger {
        color: #ff5b5b;
    }
    .pagination {
        float: right;
        margin: 0 0 5px;
    }
    .pagination li a {
        border: none;
        font-size: 13px;
        min-width: 30px;
        min-height: 30px;
        color: #999;
        margin: 0 2px;
        line-height: 30px;
        border-radius: 2px !important;
        text-align: center;
        padding: 0 6px;
    }
    .pagination li a:hover {
        color: #666;
    }	
    .pagination li.active a, .pagination li.active a.page-link {
        background: #03A9F4;
    }
    .pagination li.active a:hover {        
        background: #0397d6;
    }
    .pagination li.disabled i {
        color: #ccc;
    }
    .pagination li i {
        font-size: 16px;
        padding-top: 6px
    }
    .hint-text {
        float: left;
        margin-top: 10px;
        font-size: 13px;
    }
</style>
<script type="text/javascript">
    window.onload = function () {
        var hColl = document.getElementsByClassName('scope');
        let color = '<%=ConfigurationManager.AppSettings.Get("color").ToLower()%>';
        changeColor(hColl, color, 'scope');
        hColl = document.getElementsByClassName('table-title');
        changeColor(hColl, color, 'table-title');
        hideIf('supervisors');
        hideIf('supervisors1');
    }
    function changeColor(coll, color, cls) {
        for (var i = 0, len = coll.length; i < len; i++) {
            if (cls == 'scope') {
                coll[i].style['color'] = color;
            }
            else if (cls == 'table-title') {
                coll[i].style['background-color'] = color;
                coll[i].style['color'] = '#FFFFFF';
            }
            else {
                coll[i].style['background-color'] = color;
                coll[i].style['border-color'] = color;
                coll[i].style['color'] = '#FFFFFF';
            }
        }
    }

    function hideIf(id) {
        let role = '<%=((Request.QueryString["role"] != null) ? Request.QueryString["role"].ToString() : "")%>';
        var obj = document.getElementById(id);
        if (role === "")
            obj.style['display'] = 'none';
    }
</script>
</head>
<body>
    <br /><br />
    <h1 class="scope" style="font-size:xx-large;">Supervisors</h1>
    <div><br /><br /><br /></div>
    <form id="form1" runat="server">
        <div id="supervisors" class="table-title">
        <div class="row">
            <div class="col-sm-5">
                <h2>Supervisor List</h2>
            </div>
            <div class="col-sm-7" style="text-align: right;">
                BranchID
                <asp:TextBox ID="branchID" runat="server" ForeColor="Black" placeholder="e.g. 00"/>&nbsp;
                Name
                <asp:TextBox ID="name" runat="server" ForeColor="Black" placeholder="e.g. Tidjon Lionel"/>&nbsp;
                <asp:Button ID="Add"  onclick="btnAdd_Click" Text="Add" runat="server" style="width:99px; color:black;"/>
                &nbsp;
                <asp:LinkButton ID="btnToExcel2" CssClass="toExcel" Width="40" Height="40" runat="server" OnClick="btnToExcel2_Click" BorderColor="Transparent" ToolTip="Export to Excel">
                <img src="Images/excel.png" alt="Export to Excel" width="40" height="40"/> 
                </asp:LinkButton>
                &nbsp;&nbsp;
                <asp:LinkButton ID="btnToPDF2" Width="35" Height="35" runat="server" OnClick="btnToPDF2_Click" BorderColor="Transparent" ToolTip="Export to PDF" >
                <img src="Images/pdf.png" alt="Export to PDF" width="35" height="35"/>
                </asp:LinkButton>			
            </div>
        </div>
    </div>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div id="supervisors1" style="overflow: scroll">
      <asp:UpdatePanel ID="UpdatePanel1" runat="server">
       <ContentTemplate>
        <asp:GridView ID="supervisorsTable" runat="server"          
            AutoGenerateColumns="False"  
            DataKeyNames="Name"  
            onrowcancelingedit="supervisorsTable_RowCancelingEdit"  
            onrowediting="supervisorsTable_RowEditing"              
            onrowdeleting="supervisorsTable_RowDeleting"  
            onrowupdating="supervisorsTable_RowUpdating"  
            CssClass="table table-striped table-hover" ForeColor="Black"
            ShowFooter ="true" ShowHeader="true" ShowHeaderWhenEmpty="true" 
            AllowPaging="true" AllowSorting="true" PageSize="5"
            OnSorting="supervisorsTable_Sorting" 
            OnPageIndexChanging="supervisorsTable_PageIndexChanging">
            <PagerSettings  Mode="NextPreviousFirstLast" FirstPageText="First" PreviousPageText="Previous" NextPageText="Next" LastPageText="Last" />
            <Columns>  
            <asp:TemplateField HeaderText="Id">  
                <ItemTemplate>  
                    <%#Eval("Id")%>  
                </ItemTemplate>
                <EditItemTemplate>  
                    <asp:TextBox ID="supId" runat="server" Text='<%#Eval("Id")%>' Enabled="false" ForeColor="Black"></asp:TextBox>  
                </EditItemTemplate>   
            </asp:TemplateField>  
            <asp:TemplateField HeaderText="BranchID">  
                <ItemTemplate>     
                    <%#Eval("BranchID")%>     
                </ItemTemplate>  
                <EditItemTemplate>  
                    <asp:TextBox ID="txtbranchid1" runat="server" Text='<%#Eval("BranchID") %>' ForeColor="Black"></asp:TextBox>  
                </EditItemTemplate>  
            </asp:TemplateField>  
                <asp:TemplateField HeaderText="Name">  
                <ItemTemplate>  
                    <%#Eval("Name")%>  
                </ItemTemplate>  
                <EditItemTemplate>  
                    <asp:TextBox ID="txtname" runat="server" Text='<%#Eval("Name") %>' ForeColor="Black"></asp:TextBox>  
                </EditItemTemplate>  
            </asp:TemplateField>  
            <asp:CommandField ShowEditButton="true" ButtonType ="Image" EditImageUrl="Images/edit.png" UpdateImageUrl="Images/accept.png"  
            CancelImageUrl="Images/cancel.png" HeaderText="Edit" ControlStyle-Width="20px" ControlStyle-Height="20px"/>  
            <asp:CommandField ShowDeleteButton="true" ButtonType="Image" DeleteImageUrl="Images/delete.png" HeaderText="Delete" ControlStyle-Width="20px" ControlStyle-Height="20px"/>   
        </Columns>  
      </asp:GridView>
     </ContentTemplate>
    </asp:UpdatePanel>
   </div>
   </form>
   <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script type="text/javascript" src="Scripts/jquery.blockUI.js"></script>
    <script type="text/javascript">
        $(function () {
            BlockUI("supervisors1");
            $.blockUI.defaults.css = {};
        });
        function BlockUI(elementID) {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_beginRequest(function () {
                $("#" + elementID).block({ message: '<div align = "center">' + '<img src="Images/loadingAnim.gif"/></div>',
                    css: {},
                    overlayCSS: { backgroundColor: '<%=ConfigurationManager.AppSettings.Get("color")%>', opacity: 0.6, border: '3px solid #63B2EB' }
                });
            });
            prm.add_endRequest(function () {
                $("#" + elementID).unblock();
            });
        };
    </script>
</body>
</html>

