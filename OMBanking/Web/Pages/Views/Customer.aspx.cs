﻿using OMBanking.Web.Bank;
using OMBanking.Web.DB;
using OMBanking.Web.Log;
using OMBanking.Web.Proxy.Bank;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OMBanking.Web.Pages.Views
{
    public partial class Customer : Page
    {
        private const String SESSION_ID = "sid";
        private const String USER_ID = "userId";
        private const String BRANCH_ID = "branchId";
        private const String GSM_OP_ID = "gsmOpId";
        private const String ROLE_ID = "role";

        protected async void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                NameValueCollection SessionParams = Request.QueryString;
                String SessionID = (SessionParams[SESSION_ID] != null)
                                    ? SessionParams[SESSION_ID].ToString() : "",
                       UserID = (SessionParams[USER_ID] != null)
                                 ? SessionParams[USER_ID].ToString() : "",
                       BranchID = (SessionParams[BRANCH_ID] != null)
                                   ? SessionParams[BRANCH_ID].ToString() : "",
                       GSMOpID = (SessionParams[GSM_OP_ID] != null)
                                  ? SessionParams[GSM_OP_ID].ToString() : "",
                       RoleID = (SessionParams[ROLE_ID] != null)
                                 ? SessionParams[ROLE_ID].ToString() : "";

                if (SessionID == ""
                    || UserID == ""
                    || BranchID == ""
                    || GSMOpID == ""
                    || RoleID == "")
                {
                    Response.Redirect("~/Pages/Views/Login.aspx", false);
                }
                else
                {
                    UserProfile user = new UserProfile();
                    user.UserID = Utils.Base64Decode(UserID);
                    user.Role = Utils.Base64Decode(RoleID);
                    user.BranchID = Utils.Base64Decode(BranchID);
                    user.GSMOperationID = Utils.Base64Decode(GSMOpID);
                    bool Yes = await new USSDHelper().IsUserLogIn(user);
                    if (!Yes)
                    {
                        Response.Redirect("~/Pages/Views/Login.aspx", false);
                    }
                }

                Session[USER_ID] = UserID;
                Session[BRANCH_ID] = BranchID;
                Session[ROLE_ID] = RoleID;
            }
        }

        protected async void btnLogOut_Click(object sender, EventArgs e)
        {
            USSDHelper helper = new USSDHelper();
            UserProfile user = new UserProfile();
            if (((string)Session[USER_ID]) != ""
                && ((string)Session[BRANCH_ID]) != ""
                && ((string)Session[ROLE_ID]) != "")
            {
                user.UserID = Utils.Base64Decode(Session[USER_ID].ToString());
                user.BranchID = Utils.Base64Decode(Session[BRANCH_ID].ToString());
                user.Role = Utils.Base64Decode(Session[ROLE_ID].ToString());
            }
            user.LogOutDate = DateTime.Now;
            bool result = await helper.UpdateUser(user, false);
            if (!result)
            {
                ServiceLogger _Logger = (ServiceLogger)LogFactory.create(Logger.LoggingMode.SERVICE);
                _Logger.Log(TraceLevel.Error, "Update user Failed !");
            }
            else
            {
                Response.Redirect("~/Pages/Views/Login.aspx", false);
            }
        }
    }
}