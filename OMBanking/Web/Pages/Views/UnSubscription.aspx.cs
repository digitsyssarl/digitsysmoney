﻿using OMBanking.Web.B2W.Orange.Register;
using OMBanking.Web.Bank;
using OMBanking.Web.DB;
using OMBanking.Web.Log;
using OMBanking.Web.Pages.Controller;
using OMBanking.Web.Proxy.B2W.Register;
using OMBanking.Web.Proxy.Bank;
using OMBanking.Web.Reporting;
using OMBanking.Web.Security;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Web.UI.WebControls;

namespace OMBanking.Web.Pages.Views
{
    public partial class UnSubscription : System.Web.UI.Page
    {
        private RegisterBinding _RegisterBinding;
        private InputController _InputController;
        private DataTable _Table;
        private const String SESSION_ID = "sid";
        private const String USER_ID = "userId";
        private const String BRANCH_ID = "branchId";
        private const String GSM_OP_ID = "gsmOpId";
        private const String ROLE_ID = "role";
        private const string PDF_COUNT = "PDF_COUNT";
        private const string EXCEL_COUNT = "EXCEL_COUNT";
        private const string ISENABLED = "ISENABLED";
        private const string DATA = "TABLE";
        private const string KEY = "KEY";

        protected async void Page_Load(object sender, EventArgs e)
        {
            _RegisterBinding = new RegisterBinding();
            _InputController = new InputController();
            CacheSessionParams();
            await LoadFootPrintTable("UnSubscription", true);
            await LoadSupervisorList();
        }

        private void CacheSessionParams()
        {
            if (Session[PDF_COUNT] == null)
                Session[PDF_COUNT] = 0;
            if (Session[EXCEL_COUNT] == null)
                Session[EXCEL_COUNT] = 0;
            if (Session[ISENABLED] == null)
                Session[ISENABLED] = false;
            NameValueCollection SessionParams = Request.QueryString;
            Session[USER_ID] = (SessionParams[USER_ID] != null)
                                ? Utils.Base64Decode(SessionParams[USER_ID].ToString()) : "";
            Session[BRANCH_ID] = (SessionParams[BRANCH_ID] != null)
                                 ? Utils.Base64Decode(SessionParams[BRANCH_ID].ToString()) : "";
            Session[GSM_OP_ID] = (SessionParams[GSM_OP_ID] != null)
                                 ? Utils.Base64Decode(SessionParams[GSM_OP_ID].ToString()) : "";
            Session[ROLE_ID] = (SessionParams[ROLE_ID] != null)
                                ? Utils.Base64Decode(SessionParams[ROLE_ID].ToString()) : "";
            if ((string)Session[USER_ID] == ""
                || (string)Session[BRANCH_ID] == ""
                || (string)Session[GSM_OP_ID] == ""
                || (string)Session[ROLE_ID] == "")
            {
                Response.Redirect("~/Pages/Views/Login.aspx", false);
            }
            else
            {
                string Role = (string)Session[ROLE_ID];
                if (Role.CompareTo("Auditor") == 0
                    || Role.CompareTo("Admin") == 0
                    || Role.CompareTo("Supervisor") == 0)
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Info",
                        "alert('You are not allowed to access this menu (Role01).');", true);
                    Response.Redirect("~/Pages/Views/Loader.aspx", false);
                }
                else
                {
                    Session[KEY] = ConfigurationManager.AppSettings.Get("bic");
                    txtaliasclose.Text = (string)Session[KEY];
                }
            }
        }

        protected async void btnClose_Click(object sender, EventArgs e)
        {
            try
            {
                DELREQ req = CreateCloseRequest();
                if (txtaliasclose.Text != null && txtaliasclose.Text.Length != 0)
                {
                    btnClose.BackColor = Color.DarkGray;
                    btnClose.Text = "Wait...";
                    string currentSupervisor = SupervisorList.SelectedItem.Text + "-"
                                               + "Pending" + "-"
                                               + req.motif + "-"
                                               + req.orig;
                    await CloseLinkedAccount(req, currentSupervisor);
                    btnClose.BackColor = Color.FromName(ConfigurationManager.AppSettings.Get("color"));
                    btnClose.Text = "Close";
                }
            }
            catch (Exception)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "Info",
                        "alert('Unable to submit unsubscriptions. Contact IT department !');", true);
            }
        }

        private async Task CloseLinkedAccount(DELREQ req, string supervisorInfo)
        {
            USSDHelper helper = new USSDHelper();
            bool result = await helper.CloseLinkedAccount(req.alias, supervisorInfo);
            if (!result)
            {
                ServiceLogger _Logger = (ServiceLogger)LogFactory.create(Logger.LoggingMode.SERVICE);
                _Logger.Log(TraceLevel.Error, "Closing Linked Account Failed !");

                ClientScript.RegisterStartupScript(this.GetType(), "Info",
                        "alert('Submitting Unsubscription Failed !');", true);
            }
            else
            {
                await StoreFootPrint("Cancel Subscription " + req.alias);
                await LoadFootPrintTable("UnSubscription", false);
                ClientScript.RegisterStartupScript(this.GetType(), "Info",
                    "alert('Successfully submitted unsubscription for supervision !');", true);
            }
        }

        private DELREQ CreateCloseRequest()
        {
            DELREQ req = new DELREQ();
            req.alias = (string)Session[KEY] + txtaccountno.Text;
            req.close_date = DateTime.Now;
            req.orig = ddListOrig.SelectedValue;
            req.motif = txtmotif.Text;

            return req;
        }

        protected async void btnToPDF_Click(object sender, EventArgs e)
        {
            if (Session["State"] == null)
            {
                Session[DATA] = EnablePrintingMode(
                                  new string[] { "State", "PDF" },
                                  (DataTable)Session[DATA],
                                  true);
                responseTable.DataSource = (DataTable)Session[DATA];
                responseTable.DataBind();
            }
            else
            {
                if ((bool)Session[ISENABLED])
                {
                    String FileName = "UserActionHistory_UnSubscription" + Session[PDF_COUNT] + "-"
                                      + DateTime.Today.ToString().Split(new char[] { ' ' })[0]
                                                .Replace("/", "-") + ".pdf";
                    PDFReport report = (PDFReport)ReportFactory.create(ReportFactory.ReportType.PDF);
                    string path = Server.MapPath(@"~/Pages/Views/Store/" + FileName);
                    string dir = Server.MapPath(@"~/Pages/Views/Store/");
                    string imgPath = Server.MapPath(@"~/Pages/Views/Images/Bank.jpg");
                    try
                    {
                        DataTable dtNew = PreparePrinting((DataTable)Session[DATA]);
                        // clear selected row
                        Session[DATA] = EnablePrintingMode(
                                         new string[] { "State", "PDF" },
                                         (DataTable)Session[DATA],
                                         false);
                        responseTable.DataSource = (DataTable)Session[DATA];
                        responseTable.DataBind();

                        report.generatePDF(dtNew, "User Action History - UnSubscription", path, imgPath, dir);
                        Session[PDF_COUNT] = (int)Session[PDF_COUNT] + 1;
                        await StoreFootPrint("Export UnSubscription Actions to PDF");
                        await LoadFootPrintTable("UnSubscription", false);
                        ClientScript.RegisterStartupScript(this.GetType(), "Info",
                            "alert('Sucessfully generated PDF file !');", true);
                        // load PDF
                        OpenPDF(FileName);
                    }
                    catch (IOException)
                    {
                        //ClientScript.RegisterStartupScript(this.GetType(), "Error",
                        //    "alert('Another PDF file is open. Please close it and try again !');", true);
                    }
                }
            }
        }

        protected async void btnToExcel_Click(object sender, EventArgs e)
        {
            if ((bool)Session[ISENABLED])
            {
                String FileName = "UserActionHistory_UnSubscription" + Session[EXCEL_COUNT] + "-"
                                  + DateTime.Today.ToString().Split(new char[] { ' ' })[0]
                                            .Replace("/", "-") + ".xls";
                ExcelReport report = (ExcelReport)ReportFactory.create(ReportFactory.ReportType.EXCEL);
                string path = Server.MapPath(@"~/Pages/Views/Store/" + FileName);
                string dir = Server.MapPath(@"~/Pages/Views/Store/");
                string imgPath = Server.MapPath(@"~/Pages/Views/Images/Bank.jpg");
                try
                {
                    report.generateExcel((DataTable)Session[DATA], "User Action History - UnSubscription", path, imgPath, dir);
                    Session[EXCEL_COUNT] = (int)Session[EXCEL_COUNT] + 1;
                    await StoreFootPrint("Export UnSubscription Actions to Excel");
                    await LoadFootPrintTable("UnSubscription", false);
                    ClientScript.RegisterStartupScript(this.GetType(), "Info",
                        "alert('Sucessfully generated Excel file !');", true);
                    startExcelFile(path);
                }
                catch (IOException)
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Error",
                        "alert('Another Excel file is open. Please close it and try again !');", true);
                }
            }
        }

        private void startExcelFile(string path)
        {
            FileInfo file = new FileInfo(path);
            Response.Clear();
            Response.Charset = "UTF-8";
            Response.ContentEncoding = System.Text.Encoding.UTF8;
            Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
            Response.AddHeader("Content-Length", file.Length.ToString());
            Response.ContentType = "application/ms-excel";
            Response.WriteFile(file.FullName);
            Response.End();
        }

        private void AddColumnHeader()
        {
            _Table = new DataTable();
            _Table.Columns.Add("Role", typeof(string));
            _Table.Columns.Add("UserAction", typeof(string));
            _Table.Columns.Add("AccessedBy", typeof(string));
            _Table.Columns.Add("AccessedOn", typeof(DateTime));
            _Table.Columns.Add("BranchID", typeof(string));
            _Table.Columns.Add("GSMOperationID", typeof(string));

        }

        private async Task LoadFootPrintTable(string id, bool start)
        {
            string Role = Session[ROLE_ID].ToString();
            string BranchID = Session[BRANCH_ID].ToString();
            string GSMOpID = Session[GSM_OP_ID].ToString();

            responseTable.Visible = true;
            List<FootPrint> footPrints = await new USSDHelper().FootPrintById(id, Role, BranchID, GSMOpID);
            AddColumnHeader();
            if (footPrints != null)
            {
                FillFootPrintTable(footPrints);
                responseTable.DataSource = _Table;
                responseTable.DataBind();
                Session[ISENABLED] = true;
                Session[DATA] = _Table;
            }
            else
            {
                _Table.Rows.Add(_Table.NewRow());
                responseTable.DataSource = _Table;
                responseTable.DataBind();
                responseTable.Rows[0].Visible = false;
            }
        }

        private void FillFootPrintTable(List<FootPrint> footPrints)
        {
            foreach (FootPrint fp in footPrints)
            {
                DataRow _Row = _Table.NewRow();
                _Row["Role"] = fp.Role;
                _Row["UserAction"] = fp.UserAction;
                _Row["AccessedBy"] = fp.AccessedBy.ToString();
                _Row["AccessedOn"] = fp.AccessedOn;
                _Row["BranchID"] = fp.BranchID;
                _Row["GSMOperationID"] = fp.GSMOperationID;
                _Table.Rows.Add(_Row);
            }
        }

        private async Task LoadSupervisorList()
        {
            String BranchID = Session[BRANCH_ID].ToString();

            List<Supervisor> sList = await new USSDHelper().GetSupervisors(BranchID);
            SupervisorList.Items.Clear();
            foreach (Supervisor s in sList)
            {
                SupervisorList.Items.Add(s.Name);
            }
        }

        private async Task StoreFootPrint(string action)
        {
            USSDHelper helper = new USSDHelper();
            FootPrint fp = new FootPrint();
            fp.Id = Guid.NewGuid();
            fp.UserAction = action;
            fp.AccessedBy = Session[USER_ID].ToString();
            fp.AccessedOn = DateTime.Now;
            fp.BranchID = Session[BRANCH_ID].ToString();
            fp.GSMOperationID = Session[GSM_OP_ID].ToString();
            fp.Role = Session[ROLE_ID].ToString();
            bool result = await helper.AddFootPrint(fp);
            if (!result)
            {
                ServiceLogger _Logger = (ServiceLogger)LogFactory.create(Logger.LoggingMode.SERVICE);
                _Logger.Log(TraceLevel.Error, "Adding FootPrint Failed !");
            }
        }

        protected void responseTable_PageIndexChanging(Object sender, GridViewPageEventArgs e)
        {
            responseTable.DataSource = (DataTable)Session[DATA];
            responseTable.PageIndex = e.NewPageIndex;
            responseTable.DataBind();
        }

        private async Task LoadFootPrintTable(string id, bool start, string searchTxt)
        {
            string Role = Session[ROLE_ID].ToString();
            string BranchID = Session[BRANCH_ID].ToString();
            string GSMOpID = Session[GSM_OP_ID].ToString();

            USSDHelper helper = new USSDHelper();
            List<FootPrint> footPrints = await helper.FootPrintById(id, Role, BranchID, GSMOpID);
            AddColumnHeader();
            if (footPrints != null)
            {
                FillFootPrintTable(footPrints, searchTxt);
                responseTable.DataSource = _Table;
                responseTable.DataBind();
                Session[ISENABLED] = true;
                Session[DATA] = _Table;
                if (start)
                    await StoreFootPrint("Search Unsubscription Actions");
            }
            else
            {
                _Table.Rows.Add(_Table.NewRow());
                responseTable.DataSource = _Table;
                responseTable.DataBind();
                responseTable.Rows[0].Visible = false;
            }
        }

        private void FillFootPrintTable(List<FootPrint> footPrints, string searchTxt)
        {
            foreach (FootPrint fp in footPrints)
            {
                if (fp.Role.Contains(searchTxt)
                    || fp.UserAction.Contains(searchTxt)
                    || fp.AccessedBy.ToString().Contains(searchTxt)
                    || fp.AccessedOn.ToString().Contains(searchTxt)
                    || fp.BranchID.Contains(searchTxt)
                    || fp.GSMOperationID.Contains(searchTxt))
                {
                    DataRow _Row = _Table.NewRow();
                    _Row["Role"] = fp.Role;
                    _Row["UserAction"] = fp.UserAction;
                    _Row["AccessedBy"] = fp.AccessedBy.ToString();
                    _Row["AccessedOn"] = fp.AccessedOn;
                    _Row["BranchID"] = fp.BranchID;
                    _Row["GSMOperationID"] = fp.GSMOperationID;
                    _Table.Rows.Add(_Row);
                }
            }
        }

        protected async void txtSearch1_TextChanged(object sender, EventArgs e)
        {
            if (txtSearch1.Text != null && txtSearch1.Text != "")
                await LoadFootPrintTable("UnSubscription", true, txtSearch1.Text);
        }

        protected void txtaccountno_TextChanged(object sender, EventArgs e)
        {
            txtaliasclose.Text = (string)Session[KEY] + txtaccountno.Text;
        }

        protected void responseTable_Sorting(object sender, GridViewSortEventArgs e)
        {
            bool state = (Session["State"] != null)
                          ? ((string)Session["State"]).CompareTo("Select") == 0
                          : false;
            if (!state)
            {
                DataTable dataTable = (DataTable)Session[DATA];
                if (dataTable != null)
                {
                    string sortPattern = BuildSortPattern("Sort", e.SortDirection, e.SortExpression);
                    dataTable.DefaultView.Sort = sortPattern;
                    Session[DATA] = dataTable;
                    responseTable.DataSource = dataTable;
                    responseTable.DataBind();
                }
            }
            else
            {
                DataTable dt = (DataTable)Session[DATA];
                int size = dt.Columns.Count;
                string columnName = e.SortExpression;
                for (int i = 0; i < size; i++)
                {
                    string column = dt.Columns[i].ColumnName;
                    if (column.Contains(columnName))
                    {
                        dt.Columns[i].ColumnName = column.Replace("\u2714 ", "");
                        break;
                    }
                }
                Session[DATA] = dt;
                responseTable.DataSource = (DataTable)Session[DATA];
                responseTable.DataBind();
            }
        }

        private string BuildSortPattern(string cacheKey, SortDirection sortDirection,
                                string sortExpression)
        {
            string dir = ConvertSortDirection(sortDirection),
                   sortPattern = (Session[cacheKey] != null)
                                  ? (string)Session[cacheKey] : "";
            if (sortPattern == "")
            {
                sortPattern = sortExpression + " " + dir;
                Session[cacheKey] = sortPattern;
            }
            else
            {
                if (sortPattern.Contains(sortExpression))
                {
                    sortPattern = sortPattern.Replace(" desc", " ")
                                             .Replace(" asc", " ");
                    sortPattern = sortPattern + dir;
                }
                else
                {
                    sortPattern = sortPattern.Replace(" desc", ",")
                                             .Replace(" asc", ",");
                    sortPattern = sortPattern + sortExpression + " " + dir;
                }
            }

            return sortPattern;
        }

        private string ConvertSortDirection(SortDirection sortDirection)
        {
            string newSortDirection = String.Empty;

            switch (sortDirection)
            {
                case SortDirection.Ascending:
                    newSortDirection = "asc";
                    break;

                case SortDirection.Descending:
                    newSortDirection = "desc";
                    break;
            }

            return newSortDirection;
        }

        private void OpenPDF(string FileName)
        {
            string viewPage = Server.MapPath(@"~/Pages/Views/Store/" + FileName);
            FileStream fs = new FileStream(viewPage, System.IO.FileMode.Open, System.IO.FileAccess.Read);
            int fs_len = (int)fs.Length;
            byte[] ar = new byte[fs_len];
            fs.Read(ar, 0, fs_len);
            fs.Close();

            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("Accept-Header", fs_len.ToString());
            Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName + ".pdf");
            Response.AddHeader("Expires", "0");
            Response.AddHeader("Pragma", "cache");
            Response.AddHeader("Cache-Control", "private");
            Response.ContentType = "application/pdf";
            Response.AddHeader("Accept-Ranges", "bytes");
            Response.BinaryWrite(ar);
            Response.Flush();
            try { Response.End(); }
            catch { }
        }

        private DataTable EnablePrintingMode(string[] StateName, DataTable dt, bool state)
        {
            int colCount = dt.Columns.Count;
            for (int i = 0; i < colCount; i++)
            {
                string column = dt.Columns[i].ColumnName;
                if (state)
                    dt.Columns[i].ColumnName = "\u2714 " + column;
                else
                    dt.Columns[i].ColumnName = column.Replace("\u2714 ", "");
            }

            if (state)
            {
                Session[StateName[0]] = "Select";
                Session[StateName[1]] = new System.Web.UI.WebControls.Image
                {
                    ImageUrl = "Images/select.png",
                    Width = 35,
                    Height = 35,
                    BorderColor = Color.Transparent
                };
                btnToPDF.Controls.Add((System.Web.UI.WebControls.Image)Session[StateName[1]]);
            }
            else
            {
                Session[StateName[0]] = null;
                btnToPDF.Controls.Remove((System.Web.UI.WebControls.Image)Session[StateName[1]]);
                btnToPDF.Controls.Add(new System.Web.UI.WebControls.Image
                {
                    ImageUrl = "Images/pdf.png",
                    Width = 35,
                    Height = 35,
                    BorderColor = Color.Transparent
                });
            }

            return dt;
        }

        private DataTable PreparePrinting(DataTable dt)
        {
            DataTable dtNew = dt.Copy();
            int colCount = dt.Columns.Count;
            if (colCount > 0)
            {
                for (int i = 0; i < colCount; i++)
                {
                    DataColumn c = dt.Columns[i];
                    if (!c.ColumnName.Contains("\u2714 "))
                        dtNew.Columns.Remove(c.ColumnName);
                }
                int colNewCount = dtNew.Columns.Count;
                for (int i = 0; i < colNewCount; i++)
                {
                    string column = dtNew.Columns[i].ColumnName;
                    if (column.Contains("\u2714 "))
                        dtNew.Columns[i].ColumnName = column.Replace("\u2714 ", "");
                }
            }

            return dtNew;
        }
    }
}