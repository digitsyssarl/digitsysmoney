﻿using OMBanking.Web.Bank;
using OMBanking.Web.DB;
using OMBanking.Web.Log;
using OMBanking.Web.Proxy.Bank;
using OMBanking.Web.Reporting;
using OMBanking.Web.Security;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OMBanking.Web.Pages.Views
{
    public partial class UserAccounts : Page
    {
        private const String SESSION_ID = "sid";
        private const String USER_ID = "userId";
        private const String BRANCH_ID = "branchId";
        private const String GSM_OP_ID = "gsmOpId";
        private const String ROLE_ID = "role";
        private DataTable _Table;
        private const string PDF_COUNT1 = "PDF_COUNT1";
        private const string EXCEL_COUNT1 = "EXCEL_COUNT1";
        private const string ISENABLED1 = "ISENABLED1";
        private const string DATA = "TABLE";


        protected async void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CacheSessionParams();
                await LoadUserAccounts("Consult User Accounts");
            }
        }

        private void CacheSessionParams()
        {
            if (Session[PDF_COUNT1] == null)
                Session[PDF_COUNT1] = 0;
            if (Session[EXCEL_COUNT1] == null)
                Session[EXCEL_COUNT1] = 0;
            if (Session[ISENABLED1] == null)
                Session[ISENABLED1] = false;

            NameValueCollection SessionParams = Request.QueryString;
            Session[USER_ID] = (SessionParams[USER_ID] != null)
                                ? Utils.Base64Decode(SessionParams[USER_ID].ToString()) : "";
            Session[BRANCH_ID] = (SessionParams[BRANCH_ID] != null)
                                 ? Utils.Base64Decode(SessionParams[BRANCH_ID].ToString()) : "";
            Session[GSM_OP_ID] = (SessionParams[GSM_OP_ID] != null)
                                 ? Utils.Base64Decode(SessionParams[GSM_OP_ID].ToString()) : "";
            Session[ROLE_ID] = (SessionParams[ROLE_ID] != null)
                                ? Utils.Base64Decode(SessionParams[ROLE_ID].ToString()) : "";
            if ((string)Session[USER_ID] == ""
                || (string)Session[BRANCH_ID] == ""
                || (string)Session[GSM_OP_ID] == ""
                || (string)Session[ROLE_ID] == "")
            {
                Response.Redirect("~/Pages/Views/Login.aspx", false);
            }
            else
            {
                string Role = (string)Session[ROLE_ID];
                if (Role.CompareTo("Admin") != 0)
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Info",
                        "alert('You are not allowed to access this menu (Role01).');", true);
                    Response.Redirect("~/Pages/Views/Loader.aspx", false);
                }
            }
        }

        public override void VerifyRenderingInServerForm(Control control)
        {

        }

        protected async void btnSearch1_Click(object sender, EventArgs e)
        {
            String from = txtFrom1.Text == null ? "" : txtFrom1.Text,
                   to = txtTo1.Text == null ? "" : txtTo1.Text;

            if (from != "" && to != "")
            {
                from = Convert.ToDateTime(from).ToString("yyyy-MM-dd HH:mm:ss",
                                 System.Globalization.CultureInfo.InvariantCulture);
                to = Convert.ToDateTime(to).ToString("yyyy-MM-dd HH:mm:ss",
                                 System.Globalization.CultureInfo.InvariantCulture);

                _Table = await new USSDHelper().SearchUserProfiles(from, to);
                if (_Table.Rows.Count > 0)
                {
                    userAccountsTable.DataSource = _Table;
                    userAccountsTable.DataBind();
                    Session[DATA] = _Table;
                    await StoreFootPrint("Search User Accounts");
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Info",
                        "alert('No data found! Please enter valid date period.');", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "Error",
                  "alert('Empty date inputs !');", true);
            }
        }

        protected async void btnToPDF1_Click(object sender, EventArgs e)
        {
            if (Session["State"] == null)
            {
                Session[DATA] = EnablePrintingMode(
                                  new string[] { "State", "PDF" },
                                  (DataTable)Session[DATA],
                                  true);
                userAccountsTable.DataSource = (DataTable)Session[DATA];
                userAccountsTable.DataBind();
            }
            else
            {
                if ((bool)Session[ISENABLED1])
                {
                    String FileName = "UserAccountsReport" + Session[PDF_COUNT1] + "-"
                                      + DateTime.Today.ToString().Split(new char[] { ' ' })[0]
                                                .Replace("/", "-") + ".pdf";
                    PDFReport report = (PDFReport)ReportFactory.create(ReportFactory.ReportType.PDF);
                    string path = Server.MapPath(@"~/Pages/Views/Store/" + FileName);
                    string dir = Server.MapPath(@"~/Pages/Views/Store/");
                    string imgPath = Server.MapPath(@"~/Pages/Views/Images/Bank.jpg");
                    try
                    {
                        DataTable dtNew = PreparePrinting((DataTable)Session[DATA]);
                        // clear selected row
                        Session[DATA] = EnablePrintingMode(
                                        new string[] { "State", "PDF" },
                                        (DataTable)Session[DATA],
                                        false);
                        userAccountsTable.DataSource = (DataTable)Session[DATA];
                        userAccountsTable.DataBind();

                        report.generatePDF(dtNew, "Report of User Accounts", path, imgPath, dir);
                        Session[PDF_COUNT1] = (int)Session[PDF_COUNT1] + 1;
                        await StoreFootPrint("Export User Accounts to PDF");
                        ClientScript.RegisterStartupScript(this.GetType(), "Info",
                            "alert('Sucessfully generated PDF file !');", true);
                        // load PDF
                        OpenPDF(FileName);
                    }
                    catch (IOException)
                    {
                        //ClientScript.RegisterStartupScript(this.GetType(), "Error",
                        //    "alert('Another PDF file is open. Please close it and try again !');", true);
                    }
                }
            }
        }

        protected async void btnToExcel1_Click(object sender, EventArgs e)
        {
            if ((bool)Session[ISENABLED1])
            {
                String FileName = "UserAccountsReport" + Session[EXCEL_COUNT1] + "-"
                                  + DateTime.Today.ToString().Split(new char[] { ' ' })[0]
                                            .Replace("/", "-") + ".xls";
                ExcelReport report = (ExcelReport)ReportFactory.create(ReportFactory.ReportType.EXCEL);
                string path = Server.MapPath(@"~/Pages/Views/Store/" + FileName);
                string dir = Server.MapPath(@"~/Pages/Views/Store/");
                string imgPath = Server.MapPath(@"~/Pages/Views/Images/Bank.jpg");
                try
                {
                    report.generateExcel((DataTable)Session[DATA], "User Accounts", path, imgPath, dir);
                    Session[EXCEL_COUNT1] = (int)Session[EXCEL_COUNT1] + 1;
                    await StoreFootPrint("Export User Accounts to Excel");
                    ClientScript.RegisterStartupScript(this.GetType(), "Info",
                        "alert('Sucessfully generated Excel file !!');", true);
                    startExcelFile(path);
                }
                catch (IOException)
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Error",
                        "alert('Another Excel file is open. Please close it and try again !');", true);
                }
            }
        }

        private void startExcelFile(string path)
        {
            FileInfo file = new FileInfo(path);
            Response.Clear();
            Response.Charset = "UTF-8";
            Response.ContentEncoding = System.Text.Encoding.UTF8;
            Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
            Response.AddHeader("Content-Length", file.Length.ToString());
            Response.ContentType = "application/ms-excel";
            Response.WriteFile(file.FullName);
            Response.End();
        }

        private async Task LoadUserAccounts(string action)
        {
            userAccountsTable.Visible = true;
            _Table = await new USSDHelper().UserProfiles();
            int NumRows = _Table.Rows.Count;
            if(NumRows > 0)
            {
                userAccountsTable.DataSource = _Table;
                userAccountsTable.DataBind();
                Session[ISENABLED1] = true;
                Session[DATA] = _Table;
                await StoreFootPrint(action);
            }
        }

        private async Task StoreFootPrint(string action)
        {
            USSDHelper helper = new USSDHelper();
            FootPrint fp = new FootPrint();
            fp.Id = Guid.NewGuid();
            fp.UserAction = action;
            fp.AccessedBy = Session[USER_ID].ToString();
            fp.AccessedOn = DateTime.Now;
            fp.BranchID = Session[BRANCH_ID].ToString();
            fp.GSMOperationID = Session[GSM_OP_ID].ToString();
            fp.Role = Session[ROLE_ID].ToString();
            bool result = await helper.AddFootPrint(fp);
            if (!result)
            {
                ServiceLogger _Logger = (ServiceLogger)LogFactory.create(Logger.LoggingMode.SERVICE);
                _Logger.Log(TraceLevel.Error, "Adding FootPrint Failed !");
            }
        }

        protected async void userAccountsTable_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            string UserID = userAccountsTable.DataKeys[e.RowIndex].Value.ToString();
            USSDHelper helper = new USSDHelper();
            bool result = await helper.CloseUserProfile(UserID);
            if (!result)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "Info",
                  "alert('Closing user " + UserID + " failed !');", true);
                ServiceLogger _Logger = (ServiceLogger)LogFactory.create(Logger.LoggingMode.SERVICE);
                _Logger.Log(TraceLevel.Error, "Closing User Account Failed !");
            }
            else
            {
                await LoadUserAccounts("Close User Account #" + UserID);
                ClientScript.RegisterStartupScript(this.GetType(), "Info",
                  "alert('Sucessfully deleted user " + UserID + "');", true);
            }
        }

        protected async void userAccountsTable_RowEditing(object sender, GridViewEditEventArgs e)
        {
            userAccountsTable.EditIndex = e.NewEditIndex;
            string UserID = userAccountsTable.DataKeys[e.NewEditIndex].Value.ToString();
            await LoadUserAccounts("Edit User Account #" + UserID);
        }

        protected async void userAccountsTable_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            string UserID = userAccountsTable.DataKeys[e.RowIndex].Value.ToString();
            userAccountsTable.EditIndex = -1;
            await LoadUserAccounts("Cancel Change #" + UserID);
        }
        
        protected async void userAccountsTable_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            UserProfile up = new UserProfile();
            up.Id = Guid.Parse(((TextBox)userAccountsTable.Rows[e.RowIndex].FindControl("useraccId")).Text);
            up.Email = ((TextBox)userAccountsTable.Rows[e.RowIndex].FindControl("txtemail")).Text;
            up.UserID = ((TextBox)userAccountsTable.Rows[e.RowIndex].FindControl("txtuserid")).Text;
            up.Password = ((TextBox)userAccountsTable.Rows[e.RowIndex].FindControl("txtpassword")).Text;
            up.Role = ((TextBox)userAccountsTable.Rows[e.RowIndex].FindControl("txtrole")).Text;
            up.BranchID = ((TextBox)userAccountsTable.Rows[e.RowIndex].FindControl("txtbranchid")).Text;
            up.GSMOperationID = ((TextBox)userAccountsTable.Rows[e.RowIndex].FindControl("txtgsmoperationid")).Text;
            up.SessionID = ((TextBox)userAccountsTable.Rows[e.RowIndex].FindControl("txtsessionid")).Text;
            up.HasChanged = bool.Parse(((TextBox)userAccountsTable.Rows[e.RowIndex].FindControl("txthaschanged")).Text);
            up.Active = bool.Parse(((TextBox)userAccountsTable.Rows[e.RowIndex].FindControl("txtactive")).Text);
            up.LogInDate = DateTime.Parse(((TextBox)userAccountsTable.Rows[e.RowIndex].FindControl("txtlogindate")).Text);
            up.LogOutDate = DateTime.Parse(((TextBox)userAccountsTable.Rows[e.RowIndex].FindControl("txtlogoutdate")).Text);
            userAccountsTable.EditIndex = -1;
            USSDHelper helper = new USSDHelper();
            bool result = await helper.UpdateUserProfile(up);
            if (!result)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "Info",
                    "alert('Updating user " + up.UserID + " failed !');", true);
                ServiceLogger _Logger = (ServiceLogger)LogFactory.create(Logger.LoggingMode.SERVICE);
                _Logger.Log(TraceLevel.Error, "Updating User Account Failed !");
            }
            else
            {
                await LoadUserAccounts("Update User Account #" + up.UserID);
                ClientScript.RegisterStartupScript(this.GetType(), "Info",
                    "alert('Sucessfully updated user " + up.UserID + "');", true);
            }
        }

        protected void userAccountsTable_PageIndexChanging(Object sender, GridViewPageEventArgs e)
        {
            userAccountsTable.DataSource = (DataTable)Session[DATA];
            userAccountsTable.PageIndex = e.NewPageIndex;
            userAccountsTable.DataBind();
        }

        protected void userAccountsTable_Sorting(object sender, GridViewSortEventArgs e)
        {
            bool state = (Session["State"] != null)
                          ? ((string)Session["State"]).CompareTo("Select") == 0
                          : false;
            if (!state)
            {
                DataTable dataTable = (DataTable)Session[DATA];
                if (dataTable != null)
                {
                    string sortPattern = BuildSortPattern("Sort", e.SortDirection, e.SortExpression);
                    dataTable.DefaultView.Sort = sortPattern;
                    Session[DATA] = dataTable;
                    userAccountsTable.DataSource = dataTable;
                    userAccountsTable.DataBind();
                }
            }
            else
            {
                DataTable dt = (DataTable)Session[DATA];
                int size = dt.Columns.Count;
                string columnName = e.SortExpression;
                for (int i = 0; i < size; i++)
                {
                    string column = dt.Columns[i].ColumnName;
                    if (column.Contains(columnName))
                    {
                        dt.Columns[i].ColumnName = column.Replace("\u2714 ", "");
                        break;
                    }
                }
                Session[DATA] = dt;
                userAccountsTable.DataSource = (DataTable)Session[DATA];
                userAccountsTable.DataBind();
            }
        }

        private string BuildSortPattern(string cacheKey, SortDirection sortDirection,
                                string sortExpression)
        {
            string dir = ConvertSortDirection(sortDirection),
                   sortPattern = (Session[cacheKey] != null)
                                  ? (string)Session[cacheKey] : "";
            if (sortPattern == "")
            {
                sortPattern = sortExpression + " " + dir;
                Session[cacheKey] = sortPattern;
            }
            else
            {
                if (sortPattern.Contains(sortExpression))
                {
                    sortPattern = sortPattern.Replace(" desc", " ")
                                             .Replace(" asc", " ");
                    sortPattern = sortPattern + dir;
                }
                else
                {
                    sortPattern = sortPattern.Replace(" desc", ",")
                                             .Replace(" asc", ",");
                    sortPattern = sortPattern + sortExpression + " " + dir;
                }
            }

            return sortPattern;
        }

        private string ConvertSortDirection(SortDirection sortDirection)
        {
            string newSortDirection = String.Empty;

            switch (sortDirection)
            {
                case SortDirection.Ascending:
                    newSortDirection = "asc";
                    break;

                case SortDirection.Descending:
                    newSortDirection = "desc";
                    break;
            }

            return newSortDirection;
        }

        private void OpenPDF(string FileName)
        {
            string viewPage = Server.MapPath(@"~/Pages/Views/Store/" + FileName);
            FileStream fs = new FileStream(viewPage, System.IO.FileMode.Open, System.IO.FileAccess.Read);
            int fs_len = (int)fs.Length;
            byte[] ar = new byte[fs_len];
            fs.Read(ar, 0, fs_len);
            fs.Close();

            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("Accept-Header", fs_len.ToString());
            Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName + ".pdf");
            Response.AddHeader("Expires", "0");
            Response.AddHeader("Pragma", "cache");
            Response.AddHeader("Cache-Control", "private");
            Response.ContentType = "application/pdf";
            Response.AddHeader("Accept-Ranges", "bytes");
            Response.BinaryWrite(ar);
            Response.Flush();
            try { Response.End(); }
            catch { }
        }

        private DataTable EnablePrintingMode(string[] StateName, DataTable dt, bool state)
        {
            int colCount = dt.Columns.Count;
            for (int i = 0; i < colCount; i++)
            {
                string column = dt.Columns[i].ColumnName;
                if (state)
                    dt.Columns[i].ColumnName = "\u2714 " + column;
                else
                    dt.Columns[i].ColumnName = column.Replace("\u2714 ", "");
            }

            if (state)
            {
                Session[StateName[0]] = "Select";
                Session[StateName[1]] = new System.Web.UI.WebControls.Image
                {
                    ImageUrl = "Images/select.png",
                    Width = 35,
                    Height = 35,
                    BorderColor = Color.Transparent
                };
                btnToPDF1.Controls.Add((System.Web.UI.WebControls.Image)Session[StateName[1]]);
            }
            else
            {
                Session[StateName[0]] = null;
                btnToPDF1.Controls.Remove((System.Web.UI.WebControls.Image)Session[StateName[1]]);
                btnToPDF1.Controls.Add(new System.Web.UI.WebControls.Image
                {
                    ImageUrl = "Images/pdf.png",
                    Width = 35,
                    Height = 35,
                    BorderColor = Color.Transparent
                });
            }

            return dt;
        }

        private DataTable PreparePrinting(DataTable dt)
        {
            DataTable dtNew = dt.Copy();
            int colCount = dt.Columns.Count;
            if (colCount > 0)
            {
                for (int i = 0; i < colCount; i++)
                {
                    DataColumn c = dt.Columns[i];
                    if (!c.ColumnName.Contains("\u2714 "))
                        dtNew.Columns.Remove(c.ColumnName);
                }
                int colNewCount = dtNew.Columns.Count;
                for (int i = 0; i < colNewCount; i++)
                {
                    string column = dtNew.Columns[i].ColumnName;
                    if (column.Contains("\u2714 "))
                        dtNew.Columns[i].ColumnName = column.Replace("\u2714 ", "");
                }
            }

            return dtNew;
        }
    }
}