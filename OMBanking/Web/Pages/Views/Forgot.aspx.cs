﻿using OMBanking.Web.DB;
using OMBanking.Web.Log;
using OMBanking.Web.Proxy.Bank;
using OMBanking.Web.Security;
using System;
using System.Configuration;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;

namespace OMBanking.Web.Pages.Views
{
    public partial class Forgot : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected async void btnSendRequest_Click(object sender, EventArgs e)
        {
            if(isValidEmail(email.Text))
            {
                USSDHelper helper = new USSDHelper();
                bool HasChanged = await helper.ForgotPassword(email.Text);
                if (HasChanged)
                {
                    await SendEmail();
                    txtConfirm.ForeColor = Color.Green;
                    txtConfirm.Text = "<center>Successfully sent reset link to " + email.Text + ", please check it! Thank you.</center>";
                }
                else
                {
                    txtConfirm.ForeColor = Color.Red;
                    txtConfirm.Text = "<center>Does not have an account, please sign up.</center>";
                }
            }
        }

        bool isValidEmail(string userId)
        {
            return userId != null && userId != ""
                    && !userId.Contains('"') && !userId.Contains('\'')
                    && !userId.Contains('\\') && !userId.Contains('/')
                    && !userId.Contains('~') && !userId.Contains('%')
                    && !userId.Contains('#')
                    && !userId.Contains('&') && !userId.Contains('*')
                    && !userId.Contains('!') && !userId.Contains('^')
                    && !userId.Contains('$') && !userId.Contains(',')
                    && !userId.Contains(';') && !userId.Contains('(')
                    && !userId.Contains(')') && !userId.Contains('{')
                    && !userId.Contains('}') && !userId.Contains('[')
                    && !userId.Contains(']') && !userId.Contains('`')
                    && !userId.Contains('?') && !userId.Contains('<')
                    && !userId.Contains('>') && !userId.Contains(':')
                    && !userId.Contains('=') && !userId.Contains('+')
                    && !userId.Contains('|');
        }

        private async Task SendEmail()
        {
            try
            {
                USSDHelper helper = new USSDHelper();
                string userId = await helper.GetUserByEmail(email.Text);
                String UserSessionID = DataEncryptor.EncryptString(await helper.GetSessionID(email.Text));

                StringBuilder sb = new StringBuilder();
                Utils.BaseSiteUrl = ConfigurationManager.AppSettings.Get("unics_prod_endpoint_url");
                String UriPostfix = "Pages/Views/ResetPassword.aspx";

                sb.Append("Hi " + userId + ",<br/><br/> Click on the link below to reset your password.<br/><br/>");
                sb.Append("<a href=\"" + Utils.BaseSiteUrl + UriPostfix + "?sid=" + UserSessionID);
                sb.Append("&email=" + Utils.Base64Encode(email.Text) + "&userId=" + Utils.Base64Encode(userId) + "\">" + Utils.BaseSiteUrl
                          + UriPostfix + "?sid=" + UserSessionID
                          + "&email=" + Utils.Base64Encode(email.Text) + "&userId=" + Utils.Base64Encode(userId) + " </a><br/>");
                sb.Append("<br/><br/>");
                sb.Append("Thanks, <br/>");
                sb.Append("------------ <br/>");
                sb.Append(ConfigurationManager.AppSettings.Get("bank_name"));

                var fromAddress = new MailAddress(ConfigurationManager.AppSettings.Get("b2w_unics_email"), "OM-B2W");
                var toAddress = new MailAddress(email.Text, userId);
                string fromPassword = Utils.Base64Decode(ConfigurationManager.AppSettings.Get("b2w_unics_password"));
                string subject = "Change Your Password";
                string body = sb.ToString();

                var smtp = new SmtpClient
                {
                    Host = ConfigurationManager.AppSettings.Get("smtp_host"),
                    Port = int.Parse(ConfigurationManager.AppSettings.Get("smtp_port")),
                    EnableSsl = bool.Parse(ConfigurationManager.AppSettings.Get("ssl_enabled")),
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    Credentials = new NetworkCredential(fromAddress.Address, fromPassword),
                    Timeout = 20000
                };
                using (var message = new MailMessage(fromAddress, toAddress)
                {
                    Subject = subject,
                    Body = body,
                    IsBodyHtml = true,
                    Priority = MailPriority.High
                })
                {
                    smtp.Send(message);
                }
            }
            catch (Exception e)
            {
                ServiceLogger _Logger = (ServiceLogger)LogFactory.create(LogBase.LoggingMode.SERVICE);
                _Logger.Log(TraceLevel.Error, e.Message);
            }
        }

    }
}