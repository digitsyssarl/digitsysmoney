﻿using OMBanking.Web.Bank;
using OMBanking.Web.DB;
using OMBanking.Web.Log;
using OMBanking.Web.Proxy.Bank;
using OMBanking.Web.Security;
using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading;
using System.Web.UI;

namespace OMBanking.Web.Pages.Views
{
    public partial class SignUp : Page
    {
        private const string SESSION_ID = "sid";
        private const String LOGIN_ID = "lid";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                Session[SESSION_ID] = Session.SessionID;
                NameValueCollection SessionParams = Request.QueryString;
                string LoginID = (SessionParams[LOGIN_ID] != null) ? SessionParams[LOGIN_ID] : "";
                Session[LOGIN_ID] = Utils.Base64Decode(LoginID);
            }
        }

        protected async void btnSignUp_Click(object sender, EventArgs e)
        {
            if(isValidID(userID.Text, false)
               && isValidID(email.Text, true)
               && pwd.Text != null && pwd.Text != ""
               && pwd.Text.Length >= 8)
            {
                string Id = (string)Session[LOGIN_ID];
                if ((Id.CompareTo("customer") == 0)
                    && !Utils.IsDigitsOnly(userID.Text))
                {
                    txtConfirm.ForeColor = Color.Red;
                    txtConfirm.Text = "<center>User ID should be a phone number !</center>";
                }
                else
                {
                    UserProfile user = new UserProfile();
                    user.Id = Guid.NewGuid();
                    user.Email = email.Text;
                    user.UserID = userID.Text;
                    user.Password = DataEncryptor.EncryptString(pwd.Text);
                    if (Id.CompareTo("customer") != 0)
                        user.Role = roleList.SelectedValue;
                    else
                        user.Role = "Client";

                    user.BranchID = branchIDList.SelectedValue;
                    user.HasChanged = false;
                    user.Active = true;
                    user.SessionID = Session[SESSION_ID].ToString();
                    bool result = await new USSDHelper().UserExists(user);
                    if (result)
                    {
                        txtConfirm.ForeColor = Color.Red;
                        txtConfirm.Text = "<center>User ID or Password is already used. Please try another one !</center>";
                    }
                    if (email.Text.Contains(ConfigurationManager.AppSettings.Get("unics_email_domain")))
                    {
                        int ErrCode = await new USSDHelper().AddNewUserProfile(user);
                        if (ErrCode == 0)
                        {
                            SendEmail();
                            txtConfirm.ForeColor = Color.Green;
                            txtConfirm.Text = "<center>Check your email to validate your account ! Redirecting ...</center>";
                            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "Redirecting...",
                                "setTimeout(function() { window.location.replace('Login.aspx') }, 5000);", true);
                        }
                        else
                        {
                            txtConfirm.ForeColor = Color.Red;
                            txtConfirm.Text = "<center>Bank Technical Error (E21)</center>";
                        }
                    }
                    else
                    {
                        if (user.Role.CompareTo("Client") == 0)
                        {
                            SendEmail();
                            txtConfirm.ForeColor = Color.Green;
                            txtConfirm.Text = "<center>Check your email to validate your account ! Redirecting ...</center>";
                            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "Redirecting...",
                                "setTimeout(function() { window.location.replace('Login.aspx') }, 5000);", true);
                        }
                        else
                        {
                            txtConfirm.ForeColor = Color.Red;
                            txtConfirm.Text = "<center>Not authorized email domain !</center>";
                        }
                    }
                }
            }
            else
            {
                txtConfirm.ForeColor = Color.Red;
                txtConfirm.Text = "<center>Invalid User ID or Password (min. 8 characters)</center>";
            }
        }

        bool isValidID(string userId, bool email)
        {
            return userId != null && userId != ""
                    && !userId.Contains('"') && !userId.Contains('\'')
                    && !userId.Contains('\\') && !userId.Contains('/')
                    && !userId.Contains('~') && !userId.Contains('%')
                    && !userId.Contains('#') && ((email) ? true : !userId.Contains('@'))
                    && !userId.Contains('&') && !userId.Contains('*')
                    && !userId.Contains('!') && !userId.Contains('^')
                    && !userId.Contains('$') && !userId.Contains(',')
                    && !userId.Contains(';') && !userId.Contains('(')
                    && !userId.Contains(')') && !userId.Contains('{')
                    && !userId.Contains('}') && !userId.Contains('[')
                    && !userId.Contains(']') && !userId.Contains('`')
                    && !userId.Contains('?') && !userId.Contains('<')
                    && !userId.Contains('>') && !userId.Contains(':')
                    && !userId.Contains('=') && !userId.Contains('+')
                    && !userId.Contains('|');
        }


        private void SendEmail()
        {
            try
            {
                string SessionID = DataEncryptor.EncryptString(Session[SESSION_ID].ToString());
                StringBuilder sb = new StringBuilder();
                Utils.BaseSiteUrl = ConfigurationManager.AppSettings.Get("unics_prod_endpoint_url");
                string UriPostfix = "Pages/Views/Login.aspx";

                sb.Append("Hi "+ userID.Text + ",<br/><br/> Welcome to the B2W platform !<br/><br/> "
                          + "Click on the link below to validate your registration.<br/>");
                sb.Append("<a href=\""+ Utils.BaseSiteUrl + UriPostfix + "?sid=" + SessionID);
                sb.Append("&email=" + Utils.Base64Encode(email.Text) + "\">"+ Utils.BaseSiteUrl 
                          + UriPostfix + "?sid=" + SessionID 
                          + "&email=" + Utils.Base64Encode(email.Text) +" </a><br/>");
                sb.Append("<br/><br/>");
                sb.Append("Thanks, <br/>");
                sb.Append("------------ <br/>");
                sb.Append(ConfigurationManager.AppSettings.Get("bank_name"));

                var fromAddress = new MailAddress(ConfigurationManager.AppSettings.Get("b2w_unics_email"), "om-b2w");
                var toAddress = new MailAddress(email.Text, userID.Text);
                string fromPassword = Utils.Base64Decode(ConfigurationManager.AppSettings.Get("b2w_unics_password"));
                string subject = "Validate Your Account";
                string body = sb.ToString();

                var smtp = new SmtpClient
                {
                    Host = ConfigurationManager.AppSettings.Get("smtp_host"),
                    Port = int.Parse(ConfigurationManager.AppSettings.Get("smtp_port")),
                    EnableSsl = bool.Parse(ConfigurationManager.AppSettings.Get("ssl_enabled")),
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    Credentials = new NetworkCredential(fromAddress.Address, fromPassword),
                    Timeout = 20000
                };
                using (var message = new MailMessage(fromAddress, toAddress)
                {
                    Subject = subject,
                    Body = body,
                    IsBodyHtml = true,
                    Priority = MailPriority.High
                })
                {
                    smtp.Send(message);
                }
            }
            catch (Exception e)
            {
                ServiceLogger _Logger = (ServiceLogger) LogFactory.create(LogBase.LoggingMode.SERVICE);
                _Logger.Log(TraceLevel.Error, e.Message);
            }
        }
    }
}