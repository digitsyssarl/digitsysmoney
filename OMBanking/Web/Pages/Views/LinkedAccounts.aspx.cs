﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using OMBanking.Web.DB;
using System.Data;
using OMBanking.Web.Bank;
using System.IO;
using System.Collections.Specialized;
using OMBanking.Web.Reporting;
using OMBanking.Web.Security;
using OMBanking.Web.Proxy.Bank;
using System.Threading.Tasks;
using OMBanking.Web.Log;
using System.Diagnostics;
using System.Web.UI.WebControls;
using System.Drawing;
using OMBanking.Web.Proxy.B2W.Register;
using OMBanking.Web.B2W.Orange.Register;
using System.Linq;
using System.Configuration;

namespace OMBanking.Web.Pages.Views
{
    public partial class LinkedAccounts : Page
    {
        private DataTable _Table;
        private DataTable _Table1;
        private const string PDF_COUNT = "PDF_COUNT";
        private const string PDF_COUNT1 = "PDF_COUNT1";
        private const string EXCEL_COUNT = "EXCEL_COUNT";
        private const string EXCEL_COUNT1 = "EXCEL_COUNT1";
        private const string ISENABLED = "ISENABLED";
        private const string ISENABLED1 = "ISENABLED1";
        private const string DATA = "TABLE";
        private const string DATA1 = "TABLE1";
        private const String SESSION_ID = "sid";
        private const String USER_ID = "userId";
        private const String BRANCH_ID = "branchId";
        private const String GSM_OP_ID = "gsmOpId";
        private const String ROLE_ID = "role";

        protected async void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                CacheSessionParams();
                await LoadLinkedAccounts();
                await LoadFootPrintTable("Linked Account", true);
            }
        }

        private void CacheSessionParams()
        {
            if (Session[PDF_COUNT] == null)
                Session[PDF_COUNT] = 0;
            if (Session[EXCEL_COUNT] == null)
                Session[EXCEL_COUNT] = 0;
            if (Session[ISENABLED] == null)
                Session[ISENABLED] = false;
            if (Session[PDF_COUNT1] == null)
                Session[PDF_COUNT1] = 0;
            if (Session[EXCEL_COUNT1] == null)
                Session[EXCEL_COUNT1] = 0;
            if (Session[ISENABLED1] == null)
                Session[ISENABLED1] = false;
            NameValueCollection SessionParams = Request.QueryString;
            Session[USER_ID] = (SessionParams[USER_ID] != null)
                                ? Utils.Base64Decode(SessionParams[USER_ID].ToString())
                                : "";
            Session[BRANCH_ID] = (SessionParams[BRANCH_ID] != null)
                                 ? Utils.Base64Decode(SessionParams[BRANCH_ID].ToString())
                                 : "";
            Session[GSM_OP_ID] = (SessionParams[GSM_OP_ID] != null)
                                 ? Utils.Base64Decode(SessionParams[GSM_OP_ID].ToString())
                                 : "";
            Session[ROLE_ID] = (SessionParams[ROLE_ID] != null)
                                ? Utils.Base64Decode(SessionParams[ROLE_ID].ToString())
                                : "";

            if ((string)Session[USER_ID] == ""
                || (string)Session[BRANCH_ID] == ""
                || (string)Session[GSM_OP_ID] == ""
                || (string)Session[ROLE_ID] == "")
            {
                Response.Redirect("~/Pages/Views/Login.aspx", false);
            }
            else
            {
                string Role = (string)Session[ROLE_ID];
                if (Role.CompareTo("Supervisor") == 0)
                {
                    btnValidate.Visible = true;
                    btnSelectAll.Visible = true;
                    btnDelete.Visible = true;
                }
            }
            btnValidate.BackColor = Color.FromName(ConfigurationManager.AppSettings.Get("color"));
        }

        private void AddColumnHeader()
        {
            _Table = new DataTable();
            _Table.Columns.Add("FirstName", typeof(string));
            _Table.Columns.Add("LastName", typeof(string));
            _Table.Columns.Add("AccountNo", typeof(string));
            _Table.Columns.Add("PhoneNo", typeof(string));
            _Table.Columns.Add("DateLinked", typeof(DateTime));
            _Table.Columns.Add("Operation", typeof(string));
            _Table.Columns.Add("Alias", typeof(string));
            _Table.Columns.Add("State", typeof(string));
            _Table.Columns.Add("SupervisedBy", typeof(string));
            _Table.Columns.Add("SupervisedOn", typeof(DateTime));
            _Table.Columns.Add("AuditedBy", typeof(string));
            _Table.Columns.Add("AuditedOn", typeof(DateTime));
            _Table.Columns.Add("Role", typeof(string));
            _Table.Columns.Add("BranchID", typeof(string));
            _Table.Columns.Add("GSMOperationID", typeof(string));
            _Table.Columns.Add("Comments", typeof(string));

        }

        private void AddColumnHeader1()
        {
            _Table1 = new DataTable();
            _Table1.Columns.Add("Role", typeof(string));
            _Table1.Columns.Add("UserAction", typeof(string));
            _Table1.Columns.Add("AccessedBy", typeof(string));
            _Table1.Columns.Add("AccessedOn", typeof(DateTime));
            _Table1.Columns.Add("BranchID", typeof(string));
            _Table1.Columns.Add("GSMOperationID", typeof(string));

        }

        protected async void btnSearch_Click(object sender, EventArgs e)
        {
            String from = txtFrom.Text == null ? "" : txtFrom.Text,
                   to = txtTo.Text == null ? "" : txtTo.Text;
            if (from != "" && to != "")
            {
                from = Convert.ToDateTime(from).ToString("yyyy-MM-dd HH:mm:ss",
                                 System.Globalization.CultureInfo.InvariantCulture);
                to = Convert.ToDateTime(to).ToString("yyyy-MM-dd HH:mm:ss",
                                 System.Globalization.CultureInfo.InvariantCulture);

                string Role = Session[ROLE_ID].ToString();
                string BranchID = Session[BRANCH_ID].ToString();
                USSDHelper helper = new USSDHelper();
                List<LinkedAccount> linkedAccounts = (Role.CompareTo("Auditor") == 0
                    || Role.CompareTo("Admin") == 0)
                    ? await helper.SearchLinkedAccounts(from, to)
                    : ((Role.CompareTo("Agent") == 0)
                        ? await helper.SearchLinkedAccounts(from, to,
                                    Role, Session[BRANCH_ID].ToString(),
                                    Session[GSM_OP_ID].ToString())
                        : await helper.SearchLinkedAccounts(from, to,
                                    null, ((BranchID.CompareTo("00") == 0) ? null : BranchID),
                                    Session[GSM_OP_ID].ToString())); //Supervisor
                AddColumnHeader();
                if (linkedAccounts != null)
                {
                    FillLinkedAccountsTable(linkedAccounts);
                    linkedAccountTable.DataSource = _Table;
                    linkedAccountTable.DataBind();
                    Session[DATA] = _Table;
                    await StoreFootPrint("Search Linked Accounts");
                    await LoadFootPrintTable("Linked Account", false);
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Info",
                        "alert('No data found! Please enter valid date period.');", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "Error",
                    "alert('Empty date inputs !');", true);
            }
        }

        protected async void btnToPDF_Click(object sender, EventArgs e)
        {
            if (Session["State"] == null)
            {
                Session[DATA] = EnablePrintingMode(
                                 new string[] { "State", "PDF" },
                                 (DataTable)Session[DATA],
                                 true);
                linkedAccountTable.DataSource = (DataTable)Session[DATA];
                linkedAccountTable.DataBind();
            }
            else
            {
                if ((bool)Session[ISENABLED])
                {
                    String FileName = "LinkedAccountsReport" + Session[PDF_COUNT] + "-"
                                      + DateTime.Today.ToString().Split(new char[] { ' ' })[0]
                                                .Replace("/", "-") + ".pdf";
                    PDFReport report = (PDFReport)ReportFactory.create(ReportFactory.ReportType.PDF);
                    string path = Server.MapPath(@"~/Pages/Views/Store/" + FileName);
                    string dir = Server.MapPath(@"~/Pages/Views/Store/");
                    string imgPath = Server.MapPath(@"~/Pages/Views/Images/Bank.jpg");
                    try
                    {
                        DataTable dtNew = PreparePrinting((DataTable)Session[DATA]);
                        // clear selected row
                        Session[DATA] = EnablePrintingMode(
                                         new string[] { "State", "PDF" },
                                         (DataTable)Session[DATA],
                                         false);
                        linkedAccountTable.DataSource = (DataTable)Session[DATA];
                        linkedAccountTable.DataBind();

                        report.generatePDF(dtNew, "Report of Linked Accounts", path, imgPath, dir);
                        Session[PDF_COUNT] = (int)Session[PDF_COUNT] + 1;
                        await StoreFootPrint("Export Linked Accounts to PDF");
                        await LoadFootPrintTable("Linked Account", false);
                        ClientScript.RegisterStartupScript(this.GetType(), "Info",
                            "alert('Sucessfully generated PDF file !');", true);
                        // load PDF
                        OpenPDF(FileName);
                    }
                    catch (IOException)
                    {
                        //ClientScript.RegisterStartupScript(this.GetType(), "Error",
                        //    "alert('Another PDF file is open. Please close it and try again !');", true);
                    }
                }
            }
        }

        protected async void btnToExcel_Click(object sender, EventArgs e)
        {
            if ((bool)Session[ISENABLED])
            {
                String FileName = "LinkedAccountsReport" + Session[EXCEL_COUNT] + "-"
                                  + DateTime.Today.ToString().Split(new char[] { ' ' })[0]
                                            .Replace("/", "-") + ".xls";
                ExcelReport report = (ExcelReport)ReportFactory.create(ReportFactory.ReportType.EXCEL);
                string path = Server.MapPath(@"~/Pages/Views/Store/" + FileName);
                string dir = Server.MapPath(@"~/Pages/Views/Store/");
                string imgPath = Server.MapPath(@"~/Pages/Views/Images/Bank.jpg");
                try
                {
                    report.generateExcel((DataTable)Session[DATA], "Report of Linked Accounts", path, imgPath, dir);
                    Session[EXCEL_COUNT] = (int)Session[EXCEL_COUNT] + 1;
                    await StoreFootPrint("Export Linked Accounts to Excel");
                    await LoadFootPrintTable("Linked Account", false);
                    ClientScript.RegisterStartupScript(this.GetType(), "Info",
                        "alert('Sucessfully generated Excel file !');", true);
                    startExcelFile(path);
                }
                catch (IOException)
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Error",
                        "alert('Another Excel file is open. Please close it and try again !');", true);
                }
            }
        }

        protected async void btnToPDF1_Click(object sender, EventArgs e)
        {
            if (Session["State1"] == null)
            {
                Session[DATA1] = EnablePrintingMode(
                                 new string[] { "State1", "PDF1" },
                                 (DataTable)Session[DATA1],
                                 true);
                responseTable.DataSource = (DataTable)Session[DATA1];
                responseTable.DataBind();
            }
            else
            {
                if ((bool)Session[ISENABLED1])
                {
                    String FileName = "UserActionHistory_LinkedAccounts"
                                      + Session[PDF_COUNT1] + "-"
                                      + DateTime.Today.ToString().Split(new char[] { ' ' })[0]
                                                .Replace("/", "-") + ".pdf";
                    PDFReport report = (PDFReport)ReportFactory.create(ReportFactory.ReportType.PDF);
                    string path = Server.MapPath(@"~/Pages/Views/Store/" + FileName);
                    string dir = Server.MapPath(@"~/Pages/Views/Store/");
                    string imgPath = Server.MapPath(@"~/Pages/Views/Images/Bank.jpg");
                    try
                    {
                        DataTable dtNew = PreparePrinting((DataTable)Session[DATA1]);
                        // clear selected row
                        Session[DATA1] = EnablePrintingMode(
                                         new string[] { "State1", "PDF1" },
                                         (DataTable)Session[DATA1],
                                         false);
                        responseTable.DataSource = (DataTable)Session[DATA1];
                        responseTable.DataBind();

                        report.generatePDF(dtNew, "User Action History - Linked Accounts", path, imgPath, dir);
                        Session[PDF_COUNT1] = (int)Session[PDF_COUNT1] + 1;
                        await StoreFootPrint("Export Linked Account Actions to PDF");
                        await LoadFootPrintTable("Linked Account", false);
                        ClientScript.RegisterStartupScript(this.GetType(), "Info",
                            "alert('Sucessfully generated PDF file !');", true);
                        // load PDF
                        OpenPDF(FileName);
                    }
                    catch (IOException)
                    {
                        //ClientScript.RegisterStartupScript(this.GetType(), "Error",
                        //    "alert('Another PDF file is open. Please close it and try again !');", true);
                    }
                }
            }
        }

        protected async void btnToExcel1_Click(object sender, EventArgs e)
        {
            if ((bool)Session[ISENABLED1])
            {
                String FileName = "UserActionHistory_LinkedAccounts"
                                  + Session[EXCEL_COUNT1] + "-"
                                  + DateTime.Today.ToString().Split(new char[] { ' ' })[0]
                                            .Replace("/", "-") + ".xls";
                ExcelReport report = (ExcelReport)ReportFactory.create(ReportFactory.ReportType.EXCEL);
                string path = Server.MapPath(@"~/Pages/Views/Store/" + FileName);
                string dir = Server.MapPath(@"~/Pages/Views/Store/");
                string imgPath = Server.MapPath(@"~/Pages/Views/Images/Bank.jpg");
                try
                {
                    report.generateExcel((DataTable)Session[DATA1], "User Action History - Linked Accounts", path, imgPath, dir);
                    Session[EXCEL_COUNT1] = (int)Session[EXCEL_COUNT1] + 1;
                    await StoreFootPrint("Export Linked Account Actions to Excel");
                    await LoadFootPrintTable("Linked Account", false);
                    ClientScript.RegisterStartupScript(this.GetType(), "Info",
                        "alert('Sucessfully generated Excel file !');", true);
                    startExcelFile(path);
                }
                catch (IOException)
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Error",
                        "alert('Another Excel file is open. Please close it and try again !');", true);
                }
            }
        }

        private void startExcelFile(string path)
        {
            FileInfo file = new FileInfo(path);
            Response.Clear();
            Response.Charset = "UTF-8";
            Response.ContentEncoding = System.Text.Encoding.UTF8;
            Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
            Response.AddHeader("Content-Length", file.Length.ToString());
            Response.ContentType = "application/ms-excel";
            Response.WriteFile(file.FullName);
            Response.End();
        }

        private async Task LoadFootPrintTable(string id, bool start)
        {
            string Role = Session[ROLE_ID].ToString();
            string BranchID = Session[BRANCH_ID].ToString();
            string GSMOpID = Session[GSM_OP_ID].ToString();

            responseTable.Visible = true;
            USSDHelper helper = new USSDHelper();
            List<FootPrint> footPrints = await helper.FootPrintById(id, Role, BranchID, GSMOpID);
            AddColumnHeader1();
            if (footPrints != null)
            {
                FillFootPrintTable(footPrints);
                responseTable.DataSource = _Table1;
                responseTable.DataBind();
                Session[ISENABLED1] = true;
                Session[DATA1] = _Table1;
                //if (start)
                //    await StoreFootPrint("Consult Linked Account Actions");
            }
            else
            {
                _Table1.Rows.Add(_Table1.NewRow());
                responseTable.DataSource = _Table1;
                responseTable.DataBind();
                responseTable.Rows[0].Visible = false;
            }
        }

        private void FillFootPrintTable(List<FootPrint> footPrints)
        {
            foreach (FootPrint fp in footPrints)
            {
                DataRow _Row = _Table1.NewRow();
                _Row["Role"] = fp.Role;
                _Row["UserAction"] = fp.UserAction;
                _Row["AccessedBy"] = fp.AccessedBy.ToString();
                _Row["AccessedOn"] = fp.AccessedOn;
                _Row["BranchID"] = fp.BranchID;
                _Row["GSMOperationID"] = fp.GSMOperationID;
                _Table1.Rows.Add(_Row);
            }
        }

        private async Task LoadLinkedAccounts()
        {
            string Role = Session[ROLE_ID].ToString(),
                   BranchID = Session[BRANCH_ID].ToString();

            USSDHelper helper = new USSDHelper();
            List<LinkedAccount> linkedAccounts =
                (Role.CompareTo("Auditor") == 0
                    || Role.CompareTo("Admin") == 0)
                ? await helper.LinkedAccounts()
                : ((Role.CompareTo("Agent") == 0)
                    ? await helper.LinkedAccounts(Role,
                                    Session[BRANCH_ID].ToString(),
                                    Session[GSM_OP_ID].ToString())
                    : await helper.LinkedAccounts(null,
                                    ((BranchID.CompareTo("00") == 0) ? null : BranchID),
                                    Session[GSM_OP_ID].ToString())); //Supervisor
            AddColumnHeader();
            if (linkedAccounts != null)
            {
                FillLinkedAccountsTable(linkedAccounts);
                linkedAccountTable.DataSource = _Table;
                linkedAccountTable.DataBind();
                Session[ISENABLED] = true;
                Session[DATA] = _Table;
                await StoreFootPrint("Consult Linked Accounts");
                bool result = await new USSDHelper().UpdateLinkedAccount(Session[USER_ID].ToString(),
                                        Session[ROLE_ID].ToString(), Session[BRANCH_ID].ToString(),
                                        Session[GSM_OP_ID].ToString(), false);
                if (!result)
                {
                    ServiceLogger _Logger = (ServiceLogger)LogFactory.create(Logger.LoggingMode.SERVICE);
                    _Logger.Log(TraceLevel.Error, "Updating LinkedAccount Failed !");
                }
            }
            else
            {
                _Table.Rows.Add(_Table.NewRow());
                linkedAccountTable.DataSource = _Table;
                linkedAccountTable.DataBind();
                linkedAccountTable.Rows[0].Visible = true;
            }
        }

        private void FillLinkedAccountsTable(List<LinkedAccount> linkedAccounts)
        {
            string devise = ConfigurationManager.AppSettings.Get("currency");
            Dictionary<int, REGREQ> dic = new Dictionary<int, REGREQ>();
            int i = 0;
            foreach (LinkedAccount la in linkedAccounts)
            {
                string[] data = la.SupervisedBy.Split(new char[] { '-' });
                DataRow _Row = _Table.NewRow();
                _Row["FirstName"] = la.FirstName;
                _Row["LastName"] = la.LastName;
                _Row["AccountNo"] = la.AccountNo;
                _Row["PhoneNo"] = la.PhoneNo;
                _Row["DateLinked"] = la.DateLinked;
                if(la.Active)
                   _Row["Operation"] = "Subscribe";
                else
                   _Row["Operation"] = "UnSubscribe";

                _Row["Alias"] = la.Alias;
                if (data.Length > 1)
                    _Row["State"] = data[1];
                if (data.Length > 0)
                    _Row["SupervisedBy"] = data[0];

                _Row["SupervisedOn"] = la.SupervisedOn;
                _Row["AuditedBy"] = la.AuditedBy;
                _Row["AuditedOn"] = la.AuditedOn;
                _Row["Role"] = la.Role;
                _Row["BranchID"] = la.BranchID;
                _Row["GSMOperationID"] = la.GSMOperationID;
                if (!la.Active)
                    _Row["Comments"] = (data.Length > 2) ? data[2] : "N/A";
                else
                    _Row["Comments"] = "N/A";

                _Table.Rows.Add(_Row);

                if (la.Active)
                {
                    REGREQ q = new REGREQ();
                    q.alias = la.Alias;
                    q.msisdn = la.PhoneNo;
                    q.active_date = la.DateLinked;
                    q.devise = devise;
                    if (data.Length > 2)
                        q.key = data[2];
                    if (data.Length > 3)
                        q.code_service = int.Parse(data[3]);
                    if (data.Length > 4)
                        q.libelle = data[4];

                    q.supervisedBy = la.SupervisedBy;
                    dic.Add(i, q);
                }
                else
                {
                    DELREQ q = new DELREQ();
                    q.alias = la.Alias;
                    if (data.Length > 2)
                        q.motif = data[2];
                    if (data.Length > 3)
                        q.orig = data[3];

                    q.close_date = DateTime.Now;
                    q.supervisedBy = la.SupervisedBy;
                    dic1.Add(i, q);
                }
                i++;
            }
            Session["DATATOPOST"] = dic;
            Session["DATATOPOST1"] = dic1;
        }

        private async Task StoreFootPrint(string action)
        {
            USSDHelper helper = new USSDHelper();
            FootPrint fp = new FootPrint();
            fp.Id = Guid.NewGuid();
            fp.UserAction = action;
            fp.AccessedBy = Session[USER_ID].ToString();
            fp.AccessedOn = DateTime.Now;
            fp.BranchID = Session[BRANCH_ID].ToString();
            fp.GSMOperationID = Session[GSM_OP_ID].ToString();
            fp.Role = Session[ROLE_ID].ToString();
            bool result = await helper.AddFootPrint(fp);
            if (!result)
            {
                ServiceLogger _Logger = (ServiceLogger)LogFactory.create(Logger.LoggingMode.SERVICE);
                _Logger.Log(TraceLevel.Error, "Adding FootPrint Failed !");
            }
        }

        protected void linkedAccountTable_PageIndexChanging(Object sender, GridViewPageEventArgs e)
        {
            linkedAccountTable.DataSource = (DataTable)Session[DATA];
            linkedAccountTable.PageIndex = e.NewPageIndex;
            linkedAccountTable.DataBind();
        }

        protected void responseTable_PageIndexChanging(Object sender, GridViewPageEventArgs e)
        {
            responseTable.DataSource = (DataTable)Session[DATA1];
            responseTable.PageIndex = e.NewPageIndex;
            responseTable.DataBind();
        }

        private async Task LoadFootPrintTable(string id, bool start, string searchTxt)
        {
            string Role = Session[ROLE_ID].ToString();
            string BranchID = Session[BRANCH_ID].ToString();
            string GSMOpID = Session[GSM_OP_ID].ToString();

            USSDHelper helper = new USSDHelper();
            List<FootPrint> footPrints = await helper.FootPrintById(id, Role, BranchID, GSMOpID);
            AddColumnHeader1();
            if (footPrints != null)
            {
                FillFootPrintTable(footPrints, searchTxt);
                responseTable.DataSource = _Table1;
                responseTable.DataBind();
                Session[ISENABLED1] = true;
                Session[DATA1] = _Table1;
            }
            else
            {
                _Table1.Rows.Add(_Table1.NewRow());
                responseTable.DataSource = _Table1;
                responseTable.DataBind();
                responseTable.Rows[0].Visible = false;
            }
        }

        private void FillFootPrintTable(List<FootPrint> footPrints, string searchTxt)
        {
            foreach (FootPrint fp in footPrints)
            {
                if (fp.Role.Contains(searchTxt)
                    || fp.UserAction.Contains(searchTxt)
                    || fp.AccessedBy.ToString().Contains(searchTxt)
                    || fp.AccessedOn.ToString().Contains(searchTxt)
                    || fp.BranchID.Contains(searchTxt)
                    || fp.GSMOperationID.Contains(searchTxt))
                {
                    DataRow _Row = _Table1.NewRow();
                    _Row["Role"] = fp.Role;
                    _Row["UserAction"] = fp.UserAction;
                    _Row["AccessedBy"] = fp.AccessedBy.ToString();
                    _Row["AccessedOn"] = fp.AccessedOn;
                    _Row["BranchID"] = fp.BranchID;
                    _Row["GSMOperationID"] = fp.GSMOperationID;
                    _Table1.Rows.Add(_Row);
                }
            }
        }

        protected async void txtSearch1_TextChanged(object sender, EventArgs e)
        {
            if (txtSearch1.Text != null && txtSearch1.Text != "")
                await LoadFootPrintTable("Linked Account", true, txtSearch1.Text);
        }

        protected async void txtSearch_TextChanged(object sender, EventArgs e)
        {
            if (txtSearch.Text != null && txtSearch.Text != "")
                await LoadLinkedAccounts(txtSearch.Text);
        }

        private async Task LoadLinkedAccounts(string searchTxt)
        {
            string Role = Session[ROLE_ID].ToString();
            string BranchID = Session[BRANCH_ID].ToString();

            USSDHelper helper = new USSDHelper();
            List<LinkedAccount> linkedAccounts =
                (Role.CompareTo("Auditor") == 0
                    || Role.CompareTo("Admin") == 0)
                ? await helper.LinkedAccounts()
                : ((Role.CompareTo("Agent") == 0)
                    ? await helper.LinkedAccounts(Role,
                                    Session[BRANCH_ID].ToString(),
                                    Session[GSM_OP_ID].ToString())
                    : await helper.LinkedAccounts(null,
                                    ((BranchID.CompareTo("00") == 0) ? null : BranchID),
                                    Session[GSM_OP_ID].ToString())); //Supervisor
            AddColumnHeader();
            if (linkedAccounts != null)
            {
                FillLinkedAccountsTable(linkedAccounts, searchTxt);
                linkedAccountTable.DataSource = _Table;
                linkedAccountTable.DataBind();
                Session[ISENABLED] = true;
                Session[DATA] = _Table;
            }
            else
            {
                _Table.Rows.Add(_Table.NewRow());
                linkedAccountTable.DataSource = _Table;
                linkedAccountTable.DataBind();
                linkedAccountTable.Rows[0].Visible = true;
            }
        }

        private void FillLinkedAccountsTable(List<LinkedAccount> linkedAccounts, string searchTxt)
        {
            string devise = ConfigurationManager.AppSettings.Get("currency");
            Dictionary<int, REGREQ> dic = new Dictionary<int, REGREQ>();
            Dictionary<int, DELREQ> dic1 = new Dictionary<int, DELREQ>();
            int i = 0;
            foreach (LinkedAccount la in linkedAccounts)
            {
                if ((la.AccountNo != null && la.AccountNo.Contains(searchTxt))
                   || (la.PhoneNo != null && la.PhoneNo.Contains(searchTxt))
                   || la.DateLinked.ToString().Contains(searchTxt)
                   || la.Active.ToString().Contains(searchTxt)
                   || la.Alias.ToString().Contains(searchTxt)
                   || (la.SupervisedBy != null && la.SupervisedBy.ToString().ToLower().Contains(searchTxt.ToLower()))
                   || (la.SupervisedOn != null && la.SupervisedOn.ToString().Contains(searchTxt))
                   || (la.AuditedOn != null && la.AuditedOn.ToString().Contains(searchTxt))
                   || (la.AuditedBy != null && la.AuditedBy.ToString().Contains(searchTxt))
                   || (la.Role != null && la.Role.ToString().Contains(searchTxt))
                   || (la.BranchID != null && la.BranchID.ToString().Contains(searchTxt))
                   || (la.GSMOperationID != null && la.GSMOperationID.ToString().Contains(searchTxt))
                   || (la.FirstName != null && la.FirstName.ToString().ToLower().Contains(searchTxt.ToLower()))
                   || (la.LastName != null && la.LastName.ToString().ToLower().Contains(searchTxt.ToLower())))
                {
                    string[] data = la.SupervisedBy.Split(new char[] { '-' });
                    DataRow _Row = _Table.NewRow();
                    _Row["FirstName"] = la.FirstName;
                    _Row["LastName"] = la.LastName;
                    _Row["AccountNo"] = la.AccountNo;
                    _Row["PhoneNo"] = la.PhoneNo;
                    _Row["DateLinked"] = la.DateLinked;
                    if (la.Active)
                        _Row["Operation"] = "Subscribe";
                    else
                        _Row["Operation"] = "UnSubscribe";

                    _Row["Alias"] = la.Alias;
                    if (data.Length > 1)
                        _Row["State"] = data[1];
                    if (data.Length > 0)
                        _Row["SupervisedBy"] = data[0];

                    _Row["SupervisedOn"] = la.SupervisedOn;
                    _Row["AuditedBy"] = la.AuditedBy;
                    _Row["AuditedOn"] = la.AuditedOn;
                    _Row["Role"] = la.Role;
                    _Row["BranchID"] = la.BranchID;
                    _Row["GSMOperationID"] = la.GSMOperationID;
                    if (!la.Active)
                        _Row["Comments"] = (data.Length > 2) ? data[2] : "N/A";
                    else
                        _Row["Comments"] = "N/A";

                    _Table.Rows.Add(_Row);

                    if (la.Active)
                    {
                        REGREQ q = new REGREQ();
                        q.alias = la.Alias;
                        q.msisdn = la.PhoneNo;
                        q.active_date = la.DateLinked;
                        q.devise = devise;
                        if (data.Length > 2)
                            q.key = data[2];
                        if (data.Length > 3)
                            q.code_service = int.Parse(data[3]);
                        if (data.Length > 4)
                            q.libelle = data[4];

                        q.supervisedBy = la.SupervisedBy;
                        dic.Add(i, q);
                    }
                    else
                    {
                        DELREQ q = new DELREQ();
                        q.alias = la.Alias;
                        if (data.Length > 2)
                            q.motif = data[2];
                        if (data.Length > 3)
                            q.orig = data[3];
                        
                        q.close_date = DateTime.Now;
                        q.supervisedBy = la.SupervisedBy;
                        dic1.Add(i, q);
                    }                    
                }
                i++;
            }
            Session["DATATOPOST"] = dic;
            Session["DATATOPOST1"] = dic1;
        }

        protected void responseTable_Sorting(object sender, GridViewSortEventArgs e)
        {
            bool state = (Session["State1"] != null)
                          ? ((string)Session["State1"]).CompareTo("Select") == 0
                          : false;
            if (!state)
            {
                DataTable dataTable = (DataTable)Session[DATA1];
                if (dataTable != null)
                {
                    string sortPattern = BuildSortPattern("Sort1", e.SortDirection, e.SortExpression);
                    dataTable.DefaultView.Sort = sortPattern;
                    Session[DATA1] = dataTable;
                    responseTable.DataSource = dataTable;
                    responseTable.DataBind();
                }
            }
            else
            {
                DataTable dt = (DataTable)Session[DATA1];
                int size = dt.Columns.Count;
                string columnName = e.SortExpression;
                for (int i = 0; i < size; i++)
                {
                    string column = dt.Columns[i].ColumnName;
                    if (column.Contains(columnName))
                    {
                        dt.Columns[i].ColumnName = column.Replace("\u2714 ", "");
                        break;
                    }
                }
                Session[DATA1] = dt;
                responseTable.DataSource = (DataTable)Session[DATA1];
                responseTable.DataBind();
            }
        }

        protected void linkedAccount_Sorting(object sender, GridViewSortEventArgs e)
        {
            bool state = (Session["State"] != null)
                          ? ((string)Session["State"]).CompareTo("Select") == 0
                          : false;
            if (!state)
            {
                DataTable dataTable = (DataTable)Session[DATA];
                if (dataTable != null)
                {
                    string sortPattern = BuildSortPattern("Sort", e.SortDirection, e.SortExpression);
                    dataTable.DefaultView.Sort = sortPattern;
                    Session[DATA] = dataTable;
                    linkedAccountTable.DataSource = dataTable;
                    linkedAccountTable.DataBind();
                }
            }
            else
            {
                DataTable dt = (DataTable)Session[DATA];
                int size = dt.Columns.Count;
                string columnName = e.SortExpression;
                for (int i = 0; i < size; i++)
                {
                    string column = dt.Columns[i].ColumnName;
                    if (column.Contains(columnName))
                    {
                        dt.Columns[i].ColumnName = column.Replace("\u2714 ", "");
                        break;
                    }
                }
                Session[DATA] = dt;
                linkedAccountTable.DataSource = (DataTable)Session[DATA];
                linkedAccountTable.DataBind();
            }
        }

        private string BuildSortPattern(string cacheKey, SortDirection sortDirection,
                                        string sortExpression)
        {
            string dir = ConvertSortDirection(sortDirection),
                   sortPattern = (Session[cacheKey] != null)
                                  ? (string)Session[cacheKey] : "";
            if (sortPattern == "")
            {
                sortPattern = sortExpression + " " + dir;
                Session[cacheKey] = sortPattern;
            }
            else
            {
                if (sortPattern.Contains(sortExpression))
                {
                    sortPattern = sortPattern.Replace(" desc", " ")
                                             .Replace(" asc", " ");
                    sortPattern = sortPattern + dir;
                }
                else
                {
                    sortPattern = sortPattern.Replace(" desc", ",")
                                             .Replace(" asc", ",");
                    sortPattern = sortPattern + sortExpression + " " + dir;
                }
            }

            return sortPattern;
        }

        private string ConvertSortDirection(SortDirection sortDirection)
        {
            string newSortDirection = String.Empty;

            switch (sortDirection)
            {
                case SortDirection.Ascending:
                    newSortDirection = "asc";
                    break;

                case SortDirection.Descending:
                    newSortDirection = "desc";
                    break;
            }

            return newSortDirection;
        }

        private void OpenPDF(string FileName)
        {
            string viewPage = Server.MapPath(@"~/Pages/Views/Store/" + FileName);
            FileStream fs = new FileStream(viewPage, System.IO.FileMode.Open, System.IO.FileAccess.Read);
            int fs_len = (int)fs.Length;
            byte[] ar = new byte[fs_len];
            fs.Read(ar, 0, fs_len);
            fs.Close();

            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("Accept-Header", fs_len.ToString());
            Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName + ".pdf");
            Response.AddHeader("Expires", "0");
            Response.AddHeader("Pragma", "cache");
            Response.AddHeader("Cache-Control", "private");
            Response.ContentType = "application/pdf";
            Response.AddHeader("Accept-Ranges", "bytes");
            Response.BinaryWrite(ar);
            Response.Flush();
            try { Response.End(); }
            catch { }
        }

        private DataTable EnablePrintingMode(string[] StateName, DataTable dt, bool state)
        {
            int colCount = dt.Columns.Count;
            for (int i = 0; i < colCount; i++)
            {
                string column = dt.Columns[i].ColumnName;
                if (state)
                    dt.Columns[i].ColumnName = "\u2714 " + column;
                else
                    dt.Columns[i].ColumnName = column.Replace("\u2714 ", "");
            }

            if (state)
            {
                Session[StateName[0]] = "Select";
                Session[StateName[1]] = new System.Web.UI.WebControls.Image
                {
                    ImageUrl = "Images/select.png",
                    Width = 35,
                    Height = 35,
                    BorderColor = Color.Transparent
                };
                if (StateName[0].CompareTo("State") == 0)
                    btnToPDF.Controls.Add((System.Web.UI.WebControls.Image)Session[StateName[1]]);
                else
                    btnToPDF1.Controls.Add((System.Web.UI.WebControls.Image)Session[StateName[1]]);
            }
            else
            {
                Session[StateName[0]] = null;
                if (StateName[0].CompareTo("State") == 0)
                {
                    btnToPDF.Controls.Remove((System.Web.UI.WebControls.Image)Session[StateName[1]]);
                    btnToPDF.Controls.Add(new System.Web.UI.WebControls.Image
                    {
                        ImageUrl = "Images/pdf.png",
                        Width = 35,
                        Height = 35,
                        BorderColor = Color.Transparent
                    });
                }
                else
                {
                    btnToPDF1.Controls.Remove((System.Web.UI.WebControls.Image)Session[StateName[1]]);
                    btnToPDF1.Controls.Add(new System.Web.UI.WebControls.Image
                    {
                        ImageUrl = "Images/pdf.png",
                        Width = 35,
                        Height = 35,
                        BorderColor = Color.Transparent
                    });
                }
            }

            return dt;
        }

        private DataTable PreparePrinting(DataTable dt)
        {
            DataTable dtNew = dt.Copy();
            int colCount = dt.Columns.Count;
            if (colCount > 0)
            {
                for (int i = 0; i < colCount; i++)
                {
                    DataColumn c = dt.Columns[i];
                    if (!c.ColumnName.Contains("\u2714 "))
                        dtNew.Columns.Remove(c.ColumnName);
                }
                int colNewCount = dtNew.Columns.Count;
                for (int i = 0; i < colNewCount; i++)
                {
                    string column = dtNew.Columns[i].ColumnName;
                    if (column.Contains("\u2714 "))
                        dtNew.Columns[i].ColumnName = column.Replace("\u2714 ", "");
                }
            }

            return dtNew;
        }

        protected void linkedAccountTable_RowCreated(object sender, GridViewRowEventArgs e)
        {
            string Role = Session[ROLE_ID].ToString();
            if (Role.CompareTo("Supervisor") != 0)
            {
                try
                {
                    ((DataControlField)linkedAccountTable.Columns
                      .Cast<DataControlField>()
                      .Where(fld => fld.HeaderText == "Select")
                      .SingleOrDefault()).Visible = false;
                }
                catch (Exception) { }
            }
        }

        protected async void btnDelete_Click(object sender, EventArgs e)
        {
            if(Session["DATATOPOST"] != null || Session["DATATOPOST1"] != null)
            {
                // Reload Linked Accounts
                btnValidate.BackColor = Color.DarkGray;
                btnDelete.Text = "Wait...";

                Dictionary<int, REGREQ> dic = (Dictionary<int, REGREQ>)Session["DATATOPOST"];
                Dictionary<int, DELREQ> dic1 = (Dictionary<int, DELREQ>)Session["DATATOPOST1"];
                bool ok = false;
                int pageIndex = linkedAccountTable.PageIndex,
                    rowCount = linkedAccountTable.Rows.Count;
                for (int i = 0; i < rowCount; i++)
                {
                    int idx = i,
                        cIdx = (5 * pageIndex) + i;
                    GridViewRow row = linkedAccountTable.Rows[idx];
                    CheckBox CheckRow = (row.FindControl("validationCheckBox") as CheckBox);
                    if (CheckRow != null && CheckRow.Checked)
                    {
                        USSDHelper helper = new USSDHelper();
                        bool result = dic.ContainsKey(cIdx)
                                      ? await helper.DeleteLinkedAccount(dic[cIdx].alias)
                                      : (dic1.ContainsKey(cIdx)
                                         ? await helper.DeleteLinkedAccount(dic1[cIdx].alias)
                                         : false);
                        if (!result)
                        {
                            ServiceLogger _Logger = (ServiceLogger)LogFactory.create(Logger.LoggingMode.SERVICE);
                            _Logger.Log(TraceLevel.Error, "Deleting Linked Account Failed !");
                            ok = false;
                        }
                        else
                        {
                            ok = true;
                        }
                    }
                }
                // Reload Linked Accounts
                btnDelete.Text = "Loading...";
                await LoadLinkedAccounts();
                if (ok)
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Info 6",
                    "alert('Successfully Deleted Linked account!');", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Info 6",
                    "alert('Deleting Linked account Failed!');", true);
                }
                btnDelete.Text = "Delete";
                btnValidate.BackColor = Color.FromName(ConfigurationManager.AppSettings.Get("color"));
            }
        }

        protected async void btnValidate_Click(object sender, EventArgs e)
        {
            if (Session["DATATOPOST"] != null || Session["DATATOPOST1"] != null)
            {
                btnValidate.BackColor = Color.DarkGray;
                btnValidate.Text = "Wait...";

                Dictionary<int, REGREQ> dic = (Dictionary<int, REGREQ>)Session["DATATOPOST"];
                Dictionary<int, DELREQ> dic1 = (Dictionary<int, DELREQ>)Session["DATATOPOST1"];
                Session["ok"] = false;
                Session["errorCode"] = -1;
                int pageIndex = linkedAccountTable.PageIndex,
                    rowCount = linkedAccountTable.Rows.Count;
                for (int i = 0; i < rowCount; i++)
                {
                    int idx = i,
                        cIdx = (5 * pageIndex) + i;

                    GridViewRow row = linkedAccountTable.Rows[idx];                   
                    CheckBox CheckRow = (row.FindControl("validationCheckBox") as CheckBox);
                    if (CheckRow != null && CheckRow.Checked)
                    {
                        if (dic.ContainsKey(cIdx))
                        {
                            REGREQ req = dic[cIdx];
                            string gsmOpID = (string)Session[GSM_OP_ID];
                            if (gsmOpID.CompareTo("ORANGE") == 0)
                            {
                                REGRES res = await new RegisterBinding().OmbRequest(req);
                                if (res != null && res.return_code == 200)
                                {
                                    await ValidateSubscription(req);
                                    Session["ok"] = true;
                                    dic[cIdx] = req;
                                }
                                else
                                {
                                    Session["ok"] = false;
                                    if (res != null && res.return_code != 200)
                                        Session["errorCode"] = res.return_code;

                                    ServiceLogger _Logger = (ServiceLogger)LogFactory.create(Logger.LoggingMode.SERVICE);
                                    if (res != null)
                                        _Logger.Log(TraceLevel.Error, " Subscription # " + dic[cIdx].msisdn + " Failed : " + B2W.Register.ErrorCodeHandler.Description(res.return_code));
                                } 
                            }
                            else //MTN
                            {
                                await ValidateSubscription(req);
                                Session["ok"] = true;
                                dic[cIdx] = req;
                            }
                        }
                        else if (dic1.ContainsKey(cIdx))
                        {
                            DELREQ req = dic1[cIdx];
                            string gsmOpID = (string)Session[GSM_OP_ID];
                            if (gsmOpID.CompareTo("ORANGE") == 0)
                            {
                                DELRES res = await new RegisterBinding().OmbClose(req);
                                if (res != null && res.return_code == 200)
                                {
                                    await ValidateUnSubscription(req);
                                    Session["ok"] = true;
                                    dic1[cIdx] = req;
                                }
                                else
                                {
                                    Session["ok"] = false;
                                    if (res != null && res.return_code != 200)
                                        Session["errorCode"] = res.return_code;

                                    ServiceLogger _Logger = (ServiceLogger)LogFactory.create(Logger.LoggingMode.SERVICE);
                                    if (res != null)
                                        _Logger.Log(TraceLevel.Error, " UnSubscription # " + req.alias + " Failed : " + B2W.Register.ErrorCodeHandler.Description(res.return_code));
                                }
                            }
                            else //MTN
                            {
                                await ValidateUnSubscription(req);
                                Session["ok"] = true;
                                dic1[cIdx] = req;
                            }
                        }
                    }
                    
                }

                if((bool)Session["ok"])
                {
                    Session["DATATOPOST"] = dic;
                    Session["DATATOPOST1"] = dic1;
                    ClientScript.RegisterStartupScript(this.GetType(), "Info 6",
                    "alert('Sucessfully validated (Un)subscriptions !');", true);
                }
                else
                {
                    if ((int)Session["errorCode"] == -1)
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Info",
                        "alert('Validation Failed ! Check connection with " + Session[GSM_OP_ID] + " !');", true);
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "Info",
                        "alert('Validation Failed ("+ B2W.Register.ErrorCodeHandler.Description((int)Session["errorCode"]) + ") !');", true);
                    }
                }
                Session["Validated"] = true;
                // Reload Linked Accounts
                btnValidate.Text = "Loading...";
                await LoadLinkedAccounts();

                btnValidate.BackColor = Color.FromName(ConfigurationManager.AppSettings.Get("color"));
                btnValidate.Text = "Validate";
            }
        }

        private async Task ValidateSubscription(REGREQ rr)
        {
            REGREQ req = rr;
            string supervisedBy = req.supervisedBy,
                   alias = req.alias;
            supervisedBy = supervisedBy.Replace("Pending", "Validated");
            bool result = await new USSDHelper().UpdatePostedLinkedAccount(supervisedBy, alias);
            if (!result)
            {
                ServiceLogger _Logger = (ServiceLogger)LogFactory.create(Logger.LoggingMode.SERVICE);
                _Logger.Log(TraceLevel.Error, "Posting Subscription # " + req.msisdn + " Failed !");
            }
            await TriggerSmsAlerts(req);
        }

        private async Task ValidateUnSubscription(DELREQ rr)
        {
            DELREQ req = rr;
            string supervisedBy = req.supervisedBy,
            alias = req.alias;
            supervisedBy = supervisedBy.Replace("Pending", "Validated");
            bool result = await new USSDHelper().UpdatePostedLinkedAccount(supervisedBy, alias);
            if (!result)
            {
                ServiceLogger _Logger = (ServiceLogger)LogFactory.create(Logger.LoggingMode.SERVICE);
                _Logger.Log(TraceLevel.Error, "Posting UnSubscription # " + req.msisdn + " Failed !");
            }
            await TriggerSmsAlerts(req);
        }

        protected void linkedAccountTable_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                CheckBox CheckRow = (e.Row.FindControl("validationCheckBox") as CheckBox);
                if ((e.Row.Cells[7].Text != null && e.Row.Cells[7].Text.Contains("Validated"))
                    || (e.Row.Cells[6].Text != null && e.Row.Cells[6].Text.Contains("Validated"))
                    || (e.Row.Cells[5].Text != null && e.Row.Cells[5].Text.Contains("Validated")))
                {
                    CheckRow.Enabled = false;
                }
                else
                {
                    CheckRow.Enabled = true;
                }
            }
        }

        private async Task TriggerSmsAlerts(REGREQ req)
        {
            // Triggering Alerts
            var smsAlert = new SmsAlert();
            string newNumber;
            if (req.msisdn != null && !req.msisdn.Contains("237"))
                newNumber = "237" + req.msisdn;
            else
                newNumber = req.msisdn;

            smsAlert.MOBILEnumber = newNumber;
            smsAlert.PostedOn = DateTime.Now;
            smsAlert.Provider = (string)Session[GSM_OP_ID]; //MTN
            smsAlert.messagedescription = "Dear Customer, you have been successfully subscribed"
                                          + " to Bank to Wallet. " + ConfigurationManager.AppSettings.Get("slogan");;
                                          + " From " + ConfigurationManager.AppSettings.Get("bank_name");
            string[] data = req.supervisedBy.Split(new char[] { '-' });
            if (data.Length > 0)
                smsAlert.PostedBy = data[0];

            bool result = await new B2WHelper().TriggerSmsAlert(smsAlert);
            if (!result)
            {
                ServiceLogger _Logger = (ServiceLogger)LogFactory.create(Logger.LoggingMode.SERVICE);
                _Logger.Log(TraceLevel.Error, "Triggering Alert # " + req.msisdn + " Failed !");
            }
        }

        private async Task TriggerSmsAlerts(DELREQ req)
        {
            // Triggering Alerts
            var smsAlert = new SmsAlert();
            string newNumber;
            string msisdn = await new USSDHelper().GetPhoneNoByAlias(req.alias);
            if (msisdn != null && !msisdn.Contains("237"))
                newNumber = "237" + msisdn;
            else
                newNumber = msisdn;

            smsAlert.MOBILEnumber = newNumber;
            smsAlert.PostedOn = DateTime.Now;
            smsAlert.Provider = (string)Session[GSM_OP_ID] //MTN;
            smsAlert.messagedescription = "Dear Customer, you have been successfully unsubscribed"
                                          + " from the Bank to Wallet. " + ConfigurationManager.AppSettings.Get("slogan");
                                          + " From " + ConfigurationManager.AppSettings.Get("bank_name");
            string[] data = req.supervisedBy.Split(new char[] { '-' });
            if (data.Length > 0)
                smsAlert.PostedBy = data[0];

            bool result = await new B2WHelper().TriggerSmsAlert(smsAlert);
            if (!result)
            {
                ServiceLogger _Logger = (ServiceLogger)LogFactory.create(Logger.LoggingMode.SERVICE);
                _Logger.Log(TraceLevel.Error, "Triggering Alert # " + req.alias + " Failed !");
            }
        }

        protected async void btnSelectAll_Click(object sender, EventArgs e)
        {
            if (linkedAccountTable.Rows.Count != 0)
            {
                if (btnSelectAll.Text.Contains("UnSelect All"))
                {
                    btnSelectAll.Text = "Select All";
                    foreach (GridViewRow row in linkedAccountTable.Rows)
                    {
                        CheckBox CheckRow = (row.FindControl("validationCheckBox") as CheckBox);
                        if (CheckRow != null && CheckRow.Checked)
                        {
                            CheckRow.Checked = false;
                        }
                    }
                }
                else
                {
                    btnSelectAll.Text = "UnSelect All";
                    foreach (GridViewRow row in linkedAccountTable.Rows)
                    {
                        CheckBox CheckRow = (row.FindControl("validationCheckBox") as CheckBox);
                        if (CheckRow != null && !CheckRow.Checked)
                        {
                            CheckRow.Checked = true;
                        }
                    }
                }
                // Reload Linked Accounts
                await LoadLinkedAccounts();
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "Info",
                        "alert('Subscription table is empty !');", true);
            }
        }
    }
}