﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OMBanking.Web.Pages.Controller
{
    public class InputController
    {
        public enum InputType
        {
            INT = 1,
            BOOL = 2,
            DOUBLE = 3,
            DECIMAL = 4,
            STRING = 5,
            DATETIME = 6
        };

        public void ItemController(ref System.Web.UI.WebControls.TextBox txtBox,
                                   ref System.Web.UI.WebControls.Label lbl,
                                    ref object item,
                                    string errorMsg,
                                    bool isCorrectSize,
                                    InputType type)
        {
            if (txtBox.Text == null)
            {
                txtBox.BorderColor = System.Drawing.Color.Red;
                lbl.Text += errorMsg + "<br />";
            }
            else if (txtBox.Text.Length == 0)
            {
                txtBox.BorderColor = System.Drawing.Color.Red;
                lbl.Text += errorMsg + "<br />";
            }
            else
            {
                if (isCorrectSize)
                {
                    if (type == InputType.BOOL)
                        item = Convert.ToBoolean(txtBox.Text.ToLower());
                    else if (type == InputType.INT)
                        item = Convert.ToInt32(txtBox.Text);
                    else if (type == InputType.DOUBLE)
                        item = Convert.ToDouble(txtBox.Text);
                    else if (type == InputType.DECIMAL)
                        item = Convert.ToDecimal(txtBox.Text);
                    else if (type == InputType.STRING)
                        item = txtBox.Text;
                    else if (type == InputType.DATETIME)
                        item = Convert.ToDateTime(txtBox.Text);

                    txtBox.BorderColor = System.Drawing.Color.Green;
                }
                else
                {
                    txtBox.BorderColor = System.Drawing.Color.Red;
                    lbl.Text += errorMsg + "<br />";
                }
            }
        }
    }
}