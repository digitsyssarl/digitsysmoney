using System;

namespace OMBanking.Web.B2W.Orange.Idle
{
    public class SetIdleRequest : IdleMessage
    {
        public bool idle { get; set; }

        public override string encode()
        {
            string output = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                   + "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\""
                   + " xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\""
                   + " xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\""
                   + " xmlns:om=\"http://om.btow.com\">"
                   + "<soapenv:Header/>"
                   + "<soapenv:Body>"
                   + "<om:setIdle soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">"
                   + "<idle xsi:type=\"xsd:string\">"
                   + idle.ToString().ToLower()
                   + "</idle>"
                   + "</om:setIdle>"
                   + "</soapenv:Body>"
                   + "</soapenv:Envelope>";
            return output;
        }

        public override IIdleMessage decode(string soapResponse) { return null; }
    }
}
