namespace OMBanking.Web.B2W.Orange.Idle
{   
    public interface IIdleMessage
    {
        string encode();

        IIdleMessage decode(string soapResponse);
    }
}
