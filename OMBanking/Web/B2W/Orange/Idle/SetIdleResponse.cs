using System.Linq;
using System.Xml.Linq;

namespace OMBanking.Web.B2W.Orange.Idle
{
    public class SetIdleResponse : IdleMessage
    {
        public bool responseCode { get; set; }

        public override string encode() { return null; }

        public override IIdleMessage decode(string soapResponse)
        {
            XDocument xml = XDocument.Parse(soapResponse);
            var response1 = xml.Descendants()
                               .Where(x => (x.Name.LocalName == "setIdleResponse"));
            if (response1 != null)
            {
                var response2 = response1.Select(x => new SetIdleResponse()
                {
                    responseCode = (((string)x.Element("responseCode")) != null
                                    && ((string)x.Element("responseCode")) != "")
                                    ? bool.Parse((string)x.Element("responseCode"))
                                    : false
                }).FirstOrDefault();

                return response2;
            }

            return null;
        }
    }
}
