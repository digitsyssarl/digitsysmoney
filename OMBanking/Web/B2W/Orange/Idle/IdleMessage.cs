namespace OMBanking.Web.B2W.Orange.Idle
{   
    public abstract class IdleMessage : IIdleMessage
    {
        public abstract string encode();

        public abstract IIdleMessage decode(string soapResponse);
    }
}
