namespace OMBanking.Web.B2W.Orange.StatusInquiry
{
    public enum Step
    {
        ORANGE_CREDIT,
        ORANGE_DEBIT,
        ORANGE_AUTHORIZATION,
        BANK_CREDIT,
        BANK_DEBIT,
        BANK_CANCEL,
        BANK_STATUS_INQUIRY
    }
}
