namespace OMBanking.Web.B2W.Orange.StatusInquiry
{
    public enum TypeID
    {
        BANK_ID,
        ORANGE_ID,
        MTN_ID,
        NONE
    }
}
