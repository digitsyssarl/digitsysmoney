namespace OMBanking.Web.B2W.Orange.StatusInquiry
{
    public enum StepStatus
    {
        OK,
        TIMEOUT,
        KO
    }
}
