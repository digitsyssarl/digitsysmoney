using System;

namespace OMBanking.Web.B2W.Orange.StatusInquiry
{   
    public class DetailedStatus
    {
        public Step step { get; set; }

        public StepStatus status { get; set; }

        public DateTime requestDate { get; set; }
    }
}
