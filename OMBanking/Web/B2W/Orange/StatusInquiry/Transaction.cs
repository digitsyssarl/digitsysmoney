using System;

namespace OMBanking.Web.B2W.Orange.StatusInquiry
{   
    public class Transaction
    {
        public string orangeID { get; set; }

        public string bankID { get; set; }

        public DateTime dateTime { get; set; }

        public TransactionType type { get; set; }

        public double amount { get; set; }
    }
}
