using System.Collections.Generic;

namespace OMBanking.Web.B2W.Orange.StatusInquiry
{   
    public class TransactionStatus
    {
        public string returnCode { get; set; }

        public Transaction transaction { get; set; }

        public GlobalStatus status { get; set; }

        public List<DetailedStatus> detailedStatuses { get; set; }
    }
}
