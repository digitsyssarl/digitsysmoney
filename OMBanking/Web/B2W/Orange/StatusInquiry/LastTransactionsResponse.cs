using System;
using System.Linq;
using System.Xml.Linq;

namespace OMBanking.Web.B2W.Orange.StatusInquiry
{   
    public class LastTransactionsResponse : TransactionStatusInquiryMessage
    {
        public LastTransactions lastTransactions { get; set; }

        public override string encode() { return null; }

        public override ITransactionStatusInquiryMessage decode(string soapResponse)
        {
            XDocument xml = XDocument.Parse(soapResponse);
            var response1 = xml.Descendants()
                               .Where(x => (x.Name.LocalName == "LastTransactionsResponse"));
            if(response1 != null)
            {  
                var response2 = response1.Select(x => new LastTransactionsResponse()
                {
                    lastTransactions = x.Descendants()
                                        .Where(y => y.Name.LocalName == "lastTransactions")
                                        .Select(y => new LastTransactions()
                    {
                        returnCode   = (string) y.Element(y.Name.Namespace + "returnCode"),
                        transactions = y.Element(y.Name.Namespace + "transactions")
                                        .Descendants()
                                        .Where(z => z.Name.LocalName == "transaction")
                                        .Select(z => new Transaction()
                        {
                            orangeID = (string) z.Element(z.Name.Namespace + "orangeID"),
                            bankID   = (string) z.Element(z.Name.Namespace + "bankID"),
                            dateTime = (((string) z.Element(z.Name.Namespace + "dateTime")) != null
                                         && ((string) z.Element(z.Name.Namespace + "dateTime")) != "")
                                       ? DateTime.Parse((string) z.Element(z.Name.Namespace + "dateTime"))
                                       : DateTime.Now,
                            type = (((string) z.Element(z.Name.Namespace + "type")).CompareTo("BANK2WALLET") == 0
                                    ? TransactionType.BANK2WALLET
                                    : TransactionType.WALLET2BANK),
                            amount = (((string)z.Element(z.Name.Namespace + "amount")) != null
                                        && ((string)z.Element(z.Name.Namespace + "amount")) != "")
                                     ? Double.Parse((string)z.Element(z.Name.Namespace + "amount"), System.Globalization.NumberStyles.AllowDecimalPoint, System.Globalization.NumberFormatInfo.InvariantInfo)
                                     : -1

                        }).ToList()

                    }).FirstOrDefault()

                }).FirstOrDefault();

                return response2;
            }
            return null;
        }        
    }
}