using System;
using System.Linq;
using System.Xml.Linq;

namespace OMBanking.Web.B2W.Orange.StatusInquiry
{
    public class TransactionStatusInquiryResponse : TransactionStatusInquiryMessage
    {
        public TransactionStatus statusInquiry { get; set; }

        public override string encode() { return null; }

        public override ITransactionStatusInquiryMessage decode(string soapResponse)
        {
            XDocument xml = XDocument.Parse(soapResponse);
            var response1 = xml.Descendants()
                               .Where(x => (x.Name.LocalName == "TransactionStatusInquiryResponse"));
            if(response1 != null)
            {
                var response2 = response1.Select(x => new TransactionStatusInquiryResponse()
                {
                    statusInquiry = (x.Descendants()
                                      .Where(y => y.Name.LocalName == "statusInquiry") != null)
                                    ?
                                    x.Descendants()
                                     .Where(y => y.Name.LocalName == "statusInquiry")
                                     .Select(y => new TransactionStatus()
                    {
                        returnCode  = (string)y.Element(y.Name.Namespace + "returnCode"),
                        transaction =  (y.Descendants()
                                         .Where(z => z.Name.LocalName == "transaction") != null)
                                       ?
                                       y.Descendants()
                                        .Where(z => z.Name.LocalName == "transaction")
                                        .Select(z => new Transaction()
                        {
                            orangeID = (string) z.Element(z.Name.Namespace + "orangeID"),
                            bankID   = (string) z.Element(z.Name.Namespace + "bankID"),
                            dateTime = (((string) z.Element(z.Name.Namespace + "dateTime")) != null
                                        && ((string) z.Element(z.Name.Namespace + "dateTime")) != "")
                                       ? DateTime.Parse((string) z.Element(z.Name.Namespace + "dateTime"))
                                       : DateTime.Now,
                            type = (((string) z.Element(z.Name.Namespace + "type")).CompareTo("BANK2WALLET") == 0
                                    ? TransactionType.BANK2WALLET
                                    : TransactionType.WALLET2BANK),
                            amount = (((string) z.Element(z.Name.Namespace + "amount")) != null
                                        && ((string) z.Element(z.Name.Namespace + "amount")) != "")
                                     ? Double.Parse((string) z.Element(z.Name.Namespace + "amount"))
                                     : -1

                        }).FirstOrDefault() : new Transaction(),
                        status = (((string)y.Element(y.Name.Namespace + "status")) != null)
                                 ? 
                                 (
                                     (((string)y.Element(y.Name.Namespace + "status"))
                                                .CompareTo("OK") == 0
                                     ? GlobalStatus.OK
                                     : (((string)y.Element(y.Name.Namespace + "status"))
                                                  .CompareTo("KO") == 0
                                        ? GlobalStatus.KO
                                        : (((string)y.Element(y.Name.Namespace + "status"))
                                                     .CompareTo("ORANGE_RECONCILIATION") == 0
                                           ? GlobalStatus.ORANGE_RECONCILIATION
                                           : (((string)y.Element(y.Name.Namespace + "status"))
                                                        .CompareTo("BANK_RECONCILIATION") == 0
                                              ? GlobalStatus.BANK_RECONCILIATION
                                              : GlobalStatus.ORANGE_BANK_RECONCILIATION
                                           )
                                        )
                                     ))
                                 ) 
                                 : GlobalStatus.KO,
                        detailedStatuses = (y.Element(y.Name.Namespace + "detailedStatuses") != null)
                                           ?
                                           y.Element(y.Name.Namespace + "detailedStatuses")
                                            .Descendants()
                                            .Where(z => z.Name.LocalName == "detailedStatus")
                                            .Select(z => new DetailedStatus()
                                            {
                                                step = (((string)z.Element(z.Name.Namespace + "step"))
                                                                .CompareTo("ORANGE_CREDIT") == 0
                                                         ? Step.ORANGE_CREDIT
                                                         : (((string)z.Element(z.Name.Namespace + "step"))
                                                                      .CompareTo("ORANGE_DEBIT") == 0
                                                            ? Step.ORANGE_DEBIT
                                                            : (((string)z.Element(z.Name.Namespace + "step"))
                                                                         .CompareTo("ORANGE_AUTHORIZATION") == 0
                                                               ? Step.ORANGE_AUTHORIZATION
                                                               : (((string)z.Element(z.Name.Namespace + "step"))
                                                                            .CompareTo("BANK_CREDIT") == 0
                                                                  ? Step.BANK_CREDIT
                                                                  : (((string)z.Element(z.Name.Namespace + "step"))
                                                                               .CompareTo("BANK_DEBIT") == 0
                                                                     ? Step.BANK_DEBIT
                                                                     : (((string)z.Element(z.Name.Namespace + "step"))
                                                                                  .CompareTo("BANK_CANCEL") == 0
                                                                        ? Step.BANK_CANCEL
                                                                        : Step.BANK_STATUS_INQUIRY
                                                                     )
                                                                  )
                                                               )
                                                            )
                                                         )),
                                                status = (((string)z.Element(z.Name.Namespace + "status"))
                                                                    .CompareTo("OK") == 0
                                                         ? StepStatus.OK
                                                         : (((string)z.Element(z.Name.Namespace + "status"))
                                                                      .CompareTo("KO") == 0
                                                            ? StepStatus.KO
                                                            : StepStatus.TIMEOUT
                                                         )),
                                                requestDate = (((string)z.Element(z.Name.Namespace + "requestDate")) != null
                                                              && ((string)z.Element(z.Name.Namespace + "requestDate")) != "")
                                                             ? DateTime.Parse((string)z.Element(z.Name.Namespace + "requestDate"))
                                                             : DateTime.Now

                                         }).ToList() : new System.Collections.Generic.List<DetailedStatus>()

                       }).FirstOrDefault() : new TransactionStatus()

                }).FirstOrDefault();

                return response2;
            }

            return null;
        }        
    }
}
