namespace OMBanking.Web.B2W.Orange.StatusInquiry
{
    using System;
    using System.Runtime.CompilerServices;
    
    public enum GlobalStatus
    {
        OK,
        ORANGE_RECONCILIATION,
        BANK_RECONCILIATION,
        ORANGE_BANK_RECONCILIATION,
        KO
    }
}
