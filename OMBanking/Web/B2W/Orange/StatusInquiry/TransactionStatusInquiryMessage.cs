using System;

namespace OMBanking.Web.B2W.Orange.StatusInquiry
{   
     public abstract class TransactionStatusInquiryMessage : ITransactionStatusInquiryMessage
    {
        public abstract string encode();

        public abstract ITransactionStatusInquiryMessage decode(string soapResponse);
    }
}
