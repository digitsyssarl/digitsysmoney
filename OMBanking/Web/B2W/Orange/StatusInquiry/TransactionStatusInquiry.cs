using System;

namespace OMBanking.Web.B2W.Orange.StatusInquiry
{   
    public class TransactionStatusInquiry : TransactionStatusInquiryMessage
    {
        public TypeID typeID { get; set; }

        public string ID { get; set; }

        public override string encode()
        {
            return "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                   + "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\""
                   + " xmlns:tes=\"http://statusinquiry.b2w.orange.com/\">"
                   + "<soapenv:Header/>"
                   + "<soapenv:Body>"
                   + "<tes:TransactionStatusInquiry>"
                   + "<TypeID>"
                   + ((typeID == TypeID.ORANGE_ID) ? "ORANGE_ID" : "BANK_ID")
                   + "</TypeID>"
                   + "<ID>"
                   + ID
                   + "</ID>"
                   + "</tes:TransactionStatusInquiry>"
                   + "</soapenv:Body>"
                   + "</soapenv:Envelope>";
        }

        public override ITransactionStatusInquiryMessage decode(string soapResponse) { return null; }        
    }
}
