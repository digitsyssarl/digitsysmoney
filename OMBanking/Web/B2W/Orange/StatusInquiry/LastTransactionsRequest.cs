using System;

namespace OMBanking.Web.B2W.Orange.StatusInquiry
{   
    public class LastTransactionsRequest : TransactionStatusInquiryMessage
    {
        public string alias { get; set; }

        public override string encode()
        {
            return "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                   + "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\""
                   + " xmlns:tes=\"http://statusinquiry.b2w.orange.com/\">"
                   + "<soapenv:Header/>"
                   + "<soapenv:Body>"
                   + "<tes:LastTransactions>"
                   + "<alias>"
                   + alias
                   + "</alias>"
                   + "</tes:LastTransactions>"
                   + "</soapenv:Body>"
                   + "</soapenv:Envelope>";
        }

        public override ITransactionStatusInquiryMessage decode(string soapResponse) { return null;  }        
    }
}
