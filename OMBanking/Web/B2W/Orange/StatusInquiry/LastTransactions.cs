using System;
using System.Collections.Generic;

namespace OMBanking.Web.B2W.Orange.StatusInquiry
{   
    public class LastTransactions
    {
        public string returnCode { get; set; }

        public List<Transaction> transactions { get; set; }
    }
}
