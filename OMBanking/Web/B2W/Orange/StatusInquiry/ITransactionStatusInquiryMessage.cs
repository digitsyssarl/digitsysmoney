namespace OMBanking.Web.B2W.Orange.StatusInquiry
{
    using System;
    
    public interface ITransactionStatusInquiryMessage
    {
        string encode();

        ITransactionStatusInquiryMessage decode(string soapResponse);
    }
}
