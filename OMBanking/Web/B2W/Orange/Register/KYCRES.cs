using System.Linq;
using System.Xml.Linq;

namespace OMBanking.Web.B2W.Orange.Register
{
    public class KYCRES : RegisterMessage
    {
        public int status { get; set; }
        
        public string firstname { get; set; }
        
        public string lastname { get; set; }
        
        public string dob { get; set; }
        
        public string cin { get; set; }

        public override string encode() { return null; }

        public override IRegisterMessage decode(string soapResponse)
        {
            XDocument xml = XDocument.Parse(soapResponse);
            var response1 = xml.Descendants()
                               .Where(x => (x.Name.LocalName == "KYCRequestResponse"));
            if (response1 != null)
            {
                var response2 = response1.Select(x => new KYCRES()
                {
                    status = (((string)x.Element("status")) != null
                               && ((string)x.Element("status")) != "")
                             ? int.Parse((string)x.Element("status"))
                             : -1,
                    lastname = (string)x.Element("lastName"),
                    firstname = (string)x.Element("firstName"),
                    dob = (string)x.Element("dob"),
                    cin = (string)x.Element("cin")
                }).FirstOrDefault();

                return response2;
            }

            return null;
        }
    }
}
