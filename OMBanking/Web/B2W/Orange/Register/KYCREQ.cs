using System;
//using System.ComponentModel.DataAnnotations;

namespace OMBanking.Web.B2W.Orange.Register
{
    public class KYCREQ : RegisterMessage
    {
        //[StringLength(10, ErrorMessage = "The {0} must be minimum {2} and maximum {1} characters long.", 7)]
        public string msisdn { get; set; }
        
        //[StringLength(8, ErrorMessage = "The {0} must be  maximum {1} characters long.")]
        public string key { get; set; }

        public override string encode()
        {
            return "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                   + "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\""
                   + " xmlns:reg=\"http://om.btow.com/register\">"
                   + "<soapenv:Header/>"
                   + "<soapenv:Body>"
                   + "<reg:KYCRequest>"
                   + "<msisdn>"
                   + msisdn
                   + "</msisdn>"
                   + "<key>"
                   + key
                   + "</key>"
                   + "</reg:KYCRequest>"
                   + "</soapenv:Body>"
                   + "</soapenv:Envelope>";
        }

        public override IRegisterMessage decode(string soapResponse) { return null; }
    }
}
