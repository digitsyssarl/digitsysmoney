using System;
//using System.ComponentModel.DataAnnotations;

namespace OMBanking.Web.B2W.Orange.Register
{   
    public class REGREQ : RegisterMessage
    {
        //[StringLength(10, ErrorMessage = "The {0} must be minimum {2} and maximum {1} characters long.", 7)]
        public string msisdn { get; set; }
        
        //[StringLength(22, ErrorMessage = "The {0} must be  maximum {1} characters long.")]
        public string alias { get; set; }
        
        public int code_service { get; set; }
        
        //[StringLength(25, ErrorMessage = "The {0} must be minimum {2} and maximum {1} characters long.", 0)]
        public string libelle { get; set; }
        
        //[StringLength(3, ErrorMessage = "The {0} must be  maximum {1} characters long.")]
        public string devise { get; set; }
        
        //[StringLength(8, ErrorMessage = "The {0} must be  maximum {1} characters long.")]
        public string key { get; set; }
        
        public DateTime active_date { get; set; }

        public string supervisedBy { get; set; }

        public override string encode()
        {
            return "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                   + "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\""
                   + " xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\""
                   + " xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\""
                   + " xmlns:mban=\"http://om.btow.com/register\">"
                   + "<soapenv:Header/>"
                   + "<soapenv:Body>"
                   + "<mban:ombRequest soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">"
                   + "<msisdn xsi:type=\"xsd:string\">"
                   + msisdn
                   + "</msisdn>"
                   + "<alias xsi:type=\"xsd:string\">"
                   + alias
                   + "</alias>"
                   + "<code_service xsi:type=\"xsd:short\">"
                   + code_service.ToString()
                   + "</code_service>"
                   + "<libelle xsi:type=\"xsd:string\">"
                   + libelle
                   + "</libelle>"
                   + "<devise xsi:type=\"xsd:string\">"
                   + devise
                   + "</devise>"
                   + "<key xsi:type=\"xsd:string\">"
                   + key
                   + "</key>"
                   + "<active_date xsi:type=\"xsd:dateTime\">"
                   + active_date.ToString("yyyy-MM-dd hh:mm:ss")
                   + "</active_date>"
                   + "</mban:ombRequest>"
                   + "</soapenv:Body>"
                   + "</soapenv:Envelope>";
        }

        public override IRegisterMessage decode(string soapResponse) { return null; }
    }
}
