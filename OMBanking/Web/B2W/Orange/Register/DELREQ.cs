using System;
//using System.ComponentModel.DataAnnotations;

namespace OMBanking.Web.B2W.Orange.Register
{   
    public class DELREQ : RegisterMessage
    {
        //[StringLength(22, ErrorMessage = "The {0} must be  maximum {1} characters long.")]
        public string alias { get; set; }
        
        public DateTime close_date { get; set; }

        //[StringLength(6, ErrorMessage = "The {0} must be minimum {2} and maximum {1} characters long.", 4)]
        public string orig { get; set; }

        //[StringLength(200, ErrorMessage = "The {0} must be minimum {2} and maximum {1} characters long.", 0)]
        public string motif { get; set; }

        public override string encode()
        {
            string output = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                   + "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\""
                   + " xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\""
                   + " xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\""
                   + " xmlns:mban=\"http://om.btow.com/register\">"
                   + "<soapenv:Header/>"
                   + "<soapenv:Body>"
                   + "<mban:ombClose soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">"
                   + "<alias xsi:type=\"xsd:string\">"
                   + alias
                   + "</alias>"
                   + "<close_date xsi:type=\"xsd:dateTime\">"
                   + close_date.ToString("yyyy-MM-dd hh:mm:ss")
                   + "</close_date>"
                   + "<orig xsi:type=\"xsd:string\">"
                   + orig
                   + "</orig>"
                   + "<motif xsi:type=\"xsd:string\">"
                   + motif
                   + "</motif>"
                   + "</mban:ombClose>"
                   + "</soapenv:Body>"
                   + "</soapenv:Envelope>";
            return output;
        }

        public override IRegisterMessage decode(string soapResponse) { return null; }
    }
}
