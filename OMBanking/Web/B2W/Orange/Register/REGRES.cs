using System;
using System.Linq;
using System.Xml.Linq;
//using System.ComponentModel.DataAnnotations;

namespace OMBanking.Web.B2W.Orange.Register
{   
    public class REGRES : RegisterMessage
    {
        //[StringLength(22, ErrorMessage = "The {0} must be  maximum {1} characters long.")]
        public string alias { get; set; }
        
        public int return_code { get; set; }

        public override string encode() { return null; }

        public override IRegisterMessage decode(string soapResponse)
        {
            XDocument xml = XDocument.Parse(soapResponse);
            var response1 = xml.Descendants()
                               .Where(x => (x.Name.LocalName == "ombRequestResponse"));
            if (response1 != null)
            {
                var response2 = response1.Select(x => new REGRES()
                {
                    alias = (string)x.Element("alias"),
                    return_code = (((string)x.Element("return_code")) != null
                                    && ((string)x.Element( "return_code")) != "")
                                  ? int.Parse((string)x.Element("return_code"))
                                  : -1
                }).FirstOrDefault();

                return response2;
            }

            return null;
        }
    }
}
