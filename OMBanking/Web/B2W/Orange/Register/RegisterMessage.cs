namespace OMBanking.Web.B2W.Orange.Register
{
    public abstract class RegisterMessage : IRegisterMessage
    {
        public abstract string encode();

        public abstract IRegisterMessage decode(string soapResponse);
    }
}
