namespace OMBanking.Web.B2W.Orange.Register
{
    public class ErrorCodeHandler
    {
        private const string UNKNOWN_ERROR = "Unknown Error";
        private const string OK = "OK: Validation";
        private const string INVALID_REQUEST = "KO: Invalid Request";
        private const string WRONG_MSISDN = "KO: Wrong MSISDN Format";
        private const string UNKNOWN_ALIAS = "KO: UNNOWN_ALIAS";
        private const string INVALID_CURRENCY = "Invalid Currency";
        private const string INVALID_SERVICE_CODE = "Invalid Service Code";
        private const string ALIAS_EXISTS = "KO: Alias Already Exists";
        private const string UNKNOWN_REQUEST = "KO: Unknown Request or Timeout";
        private const string INVALID_ACTIVATION_KEY = "KO: Invalid Activation Key";
        private const string PLATFORM_UNAVAILABLE = "KO: Platform Unavailable";

        public static string Description(int code)
        {
            switch (code)
            {
                case 200:
                    return OK;
                case 302:
                    return INVALID_REQUEST;
                case 303:
                    return WRONG_MSISDN;
                case 304:
                    return UNKNOWN_ALIAS;
                case 306:
                    return INVALID_CURRENCY;
                case 307:
                    return INVALID_SERVICE_CODE;
                case 601:
                    return ALIAS_EXISTS;
                case 602:
                    return UNKNOWN_REQUEST;
                case 603:
                    return INVALID_ACTIVATION_KEY;
                case 604:
                    return PLATFORM_UNAVAILABLE;
            }

            return UNKNOWN_ERROR;
        }
    }
}
