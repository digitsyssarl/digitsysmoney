namespace OMBanking.Web.B2W.Orange.Register
{   
    public interface IRegisterMessage
    {
        string encode();

        IRegisterMessage decode(string soapResponse);
    }
}
