﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OMBanking.Web.Proxy.Bank;
using OMBanking.Web.Log;
using System.Diagnostics;
using System.Configuration;

namespace OMBanking.Web.B2W.Mtn
{
    public class Momo
    {
        private const string API_USER_URI = "v1_0/apiuser";
        private string SUBSCRIBE_KEY;
        private string URI_PATH;
        private string CALLBACK_URL;
        private string BASE_URL;
        private string TARGET_ENVIRONMENT;

        public Momo()
        {
            SUBSCRIBE_KEY = ConfigurationManager.AppSettings.Get("momo_subscription_key");
            URI_PATH = ConfigurationManager.AppSettings.Get("momo_uri_path");
            CALLBACK_URL = ConfigurationManager.AppSettings.Get("momo_callback_url");
            BASE_URL = ConfigurationManager.AppSettings.Get("momo_base_url");
            TARGET_ENVIRONMENT = ConfigurationManager.AppSettings.Get("momo_target_env");
        }

        public string CreateApiUserKey()
        {
            return Guid.NewGuid().ToString();
        }

        public async Task<bool> CreateApiUser(string apiUser)
        {
            try
            {
                var client = new HttpClient();
                var queryString = HttpUtility.ParseQueryString(string.Empty);
                client.DefaultRequestHeaders.Add("X-Reference-Id", apiUser);
                client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", SUBSCRIBE_KEY);
                var uri = BASE_URL + API_USER_URI + "?" + queryString;
                HttpResponseMessage response;
                byte[] byteData = Encoding.UTF8.GetBytes("{\"providerCallbackHost\": \""
                                                         + CALLBACK_URL + "\"}");
                using (var content = new ByteArrayContent(byteData))
                {
                    content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                    response = await client.PostAsync(uri, content);
                    if (response.StatusCode == HttpStatusCode.Created)
                        return true;
                }
            }
            catch(Exception e)
            {
                ServiceLogger _Logger = (ServiceLogger)LogFactory.create(Logger.LoggingMode.SERVICE);
                _Logger.Log(TraceLevel.Error, "Creating API User Failed : " + e.Message);
            }

            return false;
        }

        public async Task<bool> GetApiUserInfo(string apiUser)
        {
            try
            { 
                var client = new HttpClient();
                var queryString = HttpUtility.ParseQueryString(string.Empty);
                client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", SUBSCRIBE_KEY);
                var uri = BASE_URL + API_USER_URI + "/" + apiUser + "?" + queryString;
                var response = await client.GetAsync(uri);
                if (response.StatusCode == HttpStatusCode.OK)
                    return true;
            }
            catch (Exception e)
            {
                ServiceLogger _Logger = (ServiceLogger)LogFactory.create(Logger.LoggingMode.SERVICE);
                _Logger.Log(TraceLevel.Error, "Getting API User Info Failed : " + e.Message);
            }

            return false;
        }

        public async Task<string> CreateApiKey(string apiUser)
        {
            try
            { 
                var client = new HttpClient();
                var queryString = HttpUtility.ParseQueryString(string.Empty);
                client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", SUBSCRIBE_KEY);
                var uri = BASE_URL + API_USER_URI + "/" + apiUser + "apikey?" + queryString;
                HttpResponseMessage response;
                byte[] byteData = Encoding.UTF8.GetBytes("");

                using (var content = new ByteArrayContent(byteData))
                {
                    content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                    response = await client.PostAsync(uri, content);
                    if (response.StatusCode == HttpStatusCode.Created)
                    {
                        var result = response.Content.ReadAsStringAsync().Result;
                        JObject obj = (JObject)JsonConvert.DeserializeObject(result);

                        return (string)obj["apiKey"];
                    }
                }
            }
            catch (Exception e)
            {
                ServiceLogger _Logger = (ServiceLogger)LogFactory.create(Logger.LoggingMode.SERVICE);
                _Logger.Log(TraceLevel.Error, "Creating API Key Failed : " + e.Message);
            }

            return "";
        }

        public enum AuthorizationMode
        {
            OAuth,
            Basic,
            Bearer
        };

        public string BuildAuth(string key, AuthorizationMode mode)
        {
            switch (mode)
            {
                case AuthorizationMode.Basic:
                    return "Basic " + key;
                case AuthorizationMode.Bearer:
                    return "Bearer " + key;
                case AuthorizationMode.OAuth:
                    return "OAuth " + key;
            }

            return "";
        }

        public string CreateBasicToken(string apiUser, string apiKey)
        {
            return BuildAuth(Utils.Base64Encode(apiUser + ":" + apiKey), AuthorizationMode.Basic);
        }

        public async Task<string> CreateAccessToken(string basicToken)
        {
            try
            { 
                var client = new HttpClient();
                var queryString = HttpUtility.ParseQueryString(string.Empty);
                client.DefaultRequestHeaders.Add("Authorization", basicToken);
                client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", SUBSCRIBE_KEY);
                var uri = BASE_URL + URI_PATH + "token/?" + queryString;
                HttpResponseMessage response;
                byte[] byteData = Encoding.UTF8.GetBytes("");
                using (var content = new ByteArrayContent(byteData))
                {
                    content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                    response = await client.PostAsync(uri, content);

                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        var result = response.Content.ReadAsStringAsync().Result;
                        JObject obj = (JObject)JsonConvert.DeserializeObject(result);

                        return (string)obj["access_token"];
                    }
                }
            }
            catch (Exception e)
            {
                ServiceLogger _Logger = (ServiceLogger)LogFactory.create(Logger.LoggingMode.SERVICE);
                _Logger.Log(TraceLevel.Error, "Creating Access Token Failed : " + e.Message);
            }

            return "";
        }

        public async Task<Customer> GetKYC(string msisdn, string accessToken)
        {
            try
            { 
                var client = new HttpClient();
                var queryString = HttpUtility.ParseQueryString(string.Empty);
                client.DefaultRequestHeaders.Add("Authorization", BuildAuth(accessToken, AuthorizationMode.Bearer));
                client.DefaultRequestHeaders.Add("X-Target-Environment", TARGET_ENVIRONMENT);
                client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", SUBSCRIBE_KEY);
                var uri = BASE_URL + URI_PATH + "v1_0/accountholder/msisdn/"
                          + msisdn + "/basicuserinfo?" + queryString;
                var userInfo = new Customer();
                var response = await client.GetAsync(uri);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var result = response.Content.ReadAsStringAsync().Result;
                    JObject obj = (JObject)JsonConvert.DeserializeObject(result);
                    userInfo.given_name = (string)obj["given_name"];
                    userInfo.family_name = (string)obj["family_name"];
                    userInfo.birthdate = (string)obj["birthdate"];
                    userInfo.locale = (string)obj["locale"];
                    userInfo.gender = (string)obj["gender"];
                    userInfo.status = (string)obj["status"];

                    return userInfo;
                }
            }
            catch (Exception e)
            {
                ServiceLogger _Logger = (ServiceLogger)LogFactory.create(Logger.LoggingMode.SERVICE);
                _Logger.Log(TraceLevel.Error, "Getting KYC Failed : " + e.Message);
            }

            return null;
        }

        public async Task<Tuple<string, string>> GetAccountBalance(string accessToken)
        {
            try
            {
                var client = new HttpClient();
                var queryString = HttpUtility.ParseQueryString(string.Empty);
                client.DefaultRequestHeaders.Add("Authorization", BuildAuth(accessToken, AuthorizationMode.Bearer));
                client.DefaultRequestHeaders.Add("X-Target-Environment", TARGET_ENVIRONMENT);
                client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", SUBSCRIBE_KEY);
                var uri = BASE_URL + URI_PATH + "v1_0/account/balance?" + queryString;
                var response = await client.GetAsync(uri);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var result = response.Content.ReadAsStringAsync().Result;
                    JObject obj = (JObject)JsonConvert.DeserializeObject(result);

                    return new Tuple<string, string>((string)obj["availableBalance"],
                                                     (string)obj["currency"]);
                }
            }
            catch (Exception e)
            {
                ServiceLogger _Logger = (ServiceLogger)LogFactory.create(Logger.LoggingMode.SERVICE);
                _Logger.Log(TraceLevel.Error, "Getting Account Balance Failed : " + e.Message);
            }

            return null;
        }

        public async Task<bool> IsRegisteredAndActive(string msisdn, string accessToken)
        {
            try
            {
                var client = new HttpClient();
                var queryString = HttpUtility.ParseQueryString(string.Empty);
                client.DefaultRequestHeaders.Add("Authorization", BuildAuth(accessToken, AuthorizationMode.Bearer));
                client.DefaultRequestHeaders.Add("X-Target-Environment", TARGET_ENVIRONMENT);
                client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", SUBSCRIBE_KEY);
                //msisdn, email, party_code
                var uri = BASE_URL + URI_PATH + "v1_0/accountholder/msisdn/"
                          + msisdn + "/active?" + queryString;
                var response = await client.GetAsync(uri);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var result = response.Content.ReadAsStringAsync().Result;
                    JObject obj = (JObject)JsonConvert.DeserializeObject(result);

                    return Boolean.Parse((string)obj["result"]);
                }
            }
            catch (Exception e)
            {
                ServiceLogger _Logger = (ServiceLogger)LogFactory.create(Logger.LoggingMode.SERVICE);
                _Logger.Log(TraceLevel.Error, "Checking whether User is Active or Registered Failed : " + e.Message);
            }

            return false;
        }


        //login_hint=ID:{msisdn}/MSISDN&scope={scope}&access_type={online/offline}
        public async Task<string> ClaimCustomerConsent(string msisdn,     string scope, 
                                                       string accessType, string basicToken)
        {
            try
            {
                var client = new HttpClient();
                var queryString = HttpUtility.ParseQueryString(string.Empty);
                client.DefaultRequestHeaders.Add("Authorization", basicToken);
                client.DefaultRequestHeaders.Add("X-Target-Environment", TARGET_ENVIRONMENT);
                client.DefaultRequestHeaders.Add("X-Callback-Url", CALLBACK_URL);
                client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", SUBSCRIBE_KEY);
                var uri = BASE_URL + URI_PATH + "v1_0/bc-authorize?" + queryString;
                HttpResponseMessage response;
                string loginHint = "ID:" + msisdn + "/MSISDN";
                byte[] byteData = Encoding.UTF8.GetBytes("login_hint=" + loginHint + "&scope="
                                                         + scope + "&access_type=" + accessType);
                using (var content = new ByteArrayContent(byteData))
                {
                    content.Headers.ContentType = new MediaTypeHeaderValue("application/x-www-form-urlencoded");
                    response = await client.PostAsync(uri, content);
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        var result = response.Content.ReadAsStringAsync().Result;
                        JObject obj = (JObject)JsonConvert.DeserializeObject(result);

                        return (string)obj["auth_req_id"];
                    }
                }
            }
            catch (Exception e)
            {
                ServiceLogger _Logger = (ServiceLogger)LogFactory.create(Logger.LoggingMode.SERVICE);
                _Logger.Log(TraceLevel.Error, "Claiming Customer Consent Failed : " + e.Message);
            }

            return "";
        }

        public async Task<string> CreateOAuthToken(string authReqId, string basicToken)
        {
            try
            {
                var client = new HttpClient();
                var queryString = HttpUtility.ParseQueryString(string.Empty);
                client.DefaultRequestHeaders.Add("Authorization", basicToken);
                client.DefaultRequestHeaders.Add("X-Target-Environment", TARGET_ENVIRONMENT);
                client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", SUBSCRIBE_KEY);
                var uri = BASE_URL + URI_PATH + "oauth2/token/?" + queryString;
                HttpResponseMessage response;
                string grandType = "urn:openid:params:grant-type:ciba";
                byte[] byteData = Encoding.UTF8.GetBytes("grant_type=" + grandType
                                                         + "&auth_req_id=" + authReqId);
                using (var content = new ByteArrayContent(byteData))
                {
                    content.Headers.ContentType = new MediaTypeHeaderValue("application/x-www-form-urlencoded");
                    response = await client.PostAsync(uri, content);

                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        var result = response.Content.ReadAsStringAsync().Result;
                        JObject obj = (JObject)JsonConvert.DeserializeObject(result);

                        return (string)obj["access_token"];
                    }
                }
            }
            catch (Exception e)
            {
                ServiceLogger _Logger = (ServiceLogger)LogFactory.create(Logger.LoggingMode.SERVICE);
                _Logger.Log(TraceLevel.Error, "Generating OAuth Key Failed : " + e.Message);
            }

            return "";
        }

        public async Task<bool> TransfertMoney(TransferRequest req, string accessToken)
        {
            try
            {
                var client = new HttpClient();
                var queryString = HttpUtility.ParseQueryString(string.Empty);
                client.DefaultRequestHeaders.Add("Authorization", BuildAuth(accessToken, AuthorizationMode.Bearer));
                client.DefaultRequestHeaders.Add("X-Callback-Url", CALLBACK_URL);
                client.DefaultRequestHeaders.Add("X-Reference-Id", req.referenceId);
                client.DefaultRequestHeaders.Add("X-Target-Environment", TARGET_ENVIRONMENT);
                client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", SUBSCRIBE_KEY);
                var uri = BASE_URL + URI_PATH + "v1_0/transfer?" + queryString;
                HttpResponseMessage response;
                byte[] byteData = Encoding.UTF8.GetBytes("{\"amount\": \"" + req.amount + "\",\"currency\": \"" + req.currency + "\","
                                    + "\"externalId\": \"" + req.externalId + "\",\"payee\": {\"partyIdType\": \"MSISDN\","
                                    + "\"partyId\": \"" + req.payeeMSISDN + "\"},\"payerMessage\": \"" + req.payerMsg + "\","
                                    + "\"payeeNote\": \"" + req.payeeNote + "\"}");
                using (var content = new ByteArrayContent(byteData))
                {
                    content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                    response = await client.PostAsync(uri, content);
                    if (response.StatusCode == HttpStatusCode.Accepted)
                    {
                        return true;
                    }
                }
            }
            catch (Exception e)
            {
                ServiceLogger _Logger = (ServiceLogger)LogFactory.create(Logger.LoggingMode.SERVICE);
                _Logger.Log(TraceLevel.Error, "Transferring Money Failed : " + e.Message);
            }

            return false;
        }

        public async Task<TransferResponse> StatusInquiry(TransferRequest req, string accessToken)
        {
            var res = new TransferResponse();
            try
            {
                var client = new HttpClient();
                var queryString = HttpUtility.ParseQueryString(string.Empty);
                client.DefaultRequestHeaders.Add("Authorization", BuildAuth(accessToken, AuthorizationMode.Bearer));
                client.DefaultRequestHeaders.Add("X-Target-Environment", TARGET_ENVIRONMENT);
                client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", SUBSCRIBE_KEY);
                var uri = BASE_URL + URI_PATH + "v1_0/transfer/" + req.referenceId + "?" + queryString;
                var response = await client.GetAsync(uri);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var result = response.Content.ReadAsStringAsync().Result;
                    JObject obj = (JObject)JsonConvert.DeserializeObject(result);

                    res.amount = double.Parse(obj["amount"].ToString());
                    res.currency = (string)obj["currency"];
                    res.financialTransactionId = obj["financialTransactionId"].ToString();
                    res.externalId = obj["externalId"].ToString();
                    res.payeeMSISDN = obj["partyId"].ToString();
                    res.status = (string)obj["status"];

                    return res;
                }
            }
            catch (Exception e)
            {
                ServiceLogger _Logger = (ServiceLogger)LogFactory.create(Logger.LoggingMode.SERVICE);
                _Logger.Log(TraceLevel.Error, "Getting Status Inquiry Failed: " + e.Message);
            }

            return res;
        }
    }
}