﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OMBanking.Web.B2W.Mtn
{
    public class TransferResponse
    {
        public string financialTransactionId { get; set; }
        public double amount { get; set; }
        public string currency { get; set; }
        public string externalId { get; set; }
        public string payeeMSISDN { get; set; }
        public string status { get; set; }
    }
}