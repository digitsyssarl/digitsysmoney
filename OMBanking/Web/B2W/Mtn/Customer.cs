﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OMBanking.Web.B2W.Mtn
{
    public class Customer
    {
       public string given_name { get; set; }
       public string family_name { get; set; }
       public string birthdate { get; set; }
       public string locale { get; set; }
       public string gender { get; set; }
       public string status { get; set; }
    }
}