﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OMBanking.Web.B2W.Mtn
{
    public class TransferRequest
    {
        public string referenceId { get; set; }
        public double amount { get; set; }
        public string currency { get; set; }
        public string externalId { get; set; }
        public string payeeMSISDN { get; set; }
        public string payerMsg { get; set; }
        public string payeeNote { get; set; }
    }
}