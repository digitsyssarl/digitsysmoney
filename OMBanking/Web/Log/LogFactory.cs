﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OMBanking.Web.Log
{
    public class LogFactory
    {
        public static LogBase create(LogBase.LoggingMode mode)
        {
            switch (mode)
            {
                case LogBase.LoggingMode.DATABASE:
                    return new DBLogger();
                case LogBase.LoggingMode.SERVICE:
                    return new ServiceLogger();
            }

            return null;
        }
    }
}