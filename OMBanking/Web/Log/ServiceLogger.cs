﻿using System.Diagnostics;


namespace OMBanking.Web.Log
{
    public class ServiceLogger : Logger
    {
        public void Log(TraceLevel level, string message)
        {
            Log(LoggingMode.SERVICE, level, message);
        }
    }
}