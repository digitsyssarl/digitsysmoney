﻿using System.Diagnostics;

namespace OMBanking.Web.Log
{
    public abstract class LogBase
    {
        public enum LoggingMode
        {
            DATABASE,
            SERVICE
        }

        protected readonly object lockObj = new object();
        public abstract void Log(LoggingMode mode, TraceLevel level, string message);
    }
}