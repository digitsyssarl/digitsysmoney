﻿using System;

namespace OMBanking.Web.Log
{
    public class TransactionLog
    {
        public Guid Id { get; set; }

        public DateTime Date { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string PhoneNo { get; set; }

        public int TranType { get; set; }

        public string OperatorCode { get; set; }

        public string AffiliateCode { get; set; }

        public string ExternalRefNo { get; set; }

        public string RequestId { get; set; }

        public string AccountNo { get; set; }

        public decimal Amount { get; set; }

        public decimal Charge { get; set; }

        public int ResponseCode { get; set; }

        public string ResponseMessage { get; set; }

        public string TranRef { get; set; }

        public bool Complete { get; set; }

        public string Details { get; set; }

        public string SerialID { get; set; }

        public string ConsultedBy { get; set; }

        public DateTime ConsultedOn { get; set; }

        public string AuditedBy { get; set; }

        public DateTime AuditedOn { get; set; }

        public string Role { get; set; }

        public string BranchID { get; set; }

        public string GSMOperationID { get; set; }
    }
}