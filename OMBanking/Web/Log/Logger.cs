﻿using System;
using System.Diagnostics;
using System.IO;

namespace OMBanking.Web.Log
{
    public class Logger : LogBase
    {
        private String _TimeStamp;

        public override void Log(LoggingMode mode, TraceLevel level, string message)
        {
            string modeToString = LoggingModeToString(mode);
            _TimeStamp = DateTime.Today.ToString().Split(new char[] { ' ' })[0]
                                 .Replace("/", "-");

            using (StreamWriter _SW = new StreamWriter(Path.Combine(Path.GetTempPath(),
                                                "OMBanking-"
                                               + modeToString
                                               + "Logging.log-"
                                               + _TimeStamp), append: true))
            {
                _TimeStamp = DateTime.Now.ToString();
                lock (lockObj)
                {
                    switch (level)
                    {
                        case TraceLevel.Error:
                            _SW.WriteLine(_TimeStamp + " [Error] 0 [" + modeToString + "] " + message);
                            break;
                        case TraceLevel.Info:
                            _SW.WriteLine(_TimeStamp + " [Info] 0 [" + modeToString + "] " + message);
                            break;
                        case TraceLevel.Warning:
                            _SW.WriteLine(_TimeStamp + " [Warning] 0 [" + modeToString + "] " + message);
                            break;
                        case TraceLevel.Verbose:
                            _SW.WriteLine(_TimeStamp + " [Verbose] 0 [" + modeToString + "] " + message);
                            break;
                    }
                }
            }
        }

        private string LoggingModeToString(LoggingMode mode)
        {
            switch(mode)
            {
                case LoggingMode.DATABASE:
                    return "Database";
                case LoggingMode.SERVICE:
                    return "Service"; 
            }

            return null;
        }
    }
}