﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Diagnostics;
using System.Text;
using System.Threading;

namespace OMBanking.Web.Log
{
    public class DBLogger : Logger
    {
        private StringBuilder dataGrid;

        public void Log(TraceLevel level, string message)
        {
            Log(LoggingMode.DATABASE, level, message);
        }

        public void LogSqlDataReader(DbDataReader reader, TraceLevel level, string storedProc)
        {
            Thread ReadTask = new Thread(() => 
            {
                dataGrid = new StringBuilder();
                while (reader.Read())
                {
                    try
                    {
                        for (int i = 0; i < reader.FieldCount; i++)
                        {
                            dataGrid.Append("|").Append(reader.GetValue(i).ToString());
                        }
                        dataGrid.Append("\n");
                    }
                    catch(Exception e)
                    {
                        Log(TraceLevel.Error, e.Message);
                    }
                }

                Log(level, "Stored procedure " + storedProc + " return \n" + dataGrid.ToString());
            });
            ReadTask.Start();
        }
    }
}