using System;
using OMBanking.Web.Bank;
using System.Web;

namespace OMBanking.Web.Proxy.Bank
{
    public interface IB2W
    {
        void doPost(string soapAction, ref HttpContext context, IB2WMessage req);

        IB2WMessage Response(string soapResponse, IB2WMessage res);
    }
}
