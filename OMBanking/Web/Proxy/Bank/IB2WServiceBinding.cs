using System;
using OMBanking.Web.Bank;
using OMBanking.Web.DB;
using System.Web;
using System.Web.Services;
using System.Diagnostics;
using System.Threading.Tasks;

namespace OMBanking.Web.Proxy.Bank
{
    public class IB2WServiceBinding : B2WServicesPort
    {
        public IB2WServiceBinding() : base()
        {
        }

        [WebMethod]
        public override Task<HttpContext> CancelTransfer(HttpContext context, string cancelTransferRequest)
        {
            return Task.Run(async () =>
            {
                // Request emitted by Orange B2W at url:https://unicsmobile.com?soapAction=CancelTransfer
                CancelTransfer cancelTransferReq = (CancelTransfer)Response(cancelTransferRequest,
                                                                         new CancelTransfer());
                // Processing the received request
                CancelTransferResponse cancelTransferRes = await new B2WHelper().CancelTransfer(cancelTransferReq, "ORANGE");
                // Send back Response to Orange B2W 
                string soapAction = "CancelTransfer";
                HttpContext outContext = context;
                if (cancelTransferRes != null)
                    doPost(soapAction, ref outContext, cancelTransferRes);
                else
                    _Logger.Log(TraceLevel.Error, "Failed fetching CancelTransfer request from Bank DB");

                return outContext;
            });
        }

        [WebMethod]
        public override Task<HttpContext> WalletToAccountTransfer(HttpContext context, string walletToAccountTransferRequest)
        {
            return Task.Run(async () =>
            {
                // Request emitted by Orange B2W at url:https://unicsmobile.com?soapAction=c
                WalletToAccountTransfer walletToAccountTransferReq = (WalletToAccountTransfer)Response(walletToAccountTransferRequest,
                                                                                                    new WalletToAccountTransfer());
                // Processing the received request
                WalletToAccountTransferResponse walletToAccountTransferRes = await new B2WHelper().WalletToAccountTransfer(walletToAccountTransferReq, "ORANGE");
                // Send back Response to Orange B2W 
                string soapAction = "WalletToAccountTransfer";
                HttpContext outContext = context;
                if (walletToAccountTransferRes != null)
                    doPost(soapAction, ref outContext, walletToAccountTransferRes);
                else
                    _Logger.Log(TraceLevel.Error, "Failed fetching WalletToAccountTransfer request from Bank DB");

                return outContext;
            });
        }

        [WebMethod]
        public override Task<HttpContext> AccountToWalletTransfer(HttpContext context, string accountToWalletTransferRequest)
        {
            return Task.Run(async () =>
            {
                // Request emitted by Orange B2W at url:https://unicsmobile.com?soapAction=AccountToWalletTransfer
                AccountToWalletTransfer accountToWalletTransferReq = (AccountToWalletTransfer)Response(accountToWalletTransferRequest,
                                                                                                    new AccountToWalletTransfer());
                // Processing the received request
                AccountToWalletTransferResponse accountToWalletTransferRes = await new B2WHelper().AccountToWalletTransfer(accountToWalletTransferReq, "ORANGE");
                // Send back Response to Orange B2W 
                string soapAction = "AccountToWalletTransfer";
                HttpContext outContext = context;
                if (accountToWalletTransferRes != null)
                    doPost(soapAction, ref outContext, accountToWalletTransferRes);
                else
                    _Logger.Log(TraceLevel.Error, "Failed fetching AccountToWalletTransfer request from Bank DB");

                return outContext;
            });
        }

        [WebMethod]
        public override Task<HttpContext> GetAccountBalance(HttpContext context, string getAccountBalanceRequest)
        {
            return Task.Run(async () =>
            {
                // Request emitted by Orange B2W at url:https://unicsmobile.com?soapAction=GetAccountBalance
                GetAccountBalance getAccountBalanceReq = (GetAccountBalance)Response(getAccountBalanceRequest,
                                                                                 new GetAccountBalance());
                // Processing the received request
                GetAccountBalanceResponse getAccountBalanceRes = await new B2WHelper().GetAccountBalance(getAccountBalanceReq, "ORANGE");
                // Send back Response to Orange B2W 
                string soapAction = "GetAccountBalance";
                HttpContext outContext = context;
                if (getAccountBalanceRes != null)
                    doPost(soapAction, ref outContext, getAccountBalanceRes);
                else
                    _Logger.Log(TraceLevel.Error, "Failed fetching GetAccountBalance request from Bank DB");

                return outContext;
            });
        }

        [WebMethod]
        public override Task<HttpContext> TransferStatusInquiry(HttpContext context, string transferStatusInquiryRequest)
        {
            return Task.Run(async () =>
            {
                // Request emitted by Orange B2W at url:https://unicsmobile.com?soapAction=TransferStatusInquiry
                TransferStatusInquiry transferStatusInquiryReq = (TransferStatusInquiry)Response(transferStatusInquiryRequest,
                                                                                              new TransferStatusInquiry());
                // Processing the received request
                TransferStatusInquiryResponse transferStatusInquiryRes = await new B2WHelper().TransferStatusInquiry(transferStatusInquiryReq);
                // Send back Response to Orange B2W 
                string soapAction = "TransferStatusInquiry";
                HttpContext outContext = context;
                if (transferStatusInquiryRes != null)
                    doPost(soapAction, ref outContext, transferStatusInquiryRes);
                else
                    _Logger.Log(TraceLevel.Error, "Failed fetching TransferStatusInquiry request from Bank DB");

                return outContext;
            });
        }

        [WebMethod]
        public override Task<HttpContext> GetMiniStatement(HttpContext context, string getMiniStatementRequest)
        {
            return Task.Run(async () =>
            {
                // Request emitted by Orange B2W at url:https://unicsmobile.com?soapAction=GetMiniStatement
                GetMiniStatement getMiniStatementReq = (GetMiniStatement)Response(getMiniStatementRequest,
                                                                               new GetMiniStatement());
                // Processing the received request
                GetMiniStatementResponse getMiniStatementRes = await new B2WHelper().GetMiniStatement(getMiniStatementReq, "ORANGE");
                // Send back Response to Orange B2W
                string soapAction = "GetMiniStatement";
                HttpContext outContext = context;
                if (getMiniStatementRes != null)
                {
                    doPost(soapAction, ref outContext, getMiniStatementRes);
                }
                else
                    _Logger.Log(TraceLevel.Error, "Failed fetching GetMiniStatement request from Bank DB");

                return outContext;
            });
        }
    }
}
