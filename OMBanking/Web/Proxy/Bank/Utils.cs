﻿using OMBanking.Web.B2W.Mtn;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Text.RegularExpressions;

namespace OMBanking.Web.Proxy.Bank
{
    public class Utils
    {
        public static String[] _LastErrorCode;
        public static string AccessToken;
        public static string ApiUser;
        public static string BaseSiteUrl;
        public static string ReferenceID;

        public static string Base64Encode(string plainText)
        {
            if (plainText == "")
                return "";
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);

            return Convert.ToBase64String(plainTextBytes);
        }
     
        public static string Base64Decode(string base64EncodedData)
        {
            if (base64EncodedData == "")
                return "";
            try
            {
                var base64EncodedBytes = Convert.FromBase64String(base64EncodedData);

                return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
            }
            catch(Exception)
            {
                return base64EncodedData;
            }
        }

        public static bool IsDigitsOnly(string str)
        {
            string _str = str.Trim();
            foreach(char c in _str)
            {
                if (c < '0' || c > '9')
                {
                    if (c != '+')
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        public static async Task<string> GetAccessToken()
        {
            Momo m = new Momo();
            ApiUser = m.CreateApiUserKey();
            bool IsCreated = await m.CreateApiUser(ApiUser);
            if (IsCreated)
            {
                bool IsOK = await m.GetApiUserInfo(ApiUser);
                if (IsOK)
                {
                    string ApiKey = await m.CreateApiKey(ApiUser);
                    string BasicToken = m.CreateBasicToken(ApiUser, ApiKey);
                    return await m.CreateAccessToken(BasicToken);
                }
            }

            return "";
        }

        public static string ReplaceFirst(string text, string search, string replace)
        {
            var regex = new Regex(Regex.Escape(search));
            var newText = regex.Replace(text, replace, 1);

            return newText;
        }
    }
}