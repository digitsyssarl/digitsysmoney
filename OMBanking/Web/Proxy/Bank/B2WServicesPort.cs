using OMBanking.Web.Bank;
using OMBanking.Web.Log;
using System;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Diagnostics;
using System.Threading.Tasks;

namespace OMBanking.Web.Proxy.Bank
{
    public abstract class B2WServicesPort : WebService, IB2W
    {
        protected ServiceLogger _Logger;

        public B2WServicesPort()
        {
            _Logger = (ServiceLogger)LogFactory.create(LogBase.LoggingMode.SERVICE);
        }

        [WebMethod]
        public abstract Task<HttpContext> CancelTransfer(HttpContext context,  string cancelTransferRequest);

        [WebMethod]
        public abstract Task<HttpContext> WalletToAccountTransfer(HttpContext context, string walletToAccountTransferRequest);

        [WebMethod]
        public abstract Task<HttpContext> AccountToWalletTransfer(HttpContext context, string accountToWalletTransferRequest);

        [WebMethod]
        public abstract Task<HttpContext> GetAccountBalance(HttpContext context, string getAccountBalanceRequest);

        [WebMethod]
        public abstract Task<HttpContext> TransferStatusInquiry(HttpContext context, string transferStatusInquiryRequest);

        [WebMethod]
        public abstract Task<HttpContext> GetMiniStatement(HttpContext context, string getMiniStatementRequest);

        public void doPost(string soapAction, ref HttpContext context, IB2WMessage res)
        {
            string soapResponse = res.encode();
            context.Response.Clear();
            context.Response.ContentType = "text/xml;charset=\"utf-8\"";
            context.Response.ContentEncoding = Encoding.UTF8;
            context.Response.Write(soapResponse);
        }

        public IB2WMessage Response(string soapResponse, IB2WMessage res)
        {
            try
            {
                IB2WMessage o = res.decode(soapResponse);
                if (o == null)
                    _Logger.Log(TraceLevel.Error, "Failed decoding SOAP response in B2WServicesPort");

                return o;
            }
            catch(Exception)
            {
                _Logger.Log(TraceLevel.Error, "Failed decoding SOAP response in B2WServicesPort");

                return null;
            }
        }
    }
}
