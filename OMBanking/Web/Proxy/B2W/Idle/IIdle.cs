using OMBanking.Web.B2W.Orange.Idle;
using System;
using System.Threading.Tasks;

namespace OMBanking.Web.Proxy.B2W.Idle
{
    public interface IIdle 
    {
        Task<string> doPost(string soapAction, string url, IIdleMessage req);

        IIdleMessage Response(string soapResponse, IIdleMessage res);
    }
}
