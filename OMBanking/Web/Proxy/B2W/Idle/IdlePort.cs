using OMBanking.Web.B2W.Orange.Idle;
using OMBanking.Web.Service;
using System.Web.Services;
using OMBanking.Web.Log;
using System.Diagnostics;
using System.Threading.Tasks;
using System;

//using System.ComponentModel.DataAnnotations;

namespace OMBanking.Web.Proxy.B2W.Idle
{
    //[Description("Port Inscription")]
    public abstract class IdlePort : WebService, IIdle 
    {
        private ServiceLogger _Logger;

        public IdlePort()
        {
            _Logger = (ServiceLogger)LogFactory.create(LogBase.LoggingMode.SERVICE);
        }

        //[Description("Dispo de la banque")]
        [WebMethod]
        public abstract Task<SetIdleResponse> SetIdle(SetIdleRequest req);

        public Task<string> doPost(string soapAction, string url, IIdleMessage req)
        {
           return Task.Run(() =>
           {
               string soapRequest = req.encode();
               B2WServiceSOAPClient service = new B2WServiceSOAPClient(soapAction, url);
               string soapResponse = service.execute(soapRequest);
               if (soapResponse == "")
                   _Logger.Log(TraceLevel.Error, "Failed getting " + req.ToString() + " response");

               return soapResponse;
           });
        }

        public IIdleMessage Response(string soapResponse, IIdleMessage res)
        {
            try
            {
                IIdleMessage o = res.decode(soapResponse);
                if (o == null)
                    _Logger.Log(TraceLevel.Error, "Failed decoding SOAP response in IdlePort");

                return o;
            }
            catch(Exception)
            {
                _Logger.Log(TraceLevel.Error, "Failed decoding SOAP response in IdlePort");

                return null;
            }
        }
    }
}
