using OMBanking.Web.B2W.Orange.Idle;
using System.Configuration;
using System.Threading.Tasks;
using System.Web.Services;
//using System.ComponentModel.DataAnnotations;

namespace OMBanking.Web.Proxy.B2W.Idle
{
    //[Description("Binding Inscription")]
    public class IdleBinding : IdlePort
    {
        private string soapURL;
        private const string KEY = "idle?bic=";

        public IdleBinding() : base()
        {
            bool IsSimulationMode = bool.Parse(ConfigurationManager.AppSettings.Get("simulationMode"));
            if (IsSimulationMode)
                soapURL = ConfigurationManager.AppSettings.Get("orange_sim_endpoint_url");
            else
                soapURL = ConfigurationManager.AppSettings.Get("orange_prod_endpoint_url");
            soapURL += KEY;
            soapURL += ConfigurationManager.AppSettings.Get("bic");
        }

        //[Description("Dispo de la banque")]
        [WebMethod]
        public override Task<SetIdleResponse> SetIdle(SetIdleRequest req) 
        {
            return Task.Run(async () =>
            {
                string soapAction = "#setIdle";
                string soapResponse = await doPost(soapAction, soapURL, req);

                return (SetIdleResponse)Response(soapResponse, new SetIdleResponse());
            });
        }
    }
}
