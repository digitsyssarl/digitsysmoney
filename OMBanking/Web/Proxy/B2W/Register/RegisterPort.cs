using OMBanking.Web.B2W.Orange.Register;
using OMBanking.Web.Log;
using OMBanking.Web.Service;
using System;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Web.Services;
//using System.ComponentModel.DataAnnotations;

namespace OMBanking.Web.Proxy.B2W.Register
{
    //[Description("Port Inscription")]
    public abstract class RegisterPort : WebService, IRegister
    {
        private ServiceLogger _Logger;

        public RegisterPort()
        {
            _Logger = (ServiceLogger)LogFactory.create(LogBase.LoggingMode.SERVICE);
        }

        //[Description("Demande inscription")]
        [WebMethod]
        public abstract Task<REGRES> OmbRequest(REGREQ req);

        //[Description("Cloture inscription")]
        [WebMethod]
        public abstract Task<DELRES> OmbClose(DELREQ req);

        [WebMethod]
        public abstract Task<KYCRES> KYCRequest(KYCREQ req);

        public Task<string> doPost(string soapAction, string url, IRegisterMessage req)
        {
           return Task.Run(() =>
           {
               string soapRequest = req.encode();
               B2WServiceSOAPClient service = new B2WServiceSOAPClient(soapAction, url);
               string soapResponse = service.execute(soapRequest);
               if (soapResponse == "")
                   _Logger.Log(TraceLevel.Error, "Failed getting " + req.ToString() + " response");

               return soapResponse;
           });
        }

        public IRegisterMessage Response(string soapResponse, IRegisterMessage res)
        {
            try
            {
                IRegisterMessage o = res.decode(soapResponse);
                if (o == null)
                    _Logger.Log(TraceLevel.Error, "Failed decoding SOAP response in RegisterPort");

                return o;
            }
            catch (Exception)
            {
                _Logger.Log(TraceLevel.Error, "Failed decoding SOAP response in RegisterPort");

                return null;
            }
        }
    }
}
