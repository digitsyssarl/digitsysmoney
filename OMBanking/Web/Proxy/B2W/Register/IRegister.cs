using OMBanking.Web.B2W.Orange.Register;
using System;
using System.Threading.Tasks;

namespace OMBanking.Web.Proxy.B2W.Register
{
    public interface IRegister
    {
        Task<string> doPost(string soapAction, string url, IRegisterMessage req);

        IRegisterMessage Response(string soapResponse, IRegisterMessage res);
    }
}
