using OMBanking.Web.B2W.Orange.Register;
using System;
using System.Configuration;
using System.Threading.Tasks;
using System.Web.Services;
//using System.ComponentModel.DataAnnotations;

namespace OMBanking.Web.Proxy.B2W.Register
{
    //[Description("Port Inscription")]
    public class RegisterBinding : RegisterPort
    {
        private string soapURL;
        private const string KEY = "register?bic=";

        public RegisterBinding() : base()
        {
            bool IsSimulationMode = bool.Parse(ConfigurationManager.AppSettings.Get("simulationMode"));
            if (IsSimulationMode)
                soapURL = ConfigurationManager.AppSettings.Get("orange_sim_endpoint_url");
            else
                soapURL = ConfigurationManager.AppSettings.Get("orange_prod_endpoint_url");
            soapURL += KEY;
            soapURL += ConfigurationManager.AppSettings.Get("bic");
        }

        //[Description("Demande inscription")]
        [WebMethod]
        public override Task<REGRES> OmbRequest(REGREQ req)
        {
           return Task.Run(async () =>
           {
               string soapAction = "#doRegister";
               string soapResponse = await doPost(soapAction, soapURL, req);

               return (REGRES)Response(soapResponse, new REGRES());
           });
        }

        //[Description("Cloture inscription")]
        [WebMethod]
        public override Task<DELRES> OmbClose(DELREQ req)
        {
            return Task.Run(async () =>
            {
                string soapAction = "#doClose";
                string soapResponse = await doPost(soapAction, soapURL, req);

                return (DELRES)Response(soapResponse, new DELRES());
            });
        }

        [WebMethod]
        public override Task<KYCRES> KYCRequest(KYCREQ req)
        {
            return Task.Run(async () =>
            {
                string soapAction = "#NewOperation";
                string soapResponse = await doPost(soapAction, soapURL, req);

                return (KYCRES)Response(soapResponse, new KYCRES());
            });
        }
    }
}
