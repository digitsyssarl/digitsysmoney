using System.Web.Services;
using OMBanking.Web.Service;
using OMBanking.Web.Log;
using OMBanking.Web.B2W.Orange.StatusInquiry;
using System.Diagnostics;
using System.Threading.Tasks;
using System;

namespace OMBanking.Web.Proxy.B2W.StatusInquiry
{
    public abstract class TransactionStatusInquiryImplPort : WebService, ITransactionStatusInquiry
    {
        private ServiceLogger _Logger;

        public TransactionStatusInquiryImplPort()
        {
            _Logger = (ServiceLogger)LogFactory.create(LogBase.LoggingMode.SERVICE);
        }

        [WebMethod]
        public abstract Task<TransactionStatusInquiryResponse> TransactionStatusInquiry(TransactionStatusInquiry req);

        [WebMethod]
        public abstract Task<LastTransactionsResponse> LastTransactions(LastTransactionsRequest req);

        public Task<string> doPost(string soapAction, string url, ITransactionStatusInquiryMessage req)
        {
            return Task.Run(() =>
            {
                string soapRequest = req.encode();
                B2WServiceSOAPClient service = new B2WServiceSOAPClient(soapAction, url);
                string soapResponse = service.execute(soapRequest);

                if (soapResponse == "")
                    _Logger.Log(TraceLevel.Error, "Failed getting " + req.ToString() + " response");

                return soapResponse;
            });
        }

        public ITransactionStatusInquiryMessage Response(string soapResponse, ITransactionStatusInquiryMessage res)
        {
            try
            {
                ITransactionStatusInquiryMessage o = res.decode(soapResponse);
                if (o == null)
                    _Logger.Log(TraceLevel.Error, "Failed decoding SOAP response in TransactionStatusInquiryImplPort");

                return o;
            }
            catch (Exception)
            {
                _Logger.Log(TraceLevel.Error, "Failed decoding SOAP response in TransactionStatusInquiryImplPort");

                return null;
            }
        }
    }
}
