
using OMBanking.Web.B2W.Orange.StatusInquiry;
using System.Threading.Tasks;

namespace OMBanking.Web.Proxy.B2W.StatusInquiry
{
    public interface ITransactionStatusInquiry
    {
        Task<string> doPost(string soapAction, string url, ITransactionStatusInquiryMessage req);

        ITransactionStatusInquiryMessage Response(string soapResponse, ITransactionStatusInquiryMessage res);
    }
}
