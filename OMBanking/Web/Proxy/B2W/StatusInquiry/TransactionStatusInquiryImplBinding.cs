using OMBanking.Web.B2W.Orange.StatusInquiry;
using System.Configuration;
using System.Threading.Tasks;
using System.Web.Services;

namespace OMBanking.Web.Proxy.B2W.StatusInquiry
{
    public class TransactionStatusInquiryImplBinding : TransactionStatusInquiryImplPort
    {
        private string soapURL;
        private const string KEY = "status-inquiry?bic=";

        public TransactionStatusInquiryImplBinding() : base()
        {
            bool IsSimulationMode = bool.Parse(ConfigurationManager.AppSettings.Get("simulationMode"));
            if (IsSimulationMode)
                soapURL = ConfigurationManager.AppSettings.Get("orange_sim_endpoint_url");
            else
                soapURL = ConfigurationManager.AppSettings.Get("orange_prod_endpoint_url");
            soapURL += KEY;
            soapURL += ConfigurationManager.AppSettings.Get("bic");
        }

        [WebMethod]
        public override Task<TransactionStatusInquiryResponse> TransactionStatusInquiry(TransactionStatusInquiry req)
        {
            return Task.Run(async () =>
            {
                string soapAction = "";
                string soapResponse = await doPost(soapAction, soapURL, req);

                return (TransactionStatusInquiryResponse)Response(soapResponse, new TransactionStatusInquiryResponse());
            });
        }

        [WebMethod]
        public override Task<LastTransactionsResponse> LastTransactions(LastTransactionsRequest req)
        {
            return Task.Run(async () =>
            {
                string soapAction = "";
                string soapResponse = await doPost(soapAction, soapURL, req);

                return (LastTransactionsResponse)Response(soapResponse, new LastTransactionsResponse());
            });
        }
    }
}
