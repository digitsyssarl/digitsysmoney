﻿using OMBanking.Web.Bank;
using OMBanking.Web.Log;
using OMBanking.Web.Proxy.Bank;
using OMBanking.Web.Security;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace OMBanking.Web.DB
{
    public class B2WHelper
    {
        public Boolean IsSimulationMode;
        public const String  SIMULATION_ACCOUNT_NO = "37250000055003";

        private const string EXEC_STORED_PROC = "EXEC";
        private const string CANCEL_TRANSFER_STORED_PROC = "p_AcToMoMoTransactionReversal";
        private const string SERIAL_ID = "@SerialID";
        private const string CANCEL_ACTOMOMO_TRANSFER_STORED_PROC = "p_AcToMoMoReversal";
        private const string CANCEL_MOMOTOAC_TRANSFER_STORED_PROC = "p_MoMoToAcReversal";
        private const string SERIAL_ID = "@SerialID";
        private const string SERIAL_ID1 = "@SerialID1";

        private const string WALLET_TO_ACCOUNT_TRANSFER_STORED_PROC = "p_MoMoToAcTransaction";
        private const string ACCOUNT_TO_WALLET_TRANSFER_STORED_PROC = "p_AcToMoMoTransaction";
        private const string ACCOUNT_ID = "@AccountID";
        private const string ACCOUNT_ID1 = "@AccountID1";
        private const string AMOUNT = "@Amount";
        private const string FEE = "@Fee";
        private const string TRANS_DESC = "@TrxDescription";
        private const string W2B_TRANS_DESC = "Mobile to Account transfer";
        private const string B2W_TRANS_DESC = "Account to mobile transfer";
        private const string GSM_TRANS_ID = "@GSMTransactionID";
        private const string GSM_OP_ID = "@GSMOperationID";

        private const string GET_ACCOUNT_BALANCE_STORED_PROC = "p_USSDInquiry";
        private const string GET_MINI_STATEMENT_STORED_PROC  = "p_USSDInquiry";
        private const string TRANSFERT_STATUS_INQUIRY_STORED_PROC = "p_USSDInquiry";
        private const string MBANKING_ID = "@mBankingID";
        private const string INQUIRY_TYPE = "@InquiryType";

        const int ERROR_SHARING_VIOLATION = 32;
        const int ERROR_LOCK_VIOLATION = 33;

        private DBLogger _Logger;
        private string _ConnectionString;
        private static DbProviderFactory _Factory;
        private SqlConnection _SqlConnection;

        public B2WHelper()
        {
            _Logger           = (DBLogger)LogFactory.create(LogBase.LoggingMode.DATABASE);
            ConnectionStringSettings config = ConfigurationManager.ConnectionStrings["BankConnection"];
            _ConnectionString = config.ToString();
            IsSimulationMode  = Boolean.Parse(ConfigurationManager.AppSettings.Get("simulationMode"));
            _SqlConnection = new SqlConnection(_ConnectionString);
        }

        public DbConnection Open()
        {
            _Factory   = DbProviderFactories.GetFactory("System.Data.SqlClient");
            DbConnection connection     = _Factory.CreateConnection();
            connection.ConnectionString = _ConnectionString;
            try
            {
                connection.Open();

                return connection;
            }
            catch (InvalidOperationException exception)
            {
                _Logger.Log(TraceLevel.Error, exception.Message.ToString());
            }
            catch (SqlException exception1)
            {
                _Logger.Log(TraceLevel.Error, exception1.Message.ToString());
            }
            catch (Exception exception2)
            {
                _Logger.Log(TraceLevel.Error, exception2.Message.ToString());
            }

            return null;
        }
      
        public Task<DbDataReader> execStoredProcedure(string StoredProcedure, params object[] ProcedureParameters)
        {
            return Task.Run(() =>
            {
                DbConnection connection = Open();
                if (connection == null)
                    return null;

                DbCommand command = _Factory.CreateCommand();
                command.Connection = connection;
                command.CommandTimeout = 120;

                object[] objArray = ProcedureParameters;
                int idx = 0;
                string exec = "";
                while (true)
                {
                    if (idx < objArray.Length)
                    {
                        object o = objArray[idx];
                        exec = ((idx % 2) != 0)
                              ? (!(o.GetType() == typeof(string))
                              ? (!(o.GetType() == typeof(DateTime))
                              ? (!(o.GetType() == typeof(int))
                              ? (!(o.GetType() == typeof(double))
                              ? (!(o.GetType() == typeof(decimal))
                              ? (!(o.GetType() == typeof(int))
                              ? (!(o.GetType() == typeof(bool))
                              ? (exec + " '" + o.ToString() + "',")
                              : (!((bool)o) ? (exec + " 0,") : (exec + " 1,")))
                              : (exec + o.ToString() + ","))
                              : (exec + o.ToString() + ","))
                              : (exec + o.ToString() + ","))
                              : (exec + o.ToString() + ","))
                              : (exec + " '" + o.ToString() + "',"))
                              : (exec + " '" + o.ToString() + "',"))
                              : (exec + o.ToString() + "=");
                        idx++;
                        continue;
                    }
                    if (exec.Length > 1)
                        exec = exec.Substring(0, exec.Length - 1);

                    _Logger.Log(TraceLevel.Info, EXEC_STORED_PROC + " " + StoredProcedure + " " + exec);
                    StoredProcedure = EXEC_STORED_PROC + " " + StoredProcedure + " " + exec;
                    command.CommandType = CommandType.Text;
                    command.CommandText = StoredProcedure;
                    try
                    {
                        return command.ExecuteReader(CommandBehavior.CloseConnection);
                    }
                    catch (SqlException exception)
                    {
                        _Logger.Log(TraceLevel.Error, exception.Message.ToString());
                    }
                    break;
                }

                return null;
            });
        }

        public Task<DataTable> GetIdentities(string id)
        {
            return Task.Run(() =>
            {
                DataTable dt = new DataTable();
                try
                {
                    _SqlConnection.Open();
                    SqlCommand command;
                    if (id != null || id == "")
                    {
                        string query = "SELECT FirstName,LastName,MiddleName,GenderID,"
                                       + "NationalityID,DateOfBirth,Age,MaritalStatusID,"
                                       + "PassportNo,PassportIssuedCityID,PassportExpiryDate,"
                                       + "ResidentID,WorkPermitNo"
                                       + " FROM dbo.t_ClientIndividual"
                                       + " WHERE ((LOWER(FirstName)=@Id)"
                                       + " OR (LOWER(LastName)=@Id)"
                                       + " OR (LOWER(MiddleName)=@Id)"
                                       + " OR (PassportNo=@Id)"
                                       + " OR (PassportIssuedCityID=@Id)"
                                       + " OR (ResidentID=@Id)"
                                       + " OR (NationalityID=@Id)"
                                       + " OR (WorkPermitNo=@Id))";
                        command = new SqlCommand(query, _SqlConnection);
                        command.Parameters.AddWithValue("@Id", id.ToLower());                       
                    }
                    else
                    {
                        string query = "SELECT FirstName,LastName,MiddleName,GenderID,"
                                       + "NationalityID,DateOfBirth,Age,MaritalStatusID,"
                                       + "PassportNo,PassportIssuedCityID,PassportExpiryDate,"
                                       + "ResidentID,WorkPermitNo"
                                       + " FROM dbo.t_ClientIndividual";
                        command = new SqlCommand(query, _SqlConnection);
                    }
                    SqlDataAdapter dap = new SqlDataAdapter();
                    dap.SelectCommand = command;
                    dap.Fill(dt);
                    _SqlConnection.Close();

                    return dt;
                }
                catch (Exception e)
                {
                    _Logger.Log(TraceLevel.Error, e.Message);
                }

                return dt;
            });
        }


        public Task<DataTable> GetAccounts(string id)
        {
            return Task.Run(() =>
            {
                DataTable dt = new DataTable();
                try
                {
                    _SqlConnection.Open();
                    SqlCommand command;
                    if ((id != null || id == "") && (!id.Contains("'") || !id.Contains("\"")))
                    {
                        string query = "SELECT ProductID,OurBranchID,AccountID,AccountStatusID,Name,Phone1,Mobile," 
                                       + "Address1,Address2,CityID,CountryID,CreatedBy,CreatedOn," 
                                       + "ModifiedBy,ModifiedOn"
                                       + " FROM t_AccountCustomer"
                                       + " WHERE (Substring(ProductID, 3, 6) IN (SELECT ID FROM dbo.ClientAccountIDRange))"
                                       + " AND ((OurBranchID=@Id)"
                                       + " OR (AccountID LIKE '%" + id + "%')"
                                       + " OR (AccountStatusID=@Id)"
                                       + " OR (LOWER(Name) LIKE '%" + id + "%')"
                                       + " OR (Phone1 LIKE '%" + id + "%')"
                                       + " OR (Mobile LIKE '%" + id + "%')"
                                       + " OR (LOWER(Address1) LIKE '%" + id + "%')"
                                       + " OR (LOWER(Address2) LIKE '%" + id + "%')"
                                       + " OR (LOWER(CityID) LIKE '%" + id + "%')"
                                       + " OR (LOWER(CountryID)=@Id)"
                                       + " OR (LOWER(CreatedBy)=@Id)"
                                       + " OR (LOWER(ModifiedBy)=@Id))";
                        command = new SqlCommand(query, _SqlConnection);
                        command.Parameters.AddWithValue("@Id", id.ToLower());
                    }
                    else
                    {
                        string query = "SELECT ProductID,OurBranchID,AccountID,AccountStatusID,Name,Phone1,Mobile,"
                                       + "Address1,Address2,CityID,CountryID,CreatedBy,CreatedOn,"
                                       + "ModifiedBy,ModifiedOn"
                                       + " FROM t_AccountCustomer"
                                       + " WHERE (Substring(ProductID, 3, 6) IN (SELECT ID FROM dbo.ClientAccountIDRange))";
                        command = new SqlCommand(query, _SqlConnection);
                    }
                    SqlDataAdapter dap = new SqlDataAdapter();
                    dap.SelectCommand = command;
                    dap.Fill(dt);
                    _SqlConnection.Close();

                    return dt;
                }
                catch (Exception e)
                {
                    _Logger.Log(TraceLevel.Error, e.Message);
                }

                return dt;
            });
        }

        public Task<bool> TriggerSmsAlert(SmsAlert sa)
        {
            return Task.Run(() =>
            {
                try
                {
                    string sqlQuery = "INSERT INTO dbo.alerts "
                                      + "(MOBILEnumber,messagedescription,PostedOn,"
                                      + "PostedBy,Provider)"
                                      + " VALUES "
                                      + "(@MOBILEnumber,@messagedescription,@PostedOn,"
                                      + "@PostedBy,@Provider)";
                    _SqlConnection.Open();

                    SqlCommand command = new SqlCommand(sqlQuery, _SqlConnection);
                    command.Parameters.AddWithValue("@MOBILEnumber", sa.MOBILEnumber);
                    command.Parameters.AddWithValue("@messagedescription", sa.messagedescription);
                    command.Parameters.AddWithValue("@PostedOn", sa.PostedOn);
                    command.Parameters.AddWithValue("@PostedBy", sa.PostedBy);
                    command.Parameters.AddWithValue("@Provider", sa.Provider);

                    command.ExecuteNonQuery();
                    _SqlConnection.Close();

                    return true;
                }
                catch (Exception e)
                {
                    _Logger.Log(TraceLevel.Error, e.Message);
                }

                return false;
            });
        }

        public Task<CancelTransferResponse> CancelTransfer(CancelTransfer entity, string gsmOpID)
        {
            return Task.Run(async () =>
            {
                var response = new CancelTransferResponse();
                response.CBAReferenceNo = Guid.NewGuid().ToString().Replace('-', '0').Substring(0, 0x10);
                double amount = 0.0;
                if (entity.mmHeaderInfo != null)
                {
                    response.externalRefNo = entity.mmHeaderInfo.requestId;
                    response.mmHeaderInfo = new HeaderResponse();
                    response.mmHeaderInfo.requestId = entity.mmHeaderInfo.requestId;
                    response.mmHeaderInfo.operatorCode = ConfigurationManager.AppSettings.Get("operatorCode");
                    response.mmHeaderInfo.affiliateCode = ConfigurationManager.AppSettings.Get("affiliate");

                    TransactionLog tl = await new USSDHelper().SearchTransactionLogRef(entity.externalRefNo);
                    tl.Charge = (tl.Charge < 0) ? 0 : tl.Charge;
                    DbDataReader reader = null;
                    string accountNo = "";
                    if (tl.AccountNo != null)
                    {
                        accountNo = tl.AccountNo.Replace(ConfigurationManager.AppSettings.Get("bic"), "");
                        accountNo = Utils.ReplaceFirst(accountNo, ConfigurationManager.AppSettings.Get("alias_first_digits"),
                                                                  ConfigurationManager.AppSettings.Get("account_first_digits"));
                    }
                    string desc = await new USSDHelper().GetPhoneNoByAlias(tl.AccountNo);
                    if (tl.TranType == 3) // Momo to Account
                    {
                        desc = desc + " " + W2B_TRANS_DESC;
                        reader = await execStoredProcedure(CANCEL_MOMOTOAC_TRANSFER_STORED_PROC,
                                                            new object[]
                                                            {
                                                                ACCOUNT_ID  , accountNo       ,
                                                                AMOUNT      , -tl.Amount      ,
                                                                FEE         , 0               ,
                                                                TRANS_DESC  , desc            ,
                                                                GSM_TRANS_ID, entity.mmHeaderInfo.requestId,
                                                                GSM_OP_ID   , "ORANGE"
                                                            });
                    }
                    else if(tl.TranType == 2) // Account to Momo
                    {
                        desc = desc + " " + B2W_TRANS_DESC;
                        reader = await execStoredProcedure(CANCEL_ACTOMOMO_TRANSFER_STORED_PROC,
                                                            new object[]
                                                            {
                                                                ACCOUNT_ID  , accountNo       ,
                                                                AMOUNT      , -tl.Amount      ,
                                                                FEE         , 0               ,
                                                                TRANS_DESC  , desc            ,
                                                                GSM_TRANS_ID, entity.mmHeaderInfo.requestId,
                                                                GSM_OP_ID   , "ORANGE"
                                                            });
                    }
                    string bank_response = null;
                    if (reader != null)
                    {
                        bank_response = (string)ReadSqlData(reader, "FieldValue", null, null);
                        if (bank_response != null)
                        {
                            if (bank_response.CompareTo(ErrorCodeHandler.BANK_SUCCESS_CODE) == 0
                                || bank_response == "")
                            {
                                response.mmHeaderInfo.responseCode = "000";
                                response.mmHeaderInfo.responseMessage = ErrorCodeHandler.Description("000");
                                TransactionLog log = new TransactionLog
                                {
                                    Id = Guid.NewGuid(),
                                    Date = DateTime.Now,
                                    AccountNo = tl.AccountNo,
                                    AffiliateCode = entity.mmHeaderInfo.affiliateCode,
                                    Amount = Convert.ToDecimal(amount),
                                    Charge = 0m,
                                    OperatorCode = entity.mmHeaderInfo.operatorCode,
                                    Complete = true,
                                    ResponseCode = 00,
                                    ResponseMessage = response.mmHeaderInfo.responseMessage,
                                    TranType = 4,
                                    RequestId = entity.mmHeaderInfo.requestId,
                                    TranRef = response.CBAReferenceNo,
                                    GSMOperationID = gsmOpID
                                };
                                USSDHelper ussdDb = new USSDHelper();
                                bool result = await ussdDb.AddTransactionLog(log);
                                if (!result)
                                {
                                    ServiceLogger _Logger = (ServiceLogger)LogFactory.create(Logger.LoggingMode.SERVICE);
                                    _Logger.Log(TraceLevel.Error, "Adding Transaction Log Failed !");
                                }
                            }
                            else
                            {
                                HeaderResponse hResp = ErrorChecking(bank_response, "Cancel transfer failed (");
                                response.mmHeaderInfo.responseCode = hResp.responseCode;
                                response.mmHeaderInfo.responseMessage = hResp.responseMessage;
                                TransactionLog log = new TransactionLog
                                {
                                    Id = Guid.NewGuid(),
                                    Date = DateTime.Now,
                                    AccountNo = tl.AccountNo,
                                    AffiliateCode = entity.mmHeaderInfo.affiliateCode,
                                    Amount = Convert.ToDecimal(amount),
                                    Charge = 0m,
                                    OperatorCode = entity.mmHeaderInfo.operatorCode,
                                    Complete = false,
                                    ResponseCode = 17,
                                    ResponseMessage = response.mmHeaderInfo.responseMessage,
                                    TranType = 4,
                                    RequestId = entity.mmHeaderInfo.requestId,
                                    TranRef = response.CBAReferenceNo,
                                    GSMOperationID = gsmOpID
                                };
                                USSDHelper ussdDb = new USSDHelper();
                                bool result = await ussdDb.AddTransactionLog(log);
                                if (!result)
                                {
                                    ServiceLogger _Logger = (ServiceLogger)LogFactory.create(Logger.LoggingMode.SERVICE);
                                    _Logger.Log(TraceLevel.Error, "Adding Transaction Log Failed !");
                                }
                                _Logger.Log(TraceLevel.Error, "Cancel transfer failed (" + bank_response + ")");
                            }
                        }
                        else
                        {
                            response.mmHeaderInfo.responseCode = "E01";
                            response.mmHeaderInfo.responseMessage = "Cancel transfer failed (" + ErrorCodeHandler.Description("E01") + ")";
                            _Logger.Log(TraceLevel.Error, "Cancel transfer failed ");
                        }

                        _Logger.LogSqlDataReader(reader, TraceLevel.Info, CANCEL_TRANSFER_STORED_PROC);
                    }
                    else
                    {
                        response.mmHeaderInfo.responseCode = "E02";
                        response.mmHeaderInfo.responseMessage = "Cancel transfer failed (" + ErrorCodeHandler.Description("E02") + ")";
                        _Logger.Log(TraceLevel.Error, "Cancel transfer failed");
                    }

                    return response;
                }

                return null;
            });
        }

        public Task<WalletToAccountTransferResponse> WalletToAccountTransfer(WalletToAccountTransfer entity, string gsmOpID)
        {
            return Task.Run(async () =>
            {
                var response = new WalletToAccountTransferResponse();
                Utils._LastErrorCode = new String[2];
                if (gsmOpID.CompareTo("ORANGE") == 0)
                    response.CBAReferenceNo = Guid.NewGuid().ToString().Replace('-', '0').Substring(0, 0x10);
                else
                    response.CBAReferenceNo = Utils.ReferenceID;

                if (entity.mmHeaderInfo != null)
                {
                    response.externalRefNo = entity.mmHeaderInfo.requestId;
                    response.mmHeaderInfo = new HeaderResponse();
                    response.mmHeaderInfo.requestId = entity.mmHeaderInfo.requestId;
                    response.mmHeaderInfo.operatorCode = ConfigurationManager.AppSettings.Get("operatorCode");
                    response.mmHeaderInfo.affiliateCode = ConfigurationManager.AppSettings.Get("affiliate");
                    string accountNo = "";
                    if (entity.accountNo != null && entity.accountNo != "")
                        accountNo = entity.accountNo;
                    else if (entity.accountAlias != null && entity.accountAlias != "")
                    {
                        accountNo = entity.accountAlias.Replace(ConfigurationManager.AppSettings.Get("bic"), "");
                        accountNo = Utils.ReplaceFirst(accountNo, ConfigurationManager.AppSettings.Get("alias_first_digits"),
                                                                  ConfigurationManager.AppSettings.Get("account_first_digits"));
                    }
                    if (IsSimulationMode)
                        accountNo = SIMULATION_ACCOUNT_NO;

                    string desc = await new USSDHelper().GetPhoneNoByAlias(entity.accountAlias);
                    double ora_w2b_fee = Fees.GetW2BFee(entity.amount);
                    bool indirectSimMode = Boolean.Parse(ConfigurationManager.AppSettings.Get("indirectSimMode"));
                    if (ora_w2b_fee > 0)
                    {
                        desc = desc + " Wallet To Account Transfer";
                        DbDataReader reader = await execStoredProcedure(WALLET_TO_ACCOUNT_TRANSFER_STORED_PROC,
                                                                  new object[]
                                                                  {
                                                                      ACCOUNT_ID  , accountNo       ,
                                                                      AMOUNT      , entity.amount   ,
                                                                      FEE         , 0               ,
                                                                      TRANS_DESC  , desc  ,
                                                                      GSM_TRANS_ID, entity.mmHeaderInfo.requestId,
                                                                      GSM_OP_ID   , gsmOpID
                                                                  });
                        string[] bank_response = null;
                        if (reader != null)
                        {
                            bank_response = (string[])ReadSqlData(reader, "FieldValue", null, "W2A");
                            if (bank_response != null && bank_response[0] != null)
                            {
                                if (bank_response[0].CompareTo(ErrorCodeHandler.BANK_SUCCESS_CODE) == 0
                                    || bank_response[0] == "")
                                {
                                    if (indirectSimMode)
                                    {
                                        response.mmHeaderInfo.responseCode = "E11";
                                        response.mmHeaderInfo.responseMessage = ErrorCodeHandler.Description("E11");
                                    }
                                    else
                                    {
                                        response.mmHeaderInfo.responseCode = "000";
                                        response.mmHeaderInfo.responseMessage = ErrorCodeHandler.Description("000");
                                    }
                                    Utils._LastErrorCode[0] = "000";
                                    Utils._LastErrorCode[1] = ErrorCodeHandler.Description("000");
                                    TransactionLog log = new TransactionLog
                                    {
                                        Id = Guid.NewGuid(),
                                        Date = DateTime.Now,
                                        AccountNo = entity.accountAlias,
                                        AffiliateCode = entity.mmHeaderInfo.affiliateCode,
                                        Amount = Convert.ToDecimal(entity.amount),
                                        Charge = Convert.ToDecimal(ora_w2b_fee),
                                        OperatorCode = entity.mmHeaderInfo.operatorCode,
                                        Complete = true,
                                        ResponseCode = 00,
                                        ResponseMessage = ((indirectSimMode)
                                                            ? "Indirect Success"
                                                            : response.mmHeaderInfo.responseMessage),
                                        TranType = 3,
                                        RequestId = entity.mmHeaderInfo.requestId,
                                        TranRef = response.CBAReferenceNo,
                                        SerialID = bank_response[1],
                                        GSMOperationID = gsmOpID
                                    };
                                    USSDHelper ussdDb = new USSDHelper();
                                    bool result = await ussdDb.AddTransactionLog(log);
                                    if (!result)
                                    {
                                        ServiceLogger _Logger = (ServiceLogger)LogFactory.create(Logger.LoggingMode.SERVICE);
                                        _Logger.Log(TraceLevel.Error, "Adding Transaction Log Failed !");
                                    }
                                }
                                else
                                {
                                    if (bank_response[0].CompareTo(ErrorCodeHandler.OVERLIMIT_AMOUNT_CODE) == 0)
                                    {
                                        if (indirectSimMode)
                                        {
                                            response.mmHeaderInfo.responseCode = "E11";
                                            response.mmHeaderInfo.responseMessage = ErrorCodeHandler.Description("E11");
                                        }
                                        else
                                        {
                                            response.mmHeaderInfo.responseCode = "E18";
                                            response.mmHeaderInfo.responseMessage = bank_response[1];
                                        }
                                        Utils._LastErrorCode[0] = "E18";
                                        Utils._LastErrorCode[1] = bank_response[1];
                                    }
                                    else
                                    {
                                        HeaderResponse hResp = ErrorChecking(bank_response[0], "Wallet to account transfer failed (");
                                        response.mmHeaderInfo.responseCode = "E11";
                                        response.mmHeaderInfo.responseMessage = ErrorCodeHandler.Description("E11");
                                        Utils._LastErrorCode[0] = hResp.responseCode;
                                        Utils._LastErrorCode[1] = hResp.responseMessage;
                                    }
                                    TransactionLog log = new TransactionLog
                                    {
                                        Id = Guid.NewGuid(),
                                        Date = DateTime.Now,
                                        AccountNo = entity.accountAlias,
                                        AffiliateCode = entity.mmHeaderInfo.affiliateCode,
                                        Amount = Convert.ToDecimal(entity.amount),
                                        Charge = 0m,
                                        OperatorCode = entity.mmHeaderInfo.operatorCode,
                                        Complete = false,
                                        ResponseCode = ((bank_response[0]
                                                         .CompareTo(ErrorCodeHandler.OVERLIMIT_AMOUNT_CODE) == 0)
                                                        ? 18 : 17),
                                        ResponseMessage = Utils._LastErrorCode[1],
                                        TranType = 3,
                                        RequestId = entity.mmHeaderInfo.requestId,
                                        TranRef = response.CBAReferenceNo,
                                        GSMOperationID = gsmOpID
                                    };
                                    USSDHelper ussdDb = new USSDHelper();
                                    bool result = await ussdDb.AddTransactionLog(log);
                                    if (!result)
                                    {
                                        ServiceLogger _Logger = (ServiceLogger)LogFactory.create(Logger.LoggingMode.SERVICE);
                                        _Logger.Log(TraceLevel.Error, "Adding Transaction Log Failed !");
                                    }
                                    _Logger.Log(TraceLevel.Error, "Wallet to Account transfer failed (" + bank_response[1] + ")");
                                }
                            }
                            else
                            {
                                response.mmHeaderInfo.responseCode = "E11";
                                response.mmHeaderInfo.responseMessage = ErrorCodeHandler.Description("E11");
                                Utils._LastErrorCode[0] = "E03";
                                Utils._LastErrorCode[1] = "Wallet to account transfer failed (Bank Technical Error)";
                                _Logger.Log(TraceLevel.Error, "Wallet to account transfer failed (Bank Technical Error)");
                            }

                            _Logger.LogSqlDataReader(reader, TraceLevel.Info, WALLET_TO_ACCOUNT_TRANSFER_STORED_PROC);
                        }
                        else
                        {
                            response.mmHeaderInfo.responseCode = "E11";
                            response.mmHeaderInfo.responseMessage = ErrorCodeHandler.Description("E11");
                            Utils._LastErrorCode[0] = "E04";
                            Utils._LastErrorCode[1] = "Wallet to account transfer failed (Bank Technical Error)";
                            _Logger.Log(TraceLevel.Error, "Wallet to account transfer failed (Bank Technical Error)");
                        }

                        return response;
                    }
                    else 
                    {
                        if (indirectSimMode)
                        {
                            response.mmHeaderInfo.responseCode = "E11";
                            response.mmHeaderInfo.responseMessage = ErrorCodeHandler.Description("E11");
                        }
                        else
                        {
                            response.mmHeaderInfo.responseCode = "E22";
                            response.mmHeaderInfo.responseMessage = (ora_w2b_fee < 0) 
                                                                    ? ErrorCodeHandler.Description("E22")
                                                                    : "Transaction Violates Account's Maximum Balance";
                        }
                        Utils._LastErrorCode[0] = "E22";
                        Utils._LastErrorCode[1] = (ora_w2b_fee < 0) ? ErrorCodeHandler.Description("E22")
                                                                    : "Transaction Violates Account's Maximum Balance";
                        TransactionLog log = new TransactionLog
                        {
                            Id = Guid.NewGuid(),
                            Date = DateTime.Now,
                            AccountNo = entity.accountAlias,
                            AffiliateCode = entity.mmHeaderInfo.affiliateCode,
                            Amount = Convert.ToDecimal(entity.amount),
                            Charge = 0m,
                            OperatorCode = entity.mmHeaderInfo.operatorCode,
                            Complete = false,
                            ResponseCode = 22,
                            ResponseMessage = Utils._LastErrorCode[1],
                            TranType = 3,
                            RequestId = entity.mmHeaderInfo.requestId,
                            TranRef = response.CBAReferenceNo,
                            GSMOperationID = gsmOpID
                        };
                        USSDHelper ussdDb = new USSDHelper();
                        bool result = await ussdDb.AddTransactionLog(log);
                        if (!result)
                        {
                            ServiceLogger _Logger = (ServiceLogger)LogFactory.create(Logger.LoggingMode.SERVICE);
                            _Logger.Log(TraceLevel.Error, "Adding Transaction Log Failed !");
                        }
                        _Logger.Log(TraceLevel.Error, "Wallet to Account transfer failed (" + Utils._LastErrorCode[1] + ")");

                        return response;
                    }
                }

                return null;
            });
        }

        public Task<AccountToWalletTransferResponse> AccountToWalletTransfer(AccountToWalletTransfer entity, string gsmOpID)
        {
            return Task.Run(async () =>
            {
                var response = new AccountToWalletTransferResponse();
                Utils._LastErrorCode = new String[2];
                if (gsmOpID.CompareTo("ORANGE") == 0)
                    response.CBAReferenceNo = Guid.NewGuid().ToString().Replace('-', '0').Substring(0, 0x10);
                else
                    response.CBAReferenceNo = Utils.ReferenceID;

                if (entity.mmHeaderInfo != null)
                {
                    response.externalRefNo = entity.mmHeaderInfo.requestId;
                    response.mmHeaderInfo = new HeaderResponse();
                    response.mmHeaderInfo.requestId = entity.mmHeaderInfo.requestId;
                    response.mmHeaderInfo.operatorCode = ConfigurationManager.AppSettings.Get("operatorCode");
                    response.mmHeaderInfo.affiliateCode = ConfigurationManager.AppSettings.Get("affiliate");
                    string accountNo = "";
                    if (entity.accountNo != null && entity.accountNo != "")
                        accountNo = entity.accountNo;
                    else if (entity.accountAlias != null && entity.accountAlias != "")
                    {
                        accountNo = entity.accountAlias.Replace(ConfigurationManager.AppSettings.Get("bic"), "");
                        accountNo = Utils.ReplaceFirst(accountNo, ConfigurationManager.AppSettings.Get("alias_first_digits"),
                                                                  ConfigurationManager.AppSettings.Get("account_first_digits"));
                    }
                    if (IsSimulationMode)
                        accountNo = SIMULATION_ACCOUNT_NO;

                    string desc = await new USSDHelper().GetPhoneNoByAlias(entity.accountAlias);
                    double ora_b2w_fee = Fees.GetB2WFee(entity.amount);
                    bool indirectSimMode = Boolean.Parse(ConfigurationManager.AppSettings.Get("indirectSimMode"));
                    if (ora_b2w_fee > 0)
                    {
                        desc = desc + " Account To Wallet Transfer";
                        DbDataReader reader = await execStoredProcedure(ACCOUNT_TO_WALLET_TRANSFER_STORED_PROC,
                                                                  new object[]
                                                                  {
                                                                      ACCOUNT_ID  , accountNo       ,
                                                                      AMOUNT      , entity.amount   ,
                                                                      FEE         , 0               ,
                                                                      TRANS_DESC  , desc  ,
                                                                      GSM_TRANS_ID, entity.mmHeaderInfo.requestId,
                                                                      GSM_OP_ID   , gsmOpID
                                                                  });
                        string[] bank_response = null;
                        if (reader != null)
                        {
                            bank_response = (string[])ReadSqlData(reader, "FieldValue", null, "A2W");
                            if (bank_response != null && bank_response[0] != null)
                            {
                                if (bank_response[0].CompareTo(ErrorCodeHandler.BANK_SUCCESS_CODE) == 0
                                    || bank_response[0] == "")
                                {
                                    if (indirectSimMode)
                                    {
                                        response.mmHeaderInfo.responseCode = "E11";
                                        response.mmHeaderInfo.responseMessage = ErrorCodeHandler.Description("E11");
                                    }
                                    else
                                    {
                                        response.mmHeaderInfo.responseCode = "000";
                                        response.mmHeaderInfo.responseMessage = ErrorCodeHandler.Description("000");
                                    }
                                    Utils._LastErrorCode[0] = "000";
                                    Utils._LastErrorCode[1] = ErrorCodeHandler.Description("000");
                                    TransactionLog log = new TransactionLog
                                    {
                                        Id = Guid.NewGuid(),
                                        Date = DateTime.Now,
                                        AccountNo = entity.accountAlias,
                                        AffiliateCode = entity.mmHeaderInfo.affiliateCode,
                                        Amount = Convert.ToDecimal(entity.amount),
                                        Charge = Convert.ToDecimal(ora_b2w_fee),
                                        OperatorCode = entity.mmHeaderInfo.operatorCode,
                                        Complete = true,
                                        ResponseCode = 00,
                                        ResponseMessage = ((indirectSimMode)
                                                          ? "Indirect Success"
                                                          : response.mmHeaderInfo.responseMessage),
                                        TranType = 2,
                                        RequestId = entity.mmHeaderInfo.requestId,
                                        TranRef = response.CBAReferenceNo,
                                        SerialID = bank_response[1],
                                        GSMOperationID = gsmOpID
                                    };
                                    USSDHelper ussdDb = new USSDHelper();
                                    bool result = await ussdDb.AddTransactionLog(log);
                                    if (!result)
                                    {
                                        ServiceLogger _Logger = (ServiceLogger)LogFactory.create(Logger.LoggingMode.SERVICE);
                                        _Logger.Log(TraceLevel.Error, "Adding Transaction Log Failed !");
                                    }
                                }
                                else
                                {
                                    if (bank_response[0].CompareTo(ErrorCodeHandler.INSUFFICIENT_BALANCE_CODE) == 0)
                                    {
                                        if (indirectSimMode)
                                        {
                                            response.mmHeaderInfo.responseCode = "E11";
                                            response.mmHeaderInfo.responseMessage = ErrorCodeHandler.Description("E11");
                                        }
                                        else
                                        {
                                            response.mmHeaderInfo.responseCode = "E16";
                                            response.mmHeaderInfo.responseMessage = bank_response[1];
                                        }
                                        Utils._LastErrorCode[0] = "E16";
                                        Utils._LastErrorCode[1] = bank_response[1];
                                    }
                                    else
                                    {
                                        HeaderResponse hResp = ErrorChecking(bank_response[0], "Account to wallet transfer failed (");
                                        response.mmHeaderInfo.responseCode = "E11";
                                        response.mmHeaderInfo.responseMessage = ErrorCodeHandler.Description("E11");
                                        Utils._LastErrorCode[0] = hResp.responseCode;
                                        Utils._LastErrorCode[1] = hResp.responseMessage;
                                    }
                                    TransactionLog log = new TransactionLog
                                    {
                                        Id = Guid.NewGuid(),
                                        Date = DateTime.Now,
                                        AccountNo = entity.accountAlias,
                                        AffiliateCode = entity.mmHeaderInfo.affiliateCode,
                                        Amount = Convert.ToDecimal(entity.amount),
                                        Charge = 0m,
                                        OperatorCode = entity.mmHeaderInfo.operatorCode,
                                        Complete = false,
                                        ResponseCode = ((bank_response[0]
                                                         .CompareTo(ErrorCodeHandler.INSUFFICIENT_BALANCE_CODE) == 0)
                                                        ? 16 : 17),
                                        ResponseMessage = Utils._LastErrorCode[1],
                                        TranType = 2,
                                        RequestId = entity.mmHeaderInfo.requestId,
                                        TranRef = response.CBAReferenceNo,
                                        GSMOperationID = gsmOpID
                                    };
                                    USSDHelper ussdDb = new USSDHelper();
                                    bool result = await ussdDb.AddTransactionLog(log);
                                    if (!result)
                                    {
                                        ServiceLogger _Logger = (ServiceLogger)LogFactory.create(Logger.LoggingMode.SERVICE);
                                        _Logger.Log(TraceLevel.Error, "Adding Transaction Log Failed !");
                                    }
                                    _Logger.Log(TraceLevel.Error, "Account to wallet transfer failed (" + bank_response[1] + ")");
                                }
                            }
                            else
                            {
                                response.mmHeaderInfo.responseCode = "E11";
                                response.mmHeaderInfo.responseMessage = ErrorCodeHandler.Description("E11");
                                Utils._LastErrorCode[0] = "E05";
                                Utils._LastErrorCode[1] = "Account to wallet transfer failed (Bank Technical Error)";
                                _Logger.Log(TraceLevel.Error, "Account to wallet transfer failed (Bank Technical Error)");
                            }

                            _Logger.LogSqlDataReader(reader, TraceLevel.Info, ACCOUNT_TO_WALLET_TRANSFER_STORED_PROC);
                        }
                        else
                        {
                            response.mmHeaderInfo.responseCode = "E11";
                            response.mmHeaderInfo.responseMessage = ErrorCodeHandler.Description("E11");
                            Utils._LastErrorCode[0] = "E06";
                            Utils._LastErrorCode[1] = "Account to wallet transfer failed (Bank Technical Error)";
                            _Logger.Log(TraceLevel.Error, "Account to wallet transfer failed (Bank Technical Error)");
                        }

                        return response;
                    }
                    else
                    {
                        if (indirectSimMode)
                        {
                            response.mmHeaderInfo.responseCode = "E11";
                            response.mmHeaderInfo.responseMessage = ErrorCodeHandler.Description("E11");
                        }
                        else
                        {
                            response.mmHeaderInfo.responseCode = "E22";
                            response.mmHeaderInfo.responseMessage = (ora_b2w_fee < 0)
                                                                    ? ErrorCodeHandler.Description("E22")
                                                                    : "Transaction Violates Account's Maximum Balance";
                        }
                        Utils._LastErrorCode[0] = "E22";
                        Utils._LastErrorCode[1] = (ora_b2w_fee < 0) ? ErrorCodeHandler.Description("E22")
                                                                    : "Transaction Violates Account's Maximum Balance";
                        TransactionLog log = new TransactionLog
                        {
                            Id = Guid.NewGuid(),
                            Date = DateTime.Now,
                            AccountNo = entity.accountAlias,
                            AffiliateCode = entity.mmHeaderInfo.affiliateCode,
                            Amount = Convert.ToDecimal(entity.amount),
                            Charge = 0m,
                            OperatorCode = entity.mmHeaderInfo.operatorCode,
                            Complete = false,
                            ResponseCode = 22,
                            ResponseMessage = Utils._LastErrorCode[1],
                            TranType = 2,
                            RequestId = entity.mmHeaderInfo.requestId,
                            TranRef = response.CBAReferenceNo,
                            GSMOperationID = gsmOpID
                        };
                        USSDHelper ussdDb = new USSDHelper();
                        bool result = await ussdDb.AddTransactionLog(log);
                        if (!result)
                        {
                            ServiceLogger _Logger = (ServiceLogger)LogFactory.create(Logger.LoggingMode.SERVICE);
                            _Logger.Log(TraceLevel.Error, "Adding Transaction Log Failed !");
                        }
                        _Logger.Log(TraceLevel.Error, "Wallet to Account transfer failed (" + Utils._LastErrorCode[1] + ")");

                        return response;
                    }
                }

                return null;
            });
        }

        public Task<GetAccountBalanceResponse> GetAccountBalance(GetAccountBalance entity, string gsmOpID)
        {
            return Task.Run(async () =>
            {
                var response = new GetAccountBalanceResponse();
                response.accountAlias = entity.accountAlias;
                response.accountNo = entity.accountNo;
                if (entity.mmHeaderInfo != null)
                {
                    response.mmHeaderInfo = new HeaderResponse();
                    response.mmHeaderInfo.requestId = entity.mmHeaderInfo.requestId;
                    response.mmHeaderInfo.operatorCode = ConfigurationManager
                                                         .AppSettings.Get("operatorCode");
                    response.mmHeaderInfo.affiliateCode = ConfigurationManager
                                                          .AppSettings.Get("affiliate");
                    string accountNo = "";
                    if (entity.accountNo != null && entity.accountNo != "")
                        accountNo = entity.accountNo;
                    else if (entity.accountAlias != null && entity.accountAlias != "")
                    {
                        accountNo = entity.accountAlias.Replace(ConfigurationManager.AppSettings.Get("bic"), "");
                        accountNo = Utils.ReplaceFirst(accountNo, ConfigurationManager.AppSettings.Get("alias_first_digits"),
                                                                  ConfigurationManager.AppSettings.Get("account_first_digits"));
                    }
                    if (IsSimulationMode)
                        accountNo = SIMULATION_ACCOUNT_NO;

                    string desc = await new USSDHelper().GetPhoneNoByAlias(entity.accountAlias);
                    desc = desc + " Account Balance";
                    double ora_balance_fee = Fees.GetBalanceFee();
                    DbDataReader reader = await execStoredProcedure(GET_ACCOUNT_BALANCE_STORED_PROC,
                                                              new object[]
                                                              {
                                                              ACCOUNT_ID  , accountNo        ,
                                                              FEE         , ora_balance_fee  ,
                                                              TRANS_DESC  , desc             ,
                                                              MBANKING_ID , "Balance Enquiry", // Balance Enquiry
                                                              INQUIRY_TYPE, "1"
                                                              });
                    string[] bank_response = null;
                    if (reader != null)
                    {
                        bank_response = (string[])ReadSqlData(reader, "FieldValue", "Balance", "GETACCBAL");
                        if (bank_response != null && bank_response[0] != null)
                        {
                            if (bank_response[0].CompareTo(ErrorCodeHandler.BANK_SUCCESS_CODE) == 0
                                || bank_response[0] == "")
                            {
                                response.mmHeaderInfo.responseCode = "000";
                                response.mmHeaderInfo.responseMessage = ErrorCodeHandler.Description("000");
                                response.availableBalance = (bank_response[1] != null && bank_response[1] != "")
                                                            ? Double.Parse(bank_response[1], System.Globalization.NumberStyles.AllowDecimalPoint, System.Globalization.NumberFormatInfo.InvariantInfo)
                                                            : 0;
                                response.currentBalance = response.availableBalance;
                                TransactionLog log = new TransactionLog
                                {
                                    Id = Guid.NewGuid(),
                                    Date = DateTime.Now,
                                    AccountNo = entity.accountAlias,
                                    AffiliateCode = entity.mmHeaderInfo.affiliateCode,
                                    Amount = 0m,
                                    Charge = Convert.ToDecimal(ora_balance_fee),
                                    OperatorCode = entity.mmHeaderInfo.operatorCode,
                                    Complete = true,
                                    ResponseCode = 00,
                                    ResponseMessage = response.mmHeaderInfo.responseMessage,
                                    TranType = 0,
                                    RequestId = entity.mmHeaderInfo.requestId,
                                    GSMOperationID = gsmOpID
                                };
                                USSDHelper ussdDb = new USSDHelper();
                                bool result = await ussdDb.AddTransactionLog(log);
                                if (!result)
                                {
                                    ServiceLogger _Logger = (ServiceLogger)LogFactory.create(Logger.LoggingMode.SERVICE);
                                    _Logger.Log(TraceLevel.Error, "Adding Transaction Log Failed !");
                                }
                            }
                            else
                            {
                                if (bank_response[0].CompareTo(ErrorCodeHandler.INSUFFICIENT_BALANCE_CODE) == 0)
                                {
                                    response.mmHeaderInfo.responseCode = "E16";
                                    response.mmHeaderInfo.responseMessage = bank_response[1];
                                }
                                else
                                {
                                    HeaderResponse hResp = ErrorChecking(bank_response[0], "Get account balance failed (");
                                    response.mmHeaderInfo.responseCode = hResp.responseCode;
                                    response.mmHeaderInfo.responseMessage = hResp.responseMessage;
                                    _Logger.Log(TraceLevel.Error, "Get account balance failed (" + bank_response[0] + ")");
                                }
                                TransactionLog log = new TransactionLog
                                {
                                    Id = Guid.NewGuid(),
                                    Date = DateTime.Now,
                                    AccountNo = entity.accountAlias,
                                    AffiliateCode = entity.mmHeaderInfo.affiliateCode,
                                    Amount = 0m,
                                    Charge = 0m,
                                    OperatorCode = entity.mmHeaderInfo.operatorCode,
                                    Complete = false,
                                    ResponseCode = ((bank_response[0]
                                                     .CompareTo(ErrorCodeHandler.INSUFFICIENT_BALANCE_CODE) == 0)
                                                    ? 16 : 17),
                                    ResponseMessage = response.mmHeaderInfo.responseMessage,
                                    TranType = 0,
                                    RequestId = entity.mmHeaderInfo.requestId,
                                    GSMOperationID = gsmOpID
                                };
                                USSDHelper ussdDb = new USSDHelper();
                                bool result = await ussdDb.AddTransactionLog(log);
                                if (!result)
                                {
                                    ServiceLogger _Logger = (ServiceLogger)LogFactory.create(Logger.LoggingMode.SERVICE);
                                    _Logger.Log(TraceLevel.Error, "Adding Transaction Log Failed !");
                                }
                            }
                        }
                        else
                        {
                            response.mmHeaderInfo.responseCode = "E07";
                            response.mmHeaderInfo.responseMessage = "Get account balance failed (" + ErrorCodeHandler.Description("E07") + ")";
                            _Logger.Log(TraceLevel.Error, "Get account balance failed ");
                        }

                        _Logger.LogSqlDataReader(reader, TraceLevel.Info, GET_ACCOUNT_BALANCE_STORED_PROC);
                    }
                    else
                    {
                        response.mmHeaderInfo.responseCode = "E08";
                        response.mmHeaderInfo.responseMessage = "Get account balance failed (" + ErrorCodeHandler.Description("E08") + ")";
                        _Logger.Log(TraceLevel.Error, "Get account balance failed ");
                    }
                    response.ccy = ConfigurationManager.AppSettings.Get("currency");

                    return response;
                }

                return null;
            });
        }

        
        public Task<GetMiniStatementResponse> GetMiniStatement(GetMiniStatement entity, string gsmOpID)
        {
            return Task.Run(async () =>
            {
                var response = new GetMiniStatementResponse();
                if (entity.mmHeaderInfo != null)
                {
                    response.mmHeaderInfo = new HeaderResponse();
                    response.mmHeaderInfo.requestId = entity.mmHeaderInfo.requestId;
                    response.mmHeaderInfo.operatorCode = ConfigurationManager
                                                         .AppSettings.Get("operatorCode");
                    response.mmHeaderInfo.affiliateCode = ConfigurationManager
                                                          .AppSettings.Get("affiliate");

                    string accountNo = "";
                    if (entity.accountNo != null && entity.accountNo != "")
                        accountNo = entity.accountNo;
                    else if (entity.accountAlias != null && entity.accountAlias != "")
                    {
                        accountNo = entity.accountAlias.Replace(ConfigurationManager.AppSettings.Get("bic"), "");
                        accountNo = Utils.ReplaceFirst(accountNo, ConfigurationManager.AppSettings.Get("alias_first_digits"),
                                                                  ConfigurationManager.AppSettings.Get("account_first_digits"));
                    }
                    if (IsSimulationMode)
                        accountNo = SIMULATION_ACCOUNT_NO;

                    string desc = await new USSDHelper().GetPhoneNoByAlias(entity.accountAlias);
                    desc = desc + " Mini Statement";
                    double ora_statement_fee = Fees.GetStatementFee();
                    DbDataReader reader = await execStoredProcedure(GET_MINI_STATEMENT_STORED_PROC,
                                                              new object[]
                                                              {
                                                              ACCOUNT_ID  , accountNo       ,
                                                              FEE         , ora_statement_fee,
                                                              TRANS_DESC  , "Mobile Balance",
                                                              MBANKING_ID , "Balance Enquiry", // Balance Enquiry
                                                              INQUIRY_TYPE, "2"
                                                              });
                    object[] bank_response = null;
                    if (reader != null)
                    {
                        bank_response = (object[])ReadSqlData(reader, "FieldValue", "Balance", "GETMINI");
                        if (bank_response != null && bank_response[0] != null)
                        {
                            string return_code = (string)bank_response[0];
                            if (return_code.CompareTo(ErrorCodeHandler.BANK_SUCCESS_CODE) == 0
                                || return_code == "")
                            {
                                response.mmHeaderInfo.responseCode = "000";
                                response.mmHeaderInfo.responseMessage = ErrorCodeHandler.Description("000");
                                if (bank_response[1] != null)
                                    response.TransactionList = (List<TransactionDetail>)bank_response[1];

                                TransactionLog log = new TransactionLog
                                {
                                    Id = Guid.NewGuid(),
                                    Date = DateTime.Now,
                                    AccountNo = entity.accountAlias,
                                    AffiliateCode = entity.mmHeaderInfo.affiliateCode,
                                    Amount = 0m,
                                    Charge = Convert.ToDecimal(ora_statement_fee),
                                    OperatorCode = entity.mmHeaderInfo.operatorCode,
                                    Complete = true,
                                    ResponseCode = 00,
                                    ResponseMessage = response.mmHeaderInfo.responseMessage,
                                    TranType = 1,
                                    RequestId = entity.mmHeaderInfo.requestId,
                                    GSMOperationID = gsmOpID
                                };
                                USSDHelper ussdDb = new USSDHelper();
                                bool result = await ussdDb.AddTransactionLog(log);
                                if (!result)
                                {
                                    ServiceLogger _Logger = (ServiceLogger)LogFactory.create(Logger.LoggingMode.SERVICE);
                                    _Logger.Log(TraceLevel.Error, "Adding Transaction Log Failed !");
                                }
                            }
                            else
                            {
                                if (((string)bank_response[0]).CompareTo(ErrorCodeHandler.INSUFFICIENT_BALANCE_CODE) == 0)
                                {
                                    response.mmHeaderInfo.responseCode = "E16";
                                    response.mmHeaderInfo.responseMessage = (string) bank_response[1];
                                }
                                else
                                {
                                    HeaderResponse hResp = ErrorChecking(return_code, "Get mini statement failed (");
                                    response.mmHeaderInfo.responseCode = hResp.responseCode;
                                    response.mmHeaderInfo.responseMessage = hResp.responseMessage;
                                    _Logger.Log(TraceLevel.Error, "Get mini statement failed (" + return_code + ")");
                                }
                                TransactionLog log = new TransactionLog
                                {
                                    Id = Guid.NewGuid(),
                                    Date = DateTime.Now,
                                    AccountNo = entity.accountAlias,
                                    AffiliateCode = entity.mmHeaderInfo.affiliateCode,
                                    Amount = 0m,
                                    Charge = 0m,
                                    OperatorCode = entity.mmHeaderInfo.operatorCode,
                                    Complete = false,
                                    ResponseCode = ((((string)bank_response[0])
                                                     .CompareTo(ErrorCodeHandler.INSUFFICIENT_BALANCE_CODE) == 0)
                                                    ? 16 : 17),
                                    ResponseMessage = response.mmHeaderInfo.responseMessage,
                                    TranType = 1,
                                    RequestId = entity.mmHeaderInfo.requestId,
                                    GSMOperationID = gsmOpID
                                };
                                USSDHelper ussdDb = new USSDHelper();
                                bool result = await ussdDb.AddTransactionLog(log);
                                if (!result)
                                {
                                    ServiceLogger _Logger = (ServiceLogger)LogFactory.create(Logger.LoggingMode.SERVICE);
                                    _Logger.Log(TraceLevel.Error, "Adding Transaction Log Failed !");
                                }
                            }
                        }
                        else
                        {
                            response.mmHeaderInfo.responseCode = "E09";
                            response.mmHeaderInfo.responseMessage = "Get mini statement failed (" + ErrorCodeHandler.Description("E09") + ")";
                            _Logger.Log(TraceLevel.Error, "Get mini statement failed ");
                        }

                        _Logger.LogSqlDataReader(reader, TraceLevel.Info, GET_MINI_STATEMENT_STORED_PROC);
                    }
                    else
                    {
                        response.mmHeaderInfo.responseCode = "E10";
                        response.mmHeaderInfo.responseMessage = "Get mini statement failed (" + ErrorCodeHandler.Description("E10") + ")";
                        _Logger.Log(TraceLevel.Error, "Get mini statement failed ");
                    }

                    return response;
                }

                return null;
            });
        }

        public Task<TransferStatusInquiryResponse> TransferStatusInquiry(TransferStatusInquiry entity)
        {
            return Task.Run(async () =>
            {
                var response = new TransferStatusInquiryResponse();
                if (entity.mmHeaderInfo != null)
                {
                    response.mmHeaderInfo = new HeaderResponse();
                    response.mmHeaderInfo.requestId = entity.mmHeaderInfo.requestId;
                    response.mmHeaderInfo.operatorCode = ConfigurationManager.AppSettings.Get("operatorCode");
                    response.mmHeaderInfo.affiliateCode = ConfigurationManager.AppSettings.Get("affiliate");

                    TransactionLog log = await new USSDHelper().SearchTransactionLog(entity.externalRefNo);
                    if (log != null && log.TranRef != null)
                    {
                        if (Utils._LastErrorCode[1].CompareTo(ErrorCodeHandler.Description("000")) != 0)
                        {
                            if ((Utils._LastErrorCode[0] != "") && (Utils._LastErrorCode[1] != ""))
                            {
                                response.mmHeaderInfo.responseCode = Utils._LastErrorCode[0];
                                response.mmHeaderInfo.responseMessage = Utils._LastErrorCode[1];
                            }
                            else
                            {
                                response.mmHeaderInfo.responseCode = "E12";
                                response.mmHeaderInfo.responseMessage = "Transfer status inquiry failed (Bank Technical Error)";
                            }
                            response.CBAReferenceNo = "";
                            response.externalRefNo = entity.mmHeaderInfo.requestId;
                            _Logger.Log(TraceLevel.Error, "Transfer status inquiry failed (" + Utils._LastErrorCode[0] + ")");
                        }
                        else
                        {
                            response.mmHeaderInfo.responseCode = "000";
                            response.mmHeaderInfo.responseMessage = ErrorCodeHandler.Description("000");
                            response.CBAReferenceNo = log.TranRef;
                            response.externalRefNo = entity.mmHeaderInfo.requestId;
                            _Logger.Log(TraceLevel.Info, "Transfer status inquiry succeeded");
                        }
                    }
                    else
                    {
                        if ((Utils._LastErrorCode[0] != "") && (Utils._LastErrorCode[1] != ""))
                        {
                            response.mmHeaderInfo.responseCode = Utils._LastErrorCode[0];
                            response.mmHeaderInfo.responseMessage = Utils._LastErrorCode[1];
                        }
                        else
                        {
                            response.mmHeaderInfo.responseCode = "E12";
                            response.mmHeaderInfo.responseMessage = "Transfer status inquiry failed (Bank Technical Error)";
                        }
                        response.CBAReferenceNo = "";
                        response.externalRefNo = entity.mmHeaderInfo.requestId;
                        _Logger.Log(TraceLevel.Error, "Transfer status inquiry failed (" + Utils._LastErrorCode[0] + ")");
                    }

                    return response;
                }

                return null;
            });
        }

        private object ReadSqlData(DbDataReader reader, string field1,
                                   string field2      , string field3)
        {
            object value = null;
            while (reader.Read())
            {
                try
                {
                    if (field1 != null && field2 == null)
                    {
                        if (field3 == null)
                        {
                            if (reader[field1] != DBNull.Value)
                                value = reader[field1].ToString();
                            else
                                value = "";
                        }
                        else
                        {
                            if (field3.CompareTo("A2W") == 0)
                            {
                                string errorCode = reader[field1].ToString();
                                if(errorCode.CompareTo(ErrorCodeHandler.BANK_SUCCESS_CODE) == 0)
                                    value = new string[] { ErrorCodeHandler.BANK_SUCCESS_CODE, reader.GetString(2) };
                                else if(errorCode.CompareTo(ErrorCodeHandler.INSUFFICIENT_BALANCE_CODE) == 0)
                                    value = new string[] { ErrorCodeHandler.INSUFFICIENT_BALANCE_CODE, ErrorCodeHandler.INSUFFICIENT_BALANCE};
                                else
                                    value = new string[] { errorCode, null };
                            }
                            else if (field3.CompareTo("W2A") == 0)
                            {
                                string errorCode = reader[field1].ToString();
                                if (errorCode.CompareTo(ErrorCodeHandler.BANK_SUCCESS_CODE) == 0)
                                    value = new string[] { ErrorCodeHandler.BANK_SUCCESS_CODE, reader.GetString(3) };
                                else if (errorCode.CompareTo(ErrorCodeHandler.OVERLIMIT_AMOUNT_CODE) == 0)
                                    value = new string[] { ErrorCodeHandler.OVERLIMIT_AMOUNT_CODE, ErrorCodeHandler.OVERLIMIT_AMOUNT };
                                else
                                    value = new string[] { errorCode, null };
                            }
                        }
                    }
                    else if (field1 != null && field2 != null)
                    {
                        if (field3.CompareTo("GETMINI") == 0)
                        {
                            string fieldValue;
                            try
                            {
                                fieldValue = reader.GetString(2);
                            }
                            catch (Exception)
                            {
                                fieldValue = null;
                            }
                            if (fieldValue != null)
                            {
                                List<TransactionDetail> transList = new List<TransactionDetail>();
                                string[] transStr = fieldValue.Split(new char[] { '|' });
                                int idx = 0;
                                while (idx < transStr.Length)
                                {
                                    string str = transStr[idx];
                                    if (str != null && str.Length > 15)
                                    {
                                        string[] values = str.Split(new char[] { '@' });
                                        if (values != null)
                                        {
                                            TransactionDetail td = new TransactionDetail();
                                            if (values.Length > 1)
                                                td.tranRefNo = values[1];

                                            if (values.Length > 0)
                                                td.tranDate = (values[0] != null && values[0] != "")
                                                              ? DateTime.Parse(values[0])
                                                              : DateTime.Now;

                                            if (values.Length > 3)
                                                td.crDr = (values[3] != "TD") ? "C" : "D";

                                            td.ccy = ConfigurationManager.AppSettings.Get("currencyCode");

                                            if (values.Length > 2)
                                                td.amount = (values[2] != null && values[2] != "")
                                                            ? Double.Parse(values[2], System.Globalization.NumberStyles.AllowDecimalPoint, System.Globalization.NumberFormatInfo.InvariantInfo)
                                                            : 0.0;

                                            transList.Add(td);
                                        }
                                    }
                                    idx++;
                                }
                                if (reader[field1] != DBNull.Value)
                                    value = new object[] {reader[field1].ToString(),
                                                          transList};
                                else
                                    value = new object[] { "", transList };
                            }
                            else
                            {
                                string errorCode = reader[field1].ToString();
                                if (errorCode.CompareTo(ErrorCodeHandler.INSUFFICIENT_BALANCE_CODE) == 0)
                                    value = new string[] { ErrorCodeHandler.INSUFFICIENT_BALANCE_CODE, ErrorCodeHandler.INSUFFICIENT_BALANCE };
                                else
                                    value = new string[] { errorCode, "" };
                            }
                        }
                        else
                        {
                            string fieldValue;
                            try
                            {
                                fieldValue = reader.GetString(2);
                            }
                            catch(Exception)
                            {
                                fieldValue = null;
                            }
                            if (fieldValue != null)
                            {
                                if (reader[field1] != DBNull.Value)
                                    value = new string[] { reader[field1].ToString(), fieldValue };
                                else
                                    value = new string[] { "", fieldValue };
                            }
                            else
                            {
                                string errorCode = reader[field1].ToString();
                                if (errorCode.CompareTo(ErrorCodeHandler.INSUFFICIENT_BALANCE_CODE) == 0)
                                    value = new string[] { ErrorCodeHandler.INSUFFICIENT_BALANCE_CODE, ErrorCodeHandler.INSUFFICIENT_BALANCE };
                                else
                                    value = new string[] { errorCode, "" };
                            }
                        }
                    }
                }
                catch(Exception e)
                {
                    _Logger.Log(TraceLevel.Error, e.Message);
                }
            }

            return value;
        }

        private HeaderResponse ErrorChecking(string code, string message)
        {
            HeaderResponse response = new HeaderResponse();
            string returnCode = code;

            response.responseCode    = "E17";
            response.responseMessage = message + returnCode + ")";
            
            _Logger.Log(TraceLevel.Error, message + returnCode + ")");

            return response;
        }

    }
}