﻿using OMBanking.Web.B2W.Orange.Register;
using OMBanking.Web.Bank;
using OMBanking.Web.Log;
using OMBanking.Web.Security;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Threading.Tasks;

namespace OMBanking.Web.DB
{
    public class USSDHelper
    {
        private string _ConnectionString;
        private static SqlConnection _SqlConnection;
        private DBLogger _Logger;

        public USSDHelper()
        {
            _Logger = (DBLogger)LogFactory.create(LogBase.LoggingMode.DATABASE);
            ConnectionStringSettings config = ConfigurationManager.ConnectionStrings["BankUssdConnection"];
            _ConnectionString = config.ToString();
            _SqlConnection = new SqlConnection(_ConnectionString);
        }

        public Task<bool> AddLinkedAccount(LinkedAccount la)
        {
            return Task.Run(() =>
            {
                try
                {
                    string sqlQuery = "INSERT INTO dbo.LinkedAccount "
                                      + "(Id,FirstName,LastName,AccountNo,PhoneNo,DateLinked,Active,Alias,Role,"
                                      + "SupervisedBy,SupervisedOn,AuditedBy,AuditedOn,"
                                      + "BranchID,GSMOperationID)"
                                      + " VALUES (@Id,@FirstName,@LastName,@AccountNo,@PhoneNo,@DateLinked,"
                                      + "@Active,@Alias,@Role,@SupervisedBy,@SupervisedOn,"
                                      + "@AuditedBy,@AuditedOn,@BranchID,@GSMOperationID)";
                    _SqlConnection.Open();

                    SqlCommand command = new SqlCommand(sqlQuery, _SqlConnection);
                    command.Parameters.AddWithValue("@Id", la.Id);
                    command.Parameters.AddWithValue("@AccountNo", la.AccountNo);
		    command.Parameters.AddWithValue("@FirstName", la.FirstName);
                    command.Parameters.AddWithValue("@LastName", la.LastName);
                    command.Parameters.AddWithValue("@PhoneNo", la.PhoneNo);
                    command.Parameters.AddWithValue("@DateLinked", la.DateLinked);
                    command.Parameters.AddWithValue("@Active", la.Active);
                    command.Parameters.AddWithValue("@Alias", la.Alias);
                    command.Parameters.AddWithValue("@Role", ((la.Role != null) ? la.Role : "N/A"));
                    command.Parameters.AddWithValue("@SupervisedBy", ((la.SupervisedBy != null) ? la.SupervisedBy : "N/A"));
                    command.Parameters.AddWithValue("@SupervisedOn", DateTime.Now);
                    command.Parameters.AddWithValue("@AuditedBy", ((la.AuditedBy != null) ? la.AuditedBy : "N/A"));
                    command.Parameters.AddWithValue("@AuditedOn", DateTime.Now);
                    command.Parameters.AddWithValue("@BranchID", ((la.BranchID != null) ? la.BranchID : "00"));
                    command.Parameters.AddWithValue("@GSMOperationID", ((la.GSMOperationID != null) ? la.GSMOperationID : "N/A"));

                    command.ExecuteNonQuery();
                    _SqlConnection.Close();

                    return true;
                }
                catch (Exception e)
                {
                    _Logger.Log(TraceLevel.Error, e.Message);
                }

                return false;
            });
        }

        public Task<int> AddNewUserProfile(UserProfile user)
        {
            return Task.Run(() =>
            {
                try
                {
                    string sqlQuery = "INSERT INTO dbo.UserProfile (Id,Email,UserID,Password,Role,HasChanged,"
                                      + "BranchID,GSMOperationID,LogInDate,LogOutDate,SessionID,Active)"
                                      + " VALUES (@Id,@Email,@UserID,@Password,@Role,@HasChanged,@BranchID,"
                                      + "@GSMOperationID,@LogInDate,@LogOutDate,@SessionID,@Active)";
                    _SqlConnection.Open();

                    SqlCommand command = new SqlCommand(sqlQuery, _SqlConnection);
                    command.Parameters.AddWithValue("@Id", user.Id);
                    command.Parameters.AddWithValue("@Email", user.Email);
                    command.Parameters.AddWithValue("@UserID", user.UserID);
                    command.Parameters.AddWithValue("@Password", user.Password);
                    command.Parameters.AddWithValue("@Role", ((user.Role != null) ? user.Role : "N/A"));
                    command.Parameters.AddWithValue("@HasChanged", false);
                    command.Parameters.AddWithValue("@BranchID", ((user.BranchID != null) ? user.BranchID : "00"));
                    command.Parameters.AddWithValue("@GSMOperationID", ((user.GSMOperationID != null) ? user.GSMOperationID : "N/A"));
                    command.Parameters.AddWithValue("@LogInDate", DateTime.Now);
                    command.Parameters.AddWithValue("@LogOutDate", DateTime.Now);
                    command.Parameters.AddWithValue("@SessionID", user.SessionID);
                    command.Parameters.AddWithValue("@Active", true);

                    command.ExecuteNonQuery();
                    _SqlConnection.Close();

                    return 0;
                }
                catch (Exception e)
                {
                    _Logger.Log(TraceLevel.Error, e.Message);
                }

                return -1;
            });
        }

        public Task<bool> AddTransactionLog(TransactionLog tl)
        {
            return Task.Run(() =>
            {
                try
                {
                    string sqlQuery = "INSERT INTO dbo.TransactionLog (Id,Date,TranType,OperatorCode,"
                                      + "AffiliateCode,ExternalRefNo,RequestId,AccountNo,Amount,"
                                      + "Charge,ResponseCode,ResponseMessage,TranRef,Complete,Details,"
                                      + "SerialID,Role,ConsultedBy,ConsultedOn,AuditedBy,AuditedOn,BranchID,GSMOperationID)"
                                      + " VALUES (@Id,@Date,@TranType,@OperatorCode,@AffiliateCode,"
                                      + "@ExternalRefNo,@RequestId,@AccountNo,@Amount,@Charge,@ResponseCode,"
                                      + "@ResponseMessage,@TranRef,@Complete,@Details,@SerialID,@Role,@ConsultedBy,"
                                      + "@ConsultedOn,@AuditedBy,@AuditedOn,@BranchID,@GSMOperationID)";
                    _SqlConnection.Open();

                    SqlCommand command = new SqlCommand(sqlQuery, _SqlConnection);
                    command.CommandText = sqlQuery;
                    command.Parameters.AddWithValue("@Id", tl.Id);
                    command.Parameters.AddWithValue("@Date", tl.Date);
                    command.Parameters.AddWithValue("@TranType", tl.TranType);
                    command.Parameters.AddWithValue("@OperatorCode", ((tl.OperatorCode != null) ? tl.OperatorCode : ""));
                    command.Parameters.AddWithValue("@AffiliateCode", ((tl.AffiliateCode != null) ? tl.AffiliateCode : ""));
                    command.Parameters.AddWithValue("@ExternalRefNo", ((tl.ExternalRefNo != null) ? tl.ExternalRefNo : ""));
                    command.Parameters.AddWithValue("@RequestId", ((tl.RequestId != null) ? tl.RequestId : ""));
                    command.Parameters.AddWithValue("@AccountNo", ((tl.AccountNo != null) ? tl.AccountNo : ""));
                    command.Parameters.AddWithValue("@Amount", tl.Amount);
                    command.Parameters.AddWithValue("@Charge", tl.Charge);
                    command.Parameters.AddWithValue("@ResponseCode", tl.ResponseCode);
                    command.Parameters.AddWithValue("@ResponseMessage", ((tl.ResponseMessage != null) ? tl.ResponseMessage : ""));
                    command.Parameters.AddWithValue("@TranRef", ((tl.TranRef != null) ? tl.TranRef : ""));
                    command.Parameters.AddWithValue("@Complete", tl.Complete);
                    command.Parameters.AddWithValue("@Details", ((tl.Details != null) ? tl.Details : ""));
                    command.Parameters.AddWithValue("@SerialID", ((tl.SerialID != null) ? tl.SerialID : ""));
                    command.Parameters.AddWithValue("@Role", ((tl.Role != null) ? tl.Role : "N/A"));
                    command.Parameters.AddWithValue("@ConsultedBy", ((tl.ConsultedBy != null) ? tl.ConsultedBy : "N/A"));
                    command.Parameters.AddWithValue("@ConsultedOn", DateTime.Now);
                    command.Parameters.AddWithValue("@AuditedBy", ((tl.AuditedBy != null) ? tl.AuditedBy : "N/A"));
                    command.Parameters.AddWithValue("@AuditedOn", DateTime.Now);
                    command.Parameters.AddWithValue("@BranchID", ((tl.BranchID != null) ? tl.BranchID : "00"));
                    command.Parameters.AddWithValue("@GSMOperationID", ((tl.GSMOperationID != null) ? tl.GSMOperationID : "N/A"));

                    command.ExecuteNonQuery();
                    _SqlConnection.Close();

                    return true;
                }
                catch (Exception e)
                {
                    _Logger.Log(TraceLevel.Error, e.Message);
                }

                return false;
            });
        }

        public Task<bool> AddFootPrint(FootPrint ft)
        {
            return Task.Run(() =>
            {
                try
                {
                    string sqlQuery = "INSERT INTO dbo.FootPrint (Id,Role,UserAction,"
                                      + "AccessedBy,AccessedOn,BranchID,GSMOperationID)"
                                      + " VALUES (@Id,@Role,@UserAction,@AccessedBy,@AccessedOn,"
                                      + "@BranchID,@GSMOperationID)";
                    _SqlConnection.Open();

                    SqlCommand command = new SqlCommand(sqlQuery, _SqlConnection);
                    command.Parameters.AddWithValue("@Id", ft.Id);
                    command.Parameters.AddWithValue("@Role", ft.Role);
                    command.Parameters.AddWithValue("@UserAction", ft.UserAction);
                    command.Parameters.AddWithValue("@AccessedBy", ft.AccessedBy);
                    command.Parameters.AddWithValue("@AccessedOn", ((ft.AccessedOn != null) ? ft.AccessedOn : DateTime.Now));
                    command.Parameters.AddWithValue("@BranchID", ((ft.BranchID != null) ? ft.BranchID : "00"));
                    command.Parameters.AddWithValue("@GSMOperationID", ((ft.GSMOperationID != null) ? ft.GSMOperationID : "N/A"));

                    command.ExecuteNonQuery();
                    _SqlConnection.Close();

                    return true;
                }
                catch (Exception e)
                {
                    _Logger.Log(TraceLevel.Error, e.Message);
                }

                return false;
            });
        }

        public Task<bool> AddSupervisor(Supervisor sup)
        {
            return Task.Run(() =>
            {
                try
                {
                    string sqlQuery = "INSERT INTO dbo.Supervisor (Id,BranchID,Name)"
                                      + " VALUES (@Id,@BranchID,@Name)";
                    _SqlConnection.Open();

                    SqlCommand command = new SqlCommand(sqlQuery, _SqlConnection);
                    command.Parameters.AddWithValue("@Id", sup.Id);
                    command.Parameters.AddWithValue("@BranchID", ((sup.BranchID != null) ? sup.BranchID : "00"));
                    command.Parameters.AddWithValue("@Name", sup.Name);

                    command.ExecuteNonQuery();
                    _SqlConnection.Close();

                    return true;
                }
                catch (Exception e)
                {
                    _Logger.Log(TraceLevel.Error, e.Message);
                }

                return false;
            });
        }

        public Task<List<TransactionLog>> TransactionLogs(bool isAdmin, string userID)
        {
            return Task.Run(() =>
            {

                List<TransactionLog> tlList = new List<TransactionLog>();
                try
                {
                    _SqlConnection.Open();
                    SqlCommand command = new SqlCommand("SELECT t.Id,t.Date,l.FirstName,l.LastName,l.PhoneNo,t.TranType,t.OperatorCode,t.AffiliateCode,"
                                                        + "t.ExternalRefNo,t.RequestId,t.AccountNo,t.Amount,t.Charge,"
                                                        + "t.ResponseCode,t.ResponseMessage,t.TranRef,t.Complete,"
                                                        + "t.Details,t.SerialID,"
                                                        + ((isAdmin) ? "'" + userID + "' AS By1,CAST(CURRENT_TIMESTAMP as smalldatetime) AS On1,"
                                                            : "t.ConsultedBy,t.ConsultedOn,")
                                                        + ((!isAdmin) ? "'" + userID + "' AS By1,CAST(CURRENT_TIMESTAMP as smalldatetime) AS On1,"
                                                            : "t.AuditedBy,t.AuditedOn,")
                                                        + "l.Role,l.BranchID,l.GSMOperationID FROM dbo.TransactionLog AS t"
                                                        + " INNER JOIN dbo.LinkedAccount AS l"
                                                        + " ON (t.AccountNo=l.Alias)", _SqlConnection);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            TransactionLog tl = new TransactionLog();
                            tl.Id = Guid.Parse(reader["Id"].ToString());
                            tl.Date = DateTime.Parse(reader["Date"].ToString());
                            tl.FirstName = reader["FirstName"].ToString();
                            tl.LastName = reader["LastName"].ToString();
                            tl.PhoneNo = reader["PhoneNo"].ToString();
                            tl.TranType = int.Parse(reader["TranType"].ToString());
                            tl.OperatorCode = reader["OperatorCode"].ToString();
                            tl.AffiliateCode = reader["AffiliateCode"].ToString();
                            tl.ExternalRefNo = reader["ExternalRefNo"].ToString();
                            tl.RequestId = reader["RequestId"].ToString();
                            tl.AccountNo = reader["AccountNo"].ToString();
                            tl.Amount = Decimal.Parse(reader["Amount"].ToString());
                            tl.Charge = Decimal.Parse(reader["Charge"].ToString());
                            tl.ResponseCode = int.Parse(reader["ResponseCode"].ToString());
                            tl.ResponseMessage = reader["ResponseMessage"].ToString();
                            tl.TranRef = reader["TranRef"].ToString();
                            tl.Complete = Boolean.Parse(reader["Complete"].ToString());
                            tl.Details = reader["Details"].ToString();
                            tl.SerialID = reader["SerialID"].ToString();
                            tl.ConsultedBy = reader[(isAdmin) ? "By1" : "ConsultedBy"].ToString();
                            string ConsultedOn = reader[(isAdmin) ? "On1" : "ConsultedOn"].ToString();
                            tl.ConsultedOn = (ConsultedOn != null && ConsultedOn != "") ? DateTime.Parse(ConsultedOn) : tl.ConsultedOn;
                            tl.AuditedBy = reader[(!isAdmin) ? "By1" : "AuditedBy"].ToString();
                            string AuditedOn = reader[(!isAdmin) ? "On1" : "AuditedOn"].ToString();
                            tl.AuditedOn = (AuditedOn != null && AuditedOn != "") ? DateTime.Parse(AuditedOn) : tl.AuditedOn;
                            tl.Role = reader["Role"].ToString();
                            tl.BranchID = reader["BranchID"].ToString();
                            tl.GSMOperationID = reader["GSMOperationID"].ToString();

                            tlList.Add(tl);
                        }
                    }
                    _SqlConnection.Close();
                }
                catch (Exception e)
                {
                    _Logger.Log(TraceLevel.Error, e.Message);
                }

                return tlList;
            });
        }

        public Task<List<TransactionLog>> TransactionLogs(string userID, string role, string branchID, string gsmOpID)
        {
            return Task.Run(() =>
            {
                List<TransactionLog> tlList = new List<TransactionLog>();
                try
                {
                    _SqlConnection.Open();
                    string sqlQuery = "SELECT t.Id,t.Date,l.FirstName,l.LastName,l.PhoneNo,t.TranType,t.OperatorCode,t.AffiliateCode,"
                                        + "t.ExternalRefNo,t.RequestId,t.AccountNo,t.Amount,t.Charge,"
                                        + "t.ResponseCode,t.ResponseMessage,t.TranRef,t.Complete,"
                                        + "t.Details,t.SerialID,'" + userID + "' AS By1,"
                                        + "CAST(CURRENT_TIMESTAMP as smalldatetime) AS On1,t.AuditedBy,"
                                        + "t.AuditedOn,l.Role,l.BranchID,l.GSMOperationID FROM dbo.TransactionLog AS t"
                                        + " INNER JOIN dbo.LinkedAccount AS l"
                                        + " ON (t.AccountNo=l.Alias)"
                                        + ((role != null)
                                          ? ((branchID != null) ? " WHERE ((l.Role=@Role) AND (l.BranchID=@BranchID) AND (l.GSMOperationID=@GSMOperationID))"
                                                                : " WHERE ((l.Role=@Role) AND (l.GSMOperationID=@GSMOperationID))")
                                          : ((branchID != null) ? " WHERE ((l.BranchID=@BranchID) AND (l.GSMOperationID=@GSMOperationID))"
                                                                : " WHERE (l.GSMOperationID=@GSMOperationID)"));

                    SqlCommand command = new SqlCommand(sqlQuery, _SqlConnection);
                    if (role != null)
                        command.Parameters.AddWithValue("@Role", role);
                    if(branchID != null)
                        command.Parameters.AddWithValue("@BranchID", branchID);

                    command.Parameters.AddWithValue("@GSMOperationID", gsmOpID);

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            TransactionLog tl = new TransactionLog();
                            tl.Id = Guid.Parse(reader["Id"].ToString());
                            tl.Date = DateTime.Parse(reader["Date"].ToString());
                            tl.FirstName = reader["FirstName"].ToString();
                            tl.LastName = reader["LastName"].ToString();
                            tl.PhoneNo = reader["PhoneNo"].ToString();
                            tl.TranType = int.Parse(reader["TranType"].ToString());
                            tl.OperatorCode = reader["OperatorCode"].ToString();
                            tl.AffiliateCode = reader["AffiliateCode"].ToString();
                            tl.ExternalRefNo = reader["ExternalRefNo"].ToString();
                            tl.RequestId = reader["RequestId"].ToString();
                            tl.AccountNo = reader["AccountNo"].ToString();
                            tl.Amount = Decimal.Parse(reader["Amount"].ToString());
                            tl.Charge = Decimal.Parse(reader["Charge"].ToString());
                            tl.ResponseCode = int.Parse(reader["ResponseCode"].ToString());
                            tl.ResponseMessage = reader["ResponseMessage"].ToString();
                            tl.TranRef = reader["TranRef"].ToString();
                            tl.Complete = Boolean.Parse(reader["Complete"].ToString());
                            tl.Details = reader["Details"].ToString();
                            tl.SerialID = reader["SerialID"].ToString();
                            tl.ConsultedBy = reader["By1"].ToString();
                            string ConsultedOn = reader["On1"].ToString();
                            tl.ConsultedOn = (ConsultedOn != null && ConsultedOn != "") ? DateTime.Parse(ConsultedOn) : tl.ConsultedOn;
                            tl.AuditedBy = reader["AuditedBy"].ToString();
                            string AuditedOn = reader["AuditedOn"].ToString();
                            tl.AuditedOn = (AuditedOn != null && AuditedOn != "") ? DateTime.Parse(AuditedOn) : tl.AuditedOn;
                            tl.Role = reader["Role"].ToString();
                            tl.BranchID = reader["BranchID"].ToString();
                            tl.GSMOperationID = reader["GSMOperationID"].ToString();

                            tlList.Add(tl);
                        }
                    }
                    _SqlConnection.Close();
                }
                catch (Exception e)
                {
                    _Logger.Log(TraceLevel.Error, e.Message);
                }

                return tlList;
            });
        }

        public Task<bool> HasSubscribed(string phoneNo)
        {
            return Task.Run(() =>
            {
                try
                {
                    _SqlConnection.Open();
                    SqlCommand command = new SqlCommand("SELECT COUNT(*) AS UserExists"
                                                        + " FROM dbo.LinkedAccount"
                                                        + " WHERE (PhoneNo=@PhoneNo)", _SqlConnection);
                    command.Parameters.AddWithValue("@PhoneNo", phoneNo);
                    bool UserExists = false;
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            if (reader["UserExists"] != null
                                && reader["UserExists"].ToString() != "")
                            {
                                UserExists = int.Parse(reader["UserExists"].ToString()) > 0;
                            }
                        }
                    }
                    _SqlConnection.Close();

                    return UserExists;
                }
                catch (Exception e)
                {
                    _Logger.Log(TraceLevel.Error, e.Message);
                }

                return false;
            });
        }

        public Task<List<TransactionLog>> TransactionLogsByAccountID(string accountNo, string role, 
                                                                     string branchID, string gsmOpID)
        {
            return Task.Run(() =>
            {
                List<TransactionLog> tlList = new List<TransactionLog>();
                try
                {
                    _SqlConnection.Open();
                    string sqlQuery = "SELECT t.Id,t.Date,l.FirstName,l.LastName,l.PhoneNo,t.TranType,t.OperatorCode,t.AffiliateCode,"
                                        + "t.ExternalRefNo,t.RequestId,t.AccountNo,t.Amount,t.Charge,"
                                        + "t.ResponseCode,t.ResponseMessage,t.TranRef,t.Complete,"
                                        + "t.Details,t.SerialID,l.Role,l.BranchID,l.GSMOperationID"
                                        + " FROM dbo.TransactionLog AS t"
                                        + " INNER JOIN dbo.LinkedAccount AS l"
                                        + " ON (t.AccountNo=l.Alias)"
                                        + " WHERE ((l.Role=@Role) AND (l.BranchID=@BranchID) AND (l.GSMOperationID=@GSMOperationID)"
                                        + " AND (l.AccountNo=@AccountNo))";

                    SqlCommand command = new SqlCommand(sqlQuery, _SqlConnection);
                    command.Parameters.AddWithValue("@Role", role);
                    command.Parameters.AddWithValue("@BranchID", branchID);
                    command.Parameters.AddWithValue("@GSMOperationID", gsmOpID);
                    command.Parameters.AddWithValue("@AccountNo", accountNo);

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            TransactionLog tl = new TransactionLog();
                            tl.Id = Guid.Parse(reader["Id"].ToString());
                            tl.Date = DateTime.Parse(reader["Date"].ToString());
                            tl.FirstName = reader["FirstName"].ToString();
                            tl.LastName = reader["LastName"].ToString();
                            tl.PhoneNo = reader["PhoneNo"].ToString();
                            tl.TranType = int.Parse(reader["TranType"].ToString());
                            tl.OperatorCode = reader["OperatorCode"].ToString();
                            tl.AffiliateCode = reader["AffiliateCode"].ToString();
                            tl.ExternalRefNo = reader["ExternalRefNo"].ToString();
                            tl.RequestId = reader["RequestId"].ToString();
                            tl.AccountNo = reader["AccountNo"].ToString();
                            tl.Amount = Decimal.Parse(reader["Amount"].ToString());
                            tl.Charge = Decimal.Parse(reader["Charge"].ToString());
                            tl.ResponseCode = int.Parse(reader["ResponseCode"].ToString());
                            tl.ResponseMessage = reader["ResponseMessage"].ToString();
                            tl.TranRef = reader["TranRef"].ToString();
                            tl.Complete = Boolean.Parse(reader["Complete"].ToString());
                            tl.Details = reader["Details"].ToString();
                            tl.SerialID = reader["SerialID"].ToString();
                            tl.Role = reader["Role"].ToString();
                            tl.BranchID = reader["BranchID"].ToString();
                            tl.GSMOperationID = reader["GSMOperationID"].ToString();

                            tlList.Add(tl);
                        }
                    }
                    _SqlConnection.Close();
                }
                catch (Exception e)
                {
                    _Logger.Log(TraceLevel.Error, e.Message);
                }

                return tlList;
            });
        }

        public Task<DataTable> Supervisors()
        {
            return Task.Run(() =>
            {
                DataTable dt = new DataTable();
                try
                {
                    String sqlQuery = "SELECT Id,BranchID,Name FROM dbo.Supervisor";
                    _SqlConnection.Open();

                    using (SqlDataAdapter dap = new SqlDataAdapter(sqlQuery, _SqlConnection))
                    {
                        dap.Fill(dt);
                    }
                    _SqlConnection.Close();

                    return dt;
                }
                catch (Exception e)
                {
                    _Logger.Log(TraceLevel.Error, e.Message);
                }

                return dt;
            });
        }

        public Task<List<LinkedAccount>> LinkedAccounts()
        {
            return Task.Run(() =>
            {
                List<LinkedAccount> lkList = new List<LinkedAccount>();
                try
                {
                    _SqlConnection.Open();
                    SqlCommand command = new SqlCommand("SELECT Id,FirstName,LastName,AccountNo,PhoneNo,DateLinked,Active,Alias,"
                                                        + "SupervisedBy,SupervisedOn,AuditedBy,AuditedOn,Role,"
                                                        + "BranchID,GSMOperationID"
                                                        + " FROM dbo.LinkedAccount", _SqlConnection);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            LinkedAccount lk = new LinkedAccount();
                            lk.Id = Guid.Parse(reader["Id"].ToString());
                            lk.FirstName = reader["FirstName"].ToString();
                            lk.LastName = reader["LastName"].ToString();
                            lk.AccountNo = reader["AccountNo"].ToString();
                            lk.PhoneNo = reader["PhoneNo"].ToString();
                            lk.DateLinked = DateTime.Parse(reader["DateLinked"].ToString());
                            lk.Active = Boolean.Parse(reader["Active"].ToString());
                            lk.Alias = reader["Alias"].ToString();
                            lk.SupervisedBy = reader["SupervisedBy"].ToString();
                            string SupervisedOn = reader["SupervisedOn"].ToString();
                            lk.SupervisedOn = (SupervisedOn != null && SupervisedOn != "")
                                              ? DateTime.Parse(SupervisedOn)
                                              : lk.SupervisedOn;
                            lk.AuditedBy = reader["AuditedBy"].ToString();
                            string AuditedOn = reader["AuditedOn"].ToString();
                            lk.AuditedOn = (AuditedOn != null && AuditedOn != "")
                                           ? DateTime.Parse(AuditedOn)
                                           : lk.AuditedOn;
                            lk.Role = reader["Role"].ToString();
                            lk.BranchID = reader["BranchID"].ToString();
                            lk.GSMOperationID = reader["GSMOperationID"].ToString();

                            lkList.Add(lk);
                        }
                    }
                    _SqlConnection.Close();
                }
                catch (Exception e)
                {
                    _Logger.Log(TraceLevel.Error, e.Message);
                }

                return lkList;
            });
        }

        public Task<string> GetPhoneNoByAlias(string alias)
        {
            return Task.Run(() =>
            {
                try
                {
                    string phoneNo = "";
                    string sqlQuery = "SELECT PhoneNo"
                                      + " FROM dbo.LinkedAccount"
                                      + " WHERE (Alias=@Alias)";
                    _SqlConnection.Open();
                    SqlCommand command = new SqlCommand(sqlQuery, _SqlConnection);
                    command.Parameters.AddWithValue("@Alias", alias);

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            if (reader["PhoneNo"] != null)
                                phoneNo = reader["PhoneNo"].ToString();
                        }
                    }

                    return phoneNo;
                }
                catch (Exception e)
                {
                    _Logger.Log(TraceLevel.Error, e.Message);
                }
                return null;
            });
        }

        public Task<string> GetAccountIDByPhoneNo(string phoneNo)
        {
            return Task.Run(() =>
            {
                try
                {
                    string accountNo = "";
                    string sqlQuery = "SELECT AccountNo"
                                      + " FROM dbo.LinkedAccount"
                                      + " WHERE (PhoneNo=@PhoneNo)";
                    _SqlConnection.Open();
                    SqlCommand command = new SqlCommand(sqlQuery, _SqlConnection);
                    command.Parameters.AddWithValue("@PhoneNo", phoneNo);

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            if (reader["AccountNo"] != null)
                                accountNo = reader["AccountNo"].ToString();
                        }
                    }

                    return accountNo;
                }
                catch (Exception e)
                {
                    _Logger.Log(TraceLevel.Error, e.Message);
                }
                return null;
            });
        }

        public Task<List<String>> GetLinkedAccountInfoByAlias(string alias)
        {
            return Task.Run(() =>
            {
                try
                {
                    List<String> AccountInfo = new List<String>();
                    string sqlQuery = "SELECT Role,BranchID,GSMOperationID"
                                      + " FROM dbo.LinkedAccount"
                                      + " WHERE (Alias=@Alias)";
                    _SqlConnection.Open();
                    SqlCommand command = new SqlCommand(sqlQuery, _SqlConnection);
                    command.Parameters.AddWithValue("@Alias", alias);

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        string role = "", branchID = "", gsmOpID = "";
                        if (reader.Read())
                        {
                            if (reader["Role"] != null)
                                role = reader["Role"].ToString();
                            if (reader["BranchID"] != null)
                                branchID = reader["BranchID"].ToString();
                            if (reader["GSMOperationID"] != null)
                                gsmOpID = reader["GSMOperationID"].ToString();
                        }
                        AccountInfo.Add(role);
                        AccountInfo.Add(branchID);
                        AccountInfo.Add(gsmOpID);
                    }

                    return AccountInfo;
                }
                catch (Exception e)
                {
                    _Logger.Log(TraceLevel.Error, e.Message);
                }
                return null;
            });
        }

        public Task<List<LinkedAccount>> LinkedAccounts(string role, string branchID, string gsmOpID)
        {
            return Task.Run(() =>
            {
                List<LinkedAccount> lkList = new List<LinkedAccount>();
                try
                {
                    _SqlConnection.Open();
                    SqlCommand command = new SqlCommand("SELECT Id,FirstName,LastName,AccountNo,PhoneNo,DateLinked,Active,Alias,"
                                                       + "SupervisedBy,SupervisedOn,AuditedBy,AuditedOn,Role,"
                                                       + "BranchID,GSMOperationID"
                                                       + " FROM dbo.LinkedAccount"
                                                       + ((role != null)
                                                            ? ((branchID != null) ? " WHERE ((Role=@Role) AND (BranchID=@BranchID) AND (GSMOperationID=@GSMOperationID))"
                                                                                 : " WHERE ((Role=@Role) AND (GSMOperationID=@GSMOperationID))")
                                                            : ((branchID != null) ? " WHERE ((BranchID=@BranchID) AND (GSMOperationID=@GSMOperationID))"
                                                                                  : " WHERE (GSMOperationID=@GSMOperationID)")), _SqlConnection);
                    if (role != null)
                        command.Parameters.AddWithValue("@Role", role);
                    if(branchID != null)
                        command.Parameters.AddWithValue("@BranchID", branchID);

                    command.Parameters.AddWithValue("@GSMOperationID", gsmOpID);

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            LinkedAccount lk = new LinkedAccount();
                            lk.Id = Guid.Parse(reader["Id"].ToString());
                            lk.FirstName = reader["FirstName"].ToString();
                            lk.LastName = reader["LastName"].ToString();
                            lk.AccountNo = reader["AccountNo"].ToString();
                            lk.PhoneNo = reader["PhoneNo"].ToString();
                            lk.DateLinked = DateTime.Parse(reader["DateLinked"].ToString());
                            lk.Active = Boolean.Parse(reader["Active"].ToString());
                            lk.Alias = reader["Alias"].ToString();
                            lk.SupervisedBy = reader["SupervisedBy"].ToString();
                            string SupervisedOn = reader["SupervisedOn"].ToString();
                            lk.SupervisedOn = (SupervisedOn != null && SupervisedOn != "")
                                              ? DateTime.Parse(SupervisedOn)
                                              : lk.SupervisedOn;
                            lk.AuditedBy = reader["AuditedBy"].ToString();
                            string AuditedOn = reader["AuditedOn"].ToString();
                            lk.AuditedOn = (AuditedOn != null && AuditedOn != "")
                                           ? DateTime.Parse(AuditedOn)
                                           : lk.AuditedOn;
                            lk.Role = reader["Role"].ToString();
                            lk.BranchID = reader["BranchID"].ToString();
                            lk.GSMOperationID = reader["GSMOperationID"].ToString();

                            lkList.Add(lk);
                        }
                    }
                    _SqlConnection.Close();
                }
                catch (Exception e)
                {
                    _Logger.Log(TraceLevel.Error, e.Message);
                }

                return lkList;
            });
        }

        public Task<List<FootPrint>> FootPrints()
        {
            return Task.Run(() =>
            {
                List<FootPrint> fpList = new List<FootPrint>();
                try
                {
                    _SqlConnection.Open();
                    SqlCommand command = new SqlCommand("SELECT Id,Role,UserAction,AccessedBy,"
                                                        + "AccessedOn,BranchID,GSMOperationID"
                                                        + " FROM dbo.FootPrint", _SqlConnection);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            FootPrint fp = new FootPrint();
                            fp.Id = Guid.Parse(reader["Id"].ToString());
                            fp.Role = reader["Role"].ToString();
                            fp.UserAction = reader["UserAction"].ToString();
                            fp.AccessedBy = reader["AccessedBy"].ToString();
                            string AccessedOn = reader["AccessedOn"].ToString();
                            fp.AccessedOn = (AccessedOn != null && AccessedOn != "")
                                            ? DateTime.Parse(AccessedOn)
                                            : fp.AccessedOn;
                            fp.BranchID = reader["BranchID"].ToString();
                            fp.GSMOperationID = reader["GSMOperationID"].ToString();

                            fpList.Add(fp);
                        }
                    }
                    _SqlConnection.Close();
                }
                catch (Exception e)
                {
                    _Logger.Log(TraceLevel.Error, e.Message);
                }

                return fpList;
            });
        }

        public Task<List<FootPrint>> FootPrintById(string id, string role, string branchID, string gsmOpID)
        {
            return Task.Run(() =>
            {
                List<FootPrint> fpList = new List<FootPrint>();
                try
                {
                    _SqlConnection.Open();
                    SqlCommand command = new SqlCommand("SELECT Id,Role,UserAction,AccessedBy,"
                                                        + "AccessedOn,BranchID,GSMOperationID"
                                                        + " FROM dbo.FootPrint"
                                                        + ((role.CompareTo("Agent") == 0)
                                                          ? " WHERE ((BranchID=@BranchID) AND (GSMOperationID=@GSMOperationID))"
                                                          : "")
                                                        , _SqlConnection);
                    if (role.CompareTo("Agent") == 0)
                    {
                        command.Parameters.AddWithValue("@BranchID", branchID);
                        command.Parameters.AddWithValue("@GSMOperationID", gsmOpID);
                    }

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            string action = reader["UserAction"].ToString();
                            if (action.Contains(id))
                            {
                                FootPrint fp = new FootPrint();
                                fp.Id = Guid.Parse(reader["Id"].ToString());
                                fp.Role = reader["Role"].ToString();
                                fp.UserAction = action;
                                fp.AccessedBy = reader["AccessedBy"].ToString();
                                string AccessedOn = reader["AccessedOn"].ToString();
                                fp.AccessedOn = (AccessedOn != null && AccessedOn != "")
                                                ? DateTime.Parse(AccessedOn)
                                                : fp.AccessedOn;
                                fp.BranchID = reader["BranchID"].ToString();
                                fp.GSMOperationID = reader["GSMOperationID"].ToString();

                                fpList.Add(fp);
                            }
                        }
                    }
                    _SqlConnection.Close();
                }
                catch (Exception e)
                {
                    _Logger.Log(TraceLevel.Error, e.Message);
                }

                return fpList;
            });
        }

        public Task<DataTable> UserProfiles()
        {
            return Task.Run(() =>
            {
                DataTable dt = new DataTable();
                try
                {
                    _SqlConnection.Open();
                    string query = "SELECT Id,Email,UserID,Password,Role,HasChanged,"
                                    + "BranchID,GSMOperationID,LogInDate,"
                                    + "LogOutDate,SessionID,Active"
                                    + " FROM dbo.UserProfile";
                    using (SqlDataAdapter dap = new SqlDataAdapter(query, _SqlConnection))
                    {
                        dap.Fill(dt);
                    }
                    _SqlConnection.Close();

                    return dt;
                }
                catch (Exception e)
                {
                    _Logger.Log(TraceLevel.Error, e.Message);
                }

                return dt;
            });
        }

        public Task<TransactionLog> SearchTransactionLog(string requestId)
        {
            return Task.Run(() =>
            {
                try
                {
                    _SqlConnection.Open();
                    SqlCommand command = new SqlCommand("SELECT Id,Date,TranType,OperatorCode,AffiliateCode,"
                                                        + "ExternalRefNo,RequestId,AccountNo,Amount,Charge,"
                                                        + "ResponseCode,ResponseMessage,TranRef,Complete,Details,"
                                                        + "SerialID,ConsultedBy,ConsultedOn,AuditedBy,"
                                                        + "AuditedOn,Role,BranchID,GSMOperationID"
                                                        + " FROM dbo.TransactionLog WHERE RequestId=@RequestId", _SqlConnection);
                    command.Parameters.AddWithValue("@RequestId", requestId);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            TransactionLog tl = new TransactionLog();
                            tl.Id = Guid.Parse(reader["Id"].ToString());
                            tl.Date = DateTime.Parse(reader["Date"].ToString());
                            tl.TranType = int.Parse(reader["TranType"].ToString());
                            tl.OperatorCode = reader["OperatorCode"].ToString();
                            tl.AffiliateCode = reader["AffiliateCode"].ToString();
                            tl.ExternalRefNo = reader["ExternalRefNo"].ToString();
                            tl.RequestId = reader["RequestId"].ToString();
                            tl.AccountNo = reader["AccountNo"].ToString();
                            tl.Amount = Decimal.Parse(reader["Amount"].ToString());
                            tl.Charge = Decimal.Parse(reader["Charge"].ToString());
                            tl.ResponseCode = int.Parse(reader["ResponseCode"].ToString());
                            tl.ResponseMessage = reader["ResponseMessage"].ToString();
                            tl.TranRef = reader["TranRef"].ToString();
                            tl.Complete = Boolean.Parse(reader["Complete"].ToString());
                            tl.Details = reader["Details"].ToString();
                            tl.SerialID = reader["SerialID"].ToString();
                            tl.ConsultedBy = reader["ConsultedBy"].ToString();
                            string ConsultedOn = reader["ConsultedOn"].ToString();
                            tl.ConsultedOn = (ConsultedOn != null && ConsultedOn != "")
                                             ? DateTime.Parse(ConsultedOn)
                                             : tl.ConsultedOn;
                            tl.AuditedBy = reader["AuditedBy"].ToString();
                            string AuditedOn = reader["AuditedOn"].ToString();
                            tl.AuditedOn = (AuditedOn != null && AuditedOn != "")
                                           ? DateTime.Parse(AuditedOn)
                                           : tl.AuditedOn;
                            tl.Role = reader["Role"].ToString();
                            tl.BranchID = reader["BranchID"].ToString();
                            tl.GSMOperationID = reader["GSMOperationID"].ToString();

                            return tl;
                        }
                    }
                    _SqlConnection.Close();
                }
                catch (Exception e)
                {
                    _Logger.Log(TraceLevel.Error, e.Message);
                }

                return null;
            });
        }

        public Task<TransactionLog> SearchTransactionLogRef(string tranRef)
        {
            return Task.Run(() =>
            {
                try
                {
                    _SqlConnection.Open();
                    SqlCommand command = new SqlCommand("SELECT Id,Date,TranType,OperatorCode,AffiliateCode,"
                                                        + "ExternalRefNo,RequestId,AccountNo,Amount,Charge,"
                                                        + "ResponseCode,ResponseMessage,TranRef,Complete,"
                                                        + "Details,SerialID,ConsultedBy,ConsultedOn,"
                                                        + "AuditedBy,AuditedOn,Role,BranchID,GSMOperationID"
                                                        + " FROM dbo.TransactionLog WHERE TranRef=@TranRef", _SqlConnection);
                    command.Parameters.AddWithValue("@TranRef", tranRef);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            TransactionLog tl = new TransactionLog();
                            tl.Id = Guid.Parse(reader["Id"].ToString());
                            tl.Date = DateTime.Parse(reader["Date"].ToString());
                            tl.TranType = int.Parse(reader["TranType"].ToString());
                            tl.OperatorCode = reader["OperatorCode"].ToString();
                            tl.AffiliateCode = reader["AffiliateCode"].ToString();
                            tl.ExternalRefNo = reader["ExternalRefNo"].ToString();
                            tl.RequestId = reader["RequestId"].ToString();
                            tl.AccountNo = reader["AccountNo"].ToString();
                            tl.Amount = Decimal.Parse(reader["Amount"].ToString());
                            tl.Charge = Decimal.Parse(reader["Charge"].ToString());
                            tl.ResponseCode = int.Parse(reader["ResponseCode"].ToString());
                            tl.ResponseMessage = reader["ResponseMessage"].ToString();
                            tl.TranRef = reader["TranRef"].ToString();
                            tl.Complete = Boolean.Parse(reader["Complete"].ToString());
                            tl.Details = reader["Details"].ToString();
                            tl.SerialID = reader["SerialID"].ToString();
                            tl.ConsultedBy = reader["ConsultedBy"].ToString();
                            string ConsultedOn = reader["ConsultedOn"].ToString();
                            tl.ConsultedOn = (ConsultedOn != null && ConsultedOn != "")
                                             ? DateTime.Parse(ConsultedOn)
                                             : tl.ConsultedOn;
                            tl.AuditedBy = reader["AuditedBy"].ToString();
                            string AuditedOn = reader["AuditedOn"].ToString();
                            tl.AuditedOn = (AuditedOn != null && AuditedOn != "")
                                           ? DateTime.Parse(AuditedOn)
                                           : tl.AuditedOn;
                            tl.Role = reader["Role"].ToString();
                            tl.BranchID = reader["BranchID"].ToString();
                            tl.GSMOperationID = reader["GSMOperationID"].ToString();

                            return tl;
                        }
                    }
                    _SqlConnection.Close();
                }
                catch (Exception e)
                {
                    _Logger.Log(TraceLevel.Error, e.Message);
                }

                return new TransactionLog();
            });
        }

        public Task<List<LinkedAccount>> SearchLinkedAccounts(string from, string to)
        {
            return Task.Run(() =>
            {
                List<LinkedAccount> lkList = new List<LinkedAccount>();
                try
                {
                    _SqlConnection.Open();
                    String query = "SELECT Id,FirstName,LastName,AccountNo,PhoneNo,DateLinked,Active,Alias,"
                                + "SupervisedBy,SupervisedOn,AuditedBy,AuditedOn,Role,"
                                + "BranchID,GSMOperationID"
                                + " FROM dbo.LinkedAccount "
                                + "WHERE (DateLinked BETWEEN '"
                                + from + "' AND '" + to + "')";

                    SqlCommand command = new SqlCommand(query, _SqlConnection);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            LinkedAccount lk = new LinkedAccount();
                            lk.Id = Guid.Parse(reader["Id"].ToString());
                            lk.FirstName = reader["FirstName"].ToString();
                            lk.LastName = reader["LastName"].ToString();
                            lk.AccountNo = reader["AccountNo"].ToString();
                            lk.PhoneNo = reader["PhoneNo"].ToString();
                            lk.DateLinked = DateTime.Parse(reader["DateLinked"].ToString());
                            lk.Active = Boolean.Parse(reader["Active"].ToString());
                            lk.Alias = reader["Alias"].ToString();
                            lk.SupervisedBy = reader["SupervisedBy"].ToString();
                            string SupervisedOn = reader["SupervisedOn"].ToString();
                            lk.SupervisedOn = (SupervisedOn != null && SupervisedOn != "")
                                              ? DateTime.Parse(SupervisedOn)
                                              : lk.SupervisedOn;
                            lk.AuditedBy = reader["AuditedBy"].ToString();
                            string AuditedOn = reader["AuditedOn"].ToString();
                            lk.AuditedOn = (AuditedOn != null && AuditedOn != "")
                                           ? DateTime.Parse(AuditedOn)
                                           : lk.AuditedOn;
                            lk.Role = reader["Role"].ToString();
                            lk.BranchID = reader["BranchID"].ToString();
                            lk.GSMOperationID = reader["GSMOperationID"].ToString();

                            lkList.Add(lk);
                        }
                    }
                    _SqlConnection.Close();
                }
                catch (Exception e)
                {
                    _Logger.Log(TraceLevel.Error, e.Message);
                }

                return lkList;
            });
        }

        public Task<List<TransactionLog>> SearchTransactionLogs(bool isAdmin, string from, string to, string userID)
        {
            return Task.Run(() =>
            {
                List<TransactionLog> tlList = new List<TransactionLog>();
                try
                {
                    _SqlConnection.Open();
                    String query = "SELECT t.Id,t.Date,l.FirstName,l.LastName,l.PhoneNo,t.TranType,t.OperatorCode,t.AffiliateCode,"
                                    + "t.ExternalRefNo,t.RequestId,t.AccountNo,t.Amount,t.Charge,"
                                    + "t.ResponseCode,t.ResponseMessage,t.TranRef,t.Complete,"
                                    + "t.Details,t.SerialID,"
                                    + ((isAdmin) ? "'" + userID + "' AS By1,CAST(CURRENT_TIMESTAMP as smalldatetime) AS On1,"
                                        : "t.ConsultedBy,t.ConsultedOn,")
                                    + ((!isAdmin) ? "'" + userID + "' AS By1,CAST(CURRENT_TIMESTAMP as smalldatetime) AS On1,"
                                        : "t.AuditedBy,t.AuditedOn,")
                                    + "l.Role,l.BranchID,l.GSMOperationID FROM dbo.TransactionLog AS t"
                                    + " INNER JOIN dbo.LinkedAccount AS l"
                                    + " ON (t.AccountNo=l.Alias)"
                                    + " WHERE (t.Date BETWEEN '" + from + "' AND '" + to + "')";

                    SqlCommand command = new SqlCommand(query, _SqlConnection);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            TransactionLog tl = new TransactionLog();
                            tl.Id = Guid.Parse(reader["Id"].ToString());
                            tl.Date = DateTime.Parse(reader["Date"].ToString());
                            tl.FirstName = reader["FirstName"].ToString();
                            tl.LastName = reader["LastName"].ToString();
                            tl.PhoneNo = reader["PhoneNo"].ToString();
                            tl.TranType = int.Parse(reader["TranType"].ToString());
                            tl.OperatorCode = reader["OperatorCode"].ToString();
                            tl.AffiliateCode = reader["AffiliateCode"].ToString();
                            tl.ExternalRefNo = reader["ExternalRefNo"].ToString();
                            tl.RequestId = reader["RequestId"].ToString();
                            tl.AccountNo = reader["AccountNo"].ToString();
                            tl.Amount = Decimal.Parse(reader["Amount"].ToString());
                            tl.Charge = Decimal.Parse(reader["Charge"].ToString());
                            tl.ResponseCode = int.Parse(reader["ResponseCode"].ToString());
                            tl.ResponseMessage = reader["ResponseMessage"].ToString();
                            tl.TranRef = reader["TranRef"].ToString();
                            tl.Complete = Boolean.Parse(reader["Complete"].ToString());
                            tl.Details = reader["Details"].ToString();
                            tl.SerialID = reader["SerialID"].ToString();
                            tl.ConsultedBy = reader[(isAdmin) ? "By1" : "ConsultedBy"].ToString();
                            string ConsultedOn = reader[(isAdmin) ? "On1" : "ConsultedOn"].ToString();
                            tl.ConsultedOn = (ConsultedOn != null && ConsultedOn != "")
                                             ? DateTime.Parse(ConsultedOn)
                                             : tl.ConsultedOn;
                            tl.AuditedBy = reader[(!isAdmin) ? "By1" : "AuditedBy"].ToString();
                            string AuditedOn = reader[(!isAdmin) ? "On1" : "AuditedOn"].ToString();
                            tl.AuditedOn = (AuditedOn != null && AuditedOn != "")
                                           ? DateTime.Parse(AuditedOn)
                                           : tl.AuditedOn;
                            tl.Role = reader["Role"].ToString();
                            tl.BranchID = reader["BranchID"].ToString();
                            tl.GSMOperationID = reader["GSMOperationID"].ToString();

                            tlList.Add(tl);
                        }
                    }
                    _SqlConnection.Close();
                }
                catch (Exception e)
                {
                    _Logger.Log(TraceLevel.Error, e.Message);
                }

                return tlList;
            });
        }

        public Task<List<TransactionLog>> SearchTransactionLogs(string from, string to, string userID, string role, string branchID, string gsmOpID)
        {
            return Task.Run(() =>
            {
                List<TransactionLog> tlList = new List<TransactionLog>();
                try
                {
                    _SqlConnection.Open();
                    String query = "SELECT t.Id,t.Date,l.FirstName,l.LastName,l.PhoneNo,t.TranType,t.OperatorCode,t.AffiliateCode,"
                                    + "t.ExternalRefNo,t.RequestId,t.AccountNo,t.Amount,t.Charge,"
                                    + "t.ResponseCode,t.ResponseMessage,t.TranRef,t.Complete,"
                                    + "t.Details,t.SerialID,'" + userID + "' AS By1,"
                                    + "CAST(CURRENT_TIMESTAMP as smalldatetime) AS On1,t.AuditedBy,"
                                    + "t.AuditedOn,l.Role,l.BranchID,l.GSMOperationID FROM dbo.TransactionLog AS t"
                                    + " INNER JOIN dbo.LinkedAccount AS l"
                                    + " ON (t.AccountNo=l.Alias)"
                                    + " WHERE ((t.Date BETWEEN '" + from + "' AND '" + to + "')"
                                    + ((role != null)
                                       ? ((branchID != null) ? " AND (l.Role=@Role) AND (l.BranchID=@BranchID) AND (l.GSMOperationID=@GSMOperationID))"
                                                             : " AND (l.Role=@Role) AND (l.GSMOperationID=@GSMOperationID))")
                                       : ((branchID != null) ? " AND (l.BranchID=@BranchID) AND (l.GSMOperationID=@GSMOperationID))"
                                                             : " AND (l.GSMOperationID=@GSMOperationID))"));

                    SqlCommand command = new SqlCommand(query, _SqlConnection);
                    if (role != null)
                        command.Parameters.AddWithValue("@Role", role);
                    if(branchID != null)
                        command.Parameters.AddWithValue("@BranchID", branchID);
                        
                    command.Parameters.AddWithValue("@GSMOperationID", gsmOpID);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            TransactionLog tl = new TransactionLog();
                            tl.Id = Guid.Parse(reader["Id"].ToString());
                            tl.Date = DateTime.Parse(reader["Date"].ToString());
                            tl.FirstName = reader["FirstName"].ToString();
                            tl.LastName = reader["LastName"].ToString();
                            tl.PhoneNo = reader["PhoneNo"].ToString();
                            tl.TranType = int.Parse(reader["TranType"].ToString());
                            tl.OperatorCode = reader["OperatorCode"].ToString();
                            tl.AffiliateCode = reader["AffiliateCode"].ToString();
                            tl.ExternalRefNo = reader["ExternalRefNo"].ToString();
                            tl.RequestId = reader["RequestId"].ToString();
                            tl.AccountNo = reader["AccountNo"].ToString();
                            tl.Amount = Decimal.Parse(reader["Amount"].ToString());
                            tl.Charge = Decimal.Parse(reader["Charge"].ToString());
                            tl.ResponseCode = int.Parse(reader["ResponseCode"].ToString());
                            tl.ResponseMessage = reader["ResponseMessage"].ToString();
                            tl.TranRef = reader["TranRef"].ToString();
                            tl.Complete = Boolean.Parse(reader["Complete"].ToString());
                            tl.Details = reader["Details"].ToString();
                            tl.SerialID = reader["SerialID"].ToString();
                            tl.ConsultedBy = reader["By1"].ToString();
                            string ConsultedOn = reader["On1"].ToString();
                            tl.ConsultedOn = (ConsultedOn != null && ConsultedOn != "")
                                             ? DateTime.Parse(ConsultedOn)
                                             : tl.ConsultedOn;
                            tl.AuditedBy = reader["AuditedBy"].ToString();
                            string AuditedOn = reader["AuditedOn"].ToString();
                            tl.AuditedOn = (AuditedOn != null && AuditedOn != "")
                                           ? DateTime.Parse(AuditedOn)
                                           : tl.AuditedOn;
                            tl.Role = reader["Role"].ToString();
                            tl.BranchID = reader["BranchID"].ToString();
                            tl.GSMOperationID = reader["GSMOperationID"].ToString();

                            tlList.Add(tl);
                        }
                    }
                    _SqlConnection.Close();
                }
                catch (Exception e)
                {
                    _Logger.Log(TraceLevel.Error, e.Message);
                }

                return tlList;
            });
        }

        public Task<List<TransactionLog>> SearchTransactionLogsByAccountID(string from, string to, string accountNo, string role, string branchID, string gsmOpID)
        {
            return Task.Run(() =>
            {
                List<TransactionLog> tlList = new List<TransactionLog>();
                try
                {
                    _SqlConnection.Open();
                    String query = "SELECT t.Id,t.Date,l.FirstName,l.LastName,l.PhoneNo,t.TranType,t.OperatorCode,t.AffiliateCode,"
                                    + "t.ExternalRefNo,t.RequestId,t.AccountNo,t.Amount,t.Charge,"
                                    + "t.ResponseCode,t.ResponseMessage,t.TranRef,t.Complete,"
                                    + "t.Details,t.SerialID,l.Role,l.BranchID,l.GSMOperationID"
                                    + " FROM dbo.TransactionLog AS t"
                                    + " INNER JOIN dbo.LinkedAccount AS l"
                                    + " ON (t.AccountNo=l.Alias)"
                                    + " WHERE ((t.Date BETWEEN '" + from + "' AND '" + to + "')"
                                    + " AND (l.Role=@Role) AND (l.BranchID=@BranchID) AND (l.GSMOperationID=@GSMOperationID)"
                                    + " AND (l.AccountNo=@AccountNo))";

                    SqlCommand command = new SqlCommand(query, _SqlConnection);
                    command.Parameters.AddWithValue("@Role", role);
                    command.Parameters.AddWithValue("@BranchID", branchID);
                    command.Parameters.AddWithValue("@GSMOperationID", gsmOpID);
                    command.Parameters.AddWithValue("@AccountNo", accountNo);

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            TransactionLog tl = new TransactionLog();
                            tl.Id = Guid.Parse(reader["Id"].ToString());
                            tl.Date = DateTime.Parse(reader["Date"].ToString());
                            tl.FirstName = reader["FirstName"].ToString();
                            tl.LastName = reader["LastName"].ToString();
                            tl.PhoneNo = reader["PhoneNo"].ToString();
                            tl.TranType = int.Parse(reader["TranType"].ToString());
                            tl.OperatorCode = reader["OperatorCode"].ToString();
                            tl.AffiliateCode = reader["AffiliateCode"].ToString();
                            tl.ExternalRefNo = reader["ExternalRefNo"].ToString();
                            tl.RequestId = reader["RequestId"].ToString();
                            tl.AccountNo = reader["AccountNo"].ToString();
                            tl.Amount = Decimal.Parse(reader["Amount"].ToString());
                            tl.Charge = Decimal.Parse(reader["Charge"].ToString());
                            tl.ResponseCode = int.Parse(reader["ResponseCode"].ToString());
                            tl.ResponseMessage = reader["ResponseMessage"].ToString();
                            tl.TranRef = reader["TranRef"].ToString();
                            tl.Complete = Boolean.Parse(reader["Complete"].ToString());
                            tl.Details = reader["Details"].ToString();
                            tl.SerialID = reader["SerialID"].ToString();
                            tl.Role = reader["Role"].ToString();
                            tl.BranchID = reader["BranchID"].ToString();
                            tl.GSMOperationID = reader["GSMOperationID"].ToString();

                            tlList.Add(tl);
                        }
                    }
                    _SqlConnection.Close();
                }
                catch (Exception e)
                {
                    _Logger.Log(TraceLevel.Error, e.Message);
                }

                return tlList;
            });
        }

        public Task<List<LinkedAccount>> SearchLinkedAccounts(string from, string to, string role, string branchID, string gsmOpID)
        {
            return Task.Run(() =>
            {
                List<LinkedAccount> lkList = new List<LinkedAccount>();
                try
                {
                    _SqlConnection.Open();
                    String query = "SELECT Id,FirstName,LastName,AccountNo,PhoneNo,DateLinked,Active,Alias,"
                                + "SupervisedBy,SupervisedOn,AuditedBy,AuditedOn,Role,"
                                + "BranchID,GSMOperationID"
                                + " FROM dbo.LinkedAccount "
                                + "WHERE ((DateLinked BETWEEN '"
                                + from + "' AND '" + to + "')"
                                + ((role != null)
                                   ? ((branchID != null) ? " AND (Role=@Role) AND (BranchID=@BranchID) AND (GSMOperationID=@GSMOperationID))"
                                                         : " AND (Role=@Role) AND (GSMOperationID=@GSMOperationID))")
                                   : ((branchID != null) ? " AND (BranchID=@BranchID) AND (GSMOperationID=@GSMOperationID))"
                                                         : " AND (GSMOperationID=@GSMOperationID))"));

                    SqlCommand command = new SqlCommand(query, _SqlConnection);
                    if (role != null)
                        command.Parameters.AddWithValue("@Role", role);
                    if(branchID != null)
                        command.Parameters.AddWithValue("@BranchID", branchID);

                    command.Parameters.AddWithValue("@GSMOperationID", gsmOpID);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            LinkedAccount lk = new LinkedAccount();
                            lk.Id = Guid.Parse(reader["Id"].ToString());
                            lk.FirstName = reader["FirstName"].ToString();
                            lk.LastName = reader["LastName"].ToString();
                            lk.AccountNo = reader["AccountNo"].ToString();
                            lk.PhoneNo = reader["PhoneNo"].ToString();
                            lk.DateLinked = DateTime.Parse(reader["DateLinked"].ToString());
                            lk.Active = Boolean.Parse(reader["Active"].ToString());
                            lk.Alias = reader["Alias"].ToString();
                            lk.SupervisedBy = reader["SupervisedBy"].ToString();
                            string SupervisedOn = reader["SupervisedOn"].ToString();
                            lk.SupervisedOn = (SupervisedOn != null && SupervisedOn != "")
                                              ? DateTime.Parse(SupervisedOn)
                                              : lk.SupervisedOn;
                            lk.AuditedBy = reader["AuditedBy"].ToString();
                            string AuditedOn = reader["AuditedOn"].ToString();
                            lk.AuditedOn = (AuditedOn != null && AuditedOn != "")
                                           ? DateTime.Parse(AuditedOn)
                                           : lk.AuditedOn;
                            lk.Role = reader["Role"].ToString();
                            lk.BranchID = reader["BranchID"].ToString();
                            lk.GSMOperationID = reader["GSMOperationID"].ToString();

                            lkList.Add(lk);
                        }
                    }
                    _SqlConnection.Close();
                }
                catch (Exception e)
                {
                    _Logger.Log(TraceLevel.Error, e.Message);
                }

                return lkList;
            });
        }

        public Task<DataTable> SearchUserProfiles(string from, string to)
        {
            return Task.Run(() =>
            {
                DataTable dt = new DataTable();
                try
                {
                    _SqlConnection.Open();
                    String query = "SELECT Id,Email,UserID,Password,Role,HasChanged,"
                                + "BranchID,GSMOperationID,LogInDate,"
                                + "LogOutDate,SessionID,Active"
                                + " FROM dbo.UserProfile"
                                + " WHERE (LogInDate BETWEEN '" + from + "' AND '" + to + "')";

                    using (SqlDataAdapter dap = new SqlDataAdapter(query, _SqlConnection))
                    {
                        dap.Fill(dt);
                    }
                    _SqlConnection.Close();

                    return dt;
                }
                catch (Exception e)
                {
                    _Logger.Log(TraceLevel.Error, e.Message);
                }

                return dt;
            });
        }

        public Task<List<FootPrint>> SearchFootPrints(string from, string to)
        {
            return Task.Run(() =>
            {
                List<FootPrint> fpList = new List<FootPrint>();
                try
                {
                    _SqlConnection.Open();
                    String query = "SELECT Id,Role,UserAction,AccessedBy,"
                                + "AccessedOn,BranchID,GSMOperationID"
                                + " FROM dbo.FootPrint"
                                + " WHERE (AccessedOn BETWEEN '" + from + "' AND '" + to + "')";

                    SqlCommand command = new SqlCommand(query, _SqlConnection);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            FootPrint fp = new FootPrint();
                            fp.Id = Guid.Parse(reader["Id"].ToString());
                            fp.Role = reader["Role"].ToString();
                            fp.UserAction = reader["UserAction"].ToString();
                            fp.AccessedBy = reader["AccessedBy"].ToString();
                            string AccessedOn = reader["AccessedOn"].ToString();
                            fp.AccessedOn = (AccessedOn != null && AccessedOn != "")
                                            ? DateTime.Parse(AccessedOn)
                                            : fp.AccessedOn;
                            fp.BranchID = reader["BranchID"].ToString();
                            fp.GSMOperationID = reader["GSMOperationID"].ToString();

                            fpList.Add(fp);
                        }
                    }
                    _SqlConnection.Close();
                }
                catch (Exception e)
                {
                    _Logger.Log(TraceLevel.Error, e.Message);
                }

                return fpList;
            });
        }

        public Task<bool> UserExists(UserProfile user)
        {
            return Task.Run(() =>
            {
                try
                {
                    _SqlConnection.Open();
                    SqlCommand command = new SqlCommand("SELECT COUNT(*) AS UserCount"
                                                        + " FROM dbo.UserProfile"
                                                        + " WHERE ((UserID=@UserID)"
                                                        + " AND (Password=@Password)"
                                                        + " AND (Role=@Role)"
                                                        + " AND (BranchID=@BranchID)"
                                                        + " AND (Active=1))", _SqlConnection);
                    command.Parameters.AddWithValue("@UserID", user.UserID);
                    command.Parameters.AddWithValue("@Password", user.Password);
                    command.Parameters.AddWithValue("@Role", ((user.Role != null) ? user.Role : "N/A"));
                    command.Parameters.AddWithValue("@BranchID", ((user.BranchID != null) ? user.BranchID : "00"));
                    bool UserExists = false;
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            if (reader["UserCount"] != null
                                && reader["UserCount"].ToString() != "")
                            {
                                UserExists = int.Parse(reader["UserCount"].ToString()) > 0;
                            }
                        }
                    }
                    _SqlConnection.Close();

                    return UserExists;
                }
                catch (Exception e)
                {
                    _Logger.Log(TraceLevel.Error, e.Message);
                }

                return false;
            });
        }

        public Task<bool> IsUserLogIn(UserProfile user)
        {
            return Task.Run(() =>
            {
                try
                {
                    _SqlConnection.Open();
                    SqlCommand command = new SqlCommand("SELECT COUNT(*) AS Yes"
                                                        + " FROM dbo.UserProfile"
                                                        + " WHERE ((UserID=@UserID)"
                                                        + " AND (Role=@Role)"
                                                        + " AND (BranchID=@BranchID)"
                                                        + " AND (Active=1))", _SqlConnection);
                    command.Parameters.AddWithValue("@UserID", user.UserID);
                    command.Parameters.AddWithValue("@Role", ((user.Role != null) ? user.Role : "N/A"));
                    command.Parameters.AddWithValue("@BranchID", ((user.BranchID != null) ? user.BranchID : "00"));

                    bool _Yes = false;
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            if (reader["Yes"] != null
                                && reader["Yes"].ToString() != "")
                            {
                                _Yes = int.Parse(reader["Yes"].ToString()) > 0;
                            }
                        }
                    }
                    _SqlConnection.Close();

                    return _Yes;
                }
                catch (Exception e)
                {
                    _Logger.Log(TraceLevel.Error, e.Message);
                }

                return false;
            });
        }

        public Task<bool> ForgotPassword(string Email)
        {
            return Task.Run(() =>
            {
                try
                {
                    string sqlQuery = "UPDATE dbo.UserProfile SET HasChanged=1"
                                      + " WHERE ((Email=@Email) AND (Active=1))";
                    _SqlConnection.Open();

                    SqlCommand command = new SqlCommand(sqlQuery, _SqlConnection);
                    command.Parameters.AddWithValue("@Email", Email);

                    command.ExecuteNonQuery();
                    _SqlConnection.Close();

                    return true;
                }
                catch (Exception e)
                {
                    _Logger.Log(TraceLevel.Error, e.Message);
                }

                return false;
            });
        }

        public Task<bool> UpdatePassword(string Email, string NewPassword)
        {
            return Task.Run(() =>
            {
                try
                {
                    string sqlQuery = "UPDATE dbo.UserProfile SET Password=@NewPassword"
                                      + " WHERE ((Email=@Email) AND (Active=1))";
                    _SqlConnection.Open();

                    SqlCommand command = new SqlCommand(sqlQuery, _SqlConnection);
                    command.Parameters.AddWithValue("@Email", Email);
                    command.Parameters.AddWithValue("@NewPassword", NewPassword);

                    command.ExecuteNonQuery();
                    _SqlConnection.Close();

                    return true;
                }
                catch (Exception e)
                {
                    _Logger.Log(TraceLevel.Error, e.Message);
                }

                return false;
            });
        }

        public Task<bool> UpdatePostedLinkedAccount(string supervisedBy, string alias)
        {
            return Task.Run(() =>
            {
                try
                {
                    string sqlQuery = "UPDATE dbo.LinkedAccount SET SupervisedBy=@SupervisedBy"
                                      + " WHERE (Alias=@Alias)";
                    _SqlConnection.Open();

                    SqlCommand command = new SqlCommand(sqlQuery, _SqlConnection);
                    command.Parameters.AddWithValue("@SupervisedBy", supervisedBy);
                    command.Parameters.AddWithValue("@Alias", alias);

                    command.ExecuteNonQuery();
                    _SqlConnection.Close();

                    return true;
                }
                catch (Exception e)
                {
                    _Logger.Log(TraceLevel.Error, e.Message);
                }

                return false;
            });
        }

        public Task<string> GetUserByEmail(string Email)
        {
            return Task.Run(() =>
            {
                try
                {
                    String sqlQuery = "SELECT UserID FROM dbo.UserProfile"
                                       + " WHERE ((Email=@Email) AND (Active=1))";
                    _SqlConnection.Open();

                    SqlCommand command = new SqlCommand(sqlQuery, _SqlConnection);
                    command.Parameters.AddWithValue("@Email", Email);
                    string userId = "";
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            if (reader["UserID"] != null
                                && reader["UserID"].ToString() != "")
                            {
                                userId = reader["UserID"].ToString();
                            }
                        }
                    }
                    _SqlConnection.Close();

                    return userId;
                }
                catch (Exception e)
                {
                    _Logger.Log(TraceLevel.Error, e.Message);
                }

                return null;
            });
        }

        public Task<List<Supervisor>> GetSupervisors(string BranchID)
        {
            return Task.Run(() =>
            {
                List<Supervisor> sList = new List<Supervisor>();
                try
                {
                    String sqlQuery = "SELECT BranchID,Name FROM dbo.Supervisor"
                                       + " WHERE BranchID=@BranchID";
                    _SqlConnection.Open();

                    SqlCommand command = new SqlCommand(sqlQuery, _SqlConnection);
                    command.Parameters.AddWithValue("@BranchID", BranchID);

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Supervisor s = new Supervisor();
                            s.Name = reader["Name"].ToString();
                            s.BranchID = reader["BranchID"].ToString();

                            sList.Add(s);
                        }
                    }
                    _SqlConnection.Close();
                }
                catch (Exception e)
                {
                    _Logger.Log(TraceLevel.Error, e.Message);
                }

                return sList;
            });
        }

        public Task<bool> DeleteLinkedAccount(string Alias)
        {
            return Task.Run(() =>
            {
                try
                {
                    string sqlQuery = "DELETE FROM dbo.LinkedAccount"
                                      + " WHERE Alias=@Alias";
                    _SqlConnection.Open();

                    SqlCommand command = new SqlCommand(sqlQuery, _SqlConnection);
                    command.Parameters.AddWithValue("@Alias", Alias);
                    command.ExecuteNonQuery();

                    _SqlConnection.Close();

                    return true;
                }
                catch (Exception e)
                {
                    _Logger.Log(TraceLevel.Error, e.Message);
                }

                return false;
            });
        }
        
        public Task<bool> CloseLinkedAccount(string Alias, string UserID)
        {
            return Task.Run(() =>
            {
                try
                {
                    string sqlQuery = "UPDATE dbo.LinkedAccount SET Active=0,"
                                      + "SupervisedBy=@SupervisedBy,SupervisedOn=@SupervisedOn"
                                      + " WHERE Alias=@Alias";
                    _SqlConnection.Open();

                    SqlCommand command = new SqlCommand(sqlQuery, _SqlConnection);
                    command.Parameters.AddWithValue("@SupervisedBy", UserID);
                    command.Parameters.AddWithValue("@SupervisedOn", DateTime.Now);
                    command.Parameters.AddWithValue("@Alias", Alias);
                    command.ExecuteNonQuery();

                    _SqlConnection.Close();

                    return true;
                }
                catch (Exception e)
                {
                    _Logger.Log(TraceLevel.Error, e.Message);
                }

                return false;
            });
        }

        public Task<bool> CloseUserProfile(string UserId)
        {
            return Task.Run(() =>
            {
                try
                {
                    string sqlQuery = "UPDATE dbo.UserProfile SET Active=0"
                                      + " WHERE UserID=@UserID";
                    _SqlConnection.Open();

                    SqlCommand command = new SqlCommand(sqlQuery, _SqlConnection);
                    command.Parameters.AddWithValue("@UserID", UserId);
                    command.ExecuteNonQuery();

                    _SqlConnection.Close();

                    return true;
                }
                catch (Exception e)
                {
                    _Logger.Log(TraceLevel.Error, e.Message);
                }

                return false;
            });
        }

        public Task<bool> DeleteSupervisor(string Name)
        {
            return Task.Run(() =>
            {
                try
                {
                    string sqlQuery = "DELETE FROM dbo.Supervisor"
                                      + " WHERE Name=@Name";
                    _SqlConnection.Open();

                    SqlCommand command = new SqlCommand(sqlQuery, _SqlConnection);
                    command.Parameters.AddWithValue("@Name", Name);
                    command.ExecuteNonQuery();

                    _SqlConnection.Close();

                    return false;
                }
                catch (Exception e)
                {
                    _Logger.Log(TraceLevel.Error, e.Message);
                }

                return false;
            });
        }

        public Task<bool> UpdateUser(UserProfile up, bool logIn)
        {
            return Task.Run(() =>
            {
                try
                {
                    string sqlQuery = "UPDATE dbo.UserProfile SET "
                                      + ((logIn) ? "LogInDate=@LogInDate"
                                                 : "LogOutDate=@LogOutDate")
                                      + ((logIn) ? ",GSMOperationID=@GSMOperationID"
                                                 : "")
                                      + ((logIn) ? ",SessionID=@SessionID"
                                                 : "")
                                      + " WHERE ((UserID=@UserID)"
                                      + " AND (BranchID=@BranchID)"
                                      + " AND (Role=@Role))";
                    _SqlConnection.Open();

                    SqlCommand command = new SqlCommand(sqlQuery, _SqlConnection);
                    if (logIn)
                    {
                        command.Parameters.AddWithValue("@LogInDate", up.LogInDate);
                        command.Parameters.AddWithValue("@GSMOperationID", up.GSMOperationID);
                        command.Parameters.AddWithValue("@SessionID", up.SessionID);
                    }
                    else
                    {
                        command.Parameters.AddWithValue("@LogOutDate", up.LogOutDate);
                    }

                    command.Parameters.AddWithValue("@UserID", up.UserID);
                    command.Parameters.AddWithValue("@BranchID", ((up.BranchID != null) ? up.BranchID : "00"));
                    command.Parameters.AddWithValue("@Role", ((up.Role != null) ? up.Role : "N/A"));

                    command.ExecuteNonQuery();

                    _SqlConnection.Close();

                    return true;
                }
                catch (Exception e)
                {
                    _Logger.Log(TraceLevel.Error, e.Message);
                }

                return false;
            });
        }

        public Task<bool> UpdateUserProfile(UserProfile up)
        {
            return Task.Run(() =>
            {
                try
                {
                    string sqlQuery = "UPDATE dbo.UserProfile SET "
                                      + "Email=@Email,UserID=@UserID,"
                                      + "Password=@Password,Role=@Role,"
                                      + "BranchID=@BranchID,GSMOperationID=@GSMOperationID,"
                                      + "SessionID=@SessionID,HasChanged=@HasChanged,"
                                      + "Active=@Active,LogInDate=@LogInDate,LogOutDate=@LogOutDate"
                                      + " WHERE Id=@Id";
                    _SqlConnection.Open();

                    SqlCommand command = new SqlCommand(sqlQuery, _SqlConnection);
                    command.Parameters.AddWithValue("@Email", up.Email);
                    command.Parameters.AddWithValue("@UserID", up.UserID);
                    command.Parameters.AddWithValue("@Password", up.Password);
                    command.Parameters.AddWithValue("@Role", up.Role);
                    command.Parameters.AddWithValue("@BranchID", up.BranchID);
                    command.Parameters.AddWithValue("@GSMOperationID", up.GSMOperationID);
                    command.Parameters.AddWithValue("@SessionID", up.SessionID);
                    command.Parameters.AddWithValue("@HasChanged", up.HasChanged);
                    command.Parameters.AddWithValue("@Active", up.Active);
                    command.Parameters.AddWithValue("@LogInDate", up.LogInDate);
                    command.Parameters.AddWithValue("@LogOutDate", up.LogOutDate);
                    command.Parameters.AddWithValue("@Id", up.Id);
                    command.ExecuteNonQuery();

                    _SqlConnection.Close();

                    return true;
                }
                catch (Exception e)
                {
                    _Logger.Log(TraceLevel.Error, e.Message);
                }

                return false;
            });
        }

        public Task<bool> UpdateSupervisor(Supervisor sp)
        {
            return Task.Run(() =>
            {
                try
                {
                    string sqlQuery = "UPDATE dbo.Supervisor SET "
                                      + "BranchID=@BranchID,Name=@Name"
                                      + " WHERE Id=@Id";
                    _SqlConnection.Open();

                    SqlCommand command = new SqlCommand(sqlQuery, _SqlConnection);
                    command.Parameters.AddWithValue("@BranchID", sp.BranchID);
                    command.Parameters.AddWithValue("@Name", sp.Name);
                    command.Parameters.AddWithValue("@Id", sp.Id);
                    command.ExecuteNonQuery();

                    _SqlConnection.Close();

                    return true;
                }
                catch (Exception e)
                {
                    _Logger.Log(TraceLevel.Error, e.Message);
                }

                return false;
            });
        }

        public Task<string> GetSessionID(string Email)
        {
            return Task.Run(() =>
            {
                try
                {
                    _SqlConnection.Open();

                    SqlCommand command = new SqlCommand("SELECT SessionID"
                                                        + " FROM dbo.UserProfile"
                                                        + " WHERE Email=@Email", _SqlConnection);
                    command.Parameters.AddWithValue("@Email", Email);
                    String SessionID = "";
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            SessionID = reader["SessionID"].ToString();
                        }
                    }
                    _SqlConnection.Close();

                    return SessionID;
                }
                catch (Exception e)
                {
                    _Logger.Log(TraceLevel.Error, e.Message);
                }

                return "";
            });
        }

        public Task<string> GetSessionID(string UserID, string BranchID, string Role)
        {
            return Task.Run(() =>
            {
                try
                {
                    _SqlConnection.Open();

                    SqlCommand command = new SqlCommand("SELECT SessionID"
                                                        + " FROM dbo.UserProfile"
                                                        + " WHERE ((UserID=@UserID)"
                                                        + " AND (BranchID=@BranchID)"
                                                        + " AND (Role=@Role))", _SqlConnection);
                    command.Parameters.AddWithValue("@UserID", UserID);
                    command.Parameters.AddWithValue("@BranchID", BranchID);
                    command.Parameters.AddWithValue("@Role", Role);
                    String SessionID = "";
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            SessionID = reader["SessionID"].ToString();
                        }
                    }
                    _SqlConnection.Close();

                    return SessionID;
                }
                catch (Exception e)
                {
                    _Logger.Log(TraceLevel.Error, e.Message);
                }

                return "";
            });
        }

        public Task<bool> UpdateLinkedAccount(string UserID, string Role,
                                              string BranchID, string GSMOperationID,
                                              bool isActived)
        {
            return Task.Run(() =>
            {
                try
                {
                    string sqlQuery;
                    if (Role.CompareTo("Auditor") == 0)
                    {
                        sqlQuery = "UPDATE dbo.LinkedAccount SET "
                                   + "AuditedBy=@AuditedBy,AuditedOn=@AuditedOn"
                                   + " WHERE ((BranchID=@BranchID) AND (GSMOperationID=@GSMOperationID))";
                        _SqlConnection.Open();
                        SqlCommand command = new SqlCommand(sqlQuery, _SqlConnection);
                        command.Parameters.AddWithValue("@AuditedBy", UserID);
                        command.Parameters.AddWithValue("@AuditedOn", DateTime.Now);
                        command.Parameters.AddWithValue("@BranchID", BranchID);
                        command.Parameters.AddWithValue("@GSMOperationID", GSMOperationID);
                        command.ExecuteNonQuery();
                        _SqlConnection.Close();
                    }
                    else if (Role.CompareTo("Auditor") != 0 && isActived)
                    {
                        sqlQuery = "UPDATE dbo.LinkedAccount SET "
                                   + "SupervisedBy=@SupervisedBy,SupervisedOn=@SupervisedOn"
                                   + " WHERE ((BranchID=@BranchID) AND (GSMOperationID=@GSMOperationID))";
                        _SqlConnection.Open();
                        SqlCommand command = new SqlCommand(sqlQuery, _SqlConnection);
                        command.Parameters.AddWithValue("@SupervisedBy", UserID);
                        command.Parameters.AddWithValue("@SupervisedOn", DateTime.Now);
                        command.Parameters.AddWithValue("@BranchID", BranchID);
                        command.Parameters.AddWithValue("@GSMOperationID", GSMOperationID);
                        command.ExecuteNonQuery();
                        _SqlConnection.Close();
                    }

                    return true;
                }
                catch (Exception e)
                {
                    _Logger.Log(TraceLevel.Error, e.Message);
                }

                return false;
            });
        }
    }
}