﻿
using OMBanking.Web.Log;
using OMBanking.Web.Proxy.Bank;
using System;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Diagnostics;


namespace OMBanking.Web.Handler
{
    public class AsyncWebHandler : HttpTaskAsyncHandler
    {
        public override async Task ProcessRequestAsync(HttpContext context)
        {
            IB2WServiceBinding _IB2WService = new IB2WServiceBinding();
            HttpRequest request = context.Request;
            int dataLength = context.Request.TotalBytes;
            if (dataLength > 0)
            {
                byte[] bytes = new byte[dataLength];
                string rawData = Encoding.UTF8
                                 .GetString(bytes, 0,
                                            context.Request.InputStream
                                            .Read(bytes, 0, bytes.Length));
                try
                {
                    HttpContext octx = context;
                    if (rawData.Contains("CancelTransfer"))
                    {
                        octx = await _IB2WService.CancelTransfer(context, rawData);
                    }
                    else if (rawData.Contains("WalletToAccountTransfer"))
                    {
                        octx = await _IB2WService.WalletToAccountTransfer(context, rawData);
                    }
                    else if (rawData.Contains("AccountToWalletTransfer"))
                    {
                        octx = await _IB2WService.AccountToWalletTransfer(context, rawData);
                    }
                    else if (rawData.Contains("GetAccountBalance"))
                    {
                        octx = await _IB2WService.GetAccountBalance(context, rawData);
                    }
                    else if (rawData.Contains("TransferStatusInquiry"))
                    {
                        octx = await _IB2WService.TransferStatusInquiry(context, rawData);
                    }
                    else if (rawData.Contains("GetMiniStatement"))
                    {
                        octx = await _IB2WService.GetMiniStatement(context, rawData);
                    }
                    context = octx;
                }
                catch(Exception e)
                {
                    ServiceLogger _Logger = (ServiceLogger)LogFactory.create(Logger.LoggingMode.SERVICE);
                    _Logger.Log(TraceLevel.Error, e.Message);
                }
            }
        }

        public override bool IsReusable
        {
            get { return true; }
        }
    }
}