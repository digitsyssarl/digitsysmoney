﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OMBanking.Web.Reporting
{
    public class ReportFactory
    {
        public enum ReportType {
            PDF,
            EXCEL
        }

        public static Report create(ReportType rt)
        {
            switch (rt)
            {
                case ReportType.PDF:
                    return new PDFReport();
                case ReportType.EXCEL:
                    return new ExcelReport();
            }

            return null;
        }
    }
}