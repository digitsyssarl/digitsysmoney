﻿using OMBanking.Web.Log;
using Spire.Pdf;
using Spire.Pdf.AutomaticFields;
using Spire.Pdf.Graphics;
using Spire.Pdf.Tables;
using System;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;

namespace OMBanking.Web.Reporting
{
    public class PDFReport : Report
    {
        public void generatePDF(DataTable dt, string title, string path, string imgPath, string dir)
        {
            try
            {
                PdfDocument doc = new PdfDocument();
                doc.PageSettings.Size = PdfPageSize.A4;
                PdfPageBase page = doc.Pages.Add();

                PdfUnitConvertor unitCvtr = new PdfUnitConvertor();
                PdfMargins margin = new PdfMargins();
                margin.Top = unitCvtr.ConvertUnits(2.54f, PdfGraphicsUnit.Centimeter, PdfGraphicsUnit.Point);
                margin.Bottom = margin.Top;
                margin.Left = unitCvtr.ConvertUnits(4.17f, PdfGraphicsUnit.Centimeter, PdfGraphicsUnit.Point);
                margin.Right = margin.Left;
                AddHeader(doc, margin, title, imgPath);
                PdfTable table = new PdfTable();
                table.Style.ShowHeader = true;
                table.DataSourceType = PdfTableDataSourceType.TableDirect;
                if (dt.Rows.Count < 250)
                    table.DataSource = dt;
                else
                {
                    DataTable dt1 = dt.Rows.Cast<DataRow>().Take(250).CopyToDataTable();
                    table.DataSource = dt1;
                }
                table.Draw(page, new PointF(0, margin.Top));
                AddFooter(doc, margin);
                try
                {
                    doc.SaveToFile(path);
                }
                catch (UnauthorizedAccessException)
                {
                    FileAttributes attributes = File.GetAttributes(dir);
                    if ((attributes & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
                    {
                        attributes &= ~FileAttributes.ReadOnly;
                        File.SetAttributes(path, attributes);
                        doc.SaveToFile(path);
                    }
                    else
                    {
                        throw;
                    }
                }
                Process.Start(path);
            }
            catch (Exception e)
            {
                ServiceLogger _Logger = (ServiceLogger)LogFactory.create(LogBase.LoggingMode.SERVICE);
                _Logger.Log(TraceLevel.Error, e.Message);
            }
        }

        void AddHeader(PdfDocument doc, PdfMargins margin, string title, string imgPath)
        {
            SizeF pageSize = doc.Pages[0].Size;
            PdfPageTemplateElement headerSpace = new PdfPageTemplateElement(pageSize.Width, margin.Top);
            headerSpace.Foreground = true;
            doc.Template.Top = headerSpace;
            PdfImage headerImage = PdfImage.FromFile(imgPath);
            float width = headerImage.Width / 5;
            float height = headerImage.Height / 5;
            headerSpace.Graphics.DrawImage(headerImage, 0, 0, width, height);

            PdfTrueTypeFont font = new PdfTrueTypeFont(new Font("Arial", 14f, FontStyle.Bold), true);
            PdfStringFormat format = new PdfStringFormat(PdfTextAlignment.Center);
            string headerText = title;
            float x = pageSize.Width;
            float y = 0;
            PdfPen p = new PdfPen(Color.FromName(ConfigurationManager.AppSettings.Get("color")));
            headerSpace.Graphics.DrawRectangle(p, x/2-x/3 , y, x/3+x/3, 25);
            headerSpace.Graphics.DrawString(headerText, font, PdfBrushes.Black, x/2, y, format);
            headerText = DateTime.Today.ToString().Split(new char[] { ' ' })[0];
            format = new PdfStringFormat(PdfTextAlignment.Right);
            font = new PdfTrueTypeFont(new Font("Arial", 9f, FontStyle.Italic), true);
            headerSpace.Graphics.DrawString(headerText, font, PdfBrushes.Black, x, y, format);
            
        }

        void AddFooter(PdfDocument doc, PdfMargins margin)
        {
            SizeF pageSize = doc.Pages[0].Size;

            PdfPageTemplateElement footerSpace = new PdfPageTemplateElement(pageSize.Width, margin.Bottom);
            footerSpace.Foreground = true;
            doc.Template.Bottom = footerSpace;

            PdfTrueTypeFont font = new PdfTrueTypeFont(new Font("Arial", 9f, FontStyle.Bold), true);
            PdfStringFormat format = new PdfStringFormat(PdfTextAlignment.Center);
            string headerText = "Copyright © " + DateTime.Now.Year.ToString() + " "
                                + ConfigurationManager.AppSettings.Get("bank_name")
                                + ". All Rights Reserved.";
            float x = pageSize.Width / 2;
            float y = 8f;
            footerSpace.Graphics.DrawString(headerText, font, PdfBrushes.Black, x, y, format);

            PdfPageNumberField number = new PdfPageNumberField();
            PdfPageCountField count = new PdfPageCountField();
            PdfCompositeField compositeField = new PdfCompositeField(font, PdfBrushes.Black, "Page {0} of {1}", number, count);
            compositeField.StringFormat = new PdfStringFormat(PdfTextAlignment.Center, PdfVerticalAlignment.Middle);
            compositeField.Bounds = footerSpace.Bounds;
            compositeField.Draw(footerSpace.Graphics);
        }
    }
}