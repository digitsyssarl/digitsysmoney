﻿using OMBanking.Web.Log;
using Spire.Xls;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;

namespace OMBanking.Web.Reporting
{
    public class ExcelReport : Report
    {
        public void generateExcel(DataTable dt, string title, string path, string imgPath, string dir)
        {
            try
            {
                Workbook workbook = new Workbook();
                workbook.Worksheets.Clear();
                var sheet = workbook.Worksheets.Add("Report");
                System.Drawing.Image img = System.Drawing.Image.FromFile(imgPath);
                sheet.PageSetup.LeftHeaderImage = new System.Drawing.Bitmap(img, new System.Drawing.Size(img.Width/5, img.Height/5));
                sheet.PageSetup.LeftHeader = "&G\n\n";
                sheet.PageSetup.RightHeader = "\n" + title + "\n" + DateTime.Today.ToString().Split(new char[] { ' ' })[0] + "\n \n ";
                sheet.PageSetup.CenterFooter = "Copyright © " + DateTime.Now.Year.ToString() + " "
                                                    + ConfigurationManager.AppSettings.Get("bank_name")
                                                    + ". All Rights Reserved.";
                sheet.Range["A1:Z1"].ColumnWidth = 22;
                sheet.InsertDataTable(dt, true, 1, 1);
                RichText richText = sheet.Range["A1"].RichText;
                ExcelFont fontBold = workbook.CreateFont();
                fontBold.IsBold = true;
                richText.SetFont(0, richText.Text.Length - 1, fontBold);

                try
                {
                    workbook.SaveToFile(path, ExcelVersion.Version2007);
                }
                catch (UnauthorizedAccessException)
                {
                    FileAttributes attributes = File.GetAttributes(dir);
                    if ((attributes & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
                    {
                        attributes &= ~FileAttributes.ReadOnly;
                        File.SetAttributes(path, attributes);
                        workbook.SaveToFile(path, ExcelVersion.Version2007);
                    }
                    else
                    {
                        throw;
                    }
                }
                Process.Start(path);
            }
            catch (Exception e)
            {
                ServiceLogger _Logger = (ServiceLogger)LogFactory.create(LogBase.LoggingMode.SERVICE);
                _Logger.Log(TraceLevel.Error, e.Message);
            }
        }
    }

}