﻿using OMBanking.Web.Log;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Xml;

namespace OMBanking.Web.Service
{
    public class B2WServiceSOAPClient 
    {
        private HttpWebRequest _HttpWebRequest;

        public B2WServiceSOAPClient(string soapAction, string url)
        {
            _HttpWebRequest = (HttpWebRequest) WebRequest.Create(url);
            _HttpWebRequest.ContentType = "text/xml;charset=\"utf-8\"";
            _HttpWebRequest.Method = "POST";
        }

        public string execute(string soapRequest)
        {
             var sslFailureCallback = new RemoteCertificateValidationCallback(delegate { return true; });
            string soapResponse = "";
            bool ignoreSslErrors = bool.Parse(ConfigurationManager.AppSettings.Get("ignore_ssl_errors"));

            try
            {
                if (ignoreSslErrors)
                {
                    ServicePointManager.ServerCertificateValidationCallback += sslFailureCallback;
                }

                XmlDocument SOAPReqBody = new XmlDocument();
                SOAPReqBody.LoadXml(soapRequest);
                using (Stream stream = _HttpWebRequest.GetRequestStream())
                {
                    SOAPReqBody.Save(stream);
                }
                using (WebResponse serviceRes = _HttpWebRequest.GetResponse())
                {
                    using (StreamReader rd = new StreamReader(serviceRes.GetResponseStream()))
                    {
                        soapResponse = rd.ReadToEnd();
                    }
                }
            }
            catch (Exception e)
            {
                ServiceLogger _Logger = (ServiceLogger)LogFactory.create(LogBase.LoggingMode.SERVICE);
                _Logger.Log(System.Diagnostics.TraceLevel.Error,
                            "May not have Internet connection. Please, check it and try again: " + e.Message);
            }
            finally
            {
                if (ignoreSslErrors)
                {
                    ServicePointManager.ServerCertificateValidationCallback -= sslFailureCallback;
                }
            }

            return soapResponse;
        }
    }
}