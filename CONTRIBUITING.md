## Clone this repo

`git clone https://bitbucket.org/digitsyssarl/digitsysmoney`

## Move to the test branch (DON'T USE the master branch)

`git checkout testversion`

## Pull the latest version

`git pull`

N.B: Never push without pulling the code, it will create a merge conflict

## Add your changes

`git add .`

## Commit your changes

`git commit -m "DIG-XX <your message>`

This will link the changes to the JIRA issue `DIG-XX`

## Push code

`git push`

If it is the first time,

`git push --set-upstream origin master` 

## Pull Request

To push your code, send a pull request so that we can check your code before submitting it.