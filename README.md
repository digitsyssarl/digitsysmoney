## DigitSys Mobile Money Platform

It allows one to perform Bank to Wallet and Wallet to Bank operations (get account balance, money transfer, status inquiry, know your customer, 
consult last transactions, subscribe/unsubscribe customer to the service, consult all transactions, consult subscriptions, ....) 
via Orange and MTN money.


### Requirements
+ Windows 10, RAM >= 2GB, Disk Size >= 200 GB
+ Visual Studio 2015
+ Microsoft SQL Server 2012
+ Libraries (Json.NET, iText)

### Copyright

DigitSys SARL

Immeuble TECNO, Boulevard de la Liberte

Akwa-Douala 