/*CREATE FUNCTION dbo.t_DateElapsed (@D1 DateTime,@D2 DateTime)
	Returns Table
	Return (
		with cteBN(N)   as (Select 1 From (Values(1),(1),(1),(1),(1),(1),(1),(1),(1),(1)) N(N)),
			 cteRN(R)   as (Select Row_Number() Over (Order By (Select NULL))-1 From cteBN a,cteBN b,cteBN c),
			 cteYY(N,D) as (Select Max(R),Max(DateAdd(YY,R,@D1))From cteRN R Where DateAdd(YY,R,@D1)<=@D2),
			 cteMM(N,D) as (Select Max(R),Max(DateAdd(MM,R,D))  From (Select Top 12 R From cteRN Order By 1) R, cteYY P Where DateAdd(MM,R,D)<=@D2),
			 cteDD(N,D) as (Select Max(R),Max(DateAdd(DD,R,D))  From (Select Top 31 R From cteRN Order By 1) R, cteMM P Where DateAdd(DD,R,D)<=@D2),
			 cteHH(N,D) as (Select Max(R),Max(DateAdd(HH,R,D))  From (Select Top 24 R From cteRN Order By 1) R, cteDD P Where DateAdd(HH,R,D)<=@D2),
			 cteMI(N,D) as (Select Max(R),Max(DateAdd(MI,R,D))  From (Select Top 60 R From cteRN Order By 1) R, cteHH P Where DateAdd(MI,R,D)<=@D2),
			 cteSS(N,D) as (Select Max(R),Max(DateAdd(SS,R,D))  From (Select Top 60 R From cteRN Order By 1) R, cteMI P Where DateAdd(SS,R,D)<=@D2)

		Select [Years]   = cteYY.N
			  ,[Months]  = cteMM.N
			  ,[Days]    = cteDD.N
			  ,[Hours]   = cteHH.N
			  ,[Minutes] = cteMI.N
			  ,[Seconds] = cteSS.N
			  --,[Elapsed] = Format(cteYY.N,'0000')+':'+Format(cteMM.N,'00')+':'+Format(cteDD.N,'00')+' '+Format(cteHH.N,'00')+':'+Format(cteMI.N,'00')+':'+Format(cteSS.N,'00')
		 From  cteYY,cteMM,cteDD,cteHH,cteMI,cteSS
	);
*/


DECLARE	
   @AccountID AS VARCHAR(25), 
	 @TrxQuotaD INT,
	 @TrxQuotaW INT,
	 @TrxQuotaM INT,
	 @TrxQuotaDays AS INT,
	 @TrxQuotaWeeks AS INT,
   @TrxQuotaMonths AS INT,
   @TrxStartD AS DateTime,
   @TrxStartW AS DateTime,
   @TrxStartM AS DateTime
	    
SELECT @TrxQuotaD = QuotaTrxDay,
       @TrxQuotaW = QuotaTrxWeek, 
       @TrxQuotaM = QuotaTrxMonth, 
       @TrxStartD = DateTrxDay,
       @TrxStartW = DateTrxWeek,
       @TrxStartM = DateTrxMonth
FROM t_TrxQuota WITH (UPDLOCK,SERIALIZABLE)
WHERE (AccountID=@AccountID);

IF EXISTS(SELECT TOP 1 QuotaTrxDay,QuotaTrxWeek,QuotaTrxMonth,DateTrxDay,DateTrxWeek,DateTrxMonth 
		      FROM  t_TrxQuota WITH (UPDLOCK,SERIALIZABLE)
          WHERE (AccountID = @AccountID))
 BEGIN
   
   SET @TrxQuotaDays = (SELECT Td.Days FROM t_DateElapsed(@TrxStartD,GetDate()) AS Td);
   SET @TrxQuotaWeeks = (SELECT Tw.Hours FROM t_DateElapsed(@TrxStartW,GetDate()) AS Tw);
   SET @TrxQuotaMonths = (SELECT Tm.Months FROM t_DateElapsed(@TrxStartM,GetDate()) AS Tm);

   IF @TrxQuotaDays < 1
      BEGIN
	      IF @TrxQuotaD <= 5
	        BEGIN
		   	    UPDATE t_TrxQuota 
		        SET QuotaTrxDay = QuotaTrxDay + 1, QuotaTrxWeek = QuotaTrxWeek + 1, QuotaTrxMonth = QuotaTrxMonth + 1 
		        WHERE AccountID = @AccountID;
	        END
	      ELSE
	        BEGIN
		        INSERT INTO #Response (FieldID, FieldValue) SELECT 39, '114'               
					  SELECT FieldValue,'Amount is over the daily limit' FROM #Response;
					  ROLLBACK TRAN; 
				  END
			END     
   ELSE
    BEGIN
      IF @TrxQuotaDays = 1
        BEGIN
	        UPDATE t_TrxQuota 
					SET QuotaTrxDay = 0, DateTrxDay = GETDATE() 
	        WHERE AccountID = @AccountID;
        END

      IF @TrxQuotaWeeks < 1
        BEGIN
		      IF @TrxQuotaW <= 15
		        BEGIN
		   	      UPDATE t_TrxQuota 
		          SET QuotaTrxWeek = QuotaTrxWeek + 1, QuotaTrxMonth = QuotaTrxMonth + 1 
		          WHERE AccountID = @AccountID;
		        END
		      ELSE
		        BEGIN
		          INSERT INTO #Response (FieldID, FieldValue) SELECT 39, '114'               
					    SELECT FieldValue,'Amount is over the weekly limit' FROM #Response;
					    ROLLBACK TRAN;
					  END
				END      
		  ELSE
		    BEGIN
		      IF @TrxQuotaWeeks = 1
		        BEGIN
		          UPDATE t_TrxQuota 
						  SET QuotaTrxWeek = 0, DateTrxWeek = GETDATE() 
		          WHERE AccountID = @AccountID;
		        END

		      IF @TrxQuotaMonths < 1
		        BEGIN
				      IF @TrxQuotaM <= 30
				        BEGIN
				   	      UPDATE t_TrxQuota 
				          SET QuotaTrxMonth = QuotaTrxMonth + 1 
				          WHERE AccountID = @AccountID;
				        END
				      ELSE
				        BEGIN
				          INSERT INTO #Response (FieldID, FieldValue) SELECT 39, '114'               
							    SELECT FieldValue,'Amount is over the monthly limit' FROM #Response;
							    ROLLBACK TRAN;
							  END
						END      
			    ELSE
			      BEGIN
				      IF @TrxQuotaMonths = 1
				        BEGIN
				          UPDATE t_TrxQuota 
								  SET QuotaTrxMonth = 0, DateTrxMonth = GETDATE() 
				          WHERE AccountID = @AccountID;
				        END
			      END
			  END
		END	  
 END
ELSE
 BEGIN
    INSERT INTO t_TrxQuota (AccountID, QuotaTrxDay, QuotaTrxWeek, QuotaTrxMonth, DateTrxDay, DateTrxWeek, DateTrxMonth)
    VALUES (@AccountID, 1, 1, 1, GETDATE(), GETDATE(), GETDATE());
 END  