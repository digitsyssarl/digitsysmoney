USE [BRNET_LIVE]
GO
/****** Object:  StoredProcedure [dbo].[p_USSDInquiry]    Script Date: 10/15/2021 1:41:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
--Insert Into t_SystemAcTag  
--Select 'USSD_MTN_AC','USSD MTN Account';  
  
--Insert Into t_SystemAccount  
--Select 'GLPARAM', 'USSD_MTN_AC', 0, 'A', NULL, 'L';  
  
ALTER proc [dbo].[p_USSDInquiry]            
(  
  @AccountID  VARCHAR(25),              
  @Fee Amount,                
  @TrxDescription Varchar(max),  
  @mBankingID Varchar(30),
  @InquiryType Varchar(30)
)            
AS            
BEGIN     
             
DECLARE @CurrencyID      Varchar(20),    
     @TrfSerialID   INT,      
     @SlNo     INT,  
     @WorkingDate  SmallDateTime,  
     @CreditAccountID Varchar(50),  
     @TrxBatchID    Varchar(20),  
     @SerialID   Varchar(20),  
     @HOBranchID   Varchar(20),  
     @BankID    Varchar(20),  
     @ModuleID   Varchar(20),  
     @AvailableBalance Money,  
     @MainGLID   Varchar(20),  
     @TrxDate   SmallDateTime,  
     @ProductID   Varchar(10),  
     @OurBranchID  Varchar(10),   
     @CreatedBy   Varchar(50),   
     @OperatedBy   Varchar(50),  
     @AccountStatusID Varchar(50),  
     @AccountClassID  Varchar(50),  
     @BranchStatus  bit,  
     @HOBranchStatus  bit,    
     @IsDayOpen   bit,  
     @IsTrxAllow   bit,  
     @IsHODayOpen  bit,  
     @IsHOTrxAllow  bit,  
     @HOSODDate   SmallDateTime,  
     @OurBranchSODDate SmallDateTime,  
  @GLClassID   nVarChar(10),  
  @GLAccountStatusID Char(1),  
  @GLCurrencyID  nVarChar(10),  
  @GLCategoryID  nVarChar(10),  
  @TrxBranchID  VARCHAR(25),
  @GotoPending	Bit = 0,
  @Result Varchar(max),    
  @MyTrxDescription Varchar(max)
 
        
  
  
Create table #suppresser  
(  
 SerialID INT,  
 BatchID  BIGINT  
)  
       
 SET @BranchStatus = 0
 SET @IsDayOpen	   = 0
 SET @IsTrxAllow   = 0
 SET @GotoPending  = 0

   
SET @CreatedBy = 'M-Banking USSD'    
        
SELECT @ProductID   = ProductID,  
    @OurBranchID   = OurBranchID,  
    @AccountStatusID  = AccountStatusID,  
    @AccountClassID  = AccountClassID,  
    @AvailableBalance = dbo.f_GetAvailableBalance(@OurBranchID,@AccountID)   
FROM t_AccountCustomer(nolock)   
WHERE  AccountID = @AccountID   
  
SELECT @CurrencyID = CurrencyID From t_Product(nolock) Where PRODUCTID = @ProductID        
SELECT @WorkingDate = dbo.f_GetWorkingDate(@OurBranchID)   
SELECT @BankID  = dbo.f_GetBankID(@OurBranchID)         
SELECT @HOBranchID = dbo.f_GetHOBranchID(@BankID)  
SELECT @TrxBranchID = @HOBranchID   
SELECT @TrxDate  = @WorkingDate      
SELECT @OperatedBy  = 'USSD'       
  
 SELECT @BranchStatus=BranchStatus,@IsDayOpen=IsDayOpen,@IsTrxAllow=IsTrxAllow,@OurBranchSODDate = SODDate  
 FROM t_SystemBranchStatus(nolock) WHERE OurBranchID = @OurBranchID   
  
 SELECT @HOBranchStatus=BranchStatus,@IsHODayOpen=IsDayOpen,@IsHOTrxAllow=IsTrxAllow,@HOSODDate= SODDate  
 FROM t_SystemBranchStatus(nolock) WHERE OurBranchID = @HOBranchID   
                
SELECT @MainGLID = AccountID   
FROM t_GLInterface(nolock)   
WHERE RelevantID = @ProductID AND AccountTagID = 'CONTROL_AC'                  
                    
SELECT @CreditAccountID = AccountID   
From t_GLParameter(nolock)  
Where OurBranchID =  @TrxBranchID And GLParameterID = 'USSD_ORA_AC'  
   
   
 CREATE TABLE #Response (                            
  FieldID  numeric,                            
  FieldValue varchar(4000)                            
 )                          
      
IF (ISNULL(@Fee,0)) > @AvailableBalance            
BEGIN            
INSERT INTO #Response (FieldID, FieldValue) SELECT 39, '116'               
SELECT FieldValue,'Insufficient Balance' FROM #Response            
RETURN            
END            
   
------------------Others Control---------------------  
   
 --Account Not Exists    
 IF @AccountStatusID IS NULL    
 BEGIN    
  --RAISERROR(N'BREXDB300022',16,1)    
 INSERT INTO #Response (FieldID, FieldValue) SELECT 39, '114'               
 SELECT FieldValue,'Account Not Exists' FROM #Response            
 RETURN    
 END    
     
 --- Closed Accounts , Transaction not Allowed    
 IF @AccountStatusID = 'AC' 
 BEGIN    
  --RAISERROR(N'BREXDB300011',16,1)    
 INSERT INTO #Response (FieldID, FieldValue) SELECT 39, '114'               
 SELECT FieldValue,'Closed Accounts,Transaction not Allowed' FROM #Response            
 RETURN    
 END    
 --- Blocked Account, Transaction Not Allowed     
 IF @AccountStatusID = 'AB'    
 BEGIN    
  --RAISERROR(N'BREXDB300013',16,1)    
 INSERT INTO #Response (FieldID, FieldValue) SELECT 39, '114'               
 SELECT FieldValue,'Blocked Account,Transaction Not Allowed' FROM #Response            
 RETURN    
 END    
  
--- debit is not allowed for Dormant accounts   
IF @AccountStatusID = 'AD'  
BEGIN  
 --RAISERROR(N'BREXDB300021',16,1)  
 INSERT INTO #Response (FieldID, FieldValue) SELECT 39, '114'               
 SELECT FieldValue,'Debit is not allowed for Dormant Accounts' FROM #Response            
 RETURN  
END  
--Do not allow any debit transactions --SELECT * FROM T_SPECIALCONDITIONDETAIL WHERE SPECIALCONDITIONID='902'  
--IF (select dbo.f_GetConsAccountSpecialConditions (@OurBranchID,@AccountID,@AccountClassID,902))=1  
IF EXISTS(SELECT 1 FROM T_SPECIALCONDITIONDETAIL WHERE SPECIALCONDITIONID='902' AND ClassID = @AccountClassID)  
OR (select dbo.f_GetConsAccountSpecialConditions (@OurBranchID,@AccountID,@AccountClassID,902))=1  
BEGIN  
 --RAISERROR(N'BREXDB300004',16,1)  
 INSERT INTO #Response (FieldID, FieldValue) SELECT 39, '114'               
 SELECT FieldValue,'Debit transactions not allowed-Special Condition' FROM #Response            
 RETURN  
END  
  
--All debit transactions needs supervision --SELECT * FROM T_SPECIALCONDITIONDETAIL WHERE SPECIALCONDITIONID='906'  
--IF (select dbo.f_GetConsAccountSpecialConditions (@OurBranchID,@AccountID,@AccountClassID,906))=1  
IF EXISTS(SELECT 1 FROM T_SPECIALCONDITIONDETAIL WHERE SPECIALCONDITIONID='906' AND ClassID = @AccountClassID)  
OR (select dbo.f_GetConsAccountSpecialConditions (@OurBranchID,@AccountID,@AccountClassID,906))=1  
BEGIN  
 --RAISERROR(N'BREXDB300004',16,1)  
 INSERT INTO #Response (FieldID, FieldValue) SELECT 39, '114'               
 SELECT FieldValue,'Debit Transactions Need Supervision-Special Condition' FROM #Response            
 RETURN  
END  
  
-------Do Not Allow Debit Trx For Line Of Credit Without Withdrawn Opportunity Amount  
 IF EXISTS (SELECT TODTypeID  
 FROM t_LimitTOD WHERE OurBranchID = @OurBranchID   
   And AccountID = @AccountID And TODTypeID = 'LOC' AND PurposeCodeID = 'D' And ExpiryDate >= dbo.f_GetWorkingDate(@OurBranchID)  
   And ISNULL(dbo.f_GetAccountSpecialConditionValue(OurBranchID,308,AccountID,'V'), 0) = 0)  
BEGIN  
 --RAISERROR(N'BREXDB300444',16,1)  
 INSERT INTO #Response (FieldID, FieldValue) SELECT 39, '114'               
 SELECT FieldValue,'Line Of Credit' FROM #Response            
 RETURN  
END  
  
  
---------GL Account Control-----------------  
  
SELECT @GLClassID = GLClassID,  
 @GLAccountStatusID = GLAccountStatusID,  
 @GLCurrencyID = CurrencyID,  
 @GLCategoryID= GLCategoryID  
FROM v_GLBranchDetail  
WHERE OurBranchID = @HOBranchID  
 AND AccountID = @CreditAccountID  
  
   
--Account Not Exists  
IF @GLAccountStatusID IS NULL  
BEGIN  
 --RAISERROR(N'BREXDB300022',16,1)  
 INSERT INTO #Response (FieldID, FieldValue) SELECT 39, '114'               
 SELECT FieldValue,'GL Account Not Exists' FROM #Response            
 RETURN  
END  
  
--Selected Currency And Account Currency Are Not Same  
IF @GLCurrencyID <> ISNULL(@CurrencyID,@GLCurrencyID)  
BEGIN  
 --RAISERROR(N'BREXDB300023',16,1)  
 INSERT INTO #Response (FieldID, FieldValue) SELECT 39, '114'               
 SELECT FieldValue,'Selected Currency And Account Currency Are Not Same' FROM #Response            
 RETURN  
END  
  
--- Closed Accounts , Transaction not Allowed  
IF @GLAccountStatusID = 'C'  
BEGIN  
 --RAISERROR(N'BREXDB300011',16,1)  
 INSERT INTO #Response (FieldID, FieldValue) SELECT 39, '114'           
 SELECT FieldValue,'Closed Accounts,Transaction not Allowed' FROM #Response            
 RETURN  
END  
--- Blocked Account, Transaction Not Allowed   
IF @GLAccountStatusID = 'B'  
BEGIN  
 --RAISERROR(N'BREXDB300013',16,1)  
 INSERT INTO #Response (FieldID, FieldValue) SELECT 39, '114'               
 SELECT FieldValue,'Blocked Account,Transaction Not Allowed' FROM #Response            
 RETURN  
END  
  
--These Category GL won't allow transaction  
IF @GLCategoryID IN ('MAIN','CONT','OFFB', 'CTRA')  
BEGIN  
 --RAISERROR(N'BREXDB300015',16,1)  
 INSERT INTO #Response (FieldID, FieldValue) SELECT 39, '114'               
 SELECT FieldValue,'Category GL wont allow transaction' FROM #Response            
 RETURN  
END  
  
IF @GLClassID <> ''  
BEGIN  
 --Do not allow any debit transactions  
 IF EXISTS(SELECT BankID FROM t_SpecialConditionDetail  
   WHERE BankID = @BankID  
    AND ClassID =@GLClassID  
    AND SpecialConditionID = 902)  
 BEGIN  
  --RAISERROR(N'BREXDB300004',16,1)  
  INSERT INTO #Response (FieldID, FieldValue) SELECT 39, '114'               
  SELECT FieldValue,'Do not allow any debit transactions' FROM #Response            
  RETURN  
 END  
END  
------End GL Account Control----------------  
  
--------------Branch Not At the Same Date----  
IF (@HOSODDate<>@OurBranchSODDate)  
BEGIN  
 INSERT INTO #Response (FieldID, FieldValue) SELECT 39, '91'               
 SELECT FieldValue,'Branch Not At the Same Date' FROM #Response            
 RETURN  
END  
  
------------- Day Not Open ----------------  
--IF (@HOBranchStatus<>1 OR @BranchStatus<>1   
--OR @IsHOTrxAllow<>1 OR @IsTrxAllow<>1   
--OR @IsHODayOpen<>1 OR @IsDayOpen<>1)  
--BEGIN  
-- INSERT INTO #Response (FieldID, FieldValue) SELECT 39, '91'               
-- SELECT FieldValue,'Day Not Open' FROM #Response            
-- RETURN  
--END  

 IF(@OurBranchID <> @HOBranchID) /* Avoid Pending Interbranch when SOD dates are different */
 BEGIN
 IF(@HOSODDate<>@OurBranchSODDate)
    BEGIN
		 INSERT INTO #Response (FieldID, FieldValue) SELECT 39, '91'               
		 SELECT FieldValue,'Branch Not At the Same Date' FROM #Response            
		 RETURN  
	END	
	
	/* Avoid Interbranch day_open_ok/txn_allowed_ok/branch_status_ok but SOD dates not same */
	IF(@HOBranchStatus=1 AND @BranchStatus=1 
	AND @IsHOTrxAllow=1 AND @IsTrxAllow=1 
	AND @IsHODayOpen=1 AND @IsDayOpen=1 
	AND @HOSODDate<>@OurBranchSODDate)
	BEGIN
		 INSERT INTO #Response (FieldID, FieldValue) SELECT 39, '91'               
		 SELECT FieldValue,'Branch Not At the Same Date' FROM #Response            
		 RETURN  
	END	
 END 
 
  IF (@OurBranchID=@HOBranchID) /*Local Trx Check */
  BEGIN
	IF(@BranchStatus<>1 OR @IsTrxAllow<>1 OR @IsDayOpen<>1)
		SET @GotoPending=1
  END
  ELSE
  BEGIN /*IB Trx Check */
	IF(@BranchStatus<>1 OR @IsTrxAllow<>1 OR @IsDayOpen<>1 
	OR @HOBranchStatus<>1 OR @IsHODayOpen<>1 OR @IsHOTrxAllow<>1)	
		SET @GotoPending=1  
  END

---------End Others Control-------------------------  
       
        
         
IF  @OurBranchID <> @TrxBranchID  
 SET @ModuleID = '3030'  
ELSE  
 SET @ModuleID = '3020'  
  
---------------------------  
  
 BEGIN TRAN  

/*  
  SET @TrfSerialID = ISNULL((SELECT MAX(SerialID) FROM t_TrxTransfer  
       WHERE TrxBranchID= @TrxBranchID AND OperatorID = @OperatedBy AND Remarks = @mBankingID),0) + 1     
  
   Truncate table #suppresser  
  --DEBIT SIDE WITH TRX AMOUNT  
    
  SET @SlNo = 1  
  --THE FIRST ASSUMPTION IS THAT WE WILL ALWAYS BE DEBIT ACCOUNT IN TRXBRANCH. LOGIC CAN BE CHANGED LATER TO ALLOW INTERBRANCH  
  INSERT INTO #suppresser(SerialID)  
  EXEC p_AddEditTransferTrx  
   @TrxBranchID  = @TrxBranchID,  
   @OperatorID   = @OperatedBy,  
   @ModuleID   = @ModuleID,  
   @SerialID   = @TrfSerialID,  
   @SlNo    = @SlNo,  
   @OurBranchID  = @OurBranchID,  
   @AccountTypeID  = 'C',  
   @AccountID   = @AccountID,  
   @ProductID   = @ProductID,  
   @TrxTypeID   = 'TD',  
   @TrxDate   = @WorkingDate,  
   @ValueDate   = @WorkingDate,  
   @Amount    = @Amount,  
   @LocalAmount  = @Amount,  
   @TrxCurrencyID  = @CurrencyID,  
   @TrxAmount   = @Amount,  
   @ExchangeRate  = 1,  
   @MeanRate   = 1,  
   @Profit    = 0,  
   @InstrumentTypeID = 'V',  
   @ChequeID   = 0,  
   @ChequeDate   = NULL,  
   @ReferenceNo  = NULL,----@ReferenceNo,  
   @Remarks   = @mBankingID,---@Remarks,  
   @TrxDescriptionID = '007',  
   @TrxDescription  = @TrxDescription,  
   @MainGLID   = @MainGLID,  
   @TrxFlagID   = ''    
         
   --//////////////////////CREDITING///////////////////////////////////////////  
    --CREDIT THE INCOME GL OR WHATEVER ACCOUNT HAS BEEN SUPPLIED HERE    
   --Transaction Amount Posting    
  SET @SlNo = @SlNo + 1  
    
  INSERT INTO #suppresser(SerialID)  
  EXEC p_AddEditTransferTrx  
   @TrxBranchID  = @TrxBranchID,  
   @OperatorID   = @OperatedBy,  
   @ModuleID   = @ModuleID,  
   @SerialID   = @TrfSerialID,  
   @SlNo    = @SlNo,  
   @OurBranchID  = @TrxBranchID,  
   @AccountTypeID  = 'G',  
   @AccountID   = @CreditAccountID,  
   @ProductID   = 'GL',  
   @TrxTypeID   = 'TC',  
   @TrxDate   = @WorkingDate,  
   @ValueDate   = @WorkingDate,  
   @Amount    = @Amount,  
   @LocalAmount  = @Amount,  
   @TrxCurrencyID  = @CurrencyID,  
   @TrxAmount   = @Amount,  
   @ExchangeRate  = 1,  
   @MeanRate   = 1,  
   @Profit    = 0,  
   @InstrumentTypeID = 'V',  
   @ChequeID   = 0,  
   @ChequeDate   = NULL,  
   @ReferenceNo  = NULL,  
   @Remarks   = @mBankingID, --- @Remarks,  
   @TrxDescriptionID = '008',  
   @TrxDescription  = @TrxDescription,  
   @MainGLID   = @CreditAccountID,  
   @TrxFlagID   = ''   
     
   --FOR INTERBRANCH CASES,   
 IF  @OurBranchID <> @TrxBranchID  
 BEGIN  
  INSERT INTO #suppresser(BatchID)  
  EXEC p_AddTransferTrxIB   
    @TrxBranchID = @TrxBranchID, @OperatorID  = @OperatedBy,  
    @SerialID  = @TrfSerialID, @TrxPrinted  = 0, @GoToPending = @GoToPending 
      
  --SELECT @TrxBatchID = BatchID FROM #suppresser   
  --SET @SlNo = 0  
 END  
 ELSE  
 BEGIN     
  INSERT INTO #suppresser(SerialID,BatchID)  
  EXEC p_AddTransferTrx   
    @TrxBranchID = @TrxBranchID, @OperatorID  = @OperatedBy,  
    @SerialID  = @TrfSerialID, @ModuleID  = 3020,  
    @ImageID  = 0,            @TrxPrinted  = 0, @GoToPending = @GoToPending  
      
  --SELECT @SerialID = SerialID,@TrxBatchID = BatchID FROM #suppresser        
  --SELECT * FROM #suppresser        
 END  
    
  Truncate table #suppresser  
  
*/
------------------Charges Fees-------------  
   SET @TrfSerialID = ISNULL((SELECT MAX(SerialID) FROM t_TrxTransfer  
       WHERE TrxBranchID= @TrxBranchID AND OperatorID = @OperatedBy AND Remarks = @mBankingID),0) + 1     
   
  SET @SlNo = 1  
  SET @TrxDescription = @TrxDescription+' - Fees'  
    
  --THE FIRST ASSUMPTION IS THAT WE WILL ALWAYS BE DEBIT ACCOUNT IN TRXBRANCH. LOGIC CAN BE CHANGED LATER TO ALLOW INTERBRANCH  
  INSERT INTO #suppresser(SerialID)  
  EXEC p_AddEditTransferTrx  
   @TrxBranchID  = @TrxBranchID,  
   @OperatorID   = @OperatedBy,  
   @ModuleID   = @ModuleID,  
   @SerialID   = @TrfSerialID,  
   @SlNo    = @SlNo,  
   @OurBranchID  = @OurBranchID,  
   @AccountTypeID  = 'C',  
   @AccountID   = @AccountID,  
   @ProductID   = @ProductID,  
   @TrxTypeID   = 'TD',  
   @TrxDate   = @WorkingDate,  
   @ValueDate   = @WorkingDate,  
   @Amount    = @Fee,        
   @LocalAmount  = @Fee,
   @TrxCurrencyID  = @CurrencyID,
   @TrxAmount   = @Fee,     
   @ExchangeRate  = 1,  
   @MeanRate   = 1,  
   @Profit    = 0,  
   @InstrumentTypeID = 'V',  
   @ChequeID   = 0,  
   @ChequeDate   = NULL,  
   @ReferenceNo  = NULL,----@ReferenceNo,  
   @Remarks   = @mBankingID,---@Remarks,  
   @TrxDescriptionID = '007',  
   @TrxDescription  = @TrxDescription,  
   @MainGLID   = @MainGLID,  
   @TrxFlagID   = ''    
         
   --//////////////////////CREDITING///////////////////////////////////////////  
    --CREDIT THE INCOME GL OR WHATEVER ACCOUNT HAS BEEN SUPPLIED HERE    
   --Transaction Amount Posting    
  SET @SlNo = @SlNo + 1  
    
  INSERT INTO #suppresser(SerialID)  
  EXEC p_AddEditTransferTrx  
   @TrxBranchID  = @TrxBranchID,  
   @OperatorID   = @OperatedBy,  
   @ModuleID   = @ModuleID,  
   @SerialID   = @TrfSerialID,  
   @SlNo    = @SlNo,  
   @OurBranchID  = @TrxBranchID,  
   @AccountTypeID  = 'G',  
   @AccountID   = @CreditAccountID,  
   @ProductID   = 'GL',  
   @TrxTypeID   = 'TC',  
   @TrxDate   = @WorkingDate,  
   @ValueDate   = @WorkingDate,  
   @Amount    = @Fee,        
   @LocalAmount  = @Fee,
   @TrxCurrencyID  = @CurrencyID,
   @TrxAmount   = @Fee,  
   @ExchangeRate  = 1,  
   @MeanRate   = 1,  
   @Profit    = 0,  
   @InstrumentTypeID = 'V',  
   @ChequeID   = 0,  
   @ChequeDate   = NULL,  
   @ReferenceNo  = NULL,  
   @Remarks   = @mBankingID, --- @Remarks,  
   @TrxDescriptionID = '008',  
   @TrxDescription  = @TrxDescription,  
   @MainGLID   = @CreditAccountID,  
   @TrxFlagID   = ''   
     
   --FOR INTERBRANCH CASES,   
 IF  @OurBranchID <> @TrxBranchID  
 BEGIN  
  INSERT INTO #suppresser(BatchID)  
  EXEC p_AddTransferTrxIB   
    @TrxBranchID = @TrxBranchID, @OperatorID  = @OperatedBy,  
    @SerialID  = @TrfSerialID, @TrxPrinted  = 0, @GoToPending = @GoToPending   
      
  --SELECT @TrxBatchID = BatchID FROM #suppresser   
  --SET @SlNo = 0  
 END  
 ELSE  
 BEGIN     
  INSERT INTO #suppresser(SerialID,BatchID)  
  EXEC p_AddTransferTrx   
    @TrxBranchID = @TrxBranchID, @OperatorID  = @OperatedBy,  
    @SerialID  = @TrfSerialID, @ModuleID  = 3020,  
    @ImageID  = 0,            @TrxPrinted  = 0, @GoToPending = @GoToPending   
      
  --SELECT @SerialID = SerialID,@TrxBatchID = BatchID FROM #suppresser        
 END  
----------------End Charges Fees-----------------  
      
 ----SELECT @TrxBatchID--CAST(@SerialID AS VARCHAR)    
  
-----------------------------  
  IF @InquiryType = '1'--------Balance Enquiry------
	 SET @Result = @AvailableBalance 

  IF @InquiryType = '2'--------Mini Statement------
  BEGIN
	 SET @Result = ''

	 Declare m_Result Cursor For 
	 --Select top 5 convert(varchar,ISNULL(TrxDate,''),103)+'@'+LEFT(SerialID,20)+'@'+Cast(ABS(LocalAmount) As varchar(20))+'@'+LEFT(trxTypeID,2)+'|'
	 Select top 5 convert(varchar(32),convert(datetimeoffset(3),CreatedOn),127)+'@'+LEFT(SerialID,20)+'@'+Cast(ABS(LocalAmount) As varchar(20))+'@'+LEFT(trxTypeID,2)+'|'
	 From v_AccountTrx(nolock)
	 Where (OurBranchID = @OurBranchID AND AccountID = @AccountID AND ABS(LocalAmount) > 0)
	 Order By CreatedOn Desc

	 Open m_Result
	 FETCH NEXT From m_Result Into @MyTrxDescription
	 WHILE @@FETCH_STATUS = 0
	 BEGIN
	     PRINT @MyTrxDescription
		 SET @Result = @Result + @MyTrxDescription
		 FETCH NEXT From m_Result Into @MyTrxDescription /*FETCH NEXT From m_Result Into @Result*/
	 END
	 Close m_Result
	 DeAllocate m_Result
  END


  IF @InquiryType = '3'--------Loan Statement------
  BEGIN
	 SET @Result = ''

	 Declare m_Result Cursor For
	 Select top 5 convert(varchar,ISNULL(InstallmentDueDate,''),103)+' @ '+Cast(InstallmentAmount As varchar(20))+' || '
	 From t_LoanInstallment(nolock)
	 Where OurBranchID = @OurBranchID 
	 And AccountID In (Select AccountID from t_loan(nolock) Where RepaymentAccountID =  @AccountID)
	 And InstallmentDueDate <= @WorkingDate
	 Order By InstallmentDueDate Desc

	 Open m_Result
	 FETCH NEXT From m_Result Into @MyTrxDescription
	 WHILE @@FETCH_STATUS = 0
	 BEGIN
		
		SET @Result = @Result + @MyTrxDescription
		 FETCH NEXT From m_Result Into @MyTrxDescription /*FETCH NEXT From m_Result Into @Result*/
	 END
	 Close m_Result
	 DeAllocate m_Result
  END
  

 -- select @p2, @p3, @p33, @p34,dbo.f_GetAvailableBalance(@OurBranchID,@AccountID)              
 INSERT INTO #Response (FieldID, FieldValue) SELECT 39, '00'               
 SELECT FieldValue,'Success' as Message, @Result as Balance FROM #Response   
              
   If @@ERROR > 0     
   BEGIN    
    --SET @ErrorNo = 2    
    ROLLBACK TRAN    
   END    
   ELSE    
    COMMIT TRAN    
       
   RETURN    
       
ErrDisplay:          
   ROLLBACK TRAN    
            
 SET NOCOUNT OFF    
END   
---------------------_______________________________________________________________  
  

