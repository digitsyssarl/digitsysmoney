/****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP 1000 [Id]
      ,[Date]
      ,[TranType]
      ,[OperatorCode]
      ,[AffiliateCode]
      ,[ExternalRefNo]
      ,[RequestId]
      ,[AccountNo]
      ,[Amount]
      ,[Charge]
      ,[ResponseCode]
      ,[ResponseMessage]
      ,[TranRef]
      ,[Complete]
      ,[Details]
      ,[SerialID]
      ,[Role]
      ,[ConsultedBy]
      ,[ConsultedOn]
      ,[AuditedBy]
      ,[AuditedOn]
      ,[BranchID]
      ,[GSMOperationID]
FROM [UNICSUSSD].[dbo].[TransactionLog]

SELECT * FROM TransactionLog WHERE AccountNo='37250106989095'

SELECT OurBranchID   
FROM t_AccountCustomer(nolock)   
WHERE  AccountID = '37250106989095' 

SELECT dbo.f_GetBankID('01')         
SELECT dbo.f_GetHOBranchID('12') 

SELECT AccountID   
	From t_GLParameter(nolock)  
	Where OurBranchID =  '00' And GLParameterID = 'USSD_ORA_AC'  

SELECT * FROM v_GLBranchDetail 
WHERE OurBranchID = '00' AND AccountID = '4682400000'

p_AcToMoMoTransaction           
  @AccountID = '37250000119031',              
  @AMOUNT = '100',            
  @Fee = '0',                
  @TrxDescription = '691798202 Account To Wallet Transfer',  
  @GSMTransactionID = 'OR211126O6AAAA',    
  @GSMOperationID  = 'ORANGE'

p_AcToMoMoReversal
  @AccountID = '37250000119031',              
  @AMOUNT = -100,            
  @Fee = 0,                
  @TrxDescription = '691798202 Account to Mobile Transfer Reversal',  
  @GSMTransactionID = 'OR211126O6BBBB',    
  @GSMOperationID  = 'ORANGE'

INSERT INTO dbo.alerts(MOBILEnumber,messagedescription,PostedOn,PostedBy,Provider)
VALUES ('237691798202', 'Dear Customer, your transaction UNICCMCX37250000119031 has been successfully cancelled. Your Satisfaction. Our Passion.',GETDATE(),'Alain LOWE','MTN')
 
p_AcToMoMoReversal
  @AccountID = '37250106989095',              
  @AMOUNT = -30000,            
  @Fee = 0,                
  @TrxDescription = '694637033 Account to Mobile Transfer Reversal',  
  @GSMTransactionID = 'OR211126DABBBB',    
  @GSMOperationID  = 'ORANGE'


INSERT INTO dbo.alerts(MOBILEnumber,messagedescription,PostedOn,PostedBy,Provider)
VALUES ('237694637033', 'Dear Customer, your transaction UNICCMCX37250106989095 has been successfully cancelled. Your Satisfaction. Our Passion.',GETDATE(),'Alain LOWE','MTN')

p_AcToMoMoReversal
  @AccountID = '37250307977060',              
  @AMOUNT = -10000,            
  @Fee = 0,                
  @TrxDescription = '696249468 Account to Mobile Transfer Reversal',  
  @GSMTransactionID = 'OR211126060CCC',    
  @GSMOperationID  = 'ORANGE'

INSERT INTO dbo.alerts(MOBILEnumber,messagedescription,PostedOn,PostedBy,Provider)
VALUES ('237696249468', 'Dear Customer, your transaction UNICCMCX37250307977060 has been successfully cancelled. Your Satisfaction. Our Passion.',GETDATE(),'Alain LOWE','MTN')

p_AcToMoMoReversal
  @AccountID = '37250201636023',              
  @AMOUNT = -5000,            
  @Fee = 0,                
  @TrxDescription = '691698667 Account to Mobile Transfer Reversal',  
  @GSMTransactionID = 'OR2111268U6CCC',    
  @GSMOperationID  = 'ORANGE'

INSERT INTO dbo.alerts(MOBILEnumber,messagedescription,PostedOn,PostedBy,Provider)
VALUES ('237691698667', 'Dear Customer, your transaction UNICCMCX37250201636023 has been successfully cancelled. Your Satisfaction. Our Passion.',GETDATE(),'Alain LOWE','MTN')
