USE [BRNET_LIVE]
GO
/****** Object:  StoredProcedure [dbo].[p_AcToMoMoTransaction]    Script Date: 10/18/2021 12:40:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Insert Into t_SystemAcTag  
--Select 'USSD_MTN_AC','USSD MTN Account';  
  
--Insert Into t_SystemAccount  
--Select 'GLPARAM', 'USSD_MTN_AC', 0, 'A', NULL, 'L';  
-------------
--Insert Into t_SystemAcTag  
--Select 'USSD_ORA_AC','USSD Orange Account';  
  
--Insert Into t_SystemAccount  
--Select 'GLPARAM', 'USSD_ORA_AC', 0, 'A', NULL, 'L';  

----------------------------------------
--Insert Into t_SystemAcTag  
--Select 'USSD_ORAFEE_AC','USSD Orange Fee Account';  
  
--Insert Into t_SystemAccount  
--Select 'GLPARAM', 'USSD_ORAFEE_AC', 0, 'A', NULL, 'L';  

--Insert Into t_SystemAcTag  
--Select 'USSD_MTNFEE_AC','USSD MTN Fee Account';  
  
--Insert Into t_SystemAccount  
--Select 'GLPARAM', 'USSD_MTNFEE_AC', 0, 'A', NULL, 'L';  
-------------

--Insert Into t_SystemAcTag  
--Select 'USSD_ORA_AC','USSD Orange Account';  
  
--Insert Into t_SystemAccount  
--Select 'GLPARAM', 'USSD_ORA_AC', 0, 'A', NULL, 'L';  
-------------------------

----Select * From t_SystemAccount
----Where Module = 'GLPARAM' And AccountTagID = 'USSD_MTN_AC';

----SELECT *  
----From t_GLParameter(nolock)  
----Where GLParameterID = 'USSD_MTN_AC'  
   
----SELECT *
----From t_GLParameter(nolock)  
----Where OurBranchID =  @TrxBranchID And GLParameterID = 'USSD_MTN_AC'  


ALTER proc [dbo].[p_AcToMoMoTransaction]            
(  
  @AccountID  VARCHAR(25),              
  @AMOUNT Amount,            
  @Fee Amount,                
  @TrxDescription Varchar(max),  
  @GSMTransactionID Varchar(30),    
  @GSMOperationID  Varchar(30)
 )
AS            
BEGIN     
              
DECLARE @CurrencyID      Varchar(20),    
     @TrfSerialID   INT,      
     @SlNo     INT,  
     @WorkingDate  SmallDateTime,  
     @CreditAccountID Varchar(50),  
     @CreditFeeAccountID Varchar(50),  
     @TrxBatchID    Varchar(20),  
     @SerialID   Varchar(20),  
     @HOBranchID   Varchar(20),  
     @BankID    Varchar(20),  
     @ModuleID   Varchar(20),  
     @AvailableBalance Money,  
     @MainGLID   Varchar(20),  
     @TrxDate   SmallDateTime,  
     @ProductID   Varchar(10),  
     @OurBranchID  Varchar(10),   
     @DirectExchangeOurBranchID  Varchar(10),   
     @DirectExchangeCreditAccountID Varchar(50),  
     @DirectExchangeProductID Varchar(50),  
     @DirectExchangeMainGLID Varchar(50),  
     @DirectExchangeFee Amount,                
     @TotalFee Amount,                
     @CreatedBy   Varchar(50),   
     @OperatedBy   Varchar(50),  
     @AccountStatusID Varchar(50),  
     @AccountClassID  Varchar(50),  
     @BranchStatus  bit,  
     @HOBranchStatus  bit,    
     @IsDayOpen   bit,  
     @IsTrxAllow   bit,  
     @IsHODayOpen  bit,  
     @IsHOTrxAllow  bit,  
     @HOSODDate   SmallDateTime,  
     @OurBranchSODDate SmallDateTime,  
      @GLClassID   nVarChar(10),  
      @GLAccountStatusID Char(1),  
      @GLCurrencyID  nVarChar(10),  
      @GLCategoryID  nVarChar(10),  
      @TrxBranchID  VARCHAR(25),
      @GotoPending  Bit = 0,
      @TrxQuotaD INT,
      @TrxQuotaW INT,
      @TrxQuotaM INT,
      @TrxQuotaDays AS INT,
      @TrxQuotaWeeks AS INT,
      @TrxQuotaMonths AS INT,
      @TrxStartD AS DateTime,
      @TrxStartW AS DateTime,
      @TrxStartM AS DateTime,
      @TrxAmountD AS REAL,
      @TrxAmountW AS REAL, 
      @TrxAmountM AS REAL      
  
        
  
  
Create table #suppresser  
(  
 SerialID INT,  
 BatchID  BIGINT  
)  
       
 SET @BranchStatus = 0
 SET @IsDayOpen    = 0
 SET @IsTrxAllow   = 0
 SET @GotoPending  = 0

    SET @CreatedBy = @GSMOperationID
        
SELECT @ProductID   = ProductID,  
    @OurBranchID   = OurBranchID,  
    @AccountStatusID  = AccountStatusID,  
    @AccountClassID  = AccountClassID,  
    @AvailableBalance = dbo.f_GetAvailableBalance(OurBranchID,AccountID)   
FROM t_AccountCustomer(nolock)   
WHERE  AccountID = @AccountID   
  
SELECT @CurrencyID = CurrencyID From t_Product(nolock) Where PRODUCTID = @ProductID        
SELECT @WorkingDate = dbo.f_GetWorkingDate(@OurBranchID)   
SELECT @BankID  = dbo.f_GetBankID(@OurBranchID)         
SELECT @HOBranchID = dbo.f_GetHOBranchID(@BankID)  
SELECT @TrxBranchID = @HOBranchID   
SELECT @TrxDate  = @WorkingDate      
SELECT @OperatedBy  = @CreatedBy      
 
    --ITS EAZY TO FIX MISMATCH FROM HERE
    IF dbo.f_GetAcClassID(@OurBranchID, @AccountID) <> 'TRANSIT' OR @AccountID Not In ('37120102033044')
        Exec p_UpdateAccountMismatch @OurBranchID=@OurBranchID,@OperatorID=@OperatedBy,@FromDate='02 Jan 2016',@AccountID=@AccountID

    ---------------Direct Exchange part of commission on each transaction---------
    ----select  
    ----     @DirectExchangeOurBranchID     = OurBranchID,   
    ----     @DirectExchangeCreditAccountID = AccountID,  
    ----     @DirectExchangeProductID       = ProductID,  
    ----     @DirectExchangeMainGLID        = dbo.f_GetGLInterfaceAccountID('12', ProductID, dbo.f_GetProductTypeID('12', ProductID), 'CONTROL_AC'),  
    ----     @DirectExchangeFee = 100              
    ---- from t_AccountCustomer(nolock) where accountid = '37140310273341'

    SET @DirectExchangeFee = 0
    ---------------Direct Exchange part of commission on each transaction---------

 SELECT @BranchStatus=BranchStatus,@IsDayOpen=IsDayOpen,@IsTrxAllow=IsTrxAllow,@OurBranchSODDate = SODDate  
 FROM t_SystemBranchStatus(nolock) WHERE OurBranchID = @OurBranchID   
  
 SELECT @HOBranchStatus=BranchStatus,@IsHODayOpen=IsDayOpen,@IsHOTrxAllow=IsTrxAllow,@HOSODDate= SODDate  
 FROM t_SystemBranchStatus(nolock) WHERE OurBranchID = @HOBranchID   
                
SELECT @MainGLID = AccountID   
FROM t_GLInterface(nolock)   
WHERE RelevantID = @ProductID AND AccountTagID = 'CONTROL_AC'                  
                    
IF @GSMOperationID = 'MTN'
BEGIN
    SELECT @CreditAccountID = AccountID   
    From t_GLParameter(nolock)  
    Where OurBranchID =  @TrxBranchID And GLParameterID = 'USSD_MTN_AC' 

    SELECT @CreditFeeAccountID = AccountID   
    From t_GLParameter(nolock)  
    Where OurBranchID =  @TrxBranchID And GLParameterID = 'USSD_MTNFEE_AC' 
END
ELSE IF @GSMOperationID = 'ORANGE'
BEGIN
    SELECT @CreditAccountID = AccountID   
    From t_GLParameter(nolock)  
    Where OurBranchID =  @TrxBranchID And GLParameterID = 'USSD_ORA_AC'  


    SELECT @CreditFeeAccountID = AccountID   
    From t_GLParameter(nolock)  
    Where OurBranchID =  @TrxBranchID And GLParameterID = 'USSD_ORAFEE_AC' 
END

 CREATE TABLE #Response (                            
  FieldID  numeric,                            
  FieldValue varchar(4000)                            
 )                          
      
IF (ISNULL(@AMOUNT,0) + ISNULL(@Fee,0) + ISNULL(@DirectExchangeFee,0)) > @AvailableBalance            
BEGIN            
INSERT INTO #Response (FieldID, FieldValue) SELECT 39, '116'               
SELECT FieldValue,'Insufficient Balance' FROM #Response            
RETURN            
END            

--------------- Transaction Quota per Day, Week, and Month  

 SELECT @TrxQuotaD = QuotaTrxDay,
        @TrxQuotaW = QuotaTrxWeek, 
        @TrxQuotaM = QuotaTrxMonth, 
        @TrxStartD = DateTrxDay,
        @TrxStartW = DateTrxWeek,
        @TrxStartM = DateTrxMonth,
        @TrxAmountD = AmountTrxDay,
        @TrxAmountW = AmountTrxWeek, 
        @TrxAmountM = AmountTrxMonth
FROM t_TrxQuota WITH (UPDLOCK,SERIALIZABLE)
WHERE (AccountID=@AccountID);

IF EXISTS(SELECT TOP 1 * 
          FROM  t_TrxQuota WITH (UPDLOCK,SERIALIZABLE)
          WHERE (AccountID = @AccountID))
 BEGIN
   
   SET @TrxQuotaDays = (SELECT Td.Days FROM t_DateElapsed(@TrxStartD,GetDate()) AS Td);
   SET @TrxQuotaWeeks = (SELECT Tw.Hours FROM t_DateElapsed(@TrxStartW,GetDate()) AS Tw);
   SET @TrxQuotaMonths = (SELECT Tm.Months FROM t_DateElapsed(@TrxStartM,GetDate()) AS Tm);

   IF @TrxQuotaDays < 1
      BEGIN
          IF @TrxQuotaD < 5 AND @TrxAmountD < 2000000
            BEGIN
                UPDATE t_TrxQuota 
                SET QuotaTrxDay = QuotaTrxDay + 1, QuotaTrxWeek = QuotaTrxWeek + 1, QuotaTrxMonth = QuotaTrxMonth + 1,
                    AmountTrxDay = AmountTrxDay + @AMOUNT, AmountTrxWeek = AmountTrxWeek + @AMOUNT, AmountTrxMonth = AmountTrxMonth + @AMOUNT 
                WHERE AccountID = @AccountID;
            END
          ELSE
            BEGIN
                --RAISERROR(N'BREXDB300021',16,1) 
                INSERT INTO #Response (FieldID, FieldValue) SELECT 39, '114'               
                SELECT FieldValue,'The number of transactions is over the daily limit' FROM #Response;
                RETURN
            END
         END     
   ELSE
    BEGIN
      IF @TrxQuotaDays = 1
        BEGIN
            SET @CurrentDate = CAST(CAST(GETDATE() AS Date) As DateTime);
            UPDATE t_TrxQuota 
            SET QuotaTrxDay = 1, DateTrxDay = @CurrentDate, AmountTrxDay = @AMOUNT 
            WHERE AccountID = @AccountID;
        END

      IF @TrxQuotaWeeks < 1
        BEGIN
              IF @TrxQuotaW < 15 AND @TrxAmountW < 5000000
                BEGIN
                  UPDATE t_TrxQuota 
                  SET QuotaTrxWeek = QuotaTrxWeek + 1, QuotaTrxMonth = QuotaTrxMonth + 1,
                      AmountTrxWeek = AmountTrxWeek + @AMOUNT, AmountTrxMonth = AmountTrxMonth + @AMOUNT 
                  WHERE AccountID = @AccountID;
                END
              ELSE
                BEGIN
                    --RAISERROR(N'BREXDB300021',16,1) 
                    INSERT INTO #Response (FieldID, FieldValue) SELECT 39, '114'               
                    SELECT FieldValue,'The number of transactions is over the weekly limit' FROM #Response;
                    RETURN
                 END
            END      
          ELSE
            BEGIN
              IF @TrxQuotaWeeks = 1
                BEGIN
                  SET @CurrentDate = CAST(CAST(GETDATE() AS Date) As DateTime);
                  UPDATE t_TrxQuota 
                  SET QuotaTrxWeek = 1, DateTrxWeek = @CurrentDate, AmountTrxWeek = @AMOUNT 
                  WHERE AccountID = @AccountID;
                END

              IF @TrxQuotaMonths < 1
                BEGIN
                      IF @TrxQuotaM < 30 AND @TrxAmountM < 10000000
                        BEGIN
                          UPDATE t_TrxQuota 
                          SET QuotaTrxMonth = QuotaTrxMonth + 1, AmountTrxMonth = AmountTrxMonth + @AMOUNT 
                          WHERE AccountID = @AccountID;
                        END
                      ELSE
                        BEGIN
                            --RAISERROR(N'BREXDB300021',16,1) 
                            INSERT INTO #Response (FieldID, FieldValue) SELECT 39, '114'               
                            SELECT FieldValue,'The number of transactions is over the monthly limit' FROM #Response;
                            RETURN
                        END
                    END      
                ELSE
                  BEGIN
                      IF @TrxQuotaMonths = 1
                        BEGIN
                          SET @CurrentDate = CAST(CAST(GETDATE() AS Date) As DateTime);
                          UPDATE t_TrxQuota 
                          SET QuotaTrxMonth = 1, DateTrxMonth = @CurrentDate, AmountTrxMonth = @AMOUNT 
                          WHERE AccountID = @AccountID;
                        END
                  END
              END
        END   
 END
ELSE
 BEGIN
    SET @CurrentDate = CAST(CAST(GETDATE() AS Date) As DateTime);
    INSERT INTO t_TrxQuota (AccountID, QuotaTrxDay, QuotaTrxWeek, QuotaTrxMonth, DateTrxDay, DateTrxWeek, DateTrxMonth, AmountTrxDay, AmountTrxWeek, AmountTrxMonth)
    VALUES (@AccountID, 1, 1, 1, @CurrentDate, @CurrentDate, @CurrentDate, @AMOUNT, @AMOUNT, @AMOUNT);
 END 

 ---------------------------------   
------------------Others Control---------------------  
   
 --Account Not Exists    
 IF @AccountStatusID IS NULL    
 BEGIN    
  --RAISERROR(N'BREXDB300022',16,1)    
 INSERT INTO #Response (FieldID, FieldValue) SELECT 39, '114'               
 SELECT FieldValue,'Account Not Exists' FROM #Response    
 RETURN   
 END    
     
 --- Closed Accounts , Transaction not Allowed    
 IF @AccountStatusID = 'AC' 
 BEGIN    
  --RAISERROR(N'BREXDB300011',16,1)    
 INSERT INTO #Response (FieldID, FieldValue) SELECT 39, '114'               
 SELECT FieldValue,'Closed Accounts,Transaction not Allowed' FROM #Response            
 RETURN    
 END    
 --- Blocked Account, Transaction Not Allowed     
 IF @AccountStatusID = 'AB'    
 BEGIN    
  --RAISERROR(N'BREXDB300013',16,1)    
 INSERT INTO #Response (FieldID, FieldValue) SELECT 39, '114'               
 SELECT FieldValue,'Blocked Account,Transaction Not Allowed' FROM #Response            
 RETURN    
 END    
  
--- debit is not allowed for Dormant accounts   
IF @AccountStatusID = 'AD'  
BEGIN  
 --RAISERROR(N'BREXDB300021',16,1)  
 INSERT INTO #Response (FieldID, FieldValue) SELECT 39, '114'               
 SELECT FieldValue,'Debit is not allowed for Dormant Accounts' FROM #Response            
 RETURN  
END  
--Do not allow any debit transactions --SELECT * FROM T_SPECIALCONDITIONDETAIL WHERE SPECIALCONDITIONID='902'  
--IF (select dbo.f_GetConsAccountSpecialConditions (@OurBranchID,@AccountID,@AccountClassID,902))=1  
IF EXISTS(SELECT 1 FROM T_SPECIALCONDITIONDETAIL WHERE SPECIALCONDITIONID='902' AND ClassID = @AccountClassID)  
OR (select dbo.f_GetConsAccountSpecialConditions (@OurBranchID,@AccountID,@AccountClassID,902))=1  
BEGIN  
 --RAISERROR(N'BREXDB300004',16,1)  
 INSERT INTO #Response (FieldID, FieldValue) SELECT 39, '114'               
 SELECT FieldValue,'Debit transactions not allowed-Special Condition' FROM #Response            
 RETURN  
END  
  
--All debit transactions needs supervision --SELECT * FROM T_SPECIALCONDITIONDETAIL WHERE SPECIALCONDITIONID='906'  
--IF (select dbo.f_GetConsAccountSpecialConditions (@OurBranchID,@AccountID,@AccountClassID,906))=1  
IF EXISTS(SELECT 1 FROM T_SPECIALCONDITIONDETAIL WHERE SPECIALCONDITIONID='906' AND ClassID = @AccountClassID)  
OR (select dbo.f_GetConsAccountSpecialConditions (@OurBranchID,@AccountID,@AccountClassID,906))=1  
BEGIN  
 --RAISERROR(N'BREXDB300004',16,1)  
 INSERT INTO #Response (FieldID, FieldValue) SELECT 39, '114'               
 SELECT FieldValue,'Debit Transactions Need Supervision-Special Condition' FROM #Response            
 RETURN  
END  
  
-------Do Not Allow Debit Trx For Line Of Credit Without Withdrawn Opportunity Amount  
 IF EXISTS (SELECT TODTypeID  
 FROM t_LimitTOD WHERE OurBranchID = @OurBranchID   
   And AccountID = @AccountID And TODTypeID = 'LOC' AND PurposeCodeID = 'D' And ExpiryDate >= dbo.f_GetWorkingDate(@OurBranchID)  
   And ISNULL(dbo.f_GetAccountSpecialConditionValue(OurBranchID,308,AccountID,'V'), 0) = 0)  
BEGIN  
 --RAISERROR(N'BREXDB300444',16,1)  
 INSERT INTO #Response (FieldID, FieldValue) SELECT 39, '114'               
 SELECT FieldValue,'Line Of Credit' FROM #Response            
 RETURN  
END  
  
  
---------GL Account Control-----------------  
  
SELECT @GLClassID = GLClassID,  
 @GLAccountStatusID = GLAccountStatusID,  
 @GLCurrencyID = CurrencyID,  
 @GLCategoryID= GLCategoryID  
FROM v_GLBranchDetail  
WHERE OurBranchID = @HOBranchID  
 AND AccountID = @CreditAccountID  
  
   
--Account Not Exists  
IF @GLAccountStatusID IS NULL  
BEGIN  
 --RAISERROR(N'BREXDB300022',16,1)  
 INSERT INTO #Response (FieldID, FieldValue) SELECT 39, '114'               
 SELECT FieldValue,'GL Account Not Exists' FROM #Response            
 RETURN  
END  
  
--Selected Currency And Account Currency Are Not Same  
IF @GLCurrencyID <> ISNULL(@CurrencyID,@GLCurrencyID)  
BEGIN  
 --RAISERROR(N'BREXDB300023',16,1)  
 INSERT INTO #Response (FieldID, FieldValue) SELECT 39, '114'               
 SELECT FieldValue,'Selected Currency And Account Currency Are Not Same' FROM #Response            
 RETURN  
END  
  
--- Closed Accounts , Transaction not Allowed  
IF @GLAccountStatusID = 'C' 
BEGIN  
 --RAISERROR(N'BREXDB300011',16,1)  
 INSERT INTO #Response (FieldID, FieldValue) SELECT 39, '114'           
 SELECT FieldValue,'Closed Accounts,Transaction not Allowed' FROM #Response            
 RETURN  
END  
--- Blocked Account, Transaction Not Allowed   
IF @GLAccountStatusID = 'B'  
BEGIN  
 --RAISERROR(N'BREXDB300013',16,1)  
 INSERT INTO #Response (FieldID, FieldValue) SELECT 39, '114'               
 SELECT FieldValue,'Blocked Account,Transaction Not Allowed' FROM #Response            
 RETURN  
END  
  
--These Category GL won't allow transaction  
IF @GLCategoryID IN ('MAIN','CONT','OFFB', 'CTRA')  
BEGIN  
 --RAISERROR(N'BREXDB300015',16,1)  
 INSERT INTO #Response (FieldID, FieldValue) SELECT 39, '114'               
 SELECT FieldValue,'Category GL wont allow transaction' FROM #Response            
 RETURN  
END  
  
IF @GLClassID <> ''  
BEGIN  
 --Do not allow any debit transactions  
 IF EXISTS(SELECT BankID FROM t_SpecialConditionDetail  
   WHERE BankID = @BankID  
AND ClassID =@GLClassID  
    AND SpecialConditionID = 902)  
 BEGIN  
  --RAISERROR(N'BREXDB300004',16,1)  
  INSERT INTO #Response (FieldID, FieldValue) SELECT 39, '114'               
  SELECT FieldValue,'Do not allow any debit transactions' FROM #Response            
  RETURN  
 END  
END  
------End GL Account Control----------------  
  
--------------Branch Not At the Same Date----  
IF (@HOSODDate<>@OurBranchSODDate)  
BEGIN  
 INSERT INTO #Response (FieldID, FieldValue) SELECT 39, '91'               
 SELECT FieldValue,'Branch Not At the Same Date' FROM #Response            
 RETURN  
END  
  
------------- Day Not Open ----------------  
--IF (@HOBranchStatus<>1 OR @BranchStatus<>1   
--OR @IsHOTrxAllow<>1 OR @IsTrxAllow<>1   
--OR @IsHODayOpen<>1 OR @IsDayOpen<>1)  
--BEGIN  
-- INSERT INTO #Response (FieldID, FieldValue) SELECT 39, '91'               
-- SELECT FieldValue,'Day Not Open' FROM #Response            
-- RETURN  
--END  

 IF(@OurBranchID <> @HOBranchID) /* Avoid Pending Interbranch when SOD dates are different */
 BEGIN
 IF(@HOSODDate<>@OurBranchSODDate)
    BEGIN
         INSERT INTO #Response (FieldID, FieldValue) SELECT 39, '91'               
         SELECT FieldValue,'Branch Not At the Same Date' FROM #Response            
         RETURN  
    END 
    
    /* Avoid Interbranch day_open_ok/txn_allowed_ok/branch_status_ok but SOD dates not same */
    IF(@HOBranchStatus=1 AND @BranchStatus=1 
    AND @IsHOTrxAllow=1 AND @IsTrxAllow=1 
    AND @IsHODayOpen=1 AND @IsDayOpen=1 
    AND @HOSODDate<>@OurBranchSODDate)
    BEGIN
         INSERT INTO #Response (FieldID, FieldValue) SELECT 39, '91'               
         SELECT FieldValue,'Branch Not At the Same Date' FROM #Response            
         RETURN  
    END 
 END 
 
  IF (@OurBranchID=@HOBranchID) /*Local Trx Check */
  BEGIN
    IF(@BranchStatus<>1 OR @IsTrxAllow<>1 OR @IsDayOpen<>1)
        SET @GotoPending=1
  END
  ELSE
  BEGIN /*IB Trx Check */
    IF(@BranchStatus<>1 OR @IsTrxAllow<>1 OR @IsDayOpen<>1 
    OR @HOBranchStatus<>1 OR @IsHODayOpen<>1 OR @IsHOTrxAllow<>1)   
        SET @GotoPending=1  
  END

---------End Others Control-------------------------  
  
----Last Control-------      
IF  (ISNULL(@AMOUNT,0) + ISNULL(@Fee,0) + ISNULL(@DirectExchangeFee,0)) > dbo.f_GetAvailableBalance(@OurBranchID,@AccountID)             
BEGIN            
INSERT INTO #Response (FieldID, FieldValue) SELECT 39, '116'               
SELECT FieldValue,'Insufficient Balance' FROM #Response            
RETURN            
END       
         
         
IF  @OurBranchID <> @TrxBranchID  
 SET @ModuleID = '3030'  
ELSE  
 SET @ModuleID = '3020'  
  
---------------------------  
  
 BEGIN TRAN    
  SET @TrfSerialID = ISNULL((SELECT MAX(SerialID) FROM t_TrxTransfer  
       WHERE TrxBranchID= @TrxBranchID AND OperatorID = @OperatedBy AND Remarks = @GSMTransactionID),0) + 1     
  
   Truncate table #suppresser  
  --DEBIT SIDE WITH TRX AMOUNT  
    
  SET @SlNo = 1  
  --THE FIRST ASSUMPTION IS THAT WE WILL ALWAYS BE DEBIT ACCOUNT IN TRXBRANCH. LOGIC CAN BE CHANGED LATER TO ALLOW INTERBRANCH  
  INSERT INTO #suppresser(SerialID)  
  EXEC p_AddEditTransferTrx  
   @TrxBranchID  = @TrxBranchID,  
   @OperatorID   = @OperatedBy,  
   @ModuleID   = @ModuleID,  
   @SerialID   = @TrfSerialID,  
   @SlNo    = @SlNo,  
   @OurBranchID  = @OurBranchID,  
   @AccountTypeID  = 'C',  
   @AccountID   = @AccountID,  
   @ProductID   = @ProductID,  
   @TrxTypeID   = 'TD',  
   @TrxDate   = @WorkingDate,  
   @ValueDate   = @WorkingDate,  
   @Amount    = @Amount,  
   @LocalAmount  = @Amount,  
   @TrxCurrencyID  = @CurrencyID,  
   @TrxAmount   = @Amount,  
   @ExchangeRate  = 1,  
   @MeanRate   = 1,  
   @Profit    = 0,  
   @InstrumentTypeID = 'V',  
   @ChequeID   = 0,  
   @ChequeDate   = NULL,  
   @ReferenceNo  = NULL,----@ReferenceNo,  
   @Remarks   = @GSMTransactionID,---@Remarks,  
   @TrxDescriptionID = '007',  
   @TrxDescription  = @TrxDescription,  
   @MainGLID   = @MainGLID,  
   @TrxFlagID   = ''    
         
   --//////////////////////CREDITING///////////////////////////////////////////  
    --CREDIT THE INCOME GL OR WHATEVER ACCOUNT HAS BEEN SUPPLIED HERE    
   --Transaction Amount Posting    
  SET @SlNo = @SlNo + 1  
    
  INSERT INTO #suppresser(SerialID)  
  EXEC p_AddEditTransferTrx  
   @TrxBranchID  = @TrxBranchID,  
   @OperatorID   = @OperatedBy,  
   @ModuleID   = @ModuleID,  
   @SerialID   = @TrfSerialID,  
   @SlNo    = @SlNo,  
   @OurBranchID  = @TrxBranchID,  
   @AccountTypeID  = 'G',  
   @AccountID   = @CreditAccountID,  
   @ProductID   = 'GL',  
   @TrxTypeID   = 'TC',  
   @TrxDate   = @WorkingDate,  
   @ValueDate   = @WorkingDate,  
   @Amount    = @Amount,  
   @LocalAmount  = @Amount,  
   @TrxCurrencyID  = @CurrencyID,  
   @TrxAmount   = @Amount,  
   @ExchangeRate  = 1,  
   @MeanRate   = 1,  
   @Profit    = 0,  
   @InstrumentTypeID = 'V',  
   @ChequeID   = 0,  
   @ChequeDate   = NULL,  
   @ReferenceNo  = NULL,  
   @Remarks   = @GSMTransactionID, --- @Remarks,  
   @TrxDescriptionID = '008',  
   @TrxDescription  = @TrxDescription,  
   @MainGLID   = @CreditAccountID,  
   @TrxFlagID   = ''   
     
   --FOR INTERBRANCH CASES,   
 IF  @OurBranchID <> @TrxBranchID  
 BEGIN  
  INSERT INTO #suppresser(BatchID)  
  EXEC p_AddTransferTrxIB   
    @TrxBranchID = @TrxBranchID, @OperatorID  = @OperatedBy,  
    @SerialID  = @TrfSerialID, @TrxPrinted  = 0, @GoToPending = @GoToPending 
 
  SELECT @TrxBatchID = BatchID FROM #suppresser   
  --SET @SlNo = 0  
 END  
 ELSE  
 BEGIN     
  INSERT INTO #suppresser(SerialID,BatchID)  
  EXEC p_AddTransferTrx   
    @TrxBranchID = @TrxBranchID, @OperatorID  = @OperatedBy,  
    @SerialID  = @TrfSerialID, @ModuleID  = 3020,  
    @ImageID  = 0,            @TrxPrinted  = 0, @GoToPending = @GoToPending  
      
  SELECT @SerialID = SerialID,@TrxBatchID = BatchID FROM #suppresser        
  --SELECT * FROM #suppresser        
 END  
    
  Truncate table #suppresser  
  
------------------Charges Fees-------------  
   SET @TrfSerialID = ISNULL((SELECT MAX(SerialID) FROM t_TrxTransfer  
       WHERE TrxBranchID= @TrxBranchID AND OperatorID = @OperatedBy AND Remarks = @GSMTransactionID),0) + 1     
   
  SET @SlNo = 1  
  SET @TrxDescription = @TrxDescription+' - Fees'  
    
  SET @TotalFee = ISNULL(@Fee, 0) + ISNULL(@DirectExchangeFee, 0)

  --THE FIRST ASSUMPTION IS THAT WE WILL ALWAYS BE DEBIT ACCOUNT IN TRXBRANCH. LOGIC CAN BE CHANGED LATER TO ALLOW INTERBRANCH  
  INSERT INTO #suppresser(SerialID)  
  EXEC p_AddEditTransferTrx  
   @TrxBranchID  = @TrxBranchID,  
   @OperatorID   = @OperatedBy,  
   @ModuleID   = @ModuleID,  
   @SerialID   = @TrfSerialID,  
   @SlNo    = @SlNo,  
   @OurBranchID  = @OurBranchID,  
   @AccountTypeID  = 'C',  
   @AccountID   = @AccountID,  
   @ProductID   = @ProductID,  
   @TrxTypeID   = 'TD',  
   @TrxDate   = @WorkingDate,  
   @ValueDate   = @WorkingDate,  
   @Amount    = @TotalFee,  
   @LocalAmount  = @TotalFee,  
   @TrxCurrencyID  = @CurrencyID,  
   @TrxAmount   = @TotalFee,  
   @ExchangeRate  = 1,  
   @MeanRate   = 1,  
   @Profit    = 0,  
   @InstrumentTypeID = 'V',  
   @ChequeID   = 0,  
   @ChequeDate   = NULL,  
   @ReferenceNo  = NULL,----@ReferenceNo,  
   @Remarks   = @GSMTransactionID,---@Remarks,  
   @TrxDescriptionID = '007',  
   @TrxDescription  = @TrxDescription,  
   @MainGLID   = @MainGLID,  
   @TrxFlagID   = ''    
         
   --//////////////////////CREDITING///////////////////////////////////////////  
    --CREDIT THE INCOME GL OR WHATEVER ACCOUNT HAS BEEN SUPPLIED HERE    
   --Transaction Amount Posting    
  SET @SlNo = @SlNo + 1  
    
  INSERT INTO #suppresser(SerialID)  
  EXEC p_AddEditTransferTrx  
   @TrxBranchID  = @TrxBranchID,  
   @OperatorID   = @OperatedBy,  
@ModuleID   = @ModuleID,  
   @SerialID   = @TrfSerialID,  
   @SlNo    = @SlNo,  
   @OurBranchID  = @OurBranchID,  
   @AccountTypeID  = 'G',  
   @AccountID   = @CreditFeeAccountID,  
   @ProductID   = 'GL',  
   @TrxTypeID   = 'TC',  
   @TrxDate   = @WorkingDate,  
   @ValueDate   = @WorkingDate,  
   @Amount    = @Fee,  
   @LocalAmount  = @Fee,  
   @TrxCurrencyID  = @CurrencyID,  
   @TrxAmount   = @Fee,  
   @ExchangeRate  = 1,  
   @MeanRate   = 1,  
   @Profit    = 0,  
   @InstrumentTypeID = 'V',  
   @ChequeID   = 0,  
   @ChequeDate   = NULL,  
   @ReferenceNo  = NULL,  
   @Remarks   = @GSMTransactionID, --- @Remarks,  
   @TrxDescriptionID = '008',  
   @TrxDescription  = @TrxDescription,  
   @MainGLID   = @CreditFeeAccountID,  
   @TrxFlagID   = ''   
 
         
/*---------We comment This part for now----Fabrice 29 Apr 2019---
   --//////////////////////CREDITING///////////////////////////////////////////  
    --CREDIT THE DIRECT EXCHANGE ACCOUNT HAS BEEN SUPPLIED HERE WITH THE PART OF COMMISSION LINK TO THAT   
   --Transaction Amount Posting    
  SET @SlNo = @SlNo + 1      
   
  INSERT INTO #suppresser(SerialID)  
  EXEC p_AddEditTransferTrx  
   @TrxBranchID  = @TrxBranchID,  
   @OperatorID   = @OperatedBy,  
   @ModuleID   = @ModuleID,  
   @SerialID   = @TrfSerialID,  
   @SlNo    = @SlNo,  
   @OurBranchID  = @DirectExchangeOurBranchID,  
   @AccountTypeID  = 'C',  
   @AccountID   = @DirectExchangeCreditAccountID,  
   @ProductID   = @DirectExchangeProductID,  
   @TrxTypeID   = 'TC',  
   @TrxDate   = @WorkingDate,  
   @ValueDate   = @WorkingDate,  
   @Amount    = @DirectExchangeFee,  
   @LocalAmount  = @DirectExchangeFee,  
   @TrxCurrencyID  = @CurrencyID,  
   @TrxAmount   = @DirectExchangeFee,  
   @ExchangeRate  = 1,  
   @MeanRate   = 1,  
   @Profit    = 0,  
   @InstrumentTypeID = 'V',  
   @ChequeID   = 0,  
   @ChequeDate   = NULL,  
   @ReferenceNo  = NULL,  
   @Remarks   = @GSMTransactionID, --- @Remarks,  
   @TrxDescriptionID = '008',  
   @TrxDescription  = @TrxDescription,  
   @MainGLID   = @DirectExchangeMainGLID,  
   @TrxFlagID   = ''   
---------We comment This part for now----Fabrice 29 Apr 2019---*/
    
    
   --FOR INTERBRANCH CASES,   
 IF  (@OurBranchID <> @TrxBranchID  /*---OR @TrxBranchID <> @DirectExchangeOurBranchID---*/ )
 BEGIN  
  INSERT INTO #suppresser(BatchID)  
  EXEC p_AddTransferTrxIB   
    @TrxBranchID = @TrxBranchID, @OperatorID  = @OperatedBy,  
    @SerialID  = @TrfSerialID, @TrxPrinted  = 0, @GoToPending = @GoToPending   
      
  SELECT @TrxBatchID = BatchID FROM #suppresser   
  --SET @SlNo = 0  
 END  
 ELSE  
 BEGIN     
  INSERT INTO #suppresser(SerialID,BatchID)  
  EXEC p_AddTransferTrx   
    @TrxBranchID = @TrxBranchID, @OperatorID  = @OperatedBy,  
    @SerialID  = @TrfSerialID, @ModuleID  = 3020,  
    @ImageID  = 0,            @TrxPrinted  = 0, @GoToPending = @GoToPending   
      
  SELECT @SerialID = SerialID,@TrxBatchID = BatchID FROM #suppresser        
 END  
----------------End Charges Fees-----------------  
      
 ----SELECT @TrxBatchID--CAST(@SerialID AS VARCHAR)    
  
-----------------------------  
  
 -- select @p2, @p3, @p33, @p34,dbo.f_GetAvailableBalance(@OurBranchID,@AccountID)   
 
  ----------Comment By Fabrice to show now the BatchID--------         
 ----INSERT INTO #Response (FieldID, FieldValue) SELECT 39, '00'               
 ----SELECT FieldValue,'Success' FROM #Response 
   
  INSERT INTO #Response (FieldID, FieldValue) SELECT 39, '00'               
 SELECT FieldValue, @TrxBatchID, @SerialID FROM #Response   
             
   If @@ERROR > 0     
   BEGIN    
    --SET @ErrorNo = 2    
    ROLLBACK TRAN    
   END    
   ELSE    
    COMMIT TRAN    
       
   RETURN    
       
ErrDisplay:          
   ROLLBACK TRAN    
            
 SET NOCOUNT OFF    
END   
---------------------_______________________________________________________________  
  






