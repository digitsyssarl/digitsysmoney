USE [BRNET_LIVE]
GO
/****** Object:  StoredProcedure [dbo].[p_AcToMoMoTransactionReversal]    Script Date: 10/15/2021 1:40:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER proc [dbo].[p_AcToMoMoTransactionReversal]       
( 
  @AccountID1  Varchar(25), 
  @SerialID Varchar(20),
  @Amount Amount           
)            
AS       

BEGIN     
              
DECLARE 
     @Fee Amount,
     @CurrencyID      Varchar(20),    
     @TrfSerialID   INT, 
     @AccountID  Varchar(25),    
     @SlNo     INT,  
     @WorkingDate  SmallDateTime,  
     @CreditAccountID Varchar(50),  
     @TrxBatchID    Varchar(20),  
     @HOBranchID   Varchar(20),  
     @BankID    Varchar(20),  
     @ModuleID   Varchar(20),  
     @AvailableBalance Money,  
     @MainGLID   Varchar(20),  
     @TrxDate   SmallDateTime,  
     @ProductID   Varchar(10),  
     @OurBranchID  Varchar(10),   
     @CreatedBy   Varchar(50),   
     @OperatedBy   Varchar(50),  
     @AccountStatusID Varchar(50),  
     @AccountClassID  Varchar(50),  
     @BranchStatus  bit,  
     @HOBranchStatus  bit,    
     @IsDayOpen   bit,  
     @IsTrxAllow   bit,  
     @IsHODayOpen  bit,  
     @IsHOTrxAllow  bit,  
     @HOSODDate   SmallDateTime,  
     @OurBranchSODDate SmallDateTime,  
  @GLClassID   nVarChar(10),  
  @GLAccountStatusID Char(1),  
  @GLCurrencyID  nVarChar(10),  
  @GLCategoryID  nVarChar(10),  
  @TrxBranchID  VARCHAR(25),
  @GotoPending	Bit = 0,
 @TrxDescription Varchar(max),
 @TrxAmount Amount
 
  
  
Create table #suppresser  
(  
 SerialID INT,  
 BatchID  BIGINT  
)  
       
 SET @BranchStatus = 0
 SET @IsDayOpen	   = 0
 SET @IsTrxAllow   = 0
 SET @GotoPending  = 0


SELECT @AccountID = AccountID  FROM T_TRANSACTION WHERE SerialID = @SerialID And AccountID = @AccountID1;
SELECT @TrxAmount = -1*Amount  FROM T_TRANSACTION WHERE SerialID = @SerialID AND   AccountID = @AccountID1;
SELECT @Fee = -1*Amount  FROM T_TRANSACTION WHERE SerialID = @SerialID AND   AccountID = @AccountID1;
SELECT @TrxDescription = Remarks FROM T_TRANSACTION WHERE SerialID = @SerialID AND  AccountID = @AccountID1;
SELECT @TrxDescription = @TrxDescription + ' Reversal';
         

SET @CreatedBy = 'M-Banking USSD'    
        
SELECT @ProductID   = ProductID,  
    @OurBranchID   = OurBranchID,  
    @AccountStatusID  = AccountStatusID,  
    @AccountClassID  = AccountClassID,  
    @AvailableBalance = dbo.f_GetAvailableBalance(@OurBranchID,@AccountID)   
FROM t_AccountCustomer(nolock)   
WHERE  AccountID = @AccountID   
 
SELECT @CurrencyID = CurrencyID From t_Product(nolock) Where PRODUCTID = @ProductID        
SELECT @WorkingDate = dbo.f_GetWorkingDate(@OurBranchID)   
SELECT @BankID  = dbo.f_GetBankID(@OurBranchID)         
SELECT @HOBranchID = dbo.f_GetHOBranchID(@BankID)  
SELECT @TrxBranchID = @HOBranchID   
SELECT @TrxDate  = @WorkingDate      
SELECT @OperatedBy  = 'USSD'       
  
 SELECT @BranchStatus=BranchStatus,@IsDayOpen=IsDayOpen,@IsTrxAllow=IsTrxAllow,@OurBranchSODDate = SODDate  
 FROM t_SystemBranchStatus(nolock) WHERE OurBranchID = @OurBranchID   
  
 SELECT @HOBranchStatus=BranchStatus,@IsHODayOpen=IsDayOpen,@IsHOTrxAllow=IsTrxAllow,@HOSODDate= SODDate  
 FROM t_SystemBranchStatus(nolock) WHERE OurBranchID = @HOBranchID   
                
SELECT @MainGLID = AccountID   
FROM t_GLInterface(nolock)   
WHERE RelevantID = @ProductID AND AccountTagID = 'CONTROL_AC'                  
                    
SELECT @CreditAccountID = AccountID   
From t_GLParameter(nolock)  
Where OurBranchID =  @TrxBranchID And GLParameterID = 'USSD_ORA_AC'  
   
 CREATE TABLE #Response (                            
  FieldID  numeric,                            
  FieldValue varchar(4000)                            
 )                          
      
  
  
---------GL Account Control-----------------  
  
SELECT @GLClassID = GLClassID,  
 @GLAccountStatusID = GLAccountStatusID,  
 @GLCurrencyID = CurrencyID,  
 @GLCategoryID= GLCategoryID  
FROM v_GLBranchDetail  
WHERE OurBranchID = @HOBranchID  
 AND AccountID = @CreditAccountID  
  
   
--------------Branch Not At the Same Date----  
IF (@HOSODDate<>@OurBranchSODDate)  
BEGIN  
 INSERT INTO #Response (FieldID, FieldValue) SELECT 39, '91'               
 SELECT FieldValue,'Branch Not At the Same Date' FROM #Response            
 RETURN  
END  
  
------------- Day Not Open ----------------  
--IF (@HOBranchStatus<>1 OR @BranchStatus<>1   
--OR @IsHOTrxAllow<>1 OR @IsTrxAllow<>1   
--OR @IsHODayOpen<>1 OR @IsDayOpen<>1)  
--BEGIN  
-- INSERT INTO #Response (FieldID, FieldValue) SELECT 39, '91'               
-- SELECT FieldValue,'Day Not Open' FROM #Response            
-- RETURN  
--END  

 
 
---------End Others Control-------------------------  
       
        
         
IF  @OurBranchID <> @TrxBranchID  
 SET @ModuleID = '3030'  
ELSE  
 SET @ModuleID = '3020'  
  
---------------------------  
  
 BEGIN TRAN    

   Truncate table #suppresser  
  --DEBIT SIDE WITH TRX AMOUNT  
    
  SET @SlNo = 1  
  --THE FIRST ASSUMPTION IS THAT WE WILL ALWAYS BE DEBIT ACCOUNT IN TRXBRANCH. LOGIC CAN BE CHANGED LATER TO ALLOW INTERBRANCH  
  INSERT INTO #suppresser(SerialID)  
  EXEC p_AddEditTransferTrx  
   @TrxBranchID  = @TrxBranchID,  
   @OperatorID   = @OperatedBy,  
   @ModuleID   = @ModuleID,  
   @SerialID   = @SerialID,  
   @SlNo    = @SlNo,  
   @OurBranchID  = @OurBranchID,  
   @AccountTypeID  = 'C',  
   @AccountID   = @AccountID,  
   @ProductID   = @ProductID,  
   @TrxTypeID   = 'TD',  
   @TrxDate   = @WorkingDate, 
   @ValueDate   = @WorkingDate,  
   @Amount    =  @TrxAmount,  
   @LocalAmount  = @TrxAmount,  
   @TrxCurrencyID  = @CurrencyID,  
   @TrxAmount   = @TrxAmount,  
   @ExchangeRate  = 1,  
   @MeanRate   = 1,  
   @Profit    = 0,  
   @InstrumentTypeID = 'V',  
   @ChequeID   = 0,  
   @ChequeDate   = NULL,  
   @ReferenceNo  = NULL,----@ReferenceNo,  
   @Remarks   = @TrxDescription,---@Remarks,  
   @TrxDescriptionID = '007',  
   @TrxDescription  = @TrxDescription,  
   @MainGLID   = @MainGLID,  
   @TrxFlagID   = ''    
         
   --//////////////////////CREDITING///////////////////////////////////////////  
    --CREDIT THE INCOME GL OR WHATEVER ACCOUNT HAS BEEN SUPPLIED HERE    
   --Transaction Amount Posting    
  SET @SlNo = @SlNo + 1  
    
  INSERT INTO #suppresser(SerialID)  
  EXEC p_AddEditTransferTrx  
   @TrxBranchID  = @TrxBranchID,  
   @OperatorID   = @OperatedBy,  
   @ModuleID   = @ModuleID,  
   @SerialID   = @SerialID,  
   @SlNo    = @SlNo,  
   @OurBranchID  = @TrxBranchID,  
   @AccountTypeID  = 'G',  
   @AccountID   = @CreditAccountID,  
   @ProductID   = 'GL',  
   @TrxTypeID   = 'TC',  
   @TrxDate   = @WorkingDate,  
   @ValueDate   = @WorkingDate,  
   @Amount    = @TrxAmount,  
   @LocalAmount  = @TrxAmount,  
   @TrxCurrencyID  = @CurrencyID,  
   @TrxAmount   = @TrxAmount,  
   @ExchangeRate  = 1,  
   @MeanRate   = 1,  
   @Profit    = 0,  
   @InstrumentTypeID = 'V',  
   @ChequeID   = 0,  
   @ChequeDate   = NULL,  
   @ReferenceNo  = NULL,  
   @Remarks   = @TrxDescription, --- @Remarks,  
   @TrxDescriptionID = '008',  
   @TrxDescription  = @TrxDescription,  
   @MainGLID   = @CreditAccountID,  
   @TrxFlagID   = ''   
     
   --FOR INTERBRANCH CASES,   
 IF  @OurBranchID <> @TrxBranchID  
 BEGIN  
  INSERT INTO #suppresser(BatchID)  
  EXEC p_AddTransferTrxIB   
    @TrxBranchID = @TrxBranchID, @OperatorID  = @OperatedBy,  
    @SerialID  = @SerialID, @TrxPrinted  = 0, @GoToPending = @GoToPending 
      
  --SELECT @TrxBatchID = BatchID FROM #suppresser   
  --SET @SlNo = 0  
 END  
 ELSE  
 BEGIN     
  INSERT INTO #suppresser(SerialID,BatchID)  
  EXEC p_AddTransferTrx   
    @TrxBranchID = @TrxBranchID, @OperatorID  = @OperatedBy,  
    @SerialID  = @SerialID, @ModuleID  = 3020,  
    @ImageID  = 0,            @TrxPrinted  = 0, @GoToPending = @GoToPending  
      
  --SELECT @SerialID = SerialID,@TrxBatchID = BatchID FROM #suppresser        
  --SELECT * FROM #suppresser        
 END  
    
  Truncate table #suppresser  
  
------------------Charges Fees-------------  
 
  SET @SlNo = 1  
  SET @TrxDescription = @TrxDescription+' - Fees Reversal'  
    
  --THE FIRST ASSUMPTION IS THAT WE WILL ALWAYS BE DEBIT ACCOUNT IN TRXBRANCH. LOGIC CAN BE CHANGED LATER TO ALLOW INTERBRANCH  
  INSERT INTO #suppresser(SerialID)  
  EXEC p_AddEditTransferTrx  
   @TrxBranchID  = @TrxBranchID,  
   @OperatorID   = @OperatedBy,  
   @ModuleID   = @ModuleID,  
   @SerialID   = @SerialID,  
   @SlNo    = @SlNo,  
   @OurBranchID  = @OurBranchID,  
   @AccountTypeID  = 'C',  
   @AccountID   = @AccountID,  
   @ProductID   = @ProductID,  
   @TrxTypeID   = 'TD',  
   @TrxDate   = @WorkingDate,  
   @ValueDate   = @WorkingDate,  
   @Amount    = @Fee,  
   @LocalAmount  = @Fee,  
   @TrxCurrencyID  = @CurrencyID,  
   @TrxAmount   = @Fee,  
   @ExchangeRate  = 1,  
   @MeanRate   = 1,  
   @Profit    = 0,  
   @InstrumentTypeID = 'V',  
   @ChequeID   = 0,  
   @ChequeDate   = NULL,  
   @ReferenceNo  = NULL,----@ReferenceNo,  
   @Remarks   = @TrxDescription,---@Remarks,  
   @TrxDescriptionID = '007',  
   @TrxDescription  = @TrxDescription,  
   @MainGLID   = @MainGLID,  
   @TrxFlagID   = ''    
         
   --//////////////////////CREDITING///////////////////////////////////////////  
    --CREDIT THE INCOME GL OR WHATEVER ACCOUNT HAS BEEN SUPPLIED HERE    
   --Transaction Amount Posting    
  SET @SlNo = @SlNo + 1  
    
  INSERT INTO #suppresser(SerialID)  
  EXEC p_AddEditTransferTrx  
   @TrxBranchID  = @TrxBranchID,  
   @OperatorID   = @OperatedBy,  
   @ModuleID   = @ModuleID,  
   @SerialID   = @SerialID,  
   @SlNo    = @SlNo,  
   @OurBranchID  = @TrxBranchID,  
   @AccountTypeID  = 'G',  
   @AccountID   = @CreditAccountID,  
   @ProductID   = 'GL',  
   @TrxTypeID   = 'TC',  
   @TrxDate   = @WorkingDate,  
   @ValueDate   = @WorkingDate,  
   @Amount    = @Fee,  
   @LocalAmount  = @Fee,  
   @TrxCurrencyID  = @CurrencyID,  
   @TrxAmount   = @Fee,  
   @ExchangeRate  = 1,  
   @MeanRate   = 1,  
   @Profit    = 0,  
   @InstrumentTypeID = 'V',  
   @ChequeID   = 0,  
   @ChequeDate   = NULL,  
   @ReferenceNo  = NULL,  
   @Remarks   = @TrxDescription, --- @Remarks,  
   @TrxDescriptionID = '008',  
   @TrxDescription  = @TrxDescription,  
   @MainGLID   = @CreditAccountID,  
   @TrxFlagID   = ''   
     
   --FOR INTERBRANCH CASES,   
 IF  @OurBranchID <> @TrxBranchID  
 BEGIN  
  INSERT INTO #suppresser(BatchID)  
  EXEC p_AddTransferTrxIB   
    @TrxBranchID = @TrxBranchID, @OperatorID  = @OperatedBy,  
    @SerialID  = @SerialID, @TrxPrinted  = 0, @GoToPending = @GoToPending   
      
  --SELECT @TrxBatchID = BatchID FROM #suppresser   
  --SET @SlNo = 0  
 END  
 ELSE  
 BEGIN     
  INSERT INTO #suppresser(SerialID,BatchID)  
  EXEC p_AddTransferTrx   
    @TrxBranchID = @TrxBranchID, @OperatorID  = @OperatedBy,  
    @SerialID  = @SerialID, @ModuleID  = 3020,  
    @ImageID  = 0,            @TrxPrinted  = 0, @GoToPending = @GoToPending   
      
  --SELECT @SerialID = SerialID,@TrxBatchID = BatchID FROM #suppresser        
 END  
----------------End Charges Fees-----------------  
      
 ----SELECT @TrxBatchID--CAST(@SerialID AS VARCHAR)    
  
-----------------------------  
  
 -- select @p2, @p3, @p33, @p34,dbo.f_GetAvailableBalance(@OurBranchID,@AccountID)              
 INSERT INTO #Response (FieldID, FieldValue) SELECT 39, '00'               
 SELECT FieldValue,'Success'  as Message FROM #Response   
              
   If @@ERROR > 0     
   BEGIN    
    --SET @ErrorNo = 2    
    ROLLBACK TRAN    
   END    
   ELSE    
    COMMIT TRAN    
       
   RETURN    
       
ErrDisplay:          
   ROLLBACK TRAN    
            
 SET NOCOUNT OFF    
END   
---------------------_______________________________________________________________  
  
